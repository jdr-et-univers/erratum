---
layout: layouts/home.njk
---
# Erratum

DanseRonce est un JDR de fantasy urbaine se passant dans une terre alternative contemporaine où la magie a été révélée à tous, et où une partie de la population est capable d'utiliser des pouvoirs étranges nommés les "signes". Ce JDR est basé sur un système D100 homebrew (une version antérieur de mon système pélican), et vise à être plus orienté RP que de l'utilisation de règles complexes.

Cette page a pour objectif de vous présenter à la fois l'univers et comment il interragit avec le système. Ce JDR est fourni par [Kazhnuz](https://kazhnuz.space), avec certaines règles reprise du système de [MDupoignard](https://twitter.com/MDupoignard). Il est distribué sous la Creative Commons CC-BY-SA 4.0.

<div style="text-align:center;">

![Une poupée abimée de renard bleu foncée, avec une cape noire et deux petites corne blanche. Un de ses "yeux" (des boutons) est en train de tomber.](/img/poupeeSoph.png)

</div>

## Avant propos

> « On m’a toujours dit que croire aux monstres était un truc d’enfant. Que ça devait partir une fois qu’on atteignait l’âge adulte. Autour de moi, je ne vois que des adultes qui croient au monstre, et je suis un enfant seul qui n’y croit pas. »
> <author>??? – Auteur inconnu – Date inconnue</author>

Depuis des temps immémoriaux, deux « mondes » existent, que tout oppose, mais qui doivent partager la même planète. Le monde « réel », de la rationalité et de la science. Le monde de l'imprévu, du mystérieux et de la magie. En effet, environ 3% de la population mondiale est capable de pratiquer, à l'aide de forces occultes, la « magie », et 0.7% est même capable d'en faire d'eux même, à l'aide d'un mystérieux pouvoir nommé les « douzes signes ». Des créatures magiques vivent cachés dans le monde, éloignées de tous.

Practiciens de la magie et non-practiciens vivent chacun de leur côté, se connaissant mais minimisant les véritables interactions. L'OIM et les Gardiens s'occupent de s'assurer que la magie n'est pas utilisée à tors, et à combattre les "forces du mal". Cependant, bien des conflits latents existent et pourraient se manifester… Et quels secrets cachent encore ce monde, hors de la vue même des magiciens ?

## Licence

Cet univers et ses JDRs associés sont mis à disposition selon les termes de la [licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/). Vous êtes libre d'utiliser cet univers, ainsi que les règles de jeux, comme base pour ce que vous voulez, à condition de repartager dans les mêmes conditions les contenus qui seraient basé sur cet univers. Il est à noter que cette licence ne couvre pas l'inspiration : vous pouvez vous inspirer de certains concepts ou idées sans que ce soit couvert par la licence, dans ce cas vous n'avez pas d'obligation d'utiliser la licence creative commons.

## Crédits

- Site généré grâce à [Eleventy](11ty.org)
- Icone [Night-Sky](https://game-icons.net/1x1/lorc/night-sky.html) par [Lorc](https://lorcblog.blogspot.com/) sous [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
- Icone [Pine-Tree](https://game-icons.net/1x1/lorc/pine-tree.html) par [Lorc](https://lorcblog.blogspot.com/) sous [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
- Icone [Dead wood](https://game-icons.net/1x1/lorc/dead-wood.html) par [Lorc](https://lorcblog.blogspot.com/) sous [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
- Favicon venant du set de tiles [Castlevania-esque Tilemap](https://opengameart.org/content/castlevania-esque-tilemap-stone-and-bricks) par [Richy Mackro](https://chayed-creates.itch.io/chayed-creates-pixels) sous [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
- Merci à Marcel Dupoignard et mon groupe de JDR pour de nombreux éléments de cet univers
- Relecture et illustration de la page d'accueil par Nepeta Sibirica

