---
layout: layouts/base.njk
title: Les dragons
tags: Les créatures
models:
  - Les créatures
parent: Les créatures
---

# Dragons

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Draconiens | Rares | Ovipares | Aucun | 800 ans |

Les dragons sont une espèce intelligente reptile non-sociale, indépendante qui vit dans de nombreux territoire du monde. Souvent considéré comme des "bêtes" ou au moins comme n'étant pas une civilisation de leur nature isolationiste, se sont des êtres intelligents doué de paroles, doté de cultures riche et communicant entre eux.

Ces espèces gigantesques sont magiquement très puissantes, mais ne peuvent utiliser de signes ou des magies comme les civilisations mortelles. On raconte que les dragons sont plus proche niveau puissance d'un demon/demi-titan que d'un mortel. Ils peuvent vivre des centaines d'année, et un oeuf de dragons met des années à éclore. Les dragons sont tous lié à un élément, généralement lié à l'environnement ou ils vivent. La sous-espèce du dragon affecte fortement de quels éléments ils peuvent être.

Les dragons sont présent sur tout les continents, et sont divisé en deux grandes espèce : les dragons-serpents et les dragons-griffus. Les relations entre dragons et humains ont été très différentes suivant les civilisation.

Les dragons peuvent naturellement aller jusqu'à 120 de stats, sans potentiel. Leur deuxième potentiel les montes à 180.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 23 | 15 | 12 | 4 | Griffe (2) | FOR, CON, SAG (60) | REL, DIS (35) | Le reste (50) |

## Stats et compétences

| Trait | Effet |
|:-----:|:------|
| Element | Les dragons ont un élément physique (ou pseudo-élément) de préférence auquel ils résistent |
| Souffle élémentaire | Peuvent faire une attaque élémentaire à +2 dégats |
| Terrain élémentaire (2 éclats) | Peuvent poser un terrain élémentaire |
| Faiblesses élémentaire | Se prennent x2 dégats par la glace (Remplacé par feu si dragon de glace) + un autre élément au choix du joueur |
| Vol | Peuvent voler |

## Dragons griffus

Les dragons griffus sont une espèce de dragon quadripède, dôté d'une grande paire d'aile. Ils sont très présent en Europe, Moyen-Orient, Afrique et dans les cercles polaires. Doté d'écailles, de grande corne, ils sont imposant et impressionnant.

Ces dragons sont très différents suivant les régions, et possèdent de nombreuses sous-espèces :
- Les dragons des plaines sont les dragons que l'on retrouve le plus en Europe, et dont quelques membres sont présent aussi en amérique du nord. Ils sont connue pour leur capacité à cracher du feu. Ils sont généralement de couleur vive. Ils ont une préférence pour les éléments feu, air, poison.
- Les dragons marins vivent sur les rochers dans les mers agités. On en retrouve de nombreux en méditéranné, en atlantique, mais également dans la mer rouge. Ils crachent un venin corosif, et peuvent nager sous l'eau. Ils sont généralement d'une teinte entre le bleu, le vert et le gris. Ils ont une préférence pour les éléments eau, nature.
- Les dragons du désert sont des dragons couleur sable, se terrant souvent dans les déserts et les montagnes. Ils sont très nombreux dans le désert d'arabie, le Sahara et en Espagne. On en trouve quelques uns aussi dans les désert américain. Ces dragons cherche souvent de l'ombre pour s'abriter. Ils ont une préférence pour l'élément feu, poison, esprit.
- Les dragons des glaces sont des dragons de couleur bleu clair ou blanc, vivant dans les cercles polaires. Ils ont une préférence pour l'élément glace.

Ces dragons sont connus pour leur tendance à "garder des trésor". En vérité, c'est plutôt un effet de la territorialité des dragons. Ils ne gardent pas le "trésor", mais leur lieu de vie - et souvent leur dragonneau. En effet, un dragonneau griffu reste vulnérable pendant 20 ans, ce qui fait qu'à tout moment, ils peuvent être tué durant cette période.

## Dragons serpents

Les dragons serpents sont des longs dragons sans ou avec de petites pattes, vivant à travers le monde. Ces dragons peuvent voler sans ailes, un phénomène encore non-expliqué. Elles est recouverte d'écaille, mais peut également avoir des plumes voir des poils. Cette espèces vit principalement en asie, même si certains sont présents aussi sur le territoire américain et en afrique.

Ces dragons sont généralement considéré comme en trois grandes espèces :
- Les dragons-serpents des airs. Ces dragons ont la capacité d'influencer la météorologie. Cette puissance fait qu'ils étaient vénéré, notamment en Chine ou seul l'empereur pouvait porter ses signes sur ses vêtements. En mésoamérique, les serpents à plume sont de cette espèce. Ils ont une préférence pour les éléments air, foudre, eau.
- Les dragons-serpents des terres. Ces dragons vivent souvent proche des cours d'eau, et peuvent les déplacer à leur guises, pouvant irriguer des zones entières. Ces dragons ont des migrations, passant l'été dans le ciel et l'automne dans la mer, et le reste de l'année sur terre. Ils ont une préférence pour les éléments terre, nature.
- Les dragons-serpents sousterrains. Ces dragons ont la capacicté de façonner les terrains, de creuser des vallées. Malgré leur nom, ces dragons peuvent aussi voler. Ils sont des protecteurs des trésors des profondeurs, un comportement présent aussi chez les dragons griffus. Ils ont une préférence pour les éléments terre, nature.

Leur apparence peut grandement varier suivant ces trois espèces, les dragons étant plus une espèce "magique" que fonctionnant suivant les règles classiques de la biologie.

Leurs mues ont des capacités curatives.