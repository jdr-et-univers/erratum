---
layout: layouts/base.njk
title: Les Eirons
tags: Espèce DfOS
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les Eirons

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Cristalites | Nombreux sur leur planète | ??? | Aucun | ??? ans |

Les Eirons sont des créatures cristallines vaguement humanoïdes. Ils ont de longues crêtes de "plumes" cristallines retombants jusqu'à leur deux queues, qui en sont aussi pourvues. Leur tête ressemble vaguements à un mélange entre des oiseaux et des poissons, pourvue de sorte de branchies et d'un grand bec. Ils peuvent survivre à des écarts de température extrème, mais plus vers le froid. Sur Terre, ils ne pourraient pas survivre à un été tempéré par exemple. Ils peuvent résister à l'absence d'athmosphère, et à des températures proche du zéro absolu.

Plus grandes que des humains (approchant des 2 mètres et demis), ils ont six membres (tous dôté de deux doigts à la fin), ainsi que deux appendices dorsales considérés comme des queues. Ils se déplacent en nageant (ils sont originellement aquatique) ou utilisent leur six membres constamment pour se déplacé, n'ayant pas de station debout permanente.

Ils pourrait vivre dans notre système solaire sur la plupars des planètes hors après la terre, et même la terre dans les endroits les plus frais. Ils ont cependant besoin pour vivre de méthane, que leur corps assimile et utiliser comme source d'énergie.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 12 | 12 | 12 | 5 | Laser (2, spécial) |  HAB, PER, CON (65) | FOR, VOL, REL (35) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Déplacement parfait | sur un jet d’HAB, peut éviter tout malus de vision ou de compréhension lié à ce qui les entoure directement |
| Cri stridant | Peut étourdir les ennemis à coup de cri violent, sur un jet de CHA. |
| Télépathie | Peuvent créer un lien mental avec quelqu’un, si celui-ci accepte |
| Esprit brut | Faiblesse à l'éclat et aux attaques d'esprit |

## Xipéhuz

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Cristalites | Peu nombreux | ??? | Aucun | ??? ans |

Drônes vivants minéraux, proche des Eirons et construits à l'origine par eux. Une petite colonie est arrivée et s'est fait vaincre par des humains durant l'age de pierre. Ils ont la particularité de pouvoir modeler leur corps et leur esprit pour prendre de nombreuse apparence différente.

Ils sont bien une espèce intelligente.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 20 | 8 | 10 | 7 | Laser (2, spécial) |  Trois au hasard (65) | trois au hasard (35) | Le reste (50) |

### Stats et compétences

| Trait | Effet |
|:-----:|:------|
| Main séparée | Peut avoir une arme supplémentaire. À la création du personnage, peut sacrifier ses stats bonus pour avoir deux mains au lieu d'une en plus |
| Autodestruction | Explose en faisant 1D12 dégats lorsqu’ils tombent KO, retombant à 0 PV, mais se prendront ensuite double dégats sur les attaques. |
| Être magiques | Faiblesse à l'éclat |