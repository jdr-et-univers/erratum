---
layout: layouts/base.njk
eleventyNavigation:
  key: Les équipements
  parent: Inventaire
  order: 2
models:
  - Les équipements
title: Les équipements
---

# Les équipements

Lors d'un combat, vous pouvez utiliser toutes sortes d'armes ou d'objets pour agir, attaquer et/ou vous défendre. Voici un petit panel de ce qui est possible d'avoir dans Erratum. Il est a noter que les équipements peuvent avoir des effets, qui peuvent se combiner. Leurs effets ET leur surcout (des plus faibles aux plus grands) se combinent alors pour donner le prix final de l'équipement.

## Types d'équipements

Le premier type d'équipements sont les outils, ce sont les outils que votre personnage porte sur lui pour agir. Lorsqu'ils sont équipés, ils prennent une ou deux mains de votre personnage. Parmis ces outils, pour attaquer ou vous défendre, vos personnages peuvent porter une arme ou un bouclier, mais la plupars des outils peuvent être utilisé pour le combat.

- Les [armes](armes/) sont des outils permettant de faire plus de dégats aux ennemis. Ils sont divisé en deux groupes, les armes au *corps à corps*, et les *armes à distances*.
- Les [boucliers](boucliers/) sont des outils permettant de réduire les dégats que font une attaque en augmentant le niveau d'armure pour y résister.
- Les [tenues](vetements/) sont des tenues aux effets divers, pouvant offrir protection ou autres aventages à votre personnages
- Les [accessoires](accessoires/) sont des équipements aux effets divers, qui sont limités à deux par personnages
- Les [équipements de signes](signes/) sont des équipements liés aux pouvoirs des signes