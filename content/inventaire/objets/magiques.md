---
layout: layouts/base.njk
title: Objets magiques
tags: Objets consommables
models:
  - Objets consommables
  - Objets réutilisables
parent: Les objets
---

# Objets magiques

## Objets communs

| Objet | Effet | Prix |
|:----------:|:------|:----:|
| Pierre d'intégrité | Restore entièrement tout effet métaphysique | 1000 |
| Graine élémentaire | Produit l'effet de l'élément | 50 |
| Voile élémentaire | Produit une résistance à l'élément sur toute l'équipe | 550 |
| Prisme d'élément | Quand brisé, produit un terrain élémentaire | 350 |
| Bille de stat | Ajoute +5% à la statistique trois tours. Cumulatif | 250 |
| Essence d'espèce | Change l'espèce d'un personnage trois tours | Introuvable sur le marché |
| Bombes élémentaires | Provoque une explosion de 12 dégats (sur trois ennemis proches) avec effet élémentaire | 250 |
| Cristal de téléportation | Ramène au début d'un donjon où d'un endroit | 400 |
| Fragment de signe | Rajoute le pouvoir d'un signe trois tours | Introuvable sur le marché |

## Objets parjures

| Objet | Effet | Prix |
|:----------:|:------|:----:|
| Carnet de la mort | Réussite automatique au prochain jet de survie, le suivant cependant échouera | 1500 |
| Dés pipés | Annule un échec critique, mais la réussite critique suivante sera un échec critique | 1500 |
| Herbes démoniaques | Transforme une potion à effet positif en potion satanique : augmente ses effets, mais provoque un jet de panique | 200 |
