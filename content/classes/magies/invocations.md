---
layout: layouts/base.njk
title: Les invocations
tags: Magies Communes
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# Les invocations

Les invocations sont un type de magie consistant à utiliser son encrage dans le monde physique pour invoquer un démon, une espèce proche des demi-dieux lié au *chaos* possédant un ou plusieurs pouvoirs démoniaques uniques.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Invocation  |  Peut faire un jet de VOL et de CHA pour pouvoir invoquer un démon. La VOL sert à le faire apparaître, et le CHA à le contrôler. De base, peut invoquer un « corps neutre ». Il a 15 PV, 50 partout mais ne peut pas agir, mais compte comme un personnage « présent », et compte parmis les familliers le temps de sa présence.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Diablotin (1 Eclat)  |  HAB – INT – DIS : 70<br />PER – CHA : 60<br />REL – VOL : 50<br />FOR – CON – SAG : 10<br /><br />PV&PE : 7<br /><br />Sorts : Confusion – peut rendre confus un esprit sur jet d’INT vs SAG.  |
| 1 |  Golem (1 Eclat)  |  FOR – CON : 85<br />CHA – PER : 50<br />HAB – VOL : 15<br />INT – SAG – REL – DIS : 0<br /><br />PV : 20<br />PE : 0<br /><br />+ 5 armure physique  |
| 2 |  Andromalius (2 Eclat)  |  INT-PER-REL-DIS : 70<br />HAB-SAG-VOL:50<br />FOR-CON : 30<br />PV&PE : 10<br /><br />Sorts : <br />Récupération d’objet : peut aider à trouver des objets cachés.<br /><br />La punition des voleurs : permet de ne pas perdre ses équipements tant qu’Andromalius est présent alliés.  |
| 2 |  Zagan  |  INT-SAG-VOL-REL : 70<br />HAB-PER-DIS:50<br />FOR-CON : 30<br />PV&PE : 10<br /><br />Sorts :<br />Le métal en pièce :<br />Sur un jet de REL, peut faire baisser les prix (effet différent suivant réussite)<br /><br />Les fous en SAG : Sur un jet de SAG, +10 % en SAG aux alliés.<br /><br />L’eau en vin : Tant que Zagan est présent, les attaques d’eau peuvent rendre saoul.  |
| 2 |  Connaisseurs des démons (trait)  |  +20 % pour attaquer ou communiquer avec les monstres de types démoniaques (hors des invocations).  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Shabnoc  |  FOR, CON, INT : 80<br />HAB, SAG, DIS, PER : 60<br />VOL, REL, CHA : 40<br /><br />PV&PE : 20<br /><br />Épée du démon : 1D8, ignore les boucliers.<br /><br />Bouclier miroir : 1D6, renvoie les dégâts encaissés.<br /><br />Pouvoir de la maladie : Sur un jet de INT, peut rendre malade (-20 % en stat physique) jusqu’à 5 personnages pendant 1D4 tour (une fois par combat)  |
| 3 |  Machin  |  DIS, HAB, REL : 80<br />VOL, INT, SAG, PER : 60<br />FOR, CON, CHA : 40<br /><br />PV : 15<br />PE : 25<br /><br />Troll : peut provoquer une rage grave chez un ennemi. Son attaque est boosté de 30 %, mais à une chance sur deux d’attaquer ses alliés (pendant trois tour).<br /><br />Machination : Augmente à tout les alliés les critiques de 5, mais fait baisser de 30 % en SAG.  |
| 3 |  Invocation +  |  Peut invoquer un démon supplémentaire  |
| 4 |  Baphomet  |  FOR, HAB, DIS : 100<br />PER, VOL : 60<br />CON, INT : 50<br />CHA, REL, SAG : 30<br /><br />PV&PE : 20<br /><br />Coup de corne : Une attaque à 1D6×2. Peut étourdir (effet hypoactivité).<br /><br />Manipulateur caprin : Une attaque à 1D6, sur de la DIS. Parle en bélant. Peut donc faire effet confusion du coup à ses attaques mentales si fait 1 sur 1D4.<br /><br />Maître de l’évasion : Sur un jet de DIS ou FOR, peut détruire une barrière, une porte, etc.<br /><br />Gros beauf : Se prend ×1.5 dégats mentaux s’il se fait considérer comme un meunon où une chèvre, mais fera ×1.5 dégats à ses attaques au prochain tour.  |
| 4 |  Erinye  |  PER, HAB, CHA : 100<br />VOL, INT : 60<br />FOR, CON : 50<br />DIS, REL, SAG : 30<br /><br />PV&PE : 20<br /><br />Poursuite effrénée : s’acharnent sur un ennemi, et n’attaqueront PERSONNE d’autre. Aucun taunt ne peut les retirer.<br /><br />Detection des ennemis : peuvent attaquer même les ennemis cachés.<br /><br />Cri strident : Une attaque de PE à 1D6x2.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Murmur  |  DIS, REL, CHA : 90<br />FOR, PER, SPI, INT : 60<br />SAG : 50<br />CON, HAB : 40<br /><br />PV&PE : 10<br /><br />Épée des soupirs : 1D6, +6 perce-armure psychique.<br /><br />Vol de PE : Choisi un personnage allié. À la prochaine attaque de ce personnage, les PE seront transférés à un perso au choix du démon. (1 Éclat)<br /><br />La question : Contraint l’âme d’un mort (même dont le corps est absent) à apparaître devant son maître pour répondre à toute question.  |
| 5 |  Aamon  |  FOR, CON, CHA : 90<br />HAB : 60<br />INT, DIS : 50<br />SPI, SAG, PER : 30<br />REL : 20<br /><br />PV & PE : 30<br /><br />Griffe : 1D8+2, attaque deux fois.<br /><br />Transformation en lycanthrope : transforme trois tours un personnage en lycanthrope.<br /><br />Appel de la meute : peut renforcer de 20 % un loup garou, mais il devient l’alpha de celui-ci.<br /><br />Rage de la lune : Une attaque mentale de zone de 1D6+2, attaque au CHA  |
| 5 |  Invocation +  |  Peut invoquer un démon supplémentaire  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Baal  |  CHA, FOR : 120<br />INT, SAG : 65<br />CON, PER : 50 <br />SPI, HAB, REL, DIS : 30<br /><br />PV : 1<br />Miroir : 100 PV<br /><br />Hallebarde du démon : 1D12 (attaque en tant qu’attaque esprit ou physique)<br /><br />Miroir : Renvoie-les dégâts pris directement, mais chaque attaque baisse de 1 le niveau de miroir par dégat reçu. Se regen de 10 PV miroirs<br /><br />Invincible : Ne peut pas être vaincu. Ne peut pas être désinvoqué.<br /><br />Roi des démons : Malus de 20 % en Charisme pour tout le monde qui s’oppose à lui. (Même l’invocateur au début).<br /><br />Durée limité : Ne peut rester dans le monde des mortels que 3 tours.  |
| 6 |  Asmodée  |  REL, DIS : 120<br />CHA, PER, INT : 70<br />HAB, SAG : 55<br />FOR, CON, SPI : 25<br /><br />PV : 18<br />PE : 30<br /><br />Le recruteur des enfers : Expert pour donner confiance. N’a jamais aucun malus en REL.<br /><br />Pacte : peut forcer un personnage à lui obéïr (allié comme ennemi) pendant trois tours. Jet de REL.<br /><br />Tu ne mourras point : Les PV d’une personne en pacte avec lui ne passent pas en dessous de 1PV<br /><br />Join me : Peut tenter de faire définitivement passer dans son camp une personne déjà en pacte. Est dés-invoqué immédiatement. L’ennemi fait juste un jet de CHA pour résister. S’il est recruté, tombe KO pour le reste du combat.  |