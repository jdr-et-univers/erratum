---
layout: layouts/base.njk
tags: Signes de Malice
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Capricorne
parent: Les signes
---

# Le capricorne (les émotions)

Les capricornes tirent leur pouvoir des émotions et de leur puissance. En effet, ils sont naturellement capable de capter et modifier les flux émotionnels qui traversent l'esprit. S'ils ne sont pas télépathe, ils sont naturellement empathes.

Leur pouvoir les permet de se sortir de bien des situations, mais aussi d'être les plus à même à attaquer sur l'esprit. Politiciens, espions... Mais ils peuvent aussi être des aides quand ils le veulent. Telle est toute l'ambivalence de ceux qui contrôlent les émotions.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :capricorn: | ??? (asie), ??? (égypte) | Malice et Force (Trickster DPS) | Chaos | Peu commun (10.0%) | REL et CHA | Orange |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Contrôle des émotions  |  L’utilisateur peut injecter une émotion qu’il maîtrise à un adversaire où ennemi, où manipuler les émotions hors combats. De base, peut infliger l’effet « frayeur » (-1 PE/tour)  |
| Double |  Contrôle des émotions  |  Peut apprendre des nouvelles afflictions mentales en les étudiant quand il les a vues, à hauteur d’une affliction mentale par niveau.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Tu peux le faire ! (émotion)  |  Produit un soin passif de 1 PE par tour (II: 3 PE)  |
| 1 |  Nan mais lui, vous savez… (émotion)  |  Produit -15% aux stats sociales à un ennemi (non-cumulable)  |
| 2 |  Assurance  |  Encourage un allié qui gagne 5 % à une statistique. Cumulable jusqu’à 15 % (II : Augmente de 10 %, cumulable jusqu’à 30%)  |
| 2 |  Il a insulté ta mère ! (REL)  |  Dirige l’aggro d’un PNJ ou PJ sur un personnage (peut être le capricorne)  |
| 2 |  Choc émotionnel (2 Eclats, 2 tours de cooldown)  |  Le personnage visé subit l’équivalent du nombre de tour total qu’est supposé durer l’émotion, et se remet de l’émotion. (II : Total + 2 tour)  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture focus  |  Peut passer en posture focus (II : posture focus +)  |
| 3 |  Politicien (1 Eclat par personnage supplémentaire)  |  Peut injecter 5 tours une émotion, jusqu’à 5 personnages  |
| 3 |  Émotion+  |  Passe les afflictions mentales provoquées au niveau supérieur  |
| 4 |  Le monde des idées (PER)  |  Peut, faire un diagnostic complet de l’état mental/cérébral d’une personne. Il ne connaîtra cependant pas ses intentions (II: en fait si)  |
| 4 |  Déprime (émotion)  |  Donne l’effet « Déprime » à la cible.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Nudisme  |  Créer une émotion provoquant l’effet « Nudisme » à la cible, pour un seul tour. La cible peut ensuite prendre un tour pour les rééquiper. L'action se résiste sur un jet de SAG ou VOL dégressif, à -10/tour. |
| 5 |  Nan mais là faut se calmer  |  Sur un jet de CHA ou REL, peut tenter de stopper l’action d’un autre PJ, même si celle-ci est entamée. Si celui-ci n’a pas agit de son chef, il doit faire un jet de CHA opposé pour resister ou non. S’il a agit de son chef, peut choisir d’écouter la petite voix dans sa tête. (II : L’adversaire à un malus de 30 % pour résister)  |
| 5 |  Émotion+  |  Passe les afflictions mentales provoquées au niveau supérieur  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Parler en travaillant  |  Peut injecter une émotion en plus d’une action autre qu’émotion. (II : sur deux adversaires à la fois)  |
| 6 |  Présentiment funeste (1 karma)  |  Active une émotion sur tous les personnages en combat (5 tours), qui dès qu’elle sera fini, met à -5 PE.  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Feeding the troll (REL)  |  Peut tenter de provoquer à tout moment un débat d’idée/combat mental avec une personne. Le débat prend l’attention du capricorne et de l’adversaire.  |
| Herault |  Le mythe de la caverne (6 Eclat)  |  Provoque un double jet de panique sur un adversaire au choix.  |