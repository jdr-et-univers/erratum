---
layout: layouts/base.njk
title: Les arts martiaux
tags: Magies Communes
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# Les arts martiaux

Les arts martiaux sont une magie tirée de la *force martiale*. La force martiale se concentre dans le corps pour transformer les capacités utilisés.

Les arts martiaux existent en de nombreuses écoles, et peut être apprise avec des maitres martialistes.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Changement de posture | Le personnage peut changer de posture une fois par combat à n’importe quel moment. |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Contre Attaque (1 Eclat)  |  Peut effectuer une attaque normale après une esquive réussie.  |
| 1 |  Coup Honorable  |  La prochaine attaque ne fera pas tomber l’ennemi en dessous de 0 PV  |
| 2 |  Frappe d’Acier (2 Eclat)  |  Rajoute un effet d’acier à l’attaquer.  |
| 2 |  Frappe mentale (3 Eclats)  |  La prochaine attaque frappera sur l’esprit  |
| 2 |  Damoclès  |  La prochaine attaque peut gagner 1/4 réussites d'un jet d'HAB, mais se prendra 50 % des dégâts effectués.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  A main nue  |  Les amélioration d’attaque d’art martiaux coûtent un éclat en moins si le personnage se bat à main nue.  |
| 3 |  Attaque sauté (3 Eclats)  |  Le personnage peut rajouter les niveaux de réussite d’un jet d’HAB aux dégats de la prochaine attaque. Cependant, il jette 1D4, s’il fait 4, il se prend tous les dégats de l’attaque.  |
| 3 |  Attaque risquée (2 Eclat)  |  Rajoute une +5 en pression à la prochaine attaque.  |
| 4 |  Hadoken (3 Eclats)  |  La prochaine attaque au CaC sera à distance  |
| 4 |  Double poing (trait)  |  Quand le personnage n’a pas d’arme et utilise ses poings, peut faire une deuxième attaque.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Frappe circulaire (2 Eclats)  |  La prochaine attaque pourra toucher trois ennemis proche, mais ne fera que 50 % de dégats finaux à chaque.  |
| 5 |  Mutilation (2 Eclats)  |  Sa prochaine attaque effectura le double d’incapacitation, mais ne fera que 50 % des dégats finaux.  |
| 5 |  Les pieds bien écartés (3 Eclats)  |  Ne pourra ni être déplacé d’aucune manière que ce soit pendant tous le reste du combat  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Kung Fury (1 karma, 5 éclat)  | Passe 2 tours à charger son attaque, puis peut effectuer plusieurs attaques à la suite en un tour, -10 % par attaque, le combo s’arrête quand ça se termine, et bonus de dégats +2 par attaque. Après l’attaque, un tour de cooldown sans déplacement.  |
| 6 |  Transe de combat (4 Eclats)  |  Peut rester debout en dessous de 0PV, mais -15 % à tous les jets, -3PV tours si action et ne peut pas utiliser de capacité à éclat ni de posture.  |