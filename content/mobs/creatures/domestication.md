---
layout: layouts/base.njk
title: Domestication
tags: Informations créatures
models:
  - Informations créatures
  - Les créatures
parent: Les créatures
---

# Influence et domestication

Pour influencer un animal, le joueur doit avoir une compétence de *maitre des [créature]* pour ne pas faire de jet affaibli. Ces jets sont plus ou moins difficile suivant l'attitude et affecté par la stat de *docilité* de la créature.

Avec une capacité de **domestication** approprié, un joueur peut aussi tenter de faire d'un animal sauvage un familier, si celui-ci est dans une attitude adéquate ou neutre. La compétence citée ci-dessus est également nécessaire

## Familiers

Chaque personnage peut avoir un *familier*. Un familier est une créature domestiquée qui suivra de prêt le personnage, devenant presque un mini-personnage, et pourra même se battre. Il n'y a pas de limite au nombre maximum de familiers, mais le MJ peut décider de rajouter des malus plus le joueur en a.

Les familiers sont sauf exceptions mortels, et doivent se retirer du combat avant de tomber KO, ou alors il faut les faire sortir. Un joueur ne peut avoir qu'un familier qui l'aide en combat à la fois, et pas plus. Ils jouent après ou avant le tour de leur joueur.

Les familiers utilisent des fiches de *créatures simples*.

## Montures

> **Note:** Les montures peuvent effectuer des actions de confrontations, fonctionnant alors de manière proche des confrontations de véhicules décrit dans la section véhicule.

Les montures sont des animaux qui peuvent servir de véhicules, pour amener un personnage à un endroit, ou participer à un combat de véhicules/montures. Elles sont généralement moins véloces qu'un véhicule, mais offrent quelques avantages.

La fiche d'une monture reçoit les informations suivante par rapport à une fiche ordinaire :

- Nombre de passager
- Les milieux dans lesquels la montures peut se déplacer.
- Une représentation de la distance/vitesse

La monture ne produit ni bonus, ni malus au joueur pour les différentes actions qu'iel fait, mais possède son propre tour d'action (étant une créature vivante). Pour monter une créature, les personnages nécessitent une compétence de *monter [espèce de la créature]* pour ne pas avoir de jets affaiblis lorsqu'ils monteront dessus.

La monture peut également recevoir une capacité spéciale de monture, ajoutant un effet à son arrivée en plein milieu d'un combat.
