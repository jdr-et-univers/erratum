---
layout: layouts/base.njk
title: Centaures
tags: Espèces additionnelles
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Les centaures

Les centaures sont des humanoïdes dont la fin du corps est un corps de cheval. Souvent puissant⋅e et vivant en petit groupe, ils sont une espèce rare mais qu'il ne faut pas sous-estimer. Souvent déconsidéré comme bien d'autres peuples, ils ont commencé à gagner des droits vers la fin du 20e siècles. Ils ont une méfiance de la société moderne, et souffrent souvent du manque d'accessibilité des constructions faites par et pour les humains et humanoïdes bipèdes.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 18 | 10 | 10 | 4 | Poing (0) | CON, SAG, VOL (60) | DIS, HAB (40) | Toutes les autres (50) |

## Traits et compétence

| Trait | Effet |
|:-----:|:------|
| Croupe | Peut porter deux personnes humanoïdes sans aucun malus |
| Quadripède | -50% de chance de tomber, +50% de chance de tomber dans des escaliers |

## Lycions (variante)

Les Lycions sont des centaures atteint par la malédiction lupine, et sont donc très proche des loup-garou. Ils combines les pouvoirs (et faiblesses) des deux espèces. Leur tête est une tête de loup, et leur corps est plus canin qu'équin. Leurs chevelures peuvent souvent former de forte crinière. C'est une espèce puissante, rapide, mais pouvant facilement être blessé. 

Ils sont extrèmement rares.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 16 | 10 | 10 | 4 | Griffe (2) | CON, SAG, VOL (60) | DIS, HAB (40) | Toutes les autres (50) |

### Traits et compétence

| Trait | Effet |
|:-----:|:------|
| Haine de l’argent | Subit un effet de poison (cumulable avec un poison normal) par l'argent |
| Apparence brute | +2 arme mentale | 
| Rage lupine | Peut passer dans l’état rage |
| Croupe | Peut porter deux personnes humanoïdes sans aucun malus |
| Quadripède | -50% de chance de tomber, +50% de chance de tomber dans des escaliers |

## Némédiens (variante)

Les Némédiens sont une variante de "centaure-danaïtes" non-ailés, avec des bois de cerfs. Les némediens ont des pouvoirs mélangeant celles des danaïtes et des centaures

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 14 | 8 | 10 | 5 | Mana (4 sur armure magique) | CON, SAG, VOL (60) | DIS, HAB (40) | Le reste (50) |

### Traits et compétence

| Trait | Effet |
|:-----:|:------|
| Végétal | Se prend 2x par au métal et au feu. |  
| Être de la forêt | +10 en HAB, SAG et PER dans les milieux naturels. -10 dans les lieux entièrement trop pollués. |
| Croupe | Peut porter une personnes humanoïdes sans aucun malus |
| Quadripède | -50% de chance de tomber, +50% de chance de tomber dans des escaliers |
