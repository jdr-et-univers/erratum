---
layout: layouts/base.njk
title: Écritures magiques
tags: Objets réutilisables
models:
  - Objets consommables
  - Objets réutilisables
parent: Les objets
---

# Écritures magiques

Plusieurs systèmes d'écritures existent à travers le monde, offrant des pouvoirs plus ou moins incroyables à ceux qui l'utilisent.

## Runes

Les runes sont des petites pierres gravée de la rune, qui permette d'activer un effet, une fois toutes les trois parties.

| Rune | Description | Rune | Effet |
|:----:|:------------|:----:|:------------|
| Fehu | Richesse | Eiwhaz | Fait apparaitre un arbre |
| Uruz | Auroch ; Muscle | Perth | Perte de ce qu'on tiens |
| Thursaz | Thor ; La foudre | Eiwhaz | Force vitale ; Créer une barrière vitale de 15PV |
| Ansuz | Communication avec les dieux | Sowilo | Apporte le soleil |
| Raido | Retour en terre connue | Tiwaz | Antidamnation pendant trois tours |
| Kenaz | Eclat ; Ramène 3 Eclats | Berkanan | Bitchage absolu et total |
| Gebo | Le social | Ehwaz | Fait apparaitre des chevaux |
| Wunjo | Soin (physique et mental) | Mannaz | Restaure une créature corrompue |
| Hagalaz | Provoque grele (verglas+brume+tempete de sable) | Laguz | Transformation en animal |
| Naudiz | Répond aux besoins vitaux | Ingwaz | Embrouille l'esprit |
| Isaz | Produit le froid | Dagaz | Améliore les loots |
| Jeran | Apporte la chance | Odala | Créer des bulles de réalités |
