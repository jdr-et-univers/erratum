---
layout: layouts/base.njk
title: La métaphysique
tags: Compétences spéciales
models:
  - Compétences spéciales
parent: Statistiques 
---

# La métaphysique

La métaphysique est la discipline consistant à chercher la connaissance du monde au delà des limites de ce que l'on peut percevoir ou comprendre, indépendante de l'expérience sensible du monde autour de nous. Il peut s'agir de représenter la connaissance du "monde caché" derrière les apparences, ou comprendre des principes du monde au delà de nos lois physique, ou d'acquérir de la connaissance sur les êtres au delà de notre monde.

Dans Erratum, la métaphysique est une compétence, mais dont les règles sont quelques peu particulières. Notamment, au contraire des compétences ordinaires, la métaphysique est une compétence ayant un tolé sur le joueur l'utilisant. Le but de cette page est de décrire les règles de cette compétence.

## Obtenir de la métaphysique

La métaphysique est une compétence particulière en ce qu'elle ne peut pas se monter librement. En effet, le personnage obtient 10% en métaphysique quand le MJ estime que l'expérience qu'il a vécu a augmenté sa sensibilité en ce qui est au delà du monde physique. Cela peut être le fait d'être témoins de phénomènes inexplicables, acquérir des connaissances, etc.

Cette stat ne peut donc s'augmenter que via le RP, et via les expériences se produisant lors de la partie. La métaphysique n'est pas limitée comme les compétences ordinaires, et læ MJ peut la faire dépasser les limites s'iel estime que cela est nécessaire.

## Les utilité de la métaphysique

Comme toute connaissance, la métaphysique s'applique lorsque que læ joueur⋅euse veut faire un jet pour effectuer une action lié à la compétence. Cependant, cette compétence à une particularité, en ce *que tout jet de métaphysique voit sa statistique de base divisée par deux*, ce que la compétence soit présente ou non.

La métaphysique peut s'appliquer sur plusieurs types de jets:
- Tenter de comprendre un phénomène métaphysique se produisant devant lui.
- Pour communiquer avec des êtres par delà le monde
- Pour utiliser un appareil dont la compréhension dépasse la simple compréhension du monde.

La présence de métaphysique permet aussi d'éviter de se prendre 1D4 dégats moraux à toute arrivée de phénomènes métaphysiques devant læ personnage.

## Effets négatifs

> **Note:** Résister à un phénomène métaphysique terrifiant n'étant pas un jet métaphysique, la stat n'est alors pas divisée par deux.

Si la métaphysique protège des effets néfastes de la simple survenue de phénomène métaphysique, elle affaibli læ personnage s'iel doit résister à un phénomène métaphysique particulièrement terrifiant. Parfois, savoir rend les choses pires. Dans ce cas, le personnage se verra soustraire son niveau à la statistique.

Cela vaut aussi pour résister à une attaque *physique* métaphysique.
