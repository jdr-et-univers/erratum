---
layout: layouts/base.njk
title: Anostiens
tags: Espèces légendaires
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Anostiens

Les anostiens sont des êtres composé d'éclat, dont le corps est une forme de projection, imitant les caractéristiques de la matière. Proche sur de nombreux aspects des *fantômes*, mais ayant des structures plus fiable et plus solide, ils peuvent être également assimilés aux *anges et aux diablons* et aux changeformes, ayant beaucoup de leurs caractéristique (être assexués, être composé de magie pure, etc).

Les anostiens étaient le quatrième des quatre grand peuple, vivant sur les Terres de Lémurie, aujourd'hui connu sous le nom de plateau des Kerguelen, ils étaient sous la protection du dieu *Yule*. Êtres bleu.e.s assexué.e.s, à la queue de "démon" et avec des cornes extrèmement sensibles (pouvant notamment connaître la pression de l'air et tout) ainsi que d'ailes d'éclat pur, les Anostiens sont auréolés de mystères.

En effet, iels avaient la particularité d'exister en nombre fixe : à la disparition d'un Anostiens, un autre. Le temps ne fonctionnait pas de la même manière, leur espérance de vie ayant un caractère pouvant sembler aléatoire pour les autres peuples. De plus, si leur vieillissement fonctionne avec les mêmes phase que les autres peuples, la durée semble erratique.

Les anostiens ont disparu il y a 10 000 ans, environs en même temps que se sont effondré les civilisations antiques.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 10 | 18 | 12 | 6 | Poing (0) | CHA, HAB, VOL (60) | REL, FOR, DIS (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Être magique | +10 % pour utiliser la magie ; Se prend 2x par les armes d'éclat | 
| Entropie | peut tenter une action d’entropie, pouvant provoquer des réactions en chaîne. Doit décrire la suite de réaction, le MJ indiquera les chances de réussite et la pression au moment du jet. |
