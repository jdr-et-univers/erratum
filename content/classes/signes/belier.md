---
layout: layouts/base.njk
tags: Signes de Protection
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Bélier
parent: Les signes
---

# Bélier (le groupe)

Les béliers tirent leur pouvoir des autres, et des responsabilités qu'iels ont envers leur entourage. Avec la capacités de protéger les autres et de se placer en travers de ce qui pourrait les atteindre, les Béliers ont le pouvoir de se sacrifier eux pour les autres.

Chef ou protecteur de l'ombre, ils sont des gardiens du troupeaux, ceux qui peuvent faire en sorte que c'est tous qui réussissent, et pas juste certains.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :aries: | ??? (asie), ??? (égypte) | Protection et Guérison (Tank Soigneur) | Ordre | Peu commun (9.5%) | CON et REL | Or |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Garde du corps  |  Peut consommer un déplacement pour s’interposer pour se prendre une attaque. Peut ensuite faire un jet pour se défendre face à l’attaque.  |
| Double |  Garde du corps de l’extrême (2 Eclat)  |  Peut s’interposer n’importe quand pour se prendre une attaque. Peut ensuite faire un jet pour se défendre face à l’attaque.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Soin physique  |  Peut soigner en PV l’équivalent des niveaux de réussite du jet (+1 si pas de baton de soin)  |
| 1 |  Force du troupeau (REL) (2 Eclats)  |  Peut répartir les dégâts d’une attaque subit par un membre de son équipe sur le groupe.  |
| 2 |  Soin de groupe (2 Eclat)  |  Peut soigner en PV à toute l’équipe l’équivalent des niveaux de réussite du jet (+1 si pas de baton de soin), minus un malus de 50 % (II : pas de malus)  |
| 2 |  Pas lui (3 Eclats)  |  Peut indiquer un personnage, tous les autres personnages présents auront -15 % (II : 30 %) pour l’attaquer  |
| 2 |  Bannière d’Esprit (1/combat, 3 eclats)  |  Plante une banière au sol. La bannière soigne le groupe de 1D4 PE/tour, mais inflige 1D8 PE si elle est détruite. 20 PV. (II: peut poser une deuxième bannière)  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture défensive  |  Peut passer en posture défensive  |
| 3 |  Renforcement Magique (INT) (2 Eclat)  |  Ajoute +3 (II : +5) d’armure magique à un personnage pendant trois tours si réussite du jet.  |
| 3 |  Opprobre (3 Eclat)  |  Peut indiquer un personnage, tous les personnages présents auront +15 % (II : 30 %) pour l’attaquer.  |
| 4 |  Templier (2 Eclats)  |  Lors du prochain tour (II: des deux prochains tours), toutes les attaques seront pour lui.  |
| 4 |  Bannière de vie (1/combat, 3 eclats)  |  Plante une bannière au sol. La bannière soigne le groupe de 1D4 PV/tour, mais inflige 1D8 PV si elle est détruite. 20 PV. (II: peut poser une deuxième bannière)  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Le chef du troupeau (1 karma)  |  Peut permettre à tout son groupe une seconde action, sauf lui. Les actions se font dans l’ordre des joueurs, et peuvet être fait quelle que soit les conditions de temps.  |
| 5 |  Action de groupe (5 Eclat)  |  Fait lancer à tout le monde un jet pour une action. Si tous les jets réussissent (ou 1 réussite critique), la réussite est critique.  |
| 5 |  Inventaire commun  |  Peut passer ou demander (sur accord) un objet avec un personnage, pour un déplacement. (II: la première du combat ne demande pas de déplacement)  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Partage de capacité  |  Peut utiliser une capacité d’un autre personnage, pour 2 Eclat de plus que ceux de la capacité (II : Pour deux éclats de plus, peut la fournir à quelqu’un d’autre)  |
| 6 |  Renfort (4 Eclat)  |  Peut faire remplacer un personnage KO par un autre personnage qui était hors du combat (du même joueur de préférence, voir d’un autre si le joueur n’a plus de personnage en stock).  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  L’appel à la guerre (6 Eclats) (REL ou CHA)  |  Peut ajouter +5 à chaque armure à toute l’équipe, pendant 5 tours. Toute l’équipe brillera alors d’une lueur intense.  |
| Herault |  Soin de groupe optimisé (2 éclat, 1/combat)  |  Peut, lors d’un soin de groupe, faire le calcul du soin total et décider d’optimiser la répartition du soin suivant en gardant le « surplus » pour les autres personnages  |
