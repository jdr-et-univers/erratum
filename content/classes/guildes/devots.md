---
layout: layouts/base.njk
title: Les dévots
tags: Guildes métaphysiques
models:
  - Guildes
  - Guildes métaphysiques
parent: Les guildes
---

# Les dévots

Les *dévots* sont les suiveurs de dieux, qui ont décidés de se lier aux dieux (ou à des démons) afin d'obtenir plus de puissance. Beaucoup de leurs pouvoirs sont dépendant des pouvoirs du dieu qui les aide, et ils utilisent l'exaltation comme "monnaie d'échange" avec leur dieu. En plus de eux gagner en puissance, ils font gagner en puissance leur dieu.

Chaque dieu à le droit aux éléments suivants pour aider ses dévots :

- Un *élément de prédiléction* (et un élément craint)
- Un trait de personnalité de prédiléction (avec donc stats et potentiels)

Cette classe n'est pas disponible aux demi-dieux et aux démons.

## Critères d'entrées

- Avoir un lien avec un dieu

## Pouvoirs de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Exaltation | Font un pacte avec un dieu. Pour chaque moment ou ils iront dans le sens de leur dieu, gagnent 1 point d'exaltation (max 10). Ils pourront l'utiliser pour certaines de leurs capacités, ou un point pour demander un coup de main ou un conseil à leur dieu. |
| 6 | Possession divine | Le dieu prend le contrôle du corps de son/sa dévot⋅e. Voir catégorie. Ça pique. |

## Illuminés

Le personnage a vue des possibilités au dela de son monde, a vu à travers le regarde de son dieu, qui lui offre une toute nouvelle perspective sur le monde et ses possibilités.

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Connaissance panthéonique (trait) | Peut sentir naturellement la présence de membres du panthéon de son dieu |
| 2 | Apprentissage divin (1 exaltation) | Gagne +20 à deux compétences que son dieu peut connaitre. |
| 3 | Habitude métaphysique (2 exaltation) | Peut annuler le malus métaphysique pendant 2 tours |
| 4 | Les yeux divins (trait) | +30% pour voir et comprendre les phénomène métaphysiques |
| 5 | La porte de la vérité (2 exaltation) | Peut obtneir la vérité sur un phénomène métaphysique ou lié à un dieu, mais fera un jet de panique. |

## Fanatiques

Le personnage voue un amour immodéré pour le dieux et peut amplifier ses pouvoirs, toujours au gouffre de la perte de contrôle.

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Amour immodéré (trait) | Le dieu gagne un bonus dans toutes ses stats sociales du à l'amour que lui doit le fanatique. |
| 2 | Vengeance (1 exaltation) | Durant trois tours, si un ennemi attaque il se prendra 1D4-1 dégats élémentaire de l'élément du dieu |
| 3 | Transe (2 exaltation) | Passe dans une transe ou il n'agira que pour son dieu, mais aura double chance de réussite critique, et accès au potentiel de son dieu. Dure trois tours ou il se prend -1D4 PE |
| 4 | Personnalité divine (trait) | Gagne (en mode potentiel) le trait de personnalité de son dieu |
| 5 | Sacrifice divin (VOL, 1 exaltation) | Sacrifie 1D10 PV et PE pour mettre un terrain élémentaire qui n'aura d'effet négatif que sur les ennmis, ainsi qu'une attaque élémentaire non-physique qui les touchera tous. |

## Corrompus

Les pouvoirs du dieu ont transformé le corps du corrompu pour le rendre plus proche de lui, abimant son corps mais permettant de nombreuses possibilités

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Frappe élémentaire (1 exaltation) | Peut rajouter l'élément de prédiléction de son dieu à une attaque |
| 2 | Sang divin (1 exaltation) | Peut actionner des mécanismes divins/céleste |
| 3 | Métamorphose (2 exaltation) | Peut passer dans une forme bestiale ou il recevra doublé l'effet des traits de personnalité de son dieu. Ne peut pas dépasser 300 et devient mono-dimensionnel, surtout défini par ce trait durant la transformation. Dure trois tours ou il se prend -1D4PV |
| 4 | Maitrise de l'élément (trait) | Est immunisé aux effets négatifs de l'élément de son dieu (terrain ou attaque) |
| 5 | Action de masse (3 exaltation) | Sous forme bestiale, peut effectuer une action jusqu'à 3 ennemis, mais avec /2 de niveaux de réussite et doublera les dégats de transformation. |

## Posession divine

A partir du niveau 6, il est possible de se faire posséder par un dieu. C'est très dangereux, mais très redoutable. Le personnage à le droit au pouvoirs suivants.

- Stats "divine", à le droit à `+40` à toutes ses statistiques + le double eds stats de trait de personnalité. Peut dépasser 200.
- Toutes ses armures x2
- Se prend 1D6+4 PV et PE par tour.
- Eclats infini
- A accès à l'attribut de son dieu et peut l'utiliser.
- Provoque un boost de métaphysique.