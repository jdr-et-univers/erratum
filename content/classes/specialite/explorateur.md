---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: L'explorateur
parent: Les spécialités
---

# L'explorateur

L'explorateur, le ranger, le scout, etc… beaucoup de noms sont utilisé pour ces aventurier qui explorent la nature et la connaissent comme leur poche. Ces pro de la survie ont la capacité de s'adapter à des tas de situations.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Cueilleur | Reçoit passivement des plantes lors des explorations naturelles |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Confections | INT+HAB | 3/combat (ou phase d'actions) | Peut effectuer des remèdes regénérant les niveaux de réussite en PV |
| 1 | De nouveaux horizons |  |  | Gagne 1D8 érudition à chaque fois qu'il découvre un nouveau lieu |
| 2 | Je trouve mon propre repas (trait) |  |  | En territoire naturel, n'ont pas besoin de ration. |
| 2 | Préscience | PER |  | Peut, lors d’une discussion non-superficielle avec un PNJ, savoir le niveau d’agressivité envers le groupe dudit PNJ. |
| 2 | « Miaou ! » |  | 3/partie | Peut faire passer un jet ou une confrontation de DIS raté, sauf critique |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture offensive |  |  | Peut passer en posture offensive |
| 3 | Leste bond  | HAB | 3 tours de cooldown | Peut sauter pour se mettre exactement ou le joueur veut dans la limite du raisonnable. |
| 3 | Spéléologie (trait) | PER |  | Dans un environnement fermé, gagne +20% en PER et HAB |
| 4 | Chausse-trappes |  | 1/combat | Pose des pièges qui peuvent provoquer aux ennemis *saignement* s'ils échouent un jet d'HAB |
| 4 | Repérage | PER | 2/partie | Peut dévoiler une partie de la carte du lieu environnement, avec détails sur ceux proches invisibles |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Pistage | PER |  | Permet d'avoir une estimation de la masse de personne en un lieu sans le voir directement. |
| 5 | Fuite | DIS/HAB |  | Permet de fuir entièrement le combat. Peut emporter quelqu'un avec lui. |
| 5 | Rien n'échappe à mon regard (trait) |  |  | Ne peut être pris par surprise. |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Adaptabilité |  | 1/combat | Peut reproduire (avec -30%) une capacité non-magique d'un allié |
| 6 | L'attrait de l'exploration (trait) |   |   | Quand il est dans un nouveau lieu, l'explorateur gagne +50% d'armure mentale. |