---
layout: layouts/base.njk
title: Changesformes
tags: Espèces légendaires
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Les changeformes

Les changeformes sont des êtres polymorphes, connus pour leur pouvoir de ne pas avoir d'apparence spécifique et de pouvoir prendre celle qu'ils veulent. Ils ont presque disparu lors de la *guerre des vertues*, mais ont réussi à subsisté dans certaines régions du monde. Ces esprits ont une durée de vie proche de celles des espèces humanoïdes.

Ils sont proches des anostiens d'un point de vue biologique, et possède un pouvoir de transformation en une forme animale. Peut est connu de ces êtres, si ce n'est qu'ils ont le pouvoir de se lier émotionnellement à d'autres êtres.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 12 | 12 | 10 | 5 | Poing (0) | Aucune | Aucune | Toutes (50) |

## Traits et compétence

| Trait | Effet |
|:-----:|:------|
| Changeur de forme | Peut passer à volonté entre une forme animale et humaine, sauf en cas de stress. | 
| Être magique | +10 % pour utiliser la magie ; Se prend 2x par les armes d'éclat | 
