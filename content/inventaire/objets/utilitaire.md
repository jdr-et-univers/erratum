---
layout: layouts/base.njk
title: Objet utilitaires
tags: Objets réutilisables
models:
  - Objets consommables
  - Objets réutilisables
parent: Les objets
---

# Objet utilitaires

Les objets utilitaires permettent de faire des choses diverse. Ils sont réutilisables, mais chaque personnage ne peut en avoir généralement pas plus de deux nécessaires sur lui.

| Objet | Effet | Prix |
|:----------:|:------|:----:|
| Nécessaire de camping | Permet de loger un petit groupe de personne (4~5). Réutilisable, mais prend de la place et doit prendre du temps pour être remonté | 200 |
| Nécessaire de jardinage | Rajoute +40% pour trouver des plantes ou ingrédients d'herboristeries | 800 |
| Nécessaire de soin | Rajoute +5% de réussite critique pour soigner ; permet de stabiliser quelqu'un dans le coma | 1000 |
| Nécessaire de cuisine | +30% pour cuisiner, fait de base des *repas de qualité*. Peut rajouter les effets de plantes à un plat. | 1000 |
| Établi de potion | (1/partie) Pendant cinq tours, passe en mode potion. Les potions coûtent un Éclat de moins pendant ces trois tours, mais ne peut RIEN faire d’autres | 2000 |
| Précis de démonologie | Peut tenter de prendre contrôle total (REL/CHA) d’un monstre de type démoniaque | 3000 |