---
layout: layouts/base.njk
eleventyNavigation:
  key: Les spécialités
  parent: Classes et experiences
  order: 5
models:
  - Les spécialités
title: Les spécialités
---

# Les spécialités

Les spécialités sont des classes utilisables par les personnages sans magies. Elles sont plus proche des classes traditionnelles de JDR, et utilisent plus des cooldown et autres méthodes que l'éclat pour gérer les limitations de pouvoirs.

## Listes des spécialités

## Les talents

> **Note** : Les talents sont dépréciés et seront retiré dans une version futures. Les spécialités les remplaces pour les personnages non-magiques

Voir page sur les [talents](/classes/talents/)