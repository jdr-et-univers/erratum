---
layout: layouts/base.njk
title: Kobolds
tags: Espèces additionnelles
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Les kobolds

Les kobolds sont des serviteurs des dragons. Ces petits êtres reptiliens bipède vivent souvent dans les carvernes des dragons, et les aident dans la vie de tout les jours. Ils sont souvent considéré comme peu intelligent, mais cela tiens plus du cliché qu'autre chose. Ils n'ont été reconnu comme espèce sentiante que vers la moitié du vingtième siècle. Ils ont souvent des pouvoirs élémentaires lié à ceux de leur dragon.

Ils ont presque disparus pendant un moment en Europe à cause de la chasse au dragon, était souvent attaqué parce que plus simple à attaquer qu'un vrai dragons. Certains tueurs de dragons ont largements utilisé des os de kobold pour gonfler leur score, les crânes de kobold pouvant ressembler à un crâne de dragonneau, tout en étant largement moins dangereux.

Il y a souvent eu des débats sur s'il fallait considérer ou non les Kobold comme membres du petit peuple.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 18 | 10 | 8 | 6 | Griffe (2) | CON, HAB, REL (60) | FOR, SAG, DIS (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Serviteur des dragons | -20 % en volonté face aux dragons |
| Souffle de feu | Peuvent faire une attaque de feu à +0 dégats |
| Sang froid | Se prennent x2 dégats par la glace |

## Fomoires (variante)

Les fomoires sont une variante plus forte et plus solide que les kobolds.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 18 | 10 | 8 | 3 | Griffe (2) | CON, FOR, HAB (65) | HAB, REL (35) | Le reste (50) |

### Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Serviteur des dragons | -20 % en volonté face aux dragons |
| Auto-immolation | Peuvent faire toute leurs attaques sans armes avec un élément de feu. |
| Sang froid | Se prennent x2 dégats par la glace |
