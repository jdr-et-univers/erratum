---
layout: layouts/base.njk
title: Les potions
tags: Objets consommables
models:
  - Objets consommables
  - Objets réutilisables
parent: Les objets
---

# Les potions

Les potions sont un bon moyen de construire des effets, et de faire des trucs plus ou moins prévus avec.

## Substances

Les substances et ingrédients sont le coeur des potions

Les potions se construisent en combinant différents effets pour former une potions. A noter que la combinaisons des ingrédients n'aditionne pas les effets, mais les "combines", pouvant faire des combinaisons différentes. Un effet de "feu" peut à la fois servir à réchauffer une fois dilué, faire des dégats de feu, ou provoquer un effet de feu aux attaques… voir combiné à un effet mental à "réchauffer l'esprit", "atiser le feu mental".

De même, des mêmes ingrédients dans différentes circonstances et différents effets pourront avoir des effets différents. De plus, plus une potion est puissante, plus des risques d'effets secondaires transparaises. Différentes plantes peuvent avoir les même effets de même. Plus des ingrédients aux effets différents seront utilisé dans des même buts (par exemple Nature + Vitalité), plus la potion obtenue pourra être puissante. Parfois, pour stabiliser quel effet on va prendre d'un élément par exemple, il peut être bien d'utiliser un autre type d'ingrédient avec un effet "précis" (genre Néfaste, etc).

Les substances peuvent aussi être utilisée pour créer des effets sur les armes.

## Substances communes

| Substance | Effet |
|:---------:|:-----:|
| Eau | Remplir, diluer, infuser |
| Catalyseur | Amplifier, augmenter |
| De vitalité | Soigner, guérir, améliorer |
| Renforceur | Affermis le corps |
| Psychique | Affermis l'esprit |
| Négatif | Inverse un effet |
| Néfaste | Faire du mal |
| Explosive | La potion devient volatile et peut exploser |
| *(Élément)* | Un effet lié à l'élément (réchauffer pour le feu, refroidir pour la glace), voir faire l'effet de l'élément |

## Substances spéciales

| Substance | Effet |
|:---------:|:-----:|
| Ectoplasme | Substance lié aux fantômes |
| Trèfle à Quatre Feuille | Offre de la chance |
| Eau de rose | Affecte la perception de l'entourage (uwu) |
| Bicarbonate de Soude | Peut remplacer un ingrédient |
| Mandragore | Transforme le corps |

## Matériaux

Les matériaux sont rarement utilisé en potions, plus souvent pour les armes. Mais un bon apothicaire peut les utiliser de manière originale en potion pour faire des effets nouveaux et redoutables.

| Substance | Effet |
|:---------:|:-----:|
| Argent Lunaire | Dangereux pour les loup garou |
| Or Solaire | Dangereux pour les mort-vivants |
| Orichalque | Absorbe facilement les propriétés magiques alentours et les renforce |
| Mercure Rouge | Une substance mutagène et radioactive |

## Substances métaphysiques

| Substance | Effet |
|:---------:|:-----:|
| Alosun | Donne des super-pouvoirs |
| Ambroisie | Rend temporairement divin un demi-dieu ; très dangereux pour le reste des êtres vivants (mais super bon) |
| Eclat | Provoque un surplus de magie (dangereux pour être magique) |
| Éclat noir | Draine la magie |
| Adamant | Dérivé d'éclat, ne peut pas être traversé par l'éclat |
| Anomie brute | Ajoute de l'anomie |
| Orgone | Concentré de vie pure |

## Exemples de potions

Voici quelques exemples de potions, avec des examples d'ingrédient pour arriver à y parvenir.

| Potion | Effet | Type d'ingrédients utiles |
|:------:|:------|:-------------------------:|
| Potion de soin légère | Permet de regagner 6PV | Vitalité
| Potion de soin avancé | Permet de regagner 12PV | Vitalité, Catalyseur |
| Potion de vigueur | Augmente de 10 % les stats physique pendant trois tour | Un élément "chaud", vitalité |
| Extrait de douleur | Inflige 3 dégats esprit et 3 dégats de corps | Néfaste, potentiellement d'autres ingrédients |
| Potion de lumière | Produit un malus de 15 % en PER à tout personnage touché par la potion. | Lumière, Explosive |
| Potion de rage | Booste l’attaque de 20 % mais lui donne 1 chance sur 4 d’attaquer un allié (la chance sur 4 est cumulable) | Feu, Esprit |
| Concentré de Dopage | Permet de faire rejouer | Temps, Foudre ? |
| Essence de célérité | Toujours premier pour prochain combat | Temps |
| Potion de flamme | Donne des dégats de feu et gagne +3 dégats de base | Feu, Corps |
| Potion de pestilence | Fait faire un jet de pestilence à l’ennemi quand il la boit | Nefaste, Toxine x 2 |
| Potion d'intengibilité | Donne une immunité aux dégats physiques. | Esprit, Corps (rendu négatif) / Ectoplasme |
| Filtre d’amour | Peut séduire en combat un allié et lui donner +50 % à tout ses jets. Il obéira à tout ses ordres, et ne prendra d’action sans son autorisation. | Pas besoin de vous faire un dessins uwu |
| Potion "debout les morts" | Soigne de tout les PV un perso non-mort, mais rend fortement confus 5 tours. | Vitalité (pleisn de fois, avec catalyseur) |
| Potion explosive | Provoque une explosion d'une puissance de 8 dégats | Explosive |
| Potion de Résurrection | Ramène un perso mort à la vie, si la mort date de moins d'une partie | Ectoplasme (rendu négatif) |
| Potion secrète bretonne | La personne l’ayant bu peut annuler un critique au choix | Trèfle à quatre feuille, temps |