---
layout: layouts/base.njk
tags: Signes de Force
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Lion
parent: Les signes
---

# Lion (l'héroïsme)

Les lions tirent leur pouvoir de l'héroïsme et du courage. Héros aux armes légendaires uniques forgée directement de leur âme, ils sont voués à occire le mal et à protéger l'innocent.

Ils sont plus fort quand ils sont en danger, et leur pouvoirs permettent de se battre plus facilement contre des ennemis et de faire des duels.

Leur herault est Gilgamesh, le roi-héros, aux exploits légendaires qui ont transcendé l'histoire et les légendes, vers -3000.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :leo: | Tigre (asie), ??? (égypte) | Force et Protection (DPS Tank) | Ordre | Rare (02.2%) | CHA, FOR et VOL | Rouge |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Unicité  |  Le personnage choisit un type d’arme et d’armure au choix, qu’il recevra de la part du MJ. Ne pourra employer d’autre arme, ni être désarmé. L’arme peut être enchanté ou améliorée grace à la magie. Un équipement ou objet du personnage peut être touché par unicité  |
| Double |  Unicité +  |  Les équipements touchés par Unicité ne peuvent plus être débuff.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Charisme héroïque  |  +15% (II: 30%) en charisme quand il fait une action courageuse pour quelqu’un d’autre.  |
| 1 |  Honneur du héros  |  Peut forcer un personnage à ne combattre que lui (II : peut le faire sur deux personnages à la fois).  |
| 2 |  Garde héroique  |  Peut se prendre un coup d’esprit sur les PV plutôt que les PE. L’armure compte mais pas de jet de protection.  |
| 2 |  La lame de la volonté (3 Eclats)  |  Peut faire une attaque physique mais taper sur l’esprit  |
| 2 |  Honneur du duelliste  |  Lors d’un honneur du héros, le Lion à +10 % (II : 20 %) à tout ses jets d’attaques tant qu’il n’a attaqué que son adversaire.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture offensive  |  Permet d’utiliser la posture offensive (II : posture offensive +)  |
| 3 |  Frappe de lumière (3 Eclat)  |  Peut faire une frappe qui s’encaissera sur l’armure magique et aura un effet de lumière.  |
| 3 |  Charge héroïque (5 Eclats)  |  Si la place est suffisante, consomme un déplacement et charge sur une ligne, lançant deux dés (II: trois jets) de FOR au lieu d’un.  |
| 4 |  Action irréfléchie  |  Peut une fois par partie, faire une action qui passera toutes les priorité et qui aura 20% (II: 35%) de plus de critique. Réussite comme échec.  |
| 4 |  Duel du héros  |  Peut provoquer un duel du héros où personne ne pourra venir attaquer aussi, mais lui ne pourra pas attaquer quelqu’un d’autre.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  J'attaque qui je veux  |  Est insensible au taunt  |
| 5 |  Hyperchargé  |  S'applique l'effet "hyperchargé" pendant trois tours (II: cinq tours) : augmente de moitié les dégâts infligés comme pris  |
| 5 |  Dernière chance  |  En dessous de 50% de ses PV, gagne +10% à ses jets physiques.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Le héros de l’histoire (1 karma)  |  Peut imposer que une réussite critique.  |
| 6 |  Patience  |  L’utilisateur doit encaisser les prochaines attaques durant trois tours. Il renverra cependant l’ensemble des dégâts pris, de manière inesquivable, sur un ennemi au choix.  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Armurerie du héros  |  Peut changer à tout moment le type de son arme, qui aura donc la puissance équivalent à son niveau dans le nouveau type.  |
| Herault |  Tel 1 million de lion (trait)  |  Coûte un déplacement et double ses dégats sur une troupe, un ennemi géant ou un dieu. Peut se combiner avec d'autre pouvoir.  |