---
layout: layouts/home.njk
eleventyNavigation:
  key: Personnalités
  parent: Personnages
  order: 2
---

# Personnalités

Les statistiques de votre personnages peuvent être influencée par de nombreux éléments, dont la personnalité du personnage. Chaque personnage doit séléctionner deux principes de personnalité. Ces deux principes donnent la direction générale de qui est le personnage, et pourront l'aider à se définir in-RP (cela n'est cependant qu'une base autour de laquelle vous pouvez broder), et donnera une base pour ses stats et les points généraux de son comportement.

La personnalité défini grâce aux stats améliorés par elle vos points fort (qui restent des "points fort", même si d'autres personnalité les affaiblis), qui vous permettront également de séléctionner des talents (contre XP) pour vos personnages.

On peut imaginer in-RP un moyen d’en obtenir un troisième, à la discrétion du MJ.

## Liste des personnalités

<div class="table-responsive" id="dataTable">

| Nom | +20 | +10 | -10 |   |
|:----|:---:|:---:|:---:|:--|
| **Costaud** | FOR | CON | HAB | **Presque de la télékinésie :** Peut attaquer avec n’importe quel élément du décor dans les limites de l’accord du MJ. | 
| **Brute** | FOR | DIS | REL | **Menace :** Peut dépenser un 4 éclats pour obliger un allié à faire une action, quelqu’elle soit. |
| **Bourrin** | FOR | VOL | SAG | **Je hurle mes capacités :** Le joueur peut faire un jet de CHA pour ajouter ses points de réussite à une attaque. |
| **Chevaleresque** | FOR | SAG | DIS | **Honneur du Chevalier** : Peut ignorer les réputations auprès des factions. |
| **Hippy/Naturel** | CON | PER | FOR | **Plus à l’aise au naturel :** Gagne +30 % à ses jets quand il n’a aucune arme (sauf griffe/poing) équipé. |
| **Soldat** | CON | VOL | DIS | **Suivi des ordres** : Regagne 1D4 PM lorsqu'il réussi une action demandée par un personnage allié. |
| **Sportif** | CON | HAB | PER | **Le sport c’est la santé :** Après chaque réussite physique, peut faire un jet pour gagner 1D4-2 PV. |
| **Gardien** | CON | REL | VOL | **Anti-bully :** bloque toute actions faites volontairement ou involontairement par quelqu’un contre spécifiquement ses propres alliés. |
| **Adroit** | HAB | REL | CON | **Bonne poigne :** Ne peut pas être désarçonné ou lâcher un objet qu’il tient (les objets dans le sac ne sont pas comptés). |
| **Acrobate** | HAB | FOR | DIS | **Art du cirque :** Peut attaquer sur l’habilité. |
| **Roublard** | HAB | DIS | VOL | **Entourloupeur :** Sur un jet de DIS, peut rejeter immédiatement la faute sur quelqu'un d'autre sans malus (mais sans compétence) si un vol ou une escroquerie échoue. |
| **Casse-cou** | HAB | VOL | SAG | **Danse avec la mort :** Chaque jet de survie réussi lui donne +50% dans toutes les statistiques jusqu'à la fin de la partie. |
| **Intello/Nerd** | INT | PER | CHA | **Non, ce n’est pas possible :** Fait un jet d’INT, le nombre de point de réussite s’ajoute à armure psychique. |
| **Littéraire** | INT | SAG | CON | **Le vrai monde est inintéressant :** Fait un jet de INT, le nombre de point de réussite s’ajoute à armure physique. |
| **Malin/Futé** | INT | HAB | FOR | **La voie de sortie :** une fois par partie, pour tous son éclat, annuler un combat non-boss. |
| **Rigoureux/Travailleur** | INT | CON | HAB | **Préparation :** Peut en passant un tour avant une capacité obtenir un bonus pour faire une action. |
| **Responsable** | SAG | CON | CHA | **Damage Control :** Peut sur un jet de REL tenter d'éviter les dommages à la réputation du groupe causé par une action d'un joueur unique. |
| **Ermite/Ascete** | SAG | DIS | REL | **Se contenter de peu :** Une vivre dure deux parties/déplacement long. |
| **Sage** | SAG | REL | INT | **Tout ce que je sais, c'est que je ne sais rien :** Peut dépenser des points de confiance pour se rajouter des bonus pour tenter d'accéder à une connaissance (2 points = +5%) |
| **Débrouillard** | SAG | INT | PER | **Survivant :** +20% à tout les jets de survies. |
| **Vertueux** | VOL | REL | DIS | **Colère du juste :** Face à une situation qu’il considère inacceptable, il gagne un bonus de 10% sur tout ses jets pendant une séquence d’action OU un combat. (une fois par partie.) |
| **Fanatique/Foufou** | VOL | INT | SAG | **Croisade :** +5 % aux statistiques, -1 Éclat (comme focus), mais perd 3 point (PV et/ou PM) par tour. |
| **Assuré** | VOL | CHA | PER | **Sûr de lui :** Les effets de la fatigue morale ne s’active qu’à partir de -50 % des PM, et sont divisé par deux. |
| **Saboteur/Délinquant** | VOL | DIS | CON | **La mauvaise réputation :** Gagne +5 chance de réussite critique dans un endroit ou sa réputation est mauvaise. |
| **Meneur/Stratège** | CHA | INT | HAB | **LEADER :** Peut dépenser 6 Eclats pour additionner ses statistiques et la capacité d’un personnage visé. |
| **BG** | CHA | CON | PER | **Encouragement :** Peut dépenser 2 Eclat par personne touché, une fois par partie, pour encourager tout ses alliés et faire que tout leur prochain coup seront critique. |
| **Autoritaire** | CHA | FOR | REL | **Autorité :** Peut donner un ordre, +20 % pour réussir, -20 % si on désobéit. |
| **Visionnaire** | CHA | PER | SAG | **Voilà ce qui va se passer !** Peut, une fois par partie, demander la prédiction d'une situation futur ou d'évenements qui va se passer suite aux actions du groupe. Cependant, la prédiction à 1/4 chance d'être fausse. |
| **Craintif/Lâche** | DIS | SAG | VOL | **Petite souris :** peut après une esquive disparaître de la vue des ennemis. |
| **Edgy/Mystérieux** | DIS | CHA | REL | **LES TÉNÈÈÈÈBRES :** Peut faire avant d’avoir l’aggro ou d’avoir subis toute attaque ennemi un coup inesquivable à max dégâts sur un jet de DIS. |
| **Manipulateur** | DIS | REL | FOR |  **Mephisto :** Peut tenter d’obliger à faire une action, quelqu’elle soit, jet de DIS, def en SAG, une fois par combat. |
| **Fouineur** | DIS | INT | CHA | **Retraite stratégique :** Peut sur un jet de DIS devenir coupé du reste du monde. Ne peut plus agir, mais ne peut plus être agis dessus. Le temps passe normalement. |
| **Charmeur** | REL | CHA | INT | **Beauté triomphante :** +15 % en REL/CHA/SPI contre les personnes qui pourraient être attirée par le personnage, -15 % en DIS/SAG/INT |
| **Altruiste** | REL | PER | VOL | **Calme olympien :** Peut calmer n’importe qui par un jet de social. |
| **Vendeur** | REL | INT | HAB | **VTT (vendeur tout terrain) :** Peut (sauf refus spécifique du MJ) lancer dans toute circonstance une phase d’achat/vente, sur jet de REL. |
| **Héroïque** | REL | FOR | INT | **Le discours sur l'espoir et l'amitié :** Peut, une fois par partie, faire un discours à un groupe d'ennemis basiques vaincus (mais sans mort) pour récupérer 1D4 suiveurs parmi le groupe. |
| **Bidouilleur** | PER | HAB | CHA | **MacGiver :** Annule les malus dus à la méconnaissance d’un domaine pour manipuler, bidouiller, bricoler. |
| **Observeur** | PER | SAG | CON | **Analyse améliorée :** peut obtenir toutes les informations d’un ennemi sur un jet de PER. |
| **Traqueur/Chasseur** | PER | FOR | INT | **Vol de kill :** Peut tuer un ennemi au bord de la mort sur un jet d’arme. |
| **Espion** | PER | DIS | FOR | **Anonymat :** Peut pour une partie, forcer une réputation neutre, et ne sera reconnu par personne. Il pert cependant accès aux aventages de la réputation et du prestige. |

</div>