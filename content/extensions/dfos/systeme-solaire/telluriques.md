---
layout: layouts/base.njk
tags: Système Solaire
title: Les planètes telluriques
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les planètes telluriques

Les planètes telluriques sont les planètes de roche situé dans le système solaire intérieur. Si la planète Terre est la seule planète ayant des populations permanentes larges, certaines des autres planètes ont quelques bases et populations.

C'est le coeur du système solaire, et au début la seule qui commencera à avoir de l'exploration. Cependant.

## Mercure

Mercure n'a aucune habitation permanente, mais a eut des usines automatisées, construites globalements par les atlantes pour exploiter les métaux et matières venant de la Mercure. Les machines ont continué à fonctionner automatiquement pendant quelques siècles après la chute de l'Atlantide, mais elles ont depuis cessé de fonctionner.

Quelques robots atlantes y errent encore de nos jours. La réactivation des usines mettra du temps, et lorsqu'elle sera complètement réactivée, les créateurs.

## Vénus

Venus a eut quelques bases à l'époque, ainsi que des machines pouvant permettre de potentiellement un jour débuter une terraformation, mais sont aujourd'hui dégradé au dela de toute possibilité de les utiliser à cause de l'érosion extrème. La planète est complètement non-habité.

Cependant, dans les cieux, au delà de la couche de nuages d'acide sulfuriques subsistent quelques cités flottantes, entièrement automatisées. Cette zone a été colonisée grâce à ces cités, reliées par des ponts servant à faire traverser un réseau de trains automatisés, servant de moyen de locomotion principal, avec des bateaux volants et autres vaisseaux utilisant diverses méthodes pour flotter, ainsi que des ballons dirigeables. La plupart des intérieurs sont étanches et oxygénés, grâce à des grandes réserves construite en pompant et supprimant l'oxygène du CO2 de l'athmosphère. Cependant, ces ruines sont désormais abandonnées.

## Lune

La lune possède surtout quelques bases qui ont été construite par les êtres humains. C'est souvent le spatio-port vers le reste du système solaire. Une petite espèce y vit, les *sélénites*, des sortes de petits lapin anthropomorphiques à antenne.

## Mars

Mars est la planète la plus habitée (à part la Terre), des planètes telluriques. Les lieux vivables sont grandements des cités fortifiées gérée par les espèces locales :
- Les Cités Martiennes sont des citées gérées par les *martiens* (une sous-espèces des Centauriens). Ces cités sont situées surtout proches des pôles, souvent plus ou moins rivales. Ces cités sont grandement fortifiées.
- La Ville Inversées est une grande ville sousterraines sont une ville construit par le premier peuple ayant tenté de coloniser d'autres planètes, les *crétaciens* (une espèces dinosauriennes). Cette ville est constituées de nombreux sous sols sur de nombreux étages. Cette ville à souvent des conflits avec

La surface contient aussi quelques autres ruines de villes construites par les danaïtes, qui étaient capable de conserver une athmosphère dans des grandes plantes magiques. Quelques nouvelles bases commencent à être construites, formée en des écosphères artificielles avec champs magnétique.

Autour de Mars, sur le point de Lagrange L1, se trouve un bouclier magnétique aujourd'hui désactivé (et très certainement demandant énormément de travail pour pouvoir être réparé). Il était supposé créé un bouclier magnétique qui protégerait Mars des vent solaire. C'est un ancien système Crétacien.

## Ceinture d'Astéroïde

La ceintures d'astéroïde est l'ensemble des astéroïdes se situant entre Mars et Jupiter. Elle a été un petit peu exploité pour les minéraux, mais n'est plus très exploitée. Elle sera un peu un repaire de piraterie, mais plus le système solaire sera exploré, plus la piraterie fuira plus vers la ceinture de kuiper.

Des systèmes miniers seront ajouté petit à petit pour exploiter les matériaux situé dans la ceinture d'astéroïdes, en grande partie supervisé par Frontière et des corporations minières spatiales tel que la Compagnie d'Exploitation des Asteroïde.

### Cérès

Cérès est un astéroïde commençant à être habité, en grande partie par une partie des cadres de corporations minière. Les habitants vivent dans des grandes bases avec gravité artificielle, champs magnétique de protection, ressemblant à des gros bâtiments. La vie y est considéré comme assez morne.

C'est également un endroit avec beaucoup de criminalité, lié à la véritable guerre que se cause les différentes corporations minières (qui tentent notamment aussi d'attaquer Frontière, qu'ils voient comme une menace pour leur business), qui parfois s'apparente plus à des mafia qu'à des corporations.