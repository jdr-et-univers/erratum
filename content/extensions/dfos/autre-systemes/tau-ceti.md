---
layout: layouts/base.njk
title: Tau Ceti
tags: Autre systèmes
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Tau Ceti

Tau Ceti est un systeme stellaire situé à 12 années-lumières de la Terre. Tau Ceti est la 22ième étoile la plus proche de notre système solaire, et fait partie des systèmes habités autour du notre. Son étoile principale est une naine jaune comme notre soleil, mais dont la luminosité est à 78% de la luminosité du soleil.

## Lerne

Lerne est une petite planète tellurique, bien trop chaude pour abriter la vie. Elle contient d'anciennes installations, mais personne ne semble y vivre. Dans les légendes dragonnes, cette planète à une mauvaise réputation, serait-ce né d'évenement lors de l'Empire Draconique ?

## Ladon

Ladon est la planète de type super-terre qui est connue surtout pour être la planète d'origine des dragons, wyvern, kobold et fomoires. Cette planète a eut un grand empire jadis, l'Empire Draconique, qui s'est étendu jusqu'à notre système solaire, mais a depuis complètement disparus. Les dragons ont notamment complètement perdu le savoir spatial, qui n'existe plus qu'à travers les Arches, les vaisseaux vivants qui ont disparus en tant qu'espèce, et les dragons stellaires qui peuplent les espaces interplanétaires. On pense que les dragons stellaires sont une évolutions des dragons planétaires pour pouvoir vivre dans le milieu spatial.

Cette planète est donc peuplé de peuples draconiques, avec une immense majorités de dragons. Les dragons de Ladon vivent en clans composé de plusieurs espèces de dragons chacun, et sont plus sociables et nombreux que les dragons terrestres. La planète est divisé en deux continents et un archipel, et de nombreux groupes y vivent.

De l'anomie est présente sur la planète, principalement sur une île isolée, l'île d'A'tolk, lieu de naissance d'un dragon devenu dragon stellaire, et cause de la chute de l'ancienne civilisation draconique, et finalement enfermé dans la planète naine Eris dans le système solaire. Quelques dragons y vouant un culte s'y trouvent.

## Kolchis

Kolchis désigne à la fois une planète et son satellite principal qui n'est entrée que récemment dans la zone habitable de son système solaire, il y a un milliard d'année. Elle a cependant reçu une vie proche de la Terre lorsqu'elle a été habitée par des envoyées de trois des *quatre grandes civilisations* terriennes, notamment avec l'introduction de nombreuses espèces terriennes, ce qui a permis de préserver de nombreuses espèces anciennes (on y trouve des mammouths, des tigres à dents de sabre, etc. La planète est souvent appellé Kolchis Prime, et le satellite Kolchis.

Le satellite est composé de cinq grandes factions, composé de diverses espèces terriennes : 

- Les Royaumes Humains sont un ensemble de royaumes, principautés et républiques dirigées par des humains. On y trouve des humain.es de tout types, éthnies, etc. On  y trouve aussi des centaures, lycions et lycanthropes, qui sont intégré aux royaumes. A noter que tout les royaumes ne sont pas sédentaires, de nombreux groupe de nomades aident souvent les royaumes en tant que mercenaires.
- L'Empire des Morts est une faction de vampires et un empire puissant avec un fort honneur de guerrier, vivant dans le grand nord, dans le froid. Il est la faction la moins nombreuse, mais la plus crainte - parce qu'il est assez simple si on est assez sot pour les attaquer de rejoindre leur rangs... Et mieux vaut le faire en tant que vampire que ghoule.
- La Fédération Danaïte est un ensemble de ruche danaïtes, vivant principalement dans les immenses forêts de Kolchis, protégé par des armées de Némédiens qui forme la majorité de leurs armées. Malgré le nom laissant penser à une unité, il y a beaucoup de conflits internes, voir de guerres entre les différents membres, la fédération s'occupant de jouer les arbitres avec une tendance au laisser-faire.
- Les Hordes Crétaciennes sont des grands groupes de Crétaciens qui vivent de manière nomades. 
- La Cité des Régents, cité phosophorienne, qui contient une petite population de porteurs de lumières.

On y trouve aussi de toutes les autres espèces, la malédictions loup-garou étant même aussi présente ici. Malgré l'aspect plus "médiéval" qu'à ce monde par rapport à notre Terre, les technologies y sont quand même présente et utilisées, et des vaisseaux anciens y sont encore manufacturé et utilisé. A noter que les peuples ne sont pas en guerre les uns contre les autres, mais que les conflits existent entre les différents groupes en faisant partie.

Quelques clans de dragons y sont aussi présents.

Depuis quelque année cependant deux menaces y naissent à travers deux factions visant une conquête totale : 

- L'Inquisition de la Lumière Rouge, un groupe mélangeant religion et monarchie absolu voulant transformer Kolchis en une immense théocratie obéissant à l'ancien dieu ayant supervisé jadis la conquête du satellite, Beltaine, un des fils de Lithe, dieux des humain.
- Le culte d'A'Tolk s'y trouve aussi et cause pas mal de trouble.

## Kadmos

Kadmos est la dernière planète du système de Tau Ceti, et une planète beaucoup trop froide pour avoir de la vie dessus. Cette planète est habité uniquement par un groupe de versatiles, d'ancien phosophoriens devenu des êtres composé de nanomachines. Ils ne vont pas dans l'espace, préférant vivre par eux même.