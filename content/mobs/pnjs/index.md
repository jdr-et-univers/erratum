---
layout: layouts/base.njk
eleventyNavigation:
  key: PNJs
  parent: Créatures et PNJs
  order: 1
title: PNJs
models:
  - PNJs spéciaux
---

# Personnages non-jouables

Un des deux grands types d'être que vous pourrez rencontrer dans un JDR sont les **PNJ** (Personnages Non Jouables). Ils représentent tout les êtres conscient que vous pouvez rencontrer, avec qui vous pourrez discuter ou non mais n'étant pas contrôlé par un autre joueur. Ces personnes ont besoin de différents éléments d'un personnages jouables (statistiques, equipements, pv/pe, voir quelques compétences ou capacités), mais cependant nécessite d'être plus simple pour pouvoir être construit rapidement par le MJ en cas de besoin.

Cette page explique comment sont construit les PNJ (notamment pour les MJ), et certains types de PNJ pouvant s'associer aux PJs.

## Fiche de PNJ

Un PNJ possède une fiche un peu simplifiée par rapport à celle d'un PJ, notamment expurgée de la plupars des éléments relatifs au concept d'expérience.

Un PNJ comme un PJ possède une fiche avec les éléments suivants :

- Les dix *statistiques* de base d'un personnage

- *PV, PM*

- De l'éclat (ainsi que 1 ou 2 points de karma)

- Des *équipements*

- Six capacités

- Des compétences

Cependant, il n'a aucun élément lié à l'expérience sur le PNJ de base, même s'ils peuvent être amélioré au fur et à mesure de l'amélioration de l'équipe en l'ayant rejoint.

## Comportement et attitude

Les PNJ ont un comportement envers les PJ qui peut varier suivant différent facteurs. Le premier est bien évidemment leurs traits de personnalités, qui comme pour les PJ sont la base de construction de leur personnalités. Cependant, d'autres facteurs peuvent entrer en ligne de compte, tel que la réputation générale du groupe, ou toute expérience personnelle que le PNJ a avec le groupe.

### Factions

Les PNJ peuvent appartenir à une ou plusieurs *factions*.

Les factions sont les groupes existant dans le monde du JDR, représentant les différents rapports de forces qui existent entre les personnages. Les factions peuvent apprécier plus ou moins les PJ, ce qui du coup peut influencer la manière dont le PNJ les voit.

Il est à noter qu'un PNJ peut faire partie officiellement d'une faction, mais ne pas s'intéresser à ses affaires.

## Acolytes

Les acolytes sont une sorte de "semi-PJ" lié soit à un PJ en particulier, soit au groupe de PJ. Dans le premier cas, il sera joué par le joueur du PJ, dans le second par le MJ directement.

Ils ont une fiche de PNJ, avec toutes les simplifications habituelles (les six capacités, notamment, qui sont généralement plus unique), et peuvent porter des objets, et même en changer au fur et à mesure de l'aventure. Ils peuvent utiliser l'expérience pour gagner des compétences, les dés et les statistiques, voir pour tenter d'améliorer une de leur capacité, avec accord du MJ.

Les acolytes peuvent participer en combat, et leur nombre pouvant être présent est de `nombre de personnage par joueur en combat + 1` (le nombre de personnage étant celui du joueur ayant le plus de joueur en combat). L'acolyte en question gagne son propre tour, calculé de manière ordinaire, qu'il soit joué par un joueur ou pas le MJ. Ils peuvent tomber KO et mourir comme tout personnage.

## Suiveurs

Les suiveurs sont des PNJ encore plus simplifié (statistiques + PV/PM + des compétences ou capacités) ne se battant pas et servant à accomplir des tâches très spécialisé (par exemple, un voleur engagé pour crocheté des serrures).

S'ils ne combattent pas, certains peuvent avoir un pouvoir s'activant en combat (effectuer un soin tout les trois tour, par exemple). Ils peuvent être assez nombreux dans le groupe, mais seuls un peut être actif lors d'un combat.
