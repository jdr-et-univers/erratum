---
layout: layouts/base.njk
title: Anges
tags: Espèces légendaires
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Anges

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Êtres magiques + autre espèce | Serviteurs des dieux | Aucune (assexués) | Leurs maitres | 100 ans<br /><small>(hors influence divine)</small> |

Les anges sont des êtres créé de toute pièces par les dieux (ou titans). On raconte que leur apparence d'être humains assexués dotés d'ailes de plumes seraient une moquerie envers les anostiens. D'autres légendes raconteraient qu'ils seraient des anostiens "séparé en deux principes différents". Toujours est-il que le lien entre Ange et Anostiens a souvent été fait. Cette espèce assexuée à une espérence de vie de 100 ans hors d'une influence divine et vieilli à un rythme naturel.

Les anges sont des serviteurs naturels des dieux, créé artificiellement par ceux-ci afin de les servir, et pas spécialement pour combattre.

> **Note:** Les informations du tableau ci-dessous sont relatives à l'espèce de base de l'ange.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| -2 | +2 | +2 | +2 | Tir angélique (2, perce-armure) | INT, SAG (+10) | VOL, CHA (-10) | |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Nature angélique | +5 en armure magique naturelle. | 
| Serviteurs | -20 % en volonté face aux dieux. |
| Elements forts | Lumière, Ordre |
| Éléménets faibles | Ombre, Chaos, Éclat |