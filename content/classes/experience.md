---
layout: layouts/base.njk
eleventyNavigation:
  key: L'expérience
  parent: Classes et experiences
  order: 10
---

# L'expérience

Les augmentation de niveau de signe faisait avoir obtenir d'un coup toutes les compétences d'un personnages, mais coutait de plus en plus cher, et par niveau (et non par groupe de niveau). Suite des nombres premiers suivant le niveau de difficulté. Lorsque double-signe, la liste devient.

<div class="table-responsive">

| Premier Signe ou autre type de classe (naturel) | Deuxième signe (apothéose) | Troisième signe (apothéose²) |
|:-----------:|:--:|:--:|
| 1-2-3-5-7-11 | 2-3-5-7-11-13 | 3-5-7-11-13-17 |

</div>

## Augmentations de stats

L'augmentation de stats est limitée par la présence ou non de potentiel, de la manière suivante :

- Sans potentiel, le personnage est limité à 80 dans toute ses stats (sauf si elle arrive naturellement au dessus à la création du personnage)
- Après premier potentiel, le personnage est limité à 120 dans ses stats renforcé par son trait de personnalité activé en potentiel
- Après second potentiel, le personnage est limité à 150 dans toute stat renforcé deux fois par ses deux personnalité activé en potentiel (ça peut être deux fois le même trait de personnalité), ou à 120 dans celles renforcé une fois.

**NOTE :** *En plus de la limite soft de statistique à 80/120/150, un personnage ne peut pas dépasser 255 à une statistique après l'addition de la compétence, et tout les bonus ajoutés au personnage*

| Tranches de stats | Cout en XP |
|:--------------:|:----------:|
| 000 à 100  | 1 Exp → 5% |
| > 100  | 3 Exp → 5% |

## Augmentation des PVPE

L'augmentation de PV/PE se fait de la manière suivante :

| Tranches de PV | Cout en XP |
|:--------------:|:----------:|
| <= 200% | 1 xp -> 3 points à répartir PV/PE | 
| <= 300% | 1 xp -> 2 points à répartir PV/PE |
| >  300% | 1 xp -> 1 points à utiliser PV/PE |

## Augmentation des compétences

| Tranches de Compétence | Cout en XP |
|:--------------:|:----------:|
| +10 et +20 | 1 Exp → 10% |
| +30 et +50 | 2 Exp → 10% |
| +50 | 3 Exp → 10% |

## Classes de départ

Le personnages peut commencer avec *deux classes* de la manière suivante :

- 1 signes + 1 magie ou spécialité
- 2 magies et/ou spécialités

### Gagner de nouvelles classes

Les classes peuvent se gagner de la manière suivante :

- Les signes s'obtiennent par *apothéose* (nécessite que le personnage a du potentiel magique)
- Les spécialité doivent s'apprendre de manière RP
- Les magies "communes" doivent s'apprendre de manière RP
- Les magies "rares" doivent s'apprendre de manière RP (nécessite que le personnage a du potentiel magique) 

Un personnage peut atteindre jusqu'à *quatre classes* et une guilde.
