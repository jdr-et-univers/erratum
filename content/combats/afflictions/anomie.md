---
layout: layouts/base.njk
tags: 
  - Malédictions
  - L'anomie
models:
  - Afflictions communes
  - Malédictions
  - L'anomie
title: La malédiction anomique
parent: Les afflictions
---


# La malédiction anomique

L'anomie est une corruption d'un pouvoir utilisé par l'un des personnages. L'anomie est divisée en deux parties :
- Une stat d'anomie, commençant à 0 et se terminant à 100. A 100, la personne touchée devient une abbération. A partir de 50, il devient une irrégularité, et risque de devoir faire un jet d'anomie lorsqu'il utilise un sort (en comparant un dé à la stat d'anomie).
- Une crise d'anomie, qui si elle se produit, tout jet provoque un jet d'anomie.

## Jet d'anomie

| Résultat | Effet |
|:--------:|:------|
|  1 | Réussite critique obligatoire |
|  2 | Réussite obligatoire (et t'es content, tu regagnes 1 PE) |
|  3 | Tout se passe normalement. |
|  4 | La consommation d'éclat est doublée (avec un minimum de 1). |
|  5 | Le personnage perd 1D12+l'éclat (avec un min de 1) PV. |
|  6 | Tout les alliés perdent 1D12+l'éclat (avec un min de 1) PE. |
|  7 | La technique réussie mais le personnage tombe malade (jet de pestilence) |
|  8 | Tu relances le dé :D |
|  9 | L'action se produit normalement mais se transforme en plante verte après. |
| 10 | L'effet de l'action est inversé |
| 11 | L'attaque touche un autre personnage au hasard (allié comme ennemi) |
| 12 | AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH |
| 13 | Échec automatique |
| 14 | Échec automatique + les alliés perdent tous 1D12PV + l'éclat de la capacité. |
| 15 | Échec critique obligatoire. |
| 16 | Jet de panique au hasard. |
| 17 | Jet de malédiction au hasard. |
| 18 | Les joueurs doivent passer leur fiche au joueur de gauche. |
| 19 | Le personnage tombe dans le coma. |
| 20 | Tout les personnages jettent 1D10 et gagnent ce niveau en anomie. |

