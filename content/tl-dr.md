---
layout: layouts/home.njk
---

# Erratum simplement

Erratum est un système qui vise à être simple à comprendre, mais peut contenir beaucoup d'éléments dedans. Le but de cette page est d'expliquer simplement les bases et les éléments à retenir, et de servir de petit aide-mémoire.

## Les stats

Votre personnage possède : 
- 10 statistiques de base : Force, Consitition, Habilité, Intelligence, Sagesse, Volonté, Charisme, Dissimulation, Relationnel, Perception
- 1 stat de pression, augmentant les échec/réussite critique
- Des point de vie (rendent KO quand à 0, jet de survie à -max) et des point de moral (jet de panique quand à 0)
- Des compétences donnant des bonus lors d'une action

## Faire une action

Chaque action demandent une statistique de base, et potentiellement une compétence. Additionnez les deux pour obtenir votre potentiel de réussite (par exemple, 50 en INT + 30 en lecture ancienne = 80). 

Jetez votre dé 100, et comparez le résultat au tableau suivant :

| Résultat | Effet |
|:--------:|:-----:|
| <= pression | Réussite critique ! |
| <= stat + compétence | Réussite |
| > stat + compétence | Echec |
| > 100 - pression | Echec critique |

Le maitre du jeu peut simuler la difficulté d'une action avec différents malus peuvent arriver sur une action, baissant votre stat et/ou votre compétence, voir la/les divisant par deux.

Pour chaque action, vous avez également des *niveaux de réussite* : faite le nombre de dizaine de votre stat + compétence, et retirez celles du dé. Cela peut servir à comparer deux actions (celle qui a le plus de niveau de réussite gagne), ou a faire des dégats.

## Combats

Les combats contiennent plusieurs types d'actions, dont attaquer et se défendre. En plus de ces actions, vous pouvez utiliser les pouvoirs de votre classe, dans les conditions décrites par votre classe: 
- Un trait est actif de base
- Les autres types de pouvoirs auront souvent besoin de jets et autres éléments pour être efficaces.

### Attaquer

Pour attaquer votre ennemi, vous pouvez avoir deux grandes manières de faire : les attaques physiques et les attaques mentales. Les attaques physiques visent les PV, et sont résisté sur l'armure physique. Les attaques mentales visent les PE et sont résisté sur l'armure mentale. 

Si un personnage utilise deux armes à une main, il peut alors effectuer deux attaques, mais verra ses niveaux de réussites divisé par deux.

| Type d'attaque | Stat utilisée | Dégats sur | Résiste sur | Esquive sur | Armure utilisées | Armes possibles  |
|:--------------:|:-------------:|:----------:|:-----------:|:-----------:|:----------------:|:----------------:|
| Corps-à-corps  | FOR | PV | CON | HAB | Physique | Épées, bâtons, etc |
| Distance | PER | PV | CON | HAB | Physique | Arme à feu, arc, etc |
| Intimidation  | CHA | PE | VOL | DIS | Physique | Quelques armes uniques |
| Manipulation | DIS | PE | VOL | DIS | Physique | Quelques armes uniques |

Des magies peuvent exister également pour toucher l'armure spéciale. Si un personnage peut faire plusieurs attaques en un coups (par exemple avec l'ambidextrie), le personnage fait un jet par attaque.

### Se défendre

Pour se défendre, un personnage à le choix entre l'encaissement et l'esquive. De base, si un personnage ne fait rien ou échoue sa protection, il ne résistera que de son armure.

- L'encaissement utilise un jet de CON (VOL pour les attaque mental, REL pour celles sociales) pour réussir, et permet de réduire les dégats des niveaux de réussites, auquel peut s'ajouter les boucliers.

- L'esquive fait une confrontation d'HAB avec l'attaque ennemie pour éviter entièrement l'attaque.

Si un personnage utilise deux bouclier, il peut cumuler l'effet des deux boucliers.

|  | Réussite critique | Réussite | Échec | Échec (critique) |
|:-|:--------------------|:---------|:------|:-----------------|
| **Encaissement** | Ajoute bouclier + niveaux de réussites à l'armure (doublé) | Ajoute bouclier + niveaux de réussites à l'armure | Se prend l’attaque. | Se prend l’attaque (doublée) |
| **Esquive** | Évite totalement l’attaque + a le droit à une attaque d’OP | Évite totalement l’attaque. | Se prend l’attaque. | Se prend l’attaque (doublée) |

Les personnages ont trois types d'armure : l'armure physique (qui permet d'encaisser les attaques physiques), l'armure magique (qui permet d'encaisser les attaques magiques) et l'armure mentale (qui permet d'encaisser les attaques mentales).

### Soins

Lorsqu’un allié s’est prit des dégâts, il est possible de vouloir le soigner. Lorsqu’on tente de soigner quelqu’un, un jet de soin doit alors être fait. Pour pouvoir soigner, il faut avoir une capacité qui offre la possibilité de soigner.

Le soin peut se faire généralement sur l’INT ou l’HAB (voir SAG dans certaine condition). Des objets peuvent améliorer le soin, mais l’utilisateur à un niveau de soin général de 1.

| Réussite critique | Réussite | Échec | Échec critique |
|:-----------------:|:--------:|:-----:|:--------------:|
| Soigne le niveau de réussite ×2 + potentiel équipement de soin. | Soigne le niveau de réussite + potentiel équipement de soin. | Bah t’as pas soigné. C’est ballot. | Retire le niveau d’échec à la personne visée. |

## Fouiller / Looter

Le jet de fouille est un jet qui se fait avec un jet d'un D100. Les différents objets trouvables sont divisés en catégories (commun, semi-rare, rare, exceptionnel), dont les fourchette peuvent varier suivant les lieux. Certains objets peuvent être présents en quantité limitée. Faire un échec critique lors d'un jet de fouille produit en plus un effet négatif au choix du MJ en plus du résultat de la case.

Il est possible, dans certains lieu, de chercher de l'argent au lieu de chercher des objets. Chaque case du tableau doit alors avoir un équivalent financier.

Un exemple de tableau de loot (avec les valeurs par défaut en règle générale) serait le suivant :

| Catégorie | Dés | Loot |
|:---------:|:---:|:----:|
| Rien | 100-91 |   |
| Commun | 90-56 | Graines de feu, vivre, 50$ |
| Semi-rare | 55-31 | potion de soin, parchemin de protection, 150$ |
| Rare | 30-11 | Hache, Armure, 300$ |
| Exceptionnel | 10-1 | Épée vorpale de feu (2), Bouclier de glace (1), 700$ |

Suivant les lieux, les fouilles fonctionnent différemment, comme résumé par ce tableau :

| Lieu | Fonctionnement de la fouille |
|:----:|:-----------------------------|
| Pièce | Un jet de fouille par joueur. Introduit dans l'ordre prévu par le MJ les objets présents dans la pièce puis produit le jet de fouille prévu sur le lieu entier. |
| Réserve (pièce) | Un jet de fouille par personnage, les jet de fouilles sont + spécialisés mais ont plus de contenu |
| Places | Un jet de fouille par personnage, produit le jet de fouille prévu sur la place. |
| Terres (dans des grands espaces) | Un jet de fouille par personnage. Chaque résultat contient également un jet de quantité. La présence de joueurs qui fouillent produit un jet d'évenement aléatoire |