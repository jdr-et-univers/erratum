---
layout: layouts/home.njk
eleventyNavigation:
  key: Créatures et PNJs
  order: 5
---

# Créatures et PNJs

Les créatures et PNJs sont les différents êtres que vous pourrez rencontrer dans une partie hors des joueurs. Ils sont généralement joué par le MJ, mais celui-ci peut parfaitement déléguer certains liés à un PJ au joueur du PJ en question, ce qui permet une plus grande diversité des situations.

## Pages dans cette catégories

- [Créatures](creatures/) - Présentation des créatures et de leurs concepts
- [Vertus](vertus/) - Présentation des septs légendaires vertues, des PNJs spéciaux de l'univers d'Erratum