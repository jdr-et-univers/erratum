---
layout: layouts/base.njk
tags: 
  - Les éléments
  - L'anomie
models:
  - Les éléments
  - L'anomie
title: Éléments paradoxaux
parent: Les éléments
---

# Elements paradoxaux

Les éléments paradoxaux sont les éléments existant à une échelle supérieure à celle des mondes. C'est le nom donné aux deux formes du paradoxe, la force née des contradictions même de l'univers qui rend les magiques et la métaphysique possible, l'éclat et le paradoxe pur.

| Éclat | Paradoxe |
|:-----:|:------:|
| La forme énergique du paradoxe. Une énergie volatile mais puissante, qui peut changer la réalité même. Il porte des noms différents dans de multiple univers, et n'existe à plus ou moins grande dose suivant l'imperfection de l'univers | La forme la plus pur du paradoxe. Plus qu'une énergie ou une entité, il s'agit d'un état de réalité, se produisant quand deux états contradictoire du monde existent. |

## Effets

| Élément | Effet |
|:-------:|:------|
| **Éclat** | Aucun effet particulier hors résistance élémentaire. |
| **Paradoxe** | Le jet à deux effets secondaires d'élément parmi tout ceux disponible sur cette page hors éclat (1D20). |
| **Anomie** | +1D10 point d'anomie à la personne touchée. |

## Terrains élémentaires

| Élément | Terrain | Effet |
|:-------:|:-------:|:------|
| **Éclat** | *Cieux radiants* | Toute la magie n'utilise que l'éclat environnant. Cependant, perte de 25% de PV et PE par tour. |
| **Paradoxe** | *Cieux paradoxaux* | Les effets de deux autres terrains présent sur cette page hors éclat (1D20) se produisent au hasard. |
| **Anomie** | *Cieux corrompus* | Tout jet de magie double sa puissance, mais à 1/3 chance d'être anomique |
