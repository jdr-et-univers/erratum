---
layout: layouts/home.njk
eleventyNavigation:
  key: Création
  parent: Personnages
  order: 0
---

# Création d'un personnages

La première chose à faire dans un JDR, est de créer un personnage. Erratum à pour objectif d'offrir un système simple, fondé sur la customisation et la posibilité de facilement créer et faire évoluer un personnage dans le sens qui nous intéresse. De ce fait, le but est de ne pas mettre trop d'entrave à la customisation, en limitant le moins possible les personnages.

Plusieurs éléments influencent sur la création du personnage et nous allons indiquer ici les différentes étape de création du personnage. Les éléments tels que le genre n'affectent pas les statistiques ou pouvoir des personnages.

## Principe généraux

> **Note :**
> Si le personnage est créé en cours de partie (gain d'un personnage supplémentaire), il est juste de lui donner l'equivalent de l'Exp minimum acquise par un personnage de la partie, ainsi que le rang minimum.

La constitution d'un personnage se fait en faisant les étapes suivantes

- **L'espèce** : Choix d'une espèce, qui offrira les bases des stats et autres pouvoirs spéciaux.
- **Les traits de personnalités** : Choix de deux traits de personnalités.
- **Le choix des classes** : Une ou deux classes à choisir (un signe max)
- **Traits unique** : Chaque personnage reçoit deux traits unique.
- **Répartition des compétences** : 5 compétence à 0 offertes de base et 50 point de compétence à dépenser dans diverses compétences (limité à 20% par compétence, sauf accord du MJ).
- **XP Final** : 5 point d'Exp qu'il peut dépenser comme bon lui semble.

## Le choix de l'espèce

La première étape est le choix d'une espèce. L'espèce donne des statistiques de départ, mais pas véritablement de limite sur ce qu'il est possible de faire à partir de l'espèce, d'autres éléments affectant également les statistiques de départ, et leur permettant d'évoluer.

Dans Erratum, l'espèce ne limite pas jusqu'où les stats peuvent changer. Le jeu étant joué par des êtres humains, généralement les JDR choisiront de faire de l'espèce humaine l'espèce la plus "moyenne", mais ce n'est pas forcément le cas.

Une espèce "moyenne" posséderait les caractéristiques suivantes :

- 50 à toutes ces stats de base (500 au total)

- 14 PV et PE (28 au total)

- 10 Éclats (peut d'espèce peuvent changer l'éclat)

Le but des différentes espèces est d'être équilibrée en terme de "total" entre les stats, la vitalité et l'éclat, avec un rapport stats/xp avec 5% dans les stats (ce qui coute 1xp de base) est l'équivalent de 3 PV/PE (ce qui coute également 1XP).

Les statistiques supérieur à la moyenne dans l'espèce n'ont ni d'effet sur les "talents" accessible, ni d'effet sur les limite qu'auront les stats ensuite. Tout est généralement affecté par la seconde étape…

## Les principes de personnalités

( voir section *Personnalités et potentiels*)

Chaque personnage doit choisir deux principes de personnalités, qui définiront son comportement dans les grandes lignes. Au total, ces deux principes retireront 20 point de statistique, et en rajouteront 60. Cela fait qu'après choix de la personnalité du personnage, le personnage aura 540 points de statistiques en tout. Si les deux traits de personnalités amènent une statistiques à dépasser les 80, le personnage en profitera (mais évidemment ne pourra pas l'améliorer jusqu'à 120 avant d'avoir activé son potentiel).

Les quatres statistiques améliorées par les principes de personnalités (nommés "boost") pourront également permettre au personnage de récupérer avec l'expérience des *talents*.

### Jet de personnalité

Si une personnalité doit être choisi au hasard, alors le jet suivant est fait :

1D10 pour choisir la catégorie (1 = FOR, 2 = CON… 10 = PER) et 1D4 permet de choisir quel personnalité de la catégorie est choisie.

## Les traits uniques

Lors de la création du personnage, on peut leur attribuer deux traits passif unique, suivant leur histoire. Les deux traits doivent avoir un effet global "neutres" : soit en étant un négatif et un positif, soit en étant deux ambivalent. Cela dit, si un joueur tiens à se retrouver avec deux traits négatifs, je suppose que c'est son choix ?

Des exemples de traits peuvent être des choses qui les empêches de faire certaines actions, qui affectent les ennemis, etc. 

### Exemple de traits

| Trait | Effet |
|:-----:|:------|
| La flemme | -20% à toute ses stats tant que n'a pas été directement impacté par les évenements de la parties |
| Fierté | Soigne 1D8 PE si un personnage réussi une action du à un bonus fourni par le joueur |
| Berk | -20 % en VOL contre les trucs dégueulasses |
| Sprint sans échauffement | Permet de remplacer un jet d'HAB par un jet de FOR, mais se prendra 0.5x les niveaux de réussite (ou d'échec) en dégâts. |

## Boosts finaux

> **Note:** Si le personnage est créé en cours de partie (gain d'un personnage supplémentaire), il est juste de lui donner l'equivalent de l'XP minimum acquise par un personnage de la partie, ainsi que le rang minimum.

Pour terminer la customisation, chaque joueur reçoit les particularités suivantes :
- 50 point de compétence à dépenser dans diverses compétences (limité à 20% par compétence, sauf accord du MJ).
- 5 points d'expérience à repératir comme bon lui semble
