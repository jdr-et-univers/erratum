---
layout: layouts/base.njk
eleventyNavigation:
  key: Les objets
  parent: Inventaire
  order: 2
title: Les objets
models:
  - Objets consommables
  - Objets réutilisables
---

# Les objets

Les objets peuvent être stocké dans l'inventaire de votre personnage et utilisé quand le besoin s'en fait sentir. Certains objets sont consommable, d'autres sont réutilisable, et divers effets peuvent exister pouvant être utile.

## Objets consommables

- Les [objets communs](communs/) sont des objets consommables utiles pour tout aventurier (nourriture, objets pour attaquer, utilitaire, etc).
- Les [potions](potions/) sont des concoctions magiques ayant des effets divers plus ou moins désirables.
- Les [objets magiques](magiques/) sont des objets infusés de magie, pouvant avoir de nombreux effets.

## Objets réutilisables

Certains objets sont réutilisables, et ne seront pas consommer par l'action.

- Les [objets utilitaires](utilitaire/) sont les différents objets communs pouvant vous êtres utiles durant les parties.
- Les [écritures magiques](ecritures/) permettent de construire des sorts grâce à l'intérprétation et leur combinaison.