---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Le marchand
parent: Les spécialités
---

# Le marchand

Le marchand est un spécialité de la négoce et des effets de prix. Entrepreneur ? Négociant ? Juste un riche qui aime bien l'argent ? Cette classe est pour votre personnage. Ses deux spécialités sont *l'utilisation des objets* et les *interactions avec les PNJ*.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Œil de Lynx (trait) | Peut gagner 1D12+8 PO par ennemie tombé, dialogue important avec un PNJ, ou énigme réussie |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Marchandage (trait) | REL |  | Achète 25% moins cher les objets et les revend 50% plus cher |
| 1 | Mon bon ami ! | REL |  | Peut obtenir des informations sur les compétences potentielles de certains PNJ. L'allié devient un "bon ami" du marchand. |
| 2 | Lot de consolation |  |  | Sauf échec critique, ne peut pas ne rien trouver en fouillant. |
| 2 | Adepte des objets | HAB |  | Peut ajouter la moitié de ses niveaux de réussite à la puissance d'un objet |
| 2 | Le flair des affaires |  |  | Peut voir ce que des PNJs *bon ami* pourraient avoir à vendre |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture focus |  |  | Permet de passer en posture focus |
| 3 | Porte-Bonheur |  |  | Tout les personnages gagnent +20% d'or lors des fouilles |
| 3 | Au secours! | REL |  | Demande à un allier de le protéger pendant 3 tours en se prenant les attaques à sa place. |
| 4 | Embauche | REL+INT |  | Peut embaucher un PNJs non-clef qu'il a en *bon ami* pour l'avoir comme *suiveur* et bénéficier de son talent. Plus il est fort, plus il est cher. |
| 4 | Bling-Bling |  |  | Peut avoir un accessoire en plus |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Partons du bon pied |  |  | Peut relancer jusqu'à deux jet de rencontre aléatoire par partie |
| 5 | Maitre des objets | HAB |  | Peut ajouter la totalité de ses niveaux de réussite à la puissance d'un objet |
| 5 | Muscle à louer |  |  | Peut faire agir à sa place son employé durant un tour, pour 200 Po. |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Muscle à temps pleins |  |  | Peut, pour un prix plus fort, faire que son employé compte comme un PJ supplémentaire le temps de l'emploi. |
| 6 | Livraison en temps de crise |  |  | Peut acheter à deux fois son prix un objet en plein combat, hors objets rares. |
