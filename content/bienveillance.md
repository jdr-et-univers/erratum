---
layout: layouts/home.njk
eleventyNavigation:
  key: Bienveillance
  parent: Système de jeu
  order: 5
---

# Bienveillance

Dans un JDR, vos joueur⋅euses mettront parfois beaucoup d'elleux-mêmes, de leur vécu, ou au contraire auront besoin de s'éloigner de celui-ci. Avoir une bienveillance envers les joueur⋅euses est important pour tout MJ, ainsi qu'avoir une bienveillance envers soi-même ! Bien utilisé, le JDR est un formidable outil pour gagner en confiance en soi, à la fois en tant que MJ et que joueur⋅euse.

La règle numéro 1 est que vos joueurs sont des personnes, et que **votre groupe est plus important que le jeu**.

Ce petit document a pour but de donner quelques éléments pouvant être utiles afin de rendre une partie de Pélican plus fun pour tout le monde.

## Émotions et sujets difficiles

Un JDR jouant toujours un peu sur les émotions de vos joueur⋅euses, en tant que MJ il y a toujours un risque, notamment si vous voulez parler de thématiques sérieuses dans votre JDR.  Faites attention à bien connaitre les thématiques dont vous parlez, notamment avoir l'avis de concernés sur le sujet, si vous ne l'êtes pas vous-même. De même, faites attention à celles que vos joueur⋅euses pourraient aborder. Pour le handicap, notamment si vous voulez que cela affecte le gameplay, voyez avec un⋅e concernés si vous ne l'êtes pas, ni votre joueur⋅euse (dans ce dernier cas, demandez son avis sur son personnage).

Vos joueur⋅euses auront forcément des sujets qui leurs sont difficiles. Essayez au possible d'être au courrant. Même si vous connaissez vos joueurs, *soyez au courant*, faites attention à ce qui peut leur poser soucis. Si vous ne les connaissez pas, n'hésitez pas à prendre connaissance de points qui pourraient être dur pour elleux, et adapter le JDR en conséquence.

De plus vous pouvez mettre en oeuvre des outils (tels que la x-card décrites plus bas) afin de vous assurez de pouvoir éviter de ressortir des choses difficiles à un joueur.

## Gestion de l'échec

Le JDR permet d'expérimenter, de tenter des trucs, sans avoir de conséquence sur sa vrai vie. Cependant, faites attention à bien gérer l'échec de manière pertinente. "Perdre une campagne" de JDR est frustrant, à la fois pour les joueur⋅euses et vous, parce que vous perdez du coup tout le travail que vous avez mis dans cette campagne, et vos joueur⋅euse tout l'investissement (émotionnel et temporel) qu'iels ont mis. Gérer la difficulté doit alors être fait de manière pertinente pour votre groupe ET pour le type de jeu qu'iels veulent.

Soyez sûr de mettre des échappatoires, ou juste des branchements, quand c'est pertinent. Un groupe d'ennemis qui wipe l'équipe, ça peut être l'occasion de les faire se réveiller dans une prison, et une partie évasion, par exemple. Cela rendra en plus le jeu plus organique, plus intéressant, même pour vous ! *Tout le monde y gagne*.

De plus, évitez de rendre invisible une issue frustrante pour un personnage, du genre le jet de survie au milieu d'une salle parce que le salon de l'archiduchesse de MachinTruc contenait un poison invisible. Cela ne veut pas dire de ne pas mettre de piège, plus qu'il faut avoir une pertinence entre les actions et leur conséquences, ou en tout cas une pertience dans le type de jeu convenu avec vos joueurs. L'idée n'est pas de dire que "punir" est une mauvaise idée, juste que vous devez faire attention au groupe que vous avez face à vous, et voir si c'est adapté à ce groupe.

De plus, éviter de créer des blocages plus tard. Par exemple, assurez vous de ne pas laisser perdre à jamais des indices importants qui bloqueraient vos joueur⋅euses plusieurs parties plus tard.

## No-kill rule

En combinant les deux éléments précédant, vous pouvez ajouter facilement une no-kill rule à Pélican si le besoin s'en fait sentir pour votre campagne.

La règle devient simple : les personnages tombant en dessous des 0 PV restent KO, et ni de coma ni de mort sont possibles. Cette règle peut sécuriser les joueur⋅euses si besoin, évitant le risque de mettre dans une situation problématique un⋅e de vos joueur⋅euse si le sujet est difficile pour ellui, et une possible frustration en cas de gros critiques.

## X-Card

La X-Card est un [outil théorisé par John Stavropoulos](https://docs.google.com/document/d/1SB0jsx34bWHZWbnNIVVuMjhDkrdFGo1_hSC2BWPlI3A/edit#!) sous licence CC BY-SA 3.0, une simple carte avec un X dessus pouvant permettre à toute personne du jeu (vous compris !) de sortir de toute situation avec laquelle les joueur⋅euses se sentiraient mal, **sans avoir d'explication à donner**. Lorsque la carte est posée, changez de sujet pour ramener ça à quelque chose d'autre.

L'X-Card est un outil simple pour réparer les problèmes alors qu'ils arrivent. En effet, puisque comme les JDRs sont souvent en improvisation nous ne savons pas ce qui va arriver avant que cela n'arrive, le risque existe toujours que le jeu aille dans une direction que vos joueur⋅euses ne voudraient pas.

Pour l'utiliser, vous pouvez présenter la X-Card de la manière suivante (traduit du document originel) :

> J'aimerais votre aide, pour rendre le jeu plus fun pour tout le monde. Si quoi que ce soit vous met mal à l'aise de toute manière que ce soit [dessine un X sur une carte], juste levez cette carte, ou tapoter là. [place la carte au centre de la table]. Vous n'avez pas besoin de vous expliquer. Le pourquoi n'est pas important. Quand nous levons ou tapons cette carte, nous retirons ce qui a été X-Cardé. Et s'il y a un souci, n'importe qui peut demander une pause et on peut en parler en privé. Je sais que cela peut sembler étrange, mais cela nous permettra de jouer des jeux géniaux tous ensemble, et généralement je suis celui qui utilise la x-card pour prendre soin de moi-même. S'il vous plait, n'hésitez pas à aider à rendre ce jeu fun pour tout le monde. Merci à tou⋅te⋅s !

### Quelques notes sur la X-Card

Le document contient aussi quelques notes sur la X-Card, retranscrites et traduites ici :

- Le speech d'introduction est parfois plus important que la carte elle-même. Il rend clair qu'on est tous dans ce jeu ensemble, et que l'on va s'aider, et que **le groupe qui joue est plus important que le jeu**.

- Utilisez la carte tôt, voir même sur vous-même, pour mener par l'exemple, et modéliser le comportement.

- La X-Card n'a pas besoin d'être un outil de dernier recours. Le moins ça parait "spécial", le plus vous l'utilisez, le plus il y a de chance que quelqu'un l'utilise quand cela devient urgent.

- La X-Card ne remplace pas les conversations ! Si vous préférez parler d'un sujet plutôt que d'utiliser la X-Card, faisez-le. Ce n'est pas parce que la X-Card est disponible qu'elle doit être utilisée. Mais quand elle est utilisée, respectez la personne qui l'utilise et ne demandez pas pourquoi ni ne lancez de conversation à propos du souci.

- La X-Card est un filet de sécurité, mais tout le monde ne se sentira pas à l'aise à l'utiliser. Si un⋅e joueur⋅euse à un souci avec le jeu et veut vous en parlez, **écoutez**. Ce n'est PAS okay de dire "mais tu n'as pas utilisé la X-Card" comme une défense. N'utilisez pas la X-Card comme attaque envers vos joueurs. Ecoutez et parlez.

- Si vous n'êtes pas sûr de ce qui s'est fait X-Cardé, demandez une pause avec la personne en privée.

- N'utilisez pas la X-Card comme excuse pour pousser les limites. Ce n'est pas un safeword.

- Certain⋅e⋅s MJs (généralement n'ayant pas utilisé la X-Card) ont peur que la X-Card va limiter leur créativité. D'autres MJ qui l'ont utilisé pense le contraire. Puisque læ MJ n'a pas à être télépathe, cela libère son énergie pour se concentrer sur d'autres aspects de la masterisation.

- La X-Card n'est pas une excuse pour tenter de revenir au jeu aussi vite que possible. Les gens sont plus importants que le jeu. S'il y a besoin de prendre une pause, prenez une pause.

- La X-Card ne sert pas forcément que pour les triggers et les PTSDs, et peut-être dans des situations qui n'ont rien à voir !
