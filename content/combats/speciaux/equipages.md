---
layout: layouts/base.njk
tags: Combats spéciaux
models:
  - Combats spéciaux
title: Combats d'équipages
parent: Organisation
---

# Les combats d'équipages

Les combats d'équipages sont des combats utilisant des équipages d'un bateau, d'un vaisseau, ou de tout véhicules plus grand que la moyenne. Chaque personnages peut avoir un rôle différent suivant les situations.

Chaque rôle demande des compétences spécifiques utiles.

## Orgnaisation du combat

Comme pour un combat normal, chaque personnage fait *jet d'initiative* pour connaître sa place dans le combat, en utilisant sa statistique d'HAB.

## Les rôles d'équipages de base

| Rôle | Compétences | Effets |
|:----:|:-----------:|:-------|
| Capitaine | Stratégie, Commandement | Permet d'ajouter des bonus à une action aux autres personnages s'ils obéissent aux ordres. |
| Réparateur | Mécanique | Permet de réparer le véhicule les niveaux de réussite |
| Pilote | Pilotage | Permet de produire des esquives, et/ou de définir la direction du véhicule |
| Armes | (dépend de l'arme) | Fait un tir avec les armes (dégats dépendent de l'arme) |
