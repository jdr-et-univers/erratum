---
layout: layouts/base.njk
title: L’herboristerie
---

# L’herboristerie

> **Note:** Cette page est pour la nouvelle version de l'herboristerie. Cliquez ici pour accéder à [l'ancienne](/classes/magies/herboristerie-old/).

L'herboristerie est sans doute la magie la moins magique. Elle tire son pouvoir de plantes particulières, les plantes élémentaires. Ces plantes sont alors mélangées pour créer des effets spéciaux dans des potions.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Préparation de potion  |  Peut créer des potions à deux ingrédients, prenant un tour mais produisant une potion. De base, peut pour un éclat faire une potion de soin légère qui ramène 6 PV.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Herbier | Si possède un exemplaire d'un élément, peut le cibler pour ne trouver quasiment que ça (ingrédient commun uniquement) |
| 1 | J'aime les plantes | Gagne 1D4 plantes d'un type pour chaque ennemi vaincu par le groupe |
| 2 | Potion connue (3 éclat) | Peut apprendre une potion, qu'il pourra reproduire instannément sans risque de l'échouer |
| 2 | Potion connue (3 éclat) | Peut apprendre une potion, qu'il pourra reproduire instannément sans risque de l'échouer |
| 2 | Potion++ | Peut rajouter un troisième ingrédient au potion |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 | Cataplasme | Peut transformer une potion en effet passif |
| 3 | Potion fétiche (3 éclat) | Peut apprendre une potion, qu'il pourra reproduire instannément sans risque de l'échouer avec jusqu'à un ingrédient en moins |
| 3 | Analyse de potion | Peut décomposer en recette une potion trouvée |
| 4 | Les coins de l'herboriste | Gagne x2 plantes lors des fouilles |
| 4 | Potion encore plus ++ | Peut utiliser jusqu'à cinq ingrédient dans une potion |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 | Conversion éclat/matière (2 éclat) | Peut retirer un ingrédient à une potion déjà connue |
| 5 | Potion fétiche (3 éclat) | Peut apprendre une potion, qu'il pourra reproduire instannément sans risque de l'échouer avec jusqu'à un ingrédient en moins |
| 5 | Promotion | Le créateur de potion peut créer sans prendre son tour une potion tous les trois tours (mais nécessite les ingrédient) |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 | Potion extrême | Peut employer des ingrédient métaphysiques dans les potions |
| 6 | Établi de potion (1/partie) | Pendant cinq tours, passe en mode potion. Chaque potion sera faite en double exemplaire sans surcout de matière, mais ne peut RIEN faire d’autres |
