---
layout: layouts/base.njk
tags: Signes de Malice
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Poisson
parent: Les signes
---

# Poisson (les illusions)

Les poissons tirent leur pouvoir des apparences, des mystères et du spectacle. Ils peuvent être n'importe qui, n'importe quoi. Aucune personnalité ne défini le poisson, aucune règle. Après tout, ce que vous voyez n'est-il pas qu'une illusion ?

Leur pouvoir est celui des illusions. Ils se protègent en n'étant pas présent là où vous attaquez. Ils vous font voir des choses fausses... Mais est-ce forcément offensif ? Ne peuvent-ils pas aussi être les aêdes dont les récit peuples votre imaginaire ?

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :pisces: | Singe (asie), Seth (égypte) | Malice et Protection (Trickster Tank) | Chaos | Rare (1.9%) | DIS | Pourpre |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Maître des illusions  |  À un bonus de 3 point de dégâts mentaux de base.  |
| Double |  Illusion permanente  |  À un bonus de 5 point de dégâts mentaux de base.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Pas là (3 Eclat)  |  Peut aider un personnage, quel qu’il soit (lui compris) à esquiver. Au lieu d’une confrontation entre la statistique d’attaque de l’attaquant et l’HAB de la cible, ce sera une confrontation entre la statistique d’attaque de l’attaquant et la DIS du poisson. Si échec de la confrontation, la cible se prend l’attaque.  |
| 1 |  Aura de désespoir  |  Créé une aura qui baisse de 15 % la résistance aux afflictions mentales chez les ennemis pendant 3 tours (ne peut pas être placé s’il y a déjà une aura) (II: 5 tours).  |
| 2 |  Aura d’espoir  |  Créé une aura qui augmente de 15 % la résistance aux afflictions mentales chez les alliés pendant 3 tours (ne peut pas être placé s’il y a déjà une aura).  |
| 2 |  Mais c’est intéressant par là ! (DIS, 2 Eclat)  |  Peut utiliser le jet de loot d’un autre joueur pour fouiller.  |
| 2 |  Effet Nocébo (3 Eclats)  |  Peut faire une attaque mentale, résistant sur l’armure mentale, mais qui touchera les PV.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture focus  |  Peut passer en posture focus (II : posture focus +)  |
| 3 |  Changement d’apparence (6 Eclat)  |  Peut changer d’apparence et prendre n’importe quelle apparence, tant que les personnes autour peuvent croire que c’est legit.  |
| 3 |  Fausse attaque (2 Eclats)  |  Peut faire semblant d’attaquer un personnage joueur ou non-joueur pour ensuite attaquer un autre (lui retirant son esquive). Le personnage attaqué, s’il est allié, fait un jet de SAG pour voir sa réaction.  |
| 4 |  Dédoublement  |  Crée un clone de lui-même. Les clones voient leur statistique divisés par deux pour chaque nouveau clone. (II : Le premier clone n’a pas ses statistiques divisés par 2)  |
| 4 |  Oups (DIS, 1/combat)  |  Peut rediriger une attaque ciblée vers une autre cible.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Horreur-terreurs  |  Pour chaque attaque (mentale ou physique), si le coup est critique, l’ennemi fait un jet de panique.  |
| 5 |  Forteresse Spirituelle  |  Si n’attaque pas, encaisse la moitié (II: 3/4) des dégâts d’une attaque sur l’esprit.  |
| 5 |  Cape de l’ombre (2 tour de cooldown)  |  Peut perdre toute l’agro en passant une action de son tour. (II : ne passe plus l’action du tour)  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Inversion de critique  |  Le premier échec critique devient une réussite critique.  |
| 6 |  Mirage  |  Peut dépenser 4 éclats pour ne pas subir les effets d’une action, quelle qu’elle soit.  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Bulle de réalité  |  Peut créer un espace « virtuel » dans lequel plusieurs personnages peuvent se trouver. Le terrain et les conditions sont créés par le personnage, validé par le MJ. Une fois les conditions crée, le joueur n’a plus de maîtrise dessus (à par expulser les personnages).  |
| Herault |  Tout ceci n’était qu’un rêve (1 karma)  |  Peut créer une séquence d’action en tant que simulation psychique dans la tête des personnages autour. Le choix s’il s’en souvient après où non est au poisson.  |