const sidebar = document.getElementById('sidebar');
const sommaire = document.getElementById('more-info');
const main = document.getElementById('skip');
const btn_menu = document.getElementById('menu-button');
const btn_sommaire = document.getElementById('sommaire-button');

btn_menu.addEventListener('click', function () {
    if (sidebar.classList.contains('visible')) {
        sidebar.classList.remove('visible');
        sommaire.classList.remove('visible');
        main.classList.add('visible');
        btn_menu.classList.remove('active');
        btn_sommaire.classList.remove('active');
    } else {
        sidebar.classList.add('visible');
        sommaire.classList.remove('visible');
        main.classList.remove('visible');
        btn_menu.classList.add('active');
        btn_sommaire.classList.remove('active');
    }
});

btn_sommaire.addEventListener('click', function () {
    
    if (sommaire.classList.contains('visible')) {
        sidebar.classList.remove('visible');
        sommaire.classList.remove('visible');
        main.classList.add('visible');
        btn_menu.classList.remove('active');
        btn_sommaire.classList.remove('active');
    } else {
        sidebar.classList.remove('visible');
        sommaire.classList.add('visible');
        main.classList.remove('visible');
        btn_menu.classList.remove('active');
        btn_sommaire.classList.add('active');
    }
});

document.querySelectorAll('#sommaire a').forEach(function (link) {
    link.addEventListener('click', function() {
        sidebar.classList.remove('visible');
        sommaire.classList.remove('visible');
        main.classList.add('visible');
        btn_menu.classList.remove('active');
        btn_sommaire.classList.remove('active');
    });
}); 