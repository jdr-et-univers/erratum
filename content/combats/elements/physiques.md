---
layout: layouts/base.njk
tags: Les éléments
models:
  - Les éléments
title: Éléments physiques
parent: Les éléments
---

# Éléments physiques

Les éléments physiques représentes les éléments les plus classiques et courants, présents partout de manière "normale" dans un monde.

|             |         |           |
|:-----------:|:-------:|:---------:|
| **Végétal** | **Eau** | **Glace** |
| **Terre**   |         | **Air**   |
| **Métal**   | **Feu** | **Foudre**|

## Effets

Ils peuvent avoir les effets suivants en combat :

| Élément | Effet |
|:-------:|:------|
| **Terre** | Brise armure : tous les 6 points de dégâts effectué, ajoute un malus en armure jusqu’à réparation. |
| **Végétal** | Ajoute un effet de vampirisme physique (50 % des dégâts effectués régénéré en tant que PV) à l’attaque |
| **Eau** | Maladie, le personnage à la moitié des niveaux de réussite en moins pour agir au tour suivant. |
| **Glace** | 30 % de chance de provoquer la paralysie (personnage doit faire un jet de CON pour attaquer) |
| **Air** | -50 % pour se défendre face à l’attaque |
| **Foudre** | 50 % de chance de provoquer un effet de « stunt » sur l’ennemi. |
| **Feu** | 60 % de chance de produire un effet « brulure ». Si critique, immolation (provoque des dégats continues à chaque tour).
| **Metal** | 50 % des dégats sont perce-armure (en plus de ceux potentiellement déjà p-a) |

## Terrains élémentaires

| Élément | Terrain | Effet |
|:-------:|:-------:|:------|
| **Terre** | *Tempête de sable* | 1D4 dégâts chaque tour. |
| **Végétal** | *Phytoradiation* | Les personnages regagnent tous 1D4 PV par tour. |
| **Eau** | *Brume* | Les personnages non-aveugle ont 1D4 chance d’échouer leur jet d’action. |
| **Glace** | *Verglas* | Sur chaque déplacement, le personnage fait un jet d’HAB pour pas tomber. |
| **Air** | *Tornade* | Chaque attaque à distance devient difficile (-50 %) |
| **Foudre** | *Orage* | Chaque tour, un personnage présent sur le terrain au hasard se prend 1D10 dégat. |
| **Feu** | *Sol ardent (floor is lava)* | Chaque personnage sur le sol jette 1D4, si échoue, effet brulure immédiat. |
| **Métal** | *Picots* | À chaque déplacement au sol ou arrivé sur le terrain, 1D8 dégat. |