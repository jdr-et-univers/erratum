---
layout: layouts/base.njk
title: Les alchimistes
tags: Guildes métaphysiques
models:
  - Guildes
  - Guildes métaphysiques
parent: Les guildes
---

# Les Alchimistes

L'alchimie est une pratique ancestrale, s'étant développé sous des noms différents dans de nombreuses contrées du monde. Si l'alchimie est souvent présentée sous deux angles, la création de la Pierre Philosophale et la transmutation du plomb en or, cette discipline est en fait plus complexe.

L'alchimie n'est pas en soi une *magie*, mais une meta-magie. L'alchimie est le travail des pratiques magiques à l'aune des connaissance de la métaphysique appliquée, c'est à dire l'étude et l'application des théories métaphysiques sur le fonctionnement du monde. Dis autrement, l'alchimie vise à obtenir la compréhension du monde tel qu'il est (le noumène) comparé à ce qui nous est apparant et accessible (le phénomène), et à pousser les pratiques magiques à leur paroxisme via l'application de cette connaissance

Cette pratique peut se révéler dangereuse pour l'esprit, parce qu'elle porte dans des choses que notre esprit n'est pas fait pour comprendre. Elle est notamment connue pour provoquer beaucoup de fatigue morale et psychologique, demandant du repos.

Parmis les plus célèbre alchimistes se trouvent Nicolas Flamel, Paracelse Ier et le dieux Hermès qui a le titre du Trismégite en Europe pour cela. De nombreux alchimistes ont existé dans d'autres civilisations, et peut-être que la Pierre Philosophale de Nicolas Flamel est simplement l'unique encore en activité.

## Critères d'entrées

- > 20 en métaphysiques
- Avoir un trait de personnalité donnant un bonus en intelligence, perception, volonté ou sagesse.
- Avoir une magie de Création, Enchantement, Herboristerie OU avoir le signe serpentaire ou un signe altéré OU avoir un bonus d'éclat
  - Certains alchimistes comme Flamel peuvent faire sauter cette protection

## Pouvoirs de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Compréhension du monde | Peut ajouter 5 % en SAG ou INT pour un coût de deux PE pour comprendre quelque chose lié à la métaphysique ou au noumène, où pour utiliser une compétence alchimique. Peut stack jusqu’à 40 % en plus. |

## Alchimie hermétique

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Soin métaphysique (SAG ou INT, -20) | Peut tenter de soigner un état métaphysique non permanent. |
| 2 | Conversion vitale | Peut faire un sort sur ses PV en faisant la conversion suivante : 3PV = 1 Eclat. |
| 3 | Plongée dans le noumène (SAG, 4 Eclat) | Sort du combat un personnage pendant 1 tour, et inflige 1D6 dégâts mentaux au personnage et à lui-même.
| 4 | Malédiction métaphysique | Peut faire un jet de malédiction à un ennemi au hasard, mais un allié au hasard fera un jet de panique. |
| 5 | Artillerie du Noumène (SAG, 6 Eclats) | Fait appel aux pouvoirs du Noumène pour infliger 1D20 dégâts sur jusqu’à trois ennemis. Inflige 1D8 PE dégats sur tous les alliés. |
| 6 | Ouverture du Noumène | Voir section |

## Alchimie créatrice

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Chimie explosive | Peut ajouter à des potions des effets de matériaux métaphysiques |
| 2 | Conversion vitale | Peut faire un sort sur ses PV en faisant la conversion suivante : 3PV = 1 Eclat. |
| 3 | Enchantement permanent (INT, VOL) | Peut rendre permanent un enchantement (de soi ou d'un autre). |
| 4 | Création permanente (INT, VOL) | Peut rendre permanent une création (de soi ou d'un autre). |
| 5 | Théorie atomique | Peut modifier des caractéristiques au niveau atomique d’un objet. |
| 6 | Ouverture du Noumène | Voir section |

## Ouverture du noumène

*Extrêmement dangereux. À n’utiliser qu’en cas* **de dernier recours**, *dans les situations les plus désespérées.*

*Cela produit* **très exactement** *ce qui est écrit dans le titre. C’est une très mauvaise idée.*

*À vous de voir.*

## Les pratiques alchimiques

La principale méthode de fonctionnement de l'alchimie consiste à la maitrise et l'amplification des pratiques magiques. Dans l'alchimie, chaque pratique magique est représenté par un élément, et ces éléments sont "modelé" pour atteindre des nouveaux paroxismes.

Un des exemples est la pratique d'une magie élémentaire qui toucherait les éléments métaphysiques, tels que l'ombre et la lumière.

## La Pierre Philosophale

La Pierre Philosophale est un objet célèbre de l'alchimie. La Pierre Philosophale est un objet qui consiste simplement en l'abolition des règles de la magie. La Pierre Philosophale est un paradoxe, offrant des réserves illimitées d'éclat, et une vision du monde tel qu'il est presque illimitée.

L'unique pierre philosophale recensée est celle crée par Nicolas et Perenelle Flamel. Plusieurs autres existe de manière légendaire, tel que celles construite dans la *Cité d'Or* aux Amériques, ou le légendaire *Livre Noir* dont les contes racontent que le fils du créateur serait emprisonné dedans, et fournirait son pouvoir à qui le trouverait.

Flamel lui-même blague souvent sur l'idée que le vrai Flamel serait peut-être en fait mort et qu'il ne serait que la Pierre Philosophale répliquant ce qu'il restait de lui.