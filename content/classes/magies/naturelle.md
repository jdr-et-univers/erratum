---
layout: layouts/base.njk
title: La magie naturelle
tags: Magies Rares
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# La magie naturelle

La magie naturelle est la magie consistant à utiliser à son avantage les forces de la nature et des animaux. Cette magie est grandement appréciée des gens n'ayant pas forcément de fort pouvoir magique, et peut s'apprendre en autodidacte par un fort contact avec la nature.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Recherche d’être de la nature (PER)  |  Peut détecter les animaux et les membres du petit peuples.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Langage des émotions (REL)  |  Peut parler aux animaux. Malus de 20 % sur les animaux sauvages (+ malus potentiel de circonstances)  |
| 1 |  Animal totem  |  Le personnage à un esprit animal qui apparaît prêt de lui et qui lui sert de lien avec dame-nature. L’animal totem est intouchable mais ne peut pas toucher qui que ce soit, de quelques manières que ce soit. Seul le personnage et les animaux peuvent le voir et communiquer avec.  |
| 2 |  Domestication  |  Quand un animal n’est pas trop sauvage et qu’il a réussi à lui parler, le joueur peut le domestiquer pour pouvoir ensuite pouvoir lui parler plus facilement après, en tant que familier.  |
| 2 |  Liane de plante (3 Eclats)  |  Permet de faire apparaître des lianes de plantes fines et contrôlables, utilisables à diverses fins.  |
| 2 |  Maître des bêtes (trait)  |  +20 % pour attaquer, maîtriser ou communiquer avec les monstres de types bêtes.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Vétérinaire (INT)  |  Peut faire un premier soin, exclusivement sur les animaux. Si a déjà un premier soin, +10 % sur les animaux.  |
| 3 |  Radar naturel  |  +20 % en PER dans la nature, et ne peut s’y perdre.  |
| 3 |  Frappe chlorophyllienne (FOR)  |  Peut faire une attaque normale avec un effet « végétal »  |
| 4 |  Domestication monstrueuse  |  Si un monstre de type « bête » a été vaincu mais est conscient et présent sur le terrain, peut tenter un jet de domestication dessus, à -20 % (seulement -5 % si a fait un jet de vétérinaire dessus AVANT la fin du combat)  |
| 4 |  Ami des bêtes  |  +10 % dans toutes les stats s’il a réussi à éviter la mort d’une créature animale durant la partie. Cependant, -10% si a tué lui-même une créature qui ne l’a jamais attaqué lui (sauf cas de force majeur tel besoin de se nourrir). Même le faire pour lui épargner des souffrances fait ça : parfois un choix juste nous fait du mal à nous aussi…  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Appel des compagnons  |  Peut appeler à tous moments un animal domestique, où qu’il soit. L’animal tentera de venir par les moyens disponibles.  |
| 5 |  Photosynthèse  |  Dans un espace extérieur, jette un d4 pour regagner des PV par tour. D6 dans un temps particulièrement ensoleillé.  |
| 5 |  Transformation bestiale  |  Peut prendre la forme de son animal. 80 point de statistiques sont redistribués sous cette forme (le choix est fait au maximum lors de l’accès au niveau 5). Ne peut plus utiliser ses armes, ni communiquer sous cette forme.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  LES ARBRES SE LEVENT (5 Eclats)  |  Peut animer et contrôler comme des animaux les arbres. Peut également leur demander des informations majeures.  |
| 6 |  Résurrection animale (4 Eclat)  |  Peut ramener à la vie un de ses animaux domestiqué tombé au combat. S’il est tombé durant cette partie, il ne sera à nouveau disponible qu’à la partie suivante.  |