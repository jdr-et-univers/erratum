---
layout: layouts/home.njk
eleventyNavigation:
  key: Système de combat
  order: 3
---

# Système de combat

Le système de Combat d'Erratum est un système de combat ayant pour but d'offrir aux joueurs de nombreux manières d’appréhender un problème, suivant leur personnage et leurs objectifs. Il est possible de taper à la fois sur l’esprit où sur les PVs.

Les combats peuvent être plus ou moins différents suivant les ennemis, également. En effet, les ennemis peuvent se différencier sur plus que leurs statistiques et leurs capacités/compétences. D'autres éléments qui peuvent différencier les ennemis peuvent être la manière de se battre, etc. C’est pour cela qu’il est essentiel avant de lancer un combat pour le MJ de définir les conditions de victoire et d’échec du combat (mais pas forcément de les dire aux joueurs…)

## Pages de la catégorie

- [Organisation d'un combat](organisation/) - Présente l'organisation globale d'un combat et d'un tour
- [Déroulement d'une attaque](attacc/) - Présente le déroulement d'une attaque et d'une action
- [Postures de combat](postures/) - Présente les différentes postures que vous pouvez avoir en combat
- [Vitalité](vitalite/) - Présente le fonctionnements des dégats
- [Les afflictions](afflictions/) - Présente les différentes afflictions (et effets) pouvant toucher votre perso
- [Les éléments](elements/) - Présente les effets des différents éléments