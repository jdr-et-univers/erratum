---
layout: layouts/base.njk
title: L’herboristerie
tags: Magies Communes
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# L’herboristerie

L'herboristerie est sans doute la magie la moins magique. Elle tire son pouvoir de plantes particulières, les plantes élémentaires. Ces plantes sont alors mélangées pour créer des effets spéciaux dans des potions.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Préparation de potion  |  Peut créer des potions, prenant un tour mais produisant une potion. De base, peut pour un éclat faire une potion de soin légère qui ramène 6 PV.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Potion de vigueur (3 eclat)  |  Augmente de 10 % les stats physique pendant trois tour (non cumulable avec les pouvoirs loup-garou)  |
| 1 |  Extrait de douleur  (2 eclat)  |  Inflige 3 dégats esprit et 3 dégats de corps  |
| 2 |  Potion de lumière (2 Eclats)  |  Produit un malus de 15 % en PER à tout personnage touché par la potion.  |
| 2 |  Potion de Rage (3 Eclats)  |  Booste l’attaque de 20 % mais lui donne 1 chance sur 4 d’attaquer un allié (la chance sur 4 est cumulable)  |
| 2 |  Concentré de Dopage  |  Permet de faire rejouer (6 Eclats)  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Essance de célérité (2 Eclats)  |  Toujours premier pour prochain combat  |
| 3 |  Potion de soin avancée (3 Eclats)  |  +12 PV. Pas plus pas moins. Pour une fois t’as pas de dé, fais la fête.  |
| 3 |  Potion de flamme (5 éclats)  |  Donne des dégats de feu et fait des dégats spéciaux.  |
| 4 |  Potion de pestilence (3 Eclat)  |  Fait faire un jet de pestilence à l’ennemi quand il la boit.  |
| 4 |  Potion d’intengibilité (4 eclat, trois tours)  |  Donne une immunité aux dégats physiques.   |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Filtre d’amour (5 eclat, trois tours)  |  Peut séduire en combat un allié et lui donner +50 % à tout ses jets. Il obéira à tout ses ordres, et ne prendra d’action sans son autorisation.  |
| 5 |  Potion « Debout les morts » (5 Eclats)  |  Soigne de tout les PV un perso non-mort, mais rend fortement confus 5 tours.   |
| 5 |  Potion explosive (6 Eclats)  |  Fait des dégâts de 8PV perce-défense à tous les ennemis proches de la personne touchée.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Potion de Résurrection (1 karma, 1 éclat)  |  Ramène à 0 PV un personnage mort.  |
| 6 |  Potion secrète bretonne (8 Eclats)  |  La personne l’ayant bu peut annuler un critique au choix  |