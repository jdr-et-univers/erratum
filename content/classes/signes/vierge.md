---
layout: layouts/base.njk
tags: Signes de Guérison
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Vierge
parent: Les signes
---

# Vierge (confiance)

Les vierges tirent leur pouvoir de la fois, de la confiance qu'elles peuvent accorder ou qu'on peut leur accorder. Il ne s'agit pas d'une confiance qu'elle donne forcément à l'heure actuelle, mais d'une qu'on pourrait ou aimerait donner. Cette confiance peut être envers une personne, un concept abstrait, etc.

Cette puissance leur offre à la fois protection et capaciter à aider leur prochain. Mais il s'agit d'une protection toute particulière : il est plus simple de se mettre en danger voir de se sacrifier quand on a confiance envers quelqu'un...

Leur hérault est **Bakhna Rakhna** première des Yumboes, une ruche danaïte d'Afrique de l'Ouest, amies des âmes défuntes.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :virgo: | Cochon (asie), Isis (égypte) | Guérison et Protection (Soigneur Tank) | Ordre | Peu commun (07.0%) | VOL, SAG et REL | Rose |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Soin spirituel (VOL ou SAG)  |  Peut soigner aussi bien les PV que les PE, mais ne soigne que la moitié d'un soin normal.  |
| Double |  Soin de la foi (VOL ou SAG)  |  Peut soigner aussi bien les PV que les PE, sans malus  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Abnégation  |  Peut à la place d'esquiver et encaisser, peut choisir de se prendre des dégats physique sur son esprit. Les armures spirituelles comptent.  |
| 1 |  Martyr  |  Peut, une fois par tour, se prendre une attaque sur les PE à la place d'un autre joueur (II: pour deux éclats, peut le refaire le même tour)  |
| 2 |  Don de soi  | Peut sacrifier ses PE pour augmenter la puissance d’un soin spirituel en mode esprit, ou physique en mode corps.  |
| 2 |  Armure spirituelle  |  +3 (II : +5) en armure spirituelle.  |
| 2 |  Le corps ou l'esprit (3 tours de cooldown)  |  Peut au début d’un tour passer en mode « corps » ou « esprit ». Dans ces modes, elle ne peut plus faire de soin d’un autre type, mais +50 % d’un soin complet à celui du bon type. (II : Plus qu’un tour de cooldown)  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture d’aide  |  Permet de passer en posture d'aide (II: posture d'aide+)  |
| 3 |  Antidote universel (VOL ou REL)  |  Peut, en mode corps ou esprit, retirer une affliction de ce type sur un personnage au choix (II : peut en retirer une au choix en mode normal, deux quand la vierge est spécialisées)  |
| 3 |  Pureté d’âme (enfin ça dépend)  |  Résistance de 15 % pour recevoir une affliction mentale hors injection d’émotion ou jet de panique.  |
| 4 |  Esprit sacré (6 eclats)  |  Reçois un renforcement mental niv3 (+30 % en SAG, INT et VOL)  |
| 4 |  La passion de la vierge  |  En dessous de 50 % de ses PV, gagne +10 % (II : 20 %) à ses jets mentaux et sociaux.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Must Protecc  |  Quand le/la vierge tombe KO, tous les alliés ont +20 % (II : 30 %) pour le/la défendre ou le/la venger. Même s’ils ne l’appréciaient pas totalement.  |
| 5 |  Sérénité  |  Lors d’un jet de panique, la vierge à 10% de chance de ne pas avoir d'affliction mentale.  |
| 5 |  Ce n’est pas grave  |  Peut forcer un échec critique à n’avoir de conséquence que sur elle/lui.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Élégiaque (3 Eclats)  |  Peut, quand elle décide de sacrifier son encaissement et son esquive sur ses PV, renvoyer 50 % (II : 100 %) des dégâts en tant que dégâts mentaux, ou soin au choix de la vierge  |
| 6 |  non. (1 karma)  |  Peut annuler le jet de survie d'un autre joueur avec un point de karma  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Lumière divine (1/partie)  |  Soigne tout les PV et PE de tout les personnages présents + retire toute affliction physique et mentale à tout les personnages. 20% de chance que chaque affliction magique non-temporaire disparaisse  |
| Herault |  Miracle  |  Iel peut effectuer dans le coma des soins sur les autres personnages, sans accès à ses objets et ses postures.  |
