---
layout: layouts/home.njk
eleventyNavigation:
  key: Jeu de plateau
  parent: Système de jeu
  order: 6
---

# Jeu de plateau

Pélican est un JDR plutôt orienté "pen and paper", mais il est possible de préférer jouer certaines parties avec une orientation plus "jeu de figurines". Le but de cette page est de fournir des règles simples sur comment faire une partie sur un plateau.

Ce système peut être joué avec plusieurs types de quadrillages, mais a été réfléchi surtout avec un système de case carrée.

## Gestion des tours

La gestion des tours est libre tant qu'il n'y a pas d'ennemis ou de conflits entre les joueurs. Dans les autres cas, les joueurs doivent faire un jet d'initiative et se déplacer dans l'ordre de l'initiative, comme dans un combat.

## Déplacement

Pour les déplacements, les joueur⋅euses doivent calculer le nombre de déplacements possible pour leur personnage. Ce nombre est calculé à partir d'une base, auquel on ajoute des bonus et des malus, de la manière suivante :

- Base : 7 (peut changer suivant les plateau)
- Bonus : Présence d'équipements améliorant l'HAB et/ou espèce du personnage avec un bonus d'HAB (+1 si l'un des deux, +2 si les deux sont vrai)
- Malus : Présence d'équipements baissant l'HAB et/ou espèce du personnage avec un malus d'HAB (-1 si l'un des deux, -2 si les deux sont vrai)

D'autres éléments peuvent jouer, selon ce que décide le MJ : le terrain, la taille relative du lieu, etc. Les déplacements sont libres, cependant si un ennemi est présent, les personnages doivent faire un jet d'initiative et le respecter.

Le personnage peut se déplacer comme il veut tant qu'il fait un déplacement du nombre de case demandé, sauf particularités de l'environnement, décidées par le MJ. Si un obstacle est sur sa route, il doit le contourner.

### Effet des compétences

Les compétences peuvent affecter les déplacements du joueur :

- S’il utilise un dash ou une compétence du même style, il devra faire une ligne droite et ne pas changer de direction.

- Les compétences permettant de se placer partout permettent de se placer partout. Sur les meubles, les ennemis, etc.

## En combat

Les attaques se font sur les ennemis en indiquant la case où ils sont. Les règles pour savoir si on peut toucher un ennemi sont les suivantes :

- *Armes au corps à corps* : L'ennemi doit être présent sur une case adjacente à l'attaquant (diagonales comprises).

- *Armes au cac avec portée (example : lance)* : L'ennemi doit être présent à une distance de deux cases du joueur (diagonales comprises) et ne pas avoir d'obstacle entre lui et le joueur.

- *Armes à distances* : L'ennemi peut être à n'importe quelle distance du joueur tant qu'il n'y a aucun obstacle entre lui et le joueur.

- *Attaques de zones* : Les attaques de zones touchent toutes les cases adjacentes à la case touchée (diagonales comprises), à l'exception des explosions qui ont une portée de deux cases.

- *Attaques touchant tout les personnages* : Les attaques touchant normalement tout les personnages présents ont généralement une limite plus tangible, comme la pièce où se trouve les personnages. Si c'est un grand terrain vague, le MJ peut poser une distance avec le point de départ de l'attaque.

Les esquives sont aussi affectées par cela, demandant d'effectuer un déplacement sur une case adjacente à la position originelle du personnage qui n'est pas sous les effets de l'attaque, sous peine de subir un bonus d'esquive de 30%.
