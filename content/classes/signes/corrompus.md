---
layout: layouts/base.njk
tags: Autour des signes
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Les signes corrompus
parent: Les signes
---

# Signes corrompus

Un signe corrompu est une variante d'un signe, qui possède des effets différents de celui du signe normal. Les effets peuvent être inversé, ou peuvent être modifié pour une variation du pouvoir du signe. Les signes corrompu sont extrèmement rare, et peuvent être aussi bien positifs que négatifs que pour le détenteur du signe, contrairement au signe anomique qui ne peut être que négatif.

Les signes corrompu se remarquant le plus sont les *gémeaux corrompu*, puisqu'ils produisent souvent des modifications de l'apparition du gémeau, qui peut par exemple apparaitre sous forme spirituelle au lieu d'apparaitre sous une apparence charnelle.

## Fonctionnement du signe corrompu

Les signes corrompus observent les règles suivantes :
- Ils sont forcément une transformation du pouvoir de base d'un des signes.
- Ils n'obtiennent que 6 pouvoirs sur 15.
- S'ils ont le pouvoir de herault, celui-ci sera affecté par la corruption

Les types de corruptions suivants sont possibles:
- Inversion de la notion du signe, ce qui va affecté le reste des effets (une vierge se basant sur la méfiance, un lion sur les attaques lachent, un poisson tentant de montrer le plus possible le réel, une balance injuste…)
- Un ajout d'un contre coup, d'une contrepartie, d'une altération à un signe (avoir un gémeau spirituel uniquement)
- Avoir des effets "glitchés" aux attaques, comme si elles bugguaient.

## Signes semi-corrompus

Ces signes se repèrent juste par une altération du pouvoir niveau 0. Le reste du signe fonctionne normalement.