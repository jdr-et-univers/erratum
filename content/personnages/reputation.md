---
layout: layouts/home.njk
eleventyNavigation:
  key: Réputation
  parent: Personnages
  order: 3
---

# Réputation et prestige

Avoir une bonne réputation peut être important pour une aventure… cependant, une réputation n'est pas forcément quelque chose de simple.

## Les différentes réputations

Ce JDR simule la réputation à travers trois éléments :

- La réputation **morale**, représentant à quel point vos actes sont juste et bons, si vous aidez les gens. Ce type de réputation sera préféré par les libre-penseur voulant voir le bien être fait.

- La réputation **d'obéissance aux règles**, représentant à quel point vous êtes chaotique ou obéissant. Ce genre de réputation sera préféré par les hauts-gradés se demandant si vous leurs serez fiable. Ce genre de réputation sera cependant moins aimé par les rebelles.

- Les différentes réputations **auprès des factions** existant dans le JDR. Ce genre de réputation sera préféré par les membres desdites factions, même si tout les membres ne seront pas forcément entre accord avec l'idée d'obéir avec quelqu'un ayant une mauvaise réputation même si vous les avez aidé…

Cependant, d'autres éléments peuvent jouer sur le prestige d'un personnage

# Les points de prestige

Le prestige est une stat d’expérience spéciale qui permet d’ajouter un bonus à certaines actions spécifiques (si le MJ l’autorise). Chaque partie rajoute un certain nombre de point de prestige, suivant comment l’équipe à fait parler positivement d’elle.

Le prestige représente à quel point vos personnages ont réussi dans l’objectif qui était défini (tandis que l’XP représente à quel point les évenements les ont forgé) et comment cela à a influencé la manière dont ils sont vu, aussi bien par les PNJ que le destin…

Les points de prestige se consomme en étant mis dans des stats unique, de confiance, d’érudition et de respect, qui représente la réputation du personnage, et qui se consommeront lors d’actions spécifiques.

- **La confiance** permet d’augmenter la puissance d’un jet de REL pour que quelqu’un d’ordinaire nous fasse confiance.

- **Le respect** permet d’augmenter la puissance d’un jet de CHA pour intimider quelqu'un en lui faisant comprendre qu’il n’a pas affaire à des premiers venus.

- **L’érudition** permet d’augmenter la puissance d’un jet de SAG pour montrer à quelqu’un qu’on maitrise les tenant et les aboutissant d’une situation.

Le prestige peut également être utilisé pour récupérer du karma. Alors, on peut considérer que c'est à quel point le personnage s'est distingué qui lui apporte une chance de contrepartie un jour. Le coût prestige/karma est de 11 point de prestige. pour 1 point de karma.
