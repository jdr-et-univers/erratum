---
layout: layouts/base.njk
tags: Signes de Protection
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Verseau
parent: Les signes
---

# Le verseau (le flux)

Les verseaux tirent leur pouvoir des flux. Leur signe leur confère une affinité naturelle avec la puissance naturelle du temps qui passe, des flux, leur offrant un pouvoir minime d'influence de ceux-ci. Cependant, même une capacité d'influence minime du temps est un pouvoir immense.

Ils peuvent modifier les évenements, effacer une action, agir avant que les conséquences de quelques chose se produise... Cependant, le coup de ces pouvoirs est souvent haut.

Leur hérault est le savant Albert Einstein, ayant découvert de nombreuses propriétés de l'espace-temps.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :aquarius: | Dragon (asie), ??? (égypte) | Protection et Malice (Tank Trickster) | Chaos | Rare (00.7%) | INT et SAG | Bleu |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Action préventive  |  Peut tenter une action lorsqu’un combat est sur le point de se déclencher.  |
| Double |  Action pré-coma (1/combat)  |  Si le personnage tombe dans le coma, a le droit à une dernière action gratuite avant. Il ne peut pas se soigner ou préparer le coma.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Grattage de place  |  Peut passer une place avant ou après dans l’ordre des tours lors d’un combat, au choix du joueur en début de combat (II : peut pour 1 éclat changé sa position par rapport à la position d’origine PENDANT le combat)  |
| 1 |  Armure temporelle  |  +2 en armure magique (II : +4)  |
| 2 |  Bis Repetita (2 Eclats)  |  Peut forcer un personnage à répéter la même action, dans les mêmes conditions, pendant les deux prochains tours (II: quatre prochains tours).  |
| 2 |  T.A.S (Time Assisted Search) (2 Eclat)  |  Peut relancer tout jet de loot (argent comme objet) une fois (II : deux fois) et choisir celui qui l’arrange le plus.  |
| 2 |  Frappe temporelle (2 Eclat)  |  Peut faire une attaque normale avec un effet « temporel ».  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture défensive  |  Peut passer en posture défensive (II : posture défensive +)  |
| 3 |  Réorganisation (3 Eclat)  |  Force l’ordre des persos à être rejouer (3 Eclats)  |
| 3 |  Timeline  |  Peut faire relancer jusqu’à trois dés par partie, sauf sur un échec critique.  |
| 4 |  Régression (4 Eclat)  |  Annule le dernier soin ou attaque qu’à reçu un personnage.  |
| 4 |  Etat de grace (une fois par partie)  |  Annule tous les dégâts pendant un tour.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Damage Control (1 fois par partie)  |  Peut annuler un critique et en faire un simple échec/réussite, sauf sur une action qui augmentait les chance de critiques.  |
| 5 |  Il ne s’est rien passé (1 fois par partie)  |  Peut effacer une action commise par un joueur et retourner a avant que cela arrive (II: peut le faire une deuxième fois pour 6 éclats)  |
| 5 |  Blocage temporel (3 Eclat)  |  Provoque pétrification sur l’ennemi pendant trois tours. (II: 5 tours)  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Bravely Second (1 karma)  |  N’importe quand dans le combat, peut arrêter le temps, offre une action inarrêtable, inesquivable à tous les héros, lui compris.  |
| 6 |  Rembobinage (1 fois par partie)  |  Retourne au début d’un combat annule toutes les morts, pertes de PE et de PV du combat. Seuls les PJ et PNJ importants gardent souvenir de ce combat. (II : pour 4 Eclat, peut exclure un personnage du rebobinage, hors boss)  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Double flux  |  Peut faire une action hors de son tour, une fois par combat.  |
| Herault |  Arrêt de flux (1/partie) |  Peut faire durer jusqu’à la fin du combat un effet temporaire (jusqu'à la fin de la partie si critique).  |