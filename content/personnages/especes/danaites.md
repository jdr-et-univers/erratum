---
layout: layouts/base.njk
title: Danaïtes
tags: Espèces de base
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---


# Danaïtes

Les Danaïte sont un autre des quatres grands autres peuples, surnommé le *peuple du printemps*. La légende des Danaïtes racontent qu'elles ont été forgées à l'image de la déesse Oresta, protectrice du continent de Mu. Leur corps aurait été fondé à l'imge des *quatres premiers dieux*, ce qui expliqueraient le fait qu'iels soient sexué.e.s sans avoir besoin de cela pour la reproduction, de même pour le peuple des forêts..

Les Danaïte ont des ailes de papillons (et des petites antennes), elles ne sont ni mammifère, ni insecte ! Comme la plupars des fées, leurs chevelures ont une plus grande gamme de couleur que les humains, pouvant prendre des teintes vives tel que le rouge pur, le bleu, le vert, etc… Leur espérence de vie est plus longue que celles des humains, pouvant aller à 110 pour les danaïtes "ordinaires" voir 150 pour les danaïtes fées.

Malgré leur apparence presque humaine, ces êtres sont plus… des végétaux. En plus de leurs caractères sexuels "humains" et de leur genre social, les fées ont également une sexuation "végétale", pouvant soit être pistil, soit androcée, soit "sans fleur" (50% de la plupars des populations fée). C'est pour ça que certaines population semblant être uniquement d'un sexe (les lutin ou scintillante) peuvent sans soucis se reproduire.

Leur reproduction se produit alors par butination des fleures pistiles par un.e fée androcée, ce qui produit des graines de danaïtes, formant ensuite une grande fleur d'où naitra l'enfant fée.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 12 | 12 | 10 | 5 | Mana (2 sur armure magique) | REL, VOL, SAG (60) | FOR, DIS (40) | Le reste (50) |

## Traits et compétence

| Trait | Effet |
|:-----:|:------|
| Végétal | Se prend 2x par au métal et au feu. |  
| Être de la forêt | +10 en HAB, SAG et PER dans les milieux naturels. -10 dans les lieux entièrement trop pollués. |

## Organisation sociale

Les danaïte forment un système de la ruche. En effet, les danaïtes forment une sorte de lien mentale entre elles, leur permettant de partager des informations ou des sentiments, appellée "ruche". Cette ruche est assez importante pour la stabilité des danaïtes les plus fragiles, même si iels peuvent vivre sans. La ruche existe sous forme d'une orbe magique, et pour fonctionner, un.e danaite pistile prend le role de "Reine", devenant le liant de la ruche. Leur organisation est très liée à cela de ce fait, les enfants sont généralement élevée en communauté, et les danaïte formant des sociétés fondée sur un rapport important à la collectivité.

Contrairement à ce qu'on pourrait croire, toutes les ruches ne sont pas forcément des monarchies : De nombreuses ruches ont adopté d'autres systèmes, et la "reine" danaïte n'est pas forcément une "reine" politique. En effet, une reine danaïte est souvent plus une protectrice, ou un "ciment social".

Les danaïtes sont souvent vu comme un peuple fier et isolationiste dans la littérature, mais en vérité c'est surtout une vision occidentale lié à la *Guerre Larvée*, les danaïtes comme les fées pouvant être très différentes suivant les régions. En effet, en Europe, les relations entre les humains et les fées étaient parfois très complexe à cause de la déforestation et du contrôle du territoire. Des bébés humains (toujours de famille noble ou puissante) étaient enlevé pour être élevé par les danaïte et devenir des *enfants des forets* tandis qu'ils étaient remplacé par des danaïtes sans-fleur, nommé dans ce genre de contexte *changeling*. Cependant, à côté de cela, de nombreuses danaïtes étaient engagée comme préceptrice des enfants, selon la tradition des *marraines fées*.

Ce phénomène peut être notamment vu en amériques, ou cette vision des danaïtes est en grande partie importée de l'europe.

## Reine danaïtes (variante)

Les reines danaïtes reçoivent +2 éclats, et font 4 dégats sur leur attaque de mana.