---
layout: layouts/base.njk
tags: Malédictions
models:
  - Afflictions communes
  - Malédictions
title: Malédictions communes
parent: Les afflictions
---

# Malédictions

Les malédictions sont des effets permanents qui touchent parfois vos joueurs, de manière random. Elles ne sont pas disponibles en tant que capacités (cependant vous pouvez créer des versions temporaire de leurs effets, tel que des gaufres transformant en plante verte pendant un tour).

## Liste des malédictions

<div class="table-auto">

| id | nom | effet |
|:--:|:---:|:------|
| 1  | **Nudisme** | Ne peut plus porter de vêtement. |
| 2  | **Plante verte** | Ne peut pas bougé ou faire des actions physiques, regagne +1D4 PV/tour si soleil, et ne peut plus esquiver. Toutes les actions mentales, la parole et la magie restent possible. À un peu la honte. |
| 3  | **Fifi** | Ne peut pas mourir, mais se prendra 1/2 attaque aléatoire. |
| 4  | **tl-dr** | Toute phrase de plus de 4 mot sera ignorée par les autres personnages |
| 5  | **Magie lente** | Tout sort n’agira que deux tours après. |
| 6  | **Malchance métaphysique** | Remplace la dizaine du D100 par un D12 (faisant le tir entre 10 et 129). Les critiques sont adapté. |
| 7  | **Apogée damnée** | Chaque critique effectué à son caractère échec/réussite tiré au hasard par une main innocente autre que le joueur. |
| 8  | **Respécialisation** | Décale les statistiques du tableau de statistiques d'un cran sur la gauche/droite (tiré au hasard). Non cumulatif |
| 9  | **Reconstruction psychique** | Changement de personnalité au hasard pour une partie |
| 10 | **Trop chanceux** | Ne se prend pas les attaques aléatoire, +20% pour trouver du loot, mais tout les jets de social sont en jet affaibli (stat/2). |
| 11 | **Cupidon est relou** | À chaque début de partie, jette 1D4. Le résultat du dé indiquera de quel PNJ/ennemi dans l'ordre d'apparition le personnage tombera amoureux. |
| 12 | **Trouille cosmique** | Au début de chaque partie, fait un jet de panique |
|    | **Ectoplasmisme** | Le personnage ne peut plus être toucher, mais ne peut plus faire d'action physique |
|    | **Pétrification** | Ne peut plus bouger. Gagne naturellement +20 en armure. Si le personnage se casse, peut être recollé avant d'être libéré. A chaque partie, peut faire un jet de SAG pour être libéré. |

</div>

Lors d'un **jet de malédiction**, le personnage jette un *dé 12* et prend dans le tableau le résultat du dés.