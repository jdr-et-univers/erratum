---
layout: layouts/base.njk
title: Groupes et factions
tags: Dragons from Outer Space
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Groupes et factions

Différents groupes et factions existent dans l'espace, dans l'univers de Dragons From Outer Space. Ici, on va parler des factions les plus importantes.

## Le conseil interplanétaire

Quelques grupes politiques se sont assemblées pour pouvoir discuter entre les différents grands groupes interplanétaire. On y retrouve

- **La Compagnie d'Exploration Solaire** est un conglomérat composé de Frontières, des différents gouvernements humains, de l'organisme des mages et différents groupes de marchand et explorateur qui visent à en découvrir plus sur les différentes planètes.
- **Les enfants d'Antichton** sont un groupe peu nombreux cherchant à éviter que soient mal utilisées les technologies des quatres anciennes civilisations. Ils sont aidé par les Arbitres et les Cultistes. Ils ont des membres un peu partout, mais sont peu influent.
- **La Fédération de Centauri** est le groupe politique principal venant de Centauri Prime. Les villes martiennes Centaurienne en font partie depuis peu.
- **Les fourmillières d'Anthropomax** sont de puissant groupes Urthoïdes, qui arrivent sur de nombreuses planètes.
- **La Fédération d'AEgir** n'est pas multi-planétaire, mais garde une grande influence dans les discussions interplanétaires.
- **Le conseil versatile** est l'ensemble des versatile, quel que soit leur planète ou leur position, même quand ils vivent à l'intérieur d'autres nations.


Ce conseil est controversé, parce qu'on retrouve dans ce groupe surtout des groupes politiques ayant une influence sur plus d'une planète. 

Les groupe politiques de Kolchis ne sont par exemple pas présent parce que considéré comme "ayant peu d'influence hors de leur planète", tout comme les royaumes draconiques de Ladon. De même, les créataciens y sont absent, malgré leur existence sur 2 voir 3 planètes depuis que certains sont retourné sur Terre dans *la vallée des dinosaure*.

## Groupes illégaux

- Les **pirates de l'espace** sont (comme leur nom l'indique) des pirates de l'espace, qui cause de nombreux soucis dans le système solaire. Leur coeur est *la cour des pirates*.
- Le **culte d'Atolk** est un culte vénérant le dragon corrompu éponyme.
- **L'Empire Néo-Atlante** est un groupe techno-fasciste suprémaciste humain né d'anciens membres des chasseurs et de quelques groupes de dieux européens, ainsi que de rémanant de l'empire de la louve. Il est illégal dans de nombreuses planètes.