---
layout: layouts/base.njk
title: Demi-dieux
tags: Les divinités
models:
  - Les divinités
parent: Les divinités
---

# Demi-dieux / Héros

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Divins + autre espèce | Rares | Dépend de l'autre espèce | Aucun | 500 ans |

Les demi-dieux sont des êtres issues de l'union d'un⋅e dieu/déesse et d'un.e mortel. Ces êtres ont des pouvoirs d'éclat très puissant, mais restent des membres de leur espèce, avec toute les particularité que cela entend.

Cependant, cela à quand même des effets assez nombreux : lors longévité est bien supérieurs à celle de leur espèce, pouvant dépasser les plusieurs siècles ! Ils étaient relativement encore nombreux durant *l'Ère des Héros*, la dernière ère ou leur infleunce était encore forte sur terre.

Parmi les célèbres demi-dieux, il y a Héraclès, Merlin, etc.

| PV | PE | Eclat | Pression | Armes |
|:--:|:--:|:-----:|:--------:|:-----:|
| +5 | +5 | +5 | -2 |  |

## Stats et compétences

|   | Forces | Faiblesses | Neutres |
|:--|:------:|:------:|:------:|
| **Stats** | Deux stats (+15) | Deux stats (-15) | |
| **Élements** | Deux éléments au choix du joueur | Deux éléments au choix du joueur + Éclat (double) |  |
| **Traits** | Esprit divin : +20 % pour comprendre les éléments métaphysiques/magique. | |  |
| **Traits** | Gène divin : peut actionner tous les mécanismes divins/céleste. | |  |