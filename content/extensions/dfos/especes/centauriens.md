---
layout: layouts/base.njk
title: Les Centauriens
tags: Espèce DfOS
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les Centauriens

Les petits hommes gris. Les Centauriens sont une espèce aliens existant depuis très longtemps sur une planète proche de la Terre, orbitant autour d’Alpha Centauri. Ils sont nos voisins les plus directs, à l'exception des nombreuses espèces venu de la terre. Ils sont l'espèce intelligente la plus commune de leur planète, mais où vivent aussi les versatiles.

Ils ont atteint le voyage spatial qu'un siècle avant que les humains le retrouve, et ont plusieurs fois fait quelques tentatives de contacts qui se sont mal passé. Ils sont souvent vu au travers de leur culture dominante mondialisée, mais ils ont en vérité une richesse de culture tout aussi grand que la terre. Leur planète est structuré en une grande fédération planètaire, qui réussi à fonctionner à peu prêt. Chaque ancien état à son monde de fonctionnement, mais également une série de conseils allant du niveau "local" au niveau planétaire. Ce conseil supervise la planète et travaille notamment sur la planification économique.

Ils ont des légendes anciennes parlant des mystérieux *porteurs de lumières*.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 10 | 18 | 10 | 4 | Poing (0) | INT, REL, VOL (65) | FOR, CON, HAB (35) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Choc mental | Peuvent faire naturellement une attaque de zone faisant la moitié des dégats d'un jet de volonté. |
| Esprit puissant | +3 d’armure spirituelle naturelle. |
| Corps faible | -2 d’armure physique |

## Marsiens

Une variante ayant quelques cités sur Mars des Centauriens. Ils sont vert, et ont une forme plus aggressive avec des pics. Ils ont les mêmes traits et compétences.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 12 | 16 | 10 | 6 | Griffe (1) | INT, FOR, VOL (65) | REL, CON, HAB (35) | Le reste (50) |