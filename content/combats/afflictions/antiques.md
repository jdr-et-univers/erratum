---
layout: layouts/base.njk
tags: Malédictions
models:
  - Afflictions communes
  - Malédictions
title: Les malédiction antiques
parent: Les afflictions
---

# Les septs malédictions antiques

Les septs maéldictions antiques sont septs malédictions affectant l'esprit, créant des comportements dangereux pour la personnes et/ou son entourage. Il s'agit de sortilèges puissants. Pour résister à ses effets, il faut faire un jet de SAG (pour se rendre compte de l'influence de la malédiction, sauf si les circonstances permettent de savoir que c'est son action qui joue ou choix du MJ) ou de VOL, le tout en jet affaiblis (stat/2).

Les malédictions antiques mélangent des effets sur le psyché, distordant la perception et la pensée, mais aussi affecte de manière métaphysique le monde autour les risques de situations causant les comportements liés à la malédictions antique sont accrus.

Les explications ci-dessous indiquent les comportements provoqués par ces malédictions. Les malédictions s'imprégnant à l'essence d'une personne, elles ne sont pas soignables, uniquement compensable.

## Orgueil

Le personnage ne voit plus ses limites, et se voit comme étant capable de tout accomplir, ce sans aucune barrière ni chance même de ne pas réussir à 100%. La personne touchée par l'orgueil ne se voit pas réussir... iel se voit comme étant victorieux *au plus haut point*. La "petite victoire pas terrible" n'existe même plus pour le personnage touché.

Le perso ne peut plus décider de fuir, esquiver le combat, où ne pas faire quoi que ce soit où il se considère comme étant bon. Il est également plus facilement taunté (dans le sens forcer une action) par toute mention qui le dévaluerais.

La plus grande manière de lutter contre l'orgueil sont les responsabilités : si iel voit un danger pour quelqu'un d'autre, voit que l'autre personne pourrait ne pas suivre sa puissance absolue, cela peut service de limitation.

## Avarice

Le personnage est constamment dans un état de craintes des autres et d'égoïsme. Il voit tout les autres personnages comme de potentiels rivaux, qui en veulent à ce qu'il est ou à de positif, et veulent le lui prendre. Il n'est cependant pas jaloux des autres, il pense vraiment que EUX sont jaloux de lui et veulent le voler. Toute aide pour lui est douteuse, les gestes désintéressé n'existent plus.

Dans de nombreuses sociétés où l'argent existe, cette malédiction c'est surtout remarqué sur le biens monétaires (même si elle a historiquement porté sur de nombreux point) du à l'influence qu'à l'économie, d'où la notion d'Avarice.

La plus grande manière de lutter contre l'Avarice est par la patience de l'entourage, où à contrario en diminuant l'importance de ce que possède la personne touchée. Voir que les gens ont une vision négative de ce qu'iel a lea mettra plus en confiance (avec l'idée qu'ils seraient trop bête pour le voir). Il faut faire attention cependant à ce que l'avare ne croit pas que c'est un piège.

## "Luxure"

Cette malédiction antique est peut-être la plus différent de son sens "pécher". Cette malédiction antique se caractérise par une envie de plaire, un besoin des autres absolu, sans aucune distinction, qui conduit à être prêt à tout pour être vu, plaire. La personne se met alors à ne plus pouvoir désobéir - sauf s'iel estime que cela pourrait lui faire encore plus plaisir - à la personne à qui iel veut plaire. Cela pourra produire une envie d’obéir aux ordres ou demande, ou bien de se faire remarquer par autrui.

Beaucoup d'universitaires se demandent quel lien s'est formé entre la malédiction et le pêcher qui lui correspond, quand celui-ici existe. Est-ce qu'une soumission ne correspond pas assez à l'idée de "pêcher", le manque de connaissance qui fait qu'on a surtout vu de loin un effet possible (de nombreuses relations amoureuses voir sexuelles), où la notion d'un "désir de l'autre", même si c'est ici un désir de révération.

Le moyen de le combattre est souvent avec une inhibition de cette envie de plaire, la création d'une sorte de "calme" et de plus de confiance en soi, par divers moyens.

## Envie

Le personnage touché par l'envie perd la capacité à résister à ce qu'il veut. Que ce soit bien matériel, ambition, objectifs... La personne fait passer cela avant tout, avant même la raison. Ils deviennent aussi plus facilement piégeable, ayant besoin de se forcer pour ne pas foncer dans un piège... parfois même en le voyant !

Parmi les effets les plus courant, on voit le fait de faire passer ses objectifs avant le bien-être des autres (parfois le bien-être des autres peut même être l'objectif en question !), voir le sien ! L'esprit étant complexe, les envies contradictoires peuvent donner des confrontation mentale interne violente : le personnage se bat littéralement contre lui-même (et perd des PE sur le passage...)

Si quelqu'un d'autre possède ce que veut l'envieux, cela peut donner une jalousie monstrueuse.

Le moyen pour combattre l'envie est par elle-même, les envies contre les envies.

## Gourmandise

Peut-être la plus "simple" des sept malédiction antique. Le personnage atteint par la malédiction de la gourmandise subit une envie de manger, une curiosité de gout de beaucoup de choses. Dès que le personnage voit un truc qu'iel peut voir comme comestible ou "goutable", iel teste. Potion, plante, truc étrange, etc. Iel test.

Le meilleurs moyen de la combattre est avec des coupes-faim.


## Colère

Le personnage atteint par la malédiction antique de la colère subit une rage interne sous-jacente constante. Cela veut pas dire que la personne est 100% du temps en colère, non. Mais qu'elle peut se produire à tout moment. La personne ne résiste plus à ses plusions.

Comme l'orgueil, la colère rend extrêmement simple à taunt, mais d'une manière différente : en faisant toute chose pouvant énerver le personnage. Mais ce n'est pas tout, toutes chose pouvant énerver le personnage fait que le personnage va attaquer, plus ou moins violemment suivant à quel point l'échec est fort. Même en cas de réussite, le personnage pourra subir de la rancune.

La colère se soigne à coup d'inhibition des pulsions, de diverses manières.

## Paresse

Le personnage atteint par la malédiction antique de la paresse est dans un état d'acédie voir d'apathie constente. Les choses ont du mal à toucher l'être atteint, qui ressent une forme d'apathie et une difficulté à s'intéresser aux choses, parfois à sa propre vie.

La personne, dans toute situation, doit se forcer à s'intéresser à ce qui se passe. En cas d'échec critique, le résultat est encore plus grave : iel tombe en état de *sommeil d'acédie*, dont iel ne pourra se réveiller qu'avec une réussite critique.

La paresse se combat à coup d'excitant très puissants et/ou d'exces d'adrénaline.