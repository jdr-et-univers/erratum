# Scénario

Cette page recense quelques scénarios possibles pour DanseRonce.

## Scénario DanseRonce

Dans ces scénario, vous jouez des élèves (ou surveillant) de l'établissement DanseRonce. Vous pourrez y vivre des aventures fantastiques, et devrez ne pas oublier la devise importante de DanseRonce : "les enfant de DanseRonce ne s'abandonnent pas les uns les autres". Il se déroule principalement dans un pensionnat pour "enfant étrange", abritant des enfants et adolescents différents de la normes sur bien des points. Vous pouvez y jouer des élèves ou des surveillants, et y vivre des aventures palpitantes. Cependant attention, des gens s'intéressent aux différentes puissantes qui peuvent exister dans l'univers.

- **La mélancolie de Merlin** : Un groupe d'élève de DanseRonce réveille accidentellement Merlin, enfermé dans un arbre à Brocéliande depuis des siècles. Cependant, celui-ci est très mal luné et semble avoir un plan néfaste. Les élèves réussiront-ils à l'arrêter et à éviter le pire ?
- **Les secrets des vertus** : Une mystérieuse élève arrive à DanseRonce, nommée *Sophrosyne*. Cette jeune hybride renarde est taciturne, solitaire, râleuse, mais semble aussi avoir un secret étrange. D'autres personnes arrivent dans l'établissement, et dans l'ombre, de *puissantes organisations* sont à leur recherche.
- **Le livre des secrets** : Lors d'un voyage scolaire proche de Hinsicht, des élèves de DanseRonce découvre un livre mystérieux. Ses secrets et ses pouvoirs feront d'eux une cible pour des gens mal intentionnés.

## Scénario cultistes et frontières

- **Cultes et mystères** : Jouez des jeunes cultistes fraîchement recrutés, et gardez les artéfacts étranges que les cultistes protègent du monde (et protègent le monde d'eux). Cependant, certains artéfacts ont disparus récemment. Réussirez vous à les trouvez ? Serait-ce l'oeuvre de Frontière, les rivaux de toujours des cultistes ?
- **Forging a new frontier** : Vous avez été recruté par le programme de création de Super Héros de Frontière, afin de mener des missions d'aide et de sauvetage à travers le monde. Mais un nouveau mage maléfique attaque récemment la ville coeur de Frontière, Néo-Atlantis, et vous devez aller mener l'enquête avec votre chef. Serait-ce l'oeuvre des cultistes, les rivaux de toujours de Frontière ?
- **Des squelettes dans les placards** : Vous jouez des jeunes gardiens recrutés par l'O.I.M. et fraîchement diplomés de Werenia. Un groupe dangereux nommé "Le groupe de Tobias" agis dans le monde depuis quelques mois, recrutant de plus en plus de supporters. Mais quels secrets sur vos supérieurs ont-t-ils ?

## Campagnes historiques

- **La guerre des vertues** : Durant le 16e siècle, les vertues se font la guerre, et une vertue à rejoint les dieux qui veulent reprendre leur puissance face aux mortels. Un petit groupe d'aventuriers décide de tenter de sauver le monde de la menace divine et doivent faire face aux roi des dieux, le puissance Akash.
- **La fin d'un Empire** : Le patrice d'Italie, Odoacre, vous a invité, vous, les *chevalier de la table ronde*, à un diner mystérieux. Cependant, des complots terribles semblent se produire dans l'Italie, et tous se demandent où est passé le dernier empereur romain d'Occident…