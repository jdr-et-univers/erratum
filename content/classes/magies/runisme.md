---
layout: layouts/base.njk
title: La magie runique
tags: Magies Communes
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# Magie runique

Les écritures magiques sont des types de magies très particulières, nombreuses, utilisant le pouvoir des mots et surtout des écritures. Ces types de magies se base sur la puissance créatrice existant naturellement dans les mots, les écrits ne faisant que donner des instructions à ces magies.

Ces types de magies sont aussi ancien que l'écriture elle-même, et sont encore très utilisé aujourd'hui pour créer des sortilèges durables, notamment par les *alchimistes*. De nombreux grands sortilèges existent dans le monde et sont étudié par les spécialistes, et servent notamment à faire fonctionner des sortilèges supposé durer longtemps.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Connaissances runiques (INT)  |  Peut reconnaître et comprendre les modalités d'utilisations d'une rune.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 1 |  Activation runique (3 eclat, 1/partie)  |  Permet de réactiver une rune après juste une partie.  |
| 2 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 2 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 2 |  Activation différée (3 Eclat)  |  Permet d'activer une rune tracée non-active. Ne consomme pas d'action.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 3 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 3 |  Maitre des runes  |  Permet de reproduire toute rune présente en tant qu'objet.  |
| 4 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 4 |  Desactivation runique (confrontation SAG)  |  Retire l'effet d'une rune.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 5 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 5 |  Explosion runique (SAG)  |  Fait exploser une rune.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Apprentissage de runes  |  Permet d'utiliser une rune  |
| 6 |  Fusion de runes (5 Eclat)  |  Permet de combiner l'effet de deux runes  |