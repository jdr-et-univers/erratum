---
layout: layouts/base.njk
title: Les dragons stellaires
tags: Dragons from Outer Space
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les dragons stellaires

Les dragons stellaire sont des créatures importantes à *Dragons from Outer Space*, donnant leur nom à l'extension.

Ils sont une espèce vivant naturellement dans l'espace, n'étant inquiété ni par le manque d'air, ni par le manque de température. Ils vivent à une échelle complètement différente de celle des être vivant sur des planète, ayant souvent été comparé à des dieux comme les titans de jadis. Les dragons peuvent cracher de véritable geyser d'énergie, destructeurs et puissants.

En effet, les dragons stellaire sont une espèce immense, pouvant atteindre plusieurs kilomètre de longueur. Ils sont capable de voler malgré l'absence d'aile dans l'espace, pouvant se déplacer jusqu'à 1/10e de la vitesse de la lumière. Leur expérence de vie est gigantesque également, se comptant en millions, voir dizaine de milion d'année. Les dragons se nourrissent sont de l'énergie de l'étoile ou il sont proche, ou bien d'hydrogène, allant souvent s'en délecter autour des géante gazeuses. 

Ces éléments en font une espèce puissante, même si elle interagit peu avec les humains. Ils sont entre cent et deux cent entre les planètes du système de Dagda, et plusieurs miliers autour de l'étoile même.

## A'tolk, le dragon corrompu

A'tolk est un dragons stellaire ancien et puissant, contaminé par l'anomie produit lors d'une calamité ancienne s'est produite dans les confins du système solaire, dans la ceinture de kuiper. Il est scellé à l'intérieur de la planète naine *Éris* depuis des dizaines de milliers d'années.

Ce dragon est extrèmement ancien et puissant, et possède un lien mental avec tout les êtres contaminé par l'anomie. Cependant, un dieu encore plus ancien lui parle, et serait la véritable source de tout ce danger.

### Culte de la calamité

Le culte d'A'tolk est un culte de la calamité et de l'anomie, continuant l'oeuvre du schisme et des autres suiveurs de l'anomies qui ont existé dans le passé, et forme une partie de la *guilde des irrégularité*.

Composé en grande partie de mage, ils pratiquent une magie corrompue, parlant souvent avec véhémance du "potentiel" d'une telle magie. Les hautes sphere du culte cependant pratique peu cette magie, et est composé surtout de nobles et évèques voulant renforcer leur puissance grâce à l'utilisation de ce culte.