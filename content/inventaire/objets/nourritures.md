---
layout: layouts/base.njk
title: La nourriture
tags: Objets consommables
models:
  - Objets consommables
  - Objets réutilisables
parent: Les objets
---

# La nourriture

| Objet | Effet | Prix |
|:----------:|:------|:----:|
| Repas complet de base | Des vivres pour une journée. Durent une partie. | 5 |
| Repas de qualité | Des vivres pour une journée + rajoute 10% à toutes les stats. Durent une partie. | 25 |
| Repas épicé | Des vivres pour une journée + 10% de résistence au froid. Durent une partie. | 25 |
| Repas fraicheur | Des vivres pour une journée + 10% de résistence à la chaleur. Durent une partie. | 25 |
| Alcool | Rend 1D12 PE, mais jet de CON pour éviter d'être ivre (-20% pour chaque conso supplémentaire). | 20 |
| Alcool ensorcelé | Effet alcool + provoque un jet de pestilence et de bénédiction à la fois | 500 |
| Pain d'ascedie | Ne périme pas, fait un repas complet - mais la sensation de faim reste et il est dégoutant. | 85 |
| Café | Offre +30% de résistence au sommeil. | 15 |
| Infusion d'Herbe | Permet d'appaiser quelque peu le personnage, rend 1D6 PE. | 15 |
| Thé | Servi en groupe, apporte +5% de REL à tout le monde. | 15 |
