---
layout: layouts/base.njk
eleventyNavigation:
  key: Lieux importants
  parent: Lore et univers
  order: 0
models:
  - Lieux magiques
  - Établissements scolaires
title: Lieux importants
---

# Lieux importants

Erratum se produit dans une terre alternative où la magie a toujours existé, et a été plus ou moins caché dans le monde suivant les époques. De ce fait, le monde d'Erratum à eu une histoire quelques peu différente que la notre, en particulier depuis la grande révélation.

Tout ces éléments font que le monde que vous allez découvrir ici possède un certains nombre de différence, géographique comme culturelles.

En effet, le monde magique avait ses propres pays, qui ont petit à petit pris de l'importance voir remplacé les pays qui existaient dans notre monde, surtout en occident.

## Etablissements scolaires

Comment apprends-on la magie ? Si la méthode la plus courante d'apprendre la magie est d'avoir un maitre qui en connait une, il existe quelques établissement scolaires plus ou moins importants qui permettent d'apprendre la magie, et dont voici une liste non-exhaustive.

- [DanseRonce](danseronce/) est un petit pentionnat magique breton, connu pour avoir des élèves souvent considéré comme "à problème".
- [Werenia](werenia/) est une université magique internationale, située en Autriche.

## Les deux mondes

Erratum est divisé donc en "deux mondes", qui sont en vérité plus deux société vivant l'une à côté de l'autre, se connaissant mais se mélangeant peu. En effet, depuis la grande guerre des vertues lors du 16e siècle, le monde a découvert l'existence des forces magiques, mais celles-ci ont perdu quelque peu en intérêt avec la montée des sciences, qui permettait à de nombreux non-mages de faire des choses exceptionnelles.

Les deux mondes vivent du coup en parallèle, avec aussi peu de croisement que possible, parfois avec même leurs propres pays chacun de leurs côtés. Quelques cultures cependant sont plus proches, suivant les lieux et les endroits. Cette séparation est en grande partie également due au fait que la magie est peu disponible au non-mages : seul 0,7% des êtres humains sont capable de pratiquer la magie des "signes", environ 5% les magies "rares". Cependant, dans les faits, les magies "communes" peuvent être appris par 100% des enfants, hors de certaines maladies telles que le *syndrome de perte d'éclat*.

Cette séparation est parfois combiné à un dédun mutuelle, mais également quelques traces de facination. Parlez de l'atome, et vous provoquerez chez de nombreux magiciens aussi bien de la peur qu'une certaine forme d'admiration, peu de magie ayant la capacité de percer ces secrets et de créer des armes aussi terribles... D'un autre côté, les secrets du noumène sont difficile à percer avec de la science.

Cependant, des mixages ont lieu : des êtres des deux mondes s'aiment, se disputent ou se fréquentes, des gouvernements non magiques ont effectué des expériences magiques, notamment durant la guerre froide, et parfois les média parlent d'évenement se passant chez leur co-habitants.

## Le « monde de la magie »

Ce qu'on appelle le « monde de la magie » est plus un ensemble d'espèce est de pratique qui étaient masqué au monde des humains jusqu'à la grande découverte de la magie.  Dans les pays occidentaux, le monde magique avait pris l'habitude depuis des siècles de se tenir éloigné du monde non-magique. Cela provoqua l'apparition de ce qu'on appelle les "Nations Occultes". De nombreux territoires colonisés virent aussi les civilisations présentes dans ces territoires former ces types de nations pour se protéger de l'invasion.

Depuis la grande révélations, plusieurs conflits larvés ont eu lieu entre mages et "normaux", surtout dans les pays ayant fortement changé à cause de la révélation.

Cette découverte à également provoqué de nombreuses querelles entre les états magique et les états non-magique.

## Le « monde de la science »

Le monde de la science est un nom récent, donnée depuis le début du 20e siècle au monde non-magique, pour souligner que l'avancée des sciences à permis de dépasser certaines capacités de la magie dans de nombreux domaines spécifiques. Cela à en grande partie provoqué un léger déclin du monde magique, et une remontée de l'intérêt pour les sciences et tout. La communication instantannée par internet, les vaccins, le voyage spatial, etc. sont de nombreux domaine dans lequel la science à dépassé la magie.

Ce monde dans beaucoup d'endroit essaie de ne pas se méler des histoires magiques, et il y a historiquement eu des conflits sur comment gérer des soucis magique touchant des non-mages.

Cependant, la magie reste quelque chose de présent même dans le monde de la science. Dans la culture, les gens aiment bien les histoires de magiciens - même s'ils sont parfois éloigné de la réalité. Et nombreux sont les enfants qui espèrent avoir un signe, ou avoir le droit d'apprendre une magie.
