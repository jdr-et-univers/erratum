---
layout: layouts/base.njk
tags: Signes de Malice
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Gémeaux
parent: Les signes
---

# Gémeaux (l'ubiquité)

Les gémeaux sont à la fois un seul être et plusieurs, un corps et un esprit dédoublé. Ce que cela donne en pratique est très différent des personnes : parfois les gémeaux seront deux personnes très différentes, avec des gouts et des visions du monde différentes, mais lié pour la vie. Parfois, il s'agit d'une seule personne, vivant avec deux corps et devant faire avec (avec plus ou moins de contrôle simultané des deux). Parfois, il s'agit d'une parmie toutes les possibilités entre ces deux extrèmes.

Le dédoublement provoque cependant toujours un effet : l'esprit étant étendu entre deux corps (que ce soit sous la forme d'un ou deux esprits), il a le pouvoir de s'étendre encore plus sur tout ce qui entoure, et d'affecter tout ce qui entoure.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :gemini: | ??? (asie), ??? (égypte) | Malice et Guérison (Trickster Healer) | Chaos | Commun (15.0%) | VOL et INT | Indigo |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Gémellité  |  L’utilisateur possède un⋅e jumeau/elle (pas forcément du même sexe) lui ressemblant quasiment en tout point. Les deux ont une barre de PV/PE/Eclat commun, mais peuvent jouer 2× par tour.  |
| Double |  Gémellité astrale  |  L’utilisateur possède également un⋅e jumeau/elle (pas forcément du même sexe) spirituel, lui ressemblant quasiment en tout point. Le jumeau astral est constamment intangible, ne pouvant pas faire d’action **physique**.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Soin mental (INT)  |  Peut soigner en PE l’équivalent des niveaux de réussite du jet (+1 si pas de baton de soin)  |
| 1 |  Coucou  |  Peut sacrifier le tour d’un de ses personnages pour que l’autre (II : les deux autres) a un effet de surprise à son attaque (empêche l’ennemi d’esquiver)  |
| 2 |  Antidote mental (VOL ou REL)  |  Soigne une (II : deux) affliction mentale sur un personnage au choix.  |
| 2 |  Lien mental  |  Peut créer un lien mental avec un autre personnage, joueur ou non, sur un jet de CHA ou REL, pour lui parler sans se faire remarquer.  |
| 2 |  Télékinésie (VOL ou INT)  |  Peut déplacer des objets à distance sur un jet de VOL ou INT. Les limitations de poids sont pareil qu'un perso aurait sur de la FOR  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture focus  |  Permet de passer en posture focus  |
| 3 |  Onde psychique (VOL ou INT)  |  Une attaque mentale combinée des jumeaux. Utilise tous les tours, mais touche trois ennemis à moitié (II: 3/4) de dégats.  |
| 3 |  Double Esprit (5 Eclat)  |  Les deux personnages peuvent tenter de mettre en commun leur connaissance dans un domaine : +30 % (II : 50 %) à toutes les compétences durant 3 tours.  |
| 4 |  Rappel  |  Si les deux jumeaux sont séparés, peuvent se mettre d’accord pour que l’un⋅e se téléporte jusqu’à l’autre (II : marche également avec le gémeau spirituel)  |
| 4 |  Action combinée  |  Les deux personnages peuvent faire ensemble une action, les niveaux de réussite se cumulent et s’ils sont à 0 ou +, l’action est réussie.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Surprise !  |  Si un coup d’un⋅e des jumeau/elle a été esquivée, l’autre (II : les deux autres) obtient une attaque d’OP sur un AUTRE personnage au choix du joueur.  |
| 5 |  Soin fou (INT)  |  Un soin de base mental, mais dont le résultat est multiplié par 1D4. Cependant, le personnage fait 1 jet de panique.  |
| 5 |  Situation stressante  |  Lorsque ce pouvoir est actif, tous les jet d’afflictions mentale aléatoire se font à partir de -50 % des PE max au lieu de -100 %. Le pouvoir à deux tours de cooldown avant de pouvoir à nouveau inversé.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Éloge de la folie  |  Si actif, +1 (II : 2) point de pression et +5 (II : 10) en volonté par personne en état Panique.  |
| 6 |  Le noir du désespoir (5 Éclats)  |  Peut sur un jet de DIS ou VOL, bloquer soit tous les ennemis, soit tous les alliés, soit tous les personnages qui se situent entre 0 et -max PE dans un état de résignation : ils ne peuvent pas repasser au-dessus de 0 PE, mais ils ne peuvent pas faire de jet de panique naturel.  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Au royaume des fous (trait)  |  Lors d’un jet de Détermination, fait un jet de bénédiction en plus de regagner 1 PE. En cas de panique, +15 % à tous les jets jusqu’à disparition de la panique.  |
| Herault |  Pouvoir de Le Mental (6 Eclats)  |  Peut sur un jet de SAG ou INT, une fois par combat, demander qu’elle sera la prochaine attaque d’un ennemi ou d’un joueur. Si le jet réussi, le MJ ou Joueur devra s’y tenir, quel que soit ce qui se passe entre-temps.  |