# Changelog

## [2.6.1] - 2024-10-17

### Ajoutés

- Ajout des combats de batailles
- Ajout d'un robots.txt

### Modifiés

- Modifications des tableaux pour utiliser plus souvent des verticaux
- Modification du site pour le rendre responsive
- Masquage des éléments futurs

## [2.6.0] - 2024-09-11

### Refonte DfOS

La plus grande modification est une refonte complète de l'extension Dragons from Outer Space ! Elle se passe désormais dans l'univers principal, et rajoute des élément pour faire des parties de space fantasy. Cela contient les ajouts suivants :

- Des points de scénario pour les autres planètes du systèmes solaires
- Deux petits systèmes solaires explorables en plus + 1 contenant une planète
- Plus de 7 nouvelles espèces à découvrir !
- Des nouvelles factions et éléments de scénario utilisable dans les parties spatiales

### Ajoutés

- Ajout des potentiels divins
- Ajout d'écoles magiques venant de folklores européens
- Ajout de l'espèce harpie

### Modifiés

- Refonte des dragons pour qu'ils puissent avoir des PJ/PNJs comme les demi-dieux
- Nerfs des armes à feux
- Ajout d'un temps de cast au Kung Fury
- Modification du saignement critique

## [2.5.0] - 2023-09-01

### Ajoutés

- Ajout des herault d'arcane
- Ajout des combats "rangés"
- Ajout des pouvoirs de wyverns
- Ajout de la variante Dragons from Outer Space
- Ajout page signe corrompu et d'un autre signe…
- Ajout système d'équipage
- Ajout des dévots, des apôtres et des irrégularités
- Ajout d'une page TL;DR

### Modifié

- Modification de la liste des heraults
- Ajout d'un coup en déplacement aux postures
- Ajout de plus d'interprétation pour les runes
- Remplacement des rangs par un système de niveau (nouveau système d'expérience)
- Rééquilibrages des spécialités
- Refonte posture offensive
- Limite "dure" des stats à 255


### Dépréciation

- Les anciens héraults (remplacé par les grands hérault et la nouvelle liste)

## [2.4.0] - 2023-06-10

- Séparation de Erratum en un JDR à part

### Site web

- Refonte du site web sous Eleventy

### Ajoutés

- Ajout d'une page sur la bienveillance
- Ajout de règle de jeu de plateau
- Ajout de pages supplémentaires de lore
- Ajout du système de grâces
- 10 traits de personnalités en plus (total: 40)
- Ajout d'un système d'influence des émotions
- Ajout des guildes
- Ajout des spécialités, des classes pour non-mages
- Ajout d'un système de rang pour indiquer comment évolue les niveaux
- Nouvelles espèces : Pumas, Kobolds, Centaures
- Nouvelles variantes d'espèces : Fomoires, Lycions, Qryst, Némédiens
- Ajout des Wyverns

### Modifié

- Ajout d'une stat d'anomie pour représenter la malédiction anomique
- Refonte légère des afflictions
- Refonte des compétences alchimiques en guilde et différents objets
- Une ambidextrie partielle est possible à tous (50% niveaux de réussite), et Ambidextrie permet d'annuler le malus désormais.
- Refonte d'un système de potions plus créatifs (facultatif pour cette version)
- Refonte du système de classe (facultatif pour cette version)

### Corrections

- Remise de la Télékinésie, oubliée entre les versions
- Fusion de l'onde psy et de l'echo psychique
- Le transfert de PV se fait après application des armures
- La situation de crise a repris son bon nom !
- Petites corrections autour des Malédictions antiques

### Dépréciation

Les éléments déprécié seront supprimé lors d'une prochaine mise à jour. Pour l'instant, ils ont des remplaçant mais peuvent être utilisé toujours dans les JDRs en cours.

- Les talents ont été déprécié et sera remplacé dans la prochaine version (remplacé par les spécialités)
- L'herboristerie pré 2.4 a été dépréciée et sera remplacé dans la prochaine version (remplacée par une nouvelle)
- Le système de classe a été déprécié et sera remplacé dans la prochaine version

## [2.3.0] - 2020-07-01

- Nouvelle adresse git

### Corrections

- [erratum/espace] Corrections de quelques confusions

### Ajoutés

- [organisation] Nouvelles sous-catégories (amélioration perso, objets).
- [organisation] Ajout d'une page pour les campagnes
- [organisation] Ajout d'une page pour l'exploration et les déplacement
- [talents] Ajout de talents perspectif supplémentaire

### Modifié

- [organisation] Extraction en page à part des postures
- [organisation] Extraction en page à part des confrontations sociales
- [organisation] Extraction en page à part des personnalités et potentiels
- [organisation] Extraction en page à part des talents
- [organisation] Extraction en page à part du prestige
- [organisation] Fusion des pages décrivant la fiche de personnage
- [forme] Réécriture page organisation des combats
- [forme] Réécriture page création des personnages
- [talent] Meilleurs organisation des talents

## [2.2.0] - 2020-03-01

### Ajoutés

- [core] Ajouts des septs malédictions antiques
- [core] Ajout des pseudo-éléments
- [core] Ajout perce-armure
- [core] Ajout perce-défense
- [core] Ajout anomie
- [core] Ajout familiers
- [core] Ajout dégâts de chutes
- [core] Ajout explosions
- [core] Ajout objets génériques

### Modifié

- [core] Séparation métaphysique et paradoxe
- [core] Amélioration afflictions métaphysiques
- [core] Rééquilibrage faiblesses élémentaires

## [2.1.1] - 2020-02-25

### Ajoutés

- [tout] Support emojis

### Modifiés

- [tout] Nouveau style pour les informations annexes.
- [coeur] Meilleurs titre afflictions.

### Corrigés

- [erratum] Correction sceptre du cancer
- [coeur] Correction erreur nom robe sacrée
- [coeur] Correction description arme à distance
- [coeur] Ajout prix boucliers
- [coeur] Réparation tableau bouclier niveau 3

## [2.1.0] - 2020-02-23

### Ajouté

- [erratum] Rulebook Erratum
- [meta] Site d'accueil directement dans les sources
- [coeur] Système de déplacement pour les combats
- [coeur] Système de jets affaiblis
- [coeur] Confrontations sociales
- [coeur] Niveaux des armes
- [coeur] Début d'un système d'argent
- [coeur] Ajout du niveau des armes
- [coeur] Ajout de quelques objets

### Modifié

- [meta] Fusion de tout les rulebook en un seul site
- [meta] Fusion des pages Anomie, Afflictions et jets spéciaux
- [coeur] Refonte système de faiblesses élémentaires

### Corrigés

- [coeur] Utilisation de la bonne description pour l'élément végétal
- [coeur] Correction d'un ordre en chaos
- [coeur] La rage à 25% d'attaquer les alliers, par les ennemis
- [coeur] Desactivation du cache

## [2.0.0] - 2020-01-07

Version initiale contenant tout l'équivalent de la version papier originelle.