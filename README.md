# Erratum

Erratum est un JDR de fantasy urbaine se passant sur une terre alternative contemporaine ou la magie a été révélée à tous, et où une partie de la population est capable d'utiliser des pouvoirs étranges nommés les "signes". Ce JDR est basé sur un système D100 homebrew (une version antérieur de mon système pélican), et vise à être plus orienté RP que de l'utilisation de règles complexes.

Cette page à pour objectif de vous présenter à la fois l'univers et comment il interragit avec le système. Ce JDR est fourni par [Kazhnuz](https://kazhnuz.space), avec certaines règles reprise du système de [MDupoignard](https://twitter.com/MDupoignard). Il est distribué sous la Creative Commons CC-BY-SA 4.0.

Ce site est généré par [Docsify](https://docsify.now.sh/).

## Avant propos

> « On m’a toujours dit que croire aux monstres était un truc d’enfant. Que ça devait partir une fois qu’on atteignait l’âge adulte. Autour de moi, je ne vois que des adultes qui croient au monstre, Et je suis un enfant seul qui n’y croit pas. »
> <author>??? – Auteur inconnu – Date inconnue</author>

Depuis des temps immémoriaux, deux « mondes » existent, que tout oppose, mais qui doivent partager la même planète. Le monde « réel », de la rationalité et de la science. Le monde de l'imprévu, du mystérieux et de la magie. En effet, environ 3% de la population mondiale est capable de pratiquer, à l'aide de forces occultes, la « magie ». 0.7% est même capable d'en faire d'eux-mêmes, à l'aide d'un mystérieux pouvoir nommé les « douzes signes ». Des créatures magiques vivent cachées dans le monde, éloignées de tous.

Ce monde est resté caché pendant des siècles, alors que la plus grande partie de la population cessait petit-à-petit d'y croire. Cependant, en Mai 1943, le célèbre mage Merlin a décidé de révéler le monde et les créatures magiques face aux horreurs de la seconde guerre mondiale. Il estimait que face à de tels massacres, les mages n'avaient pas le droit de cacher leurs pouvoirs. Une grande partie du monde magique entra alors dans le combat contre les horreurs du nazismes. Cependant, une telle découverte déséquilibra le monde au sortir de la guerre. Pays magiques et non-magiques se disputèrent territoires et se firent une longue guerre de 9 ans, jusqu'à ce qu'en 1951 c'est une toute nouvelle face que montrait la terre, à la fin de ces batailles.

Depuis, le monde semble s'être stabilisé. Practiciens de la magie et non-practiciens vivent chacun de leur côté, se connaissant mais minimisant les véritables interactions. L'OIM et les Gardiens s'occupent de s'assurer que la magie n'est pas utilisée à tors, et à combattre les "forces du mal". Cependant, bien des conflits latents existent et pourraient se manifester… Et quels secrets cachent encore ce monde, hors de la vue même des magiciens ?

