---
layout: layouts/base.njk
tags: Organismes officiels
title: O.I.M
models:
  - Organismes officiels
  - Sociétés secrètes
  - Orgnaisations criminelles
parent: Les organisations
---

# L'Organisme Internationale des Mages

*L'OIM* est l'organisation supposée supervisée les usages de la magie partout dans le monde. Elle a comme politique de laisser chacun utiliser sa magie comme bon leur semble tant que les usages ne sont pas illégaux.

La pensée majoritaire du monde magique est d'ailleurs qu'il ne faut surtout pas limiter les actions des mages, parce que malgré le danger qu'ils pourraient paraître, ils seraient aussi la solution, pouvant combattre les « mages noirs »

## Organisation

L'OIM contient également un groupe d'intervention international en cas de pépin magique : les Gardiens. Leur base centrale se trouve sur l'une des deux villes franco-allemande indépendante, la citadelle d'Hinsicht. Leur chef est nommé le *connétable*, et le connétable actuel est l'ex-gardien ancien et apprécié (malgré des actions condamnnée lors des Guerres des mages noirs) *Cyrus Lancelot-Physis*, qui a repris sa place après un court mandat du draconologue *Barthémius Davidson*.

## Les gardiens

Les Gardiens sont les unités d'intervention de l'OIM. Il s'agit de groupe de combattants magiques (généralement de 5 à 6 personnes) qui agissent sous ordre des Gardiens en cas d'incidents magiques. Ils ont pour ordre d'éviter tout conflit avec les populations civils, et ne sont ni une armée, ni une police. Cependant, des soucis existent quand même dans la gestion des gardiens.

Les gardiens sont dirigés par trois maréchaux : **Ambroise Abdère**, **Frédéric Glacier** et **Cynthia Vernal**.

La formation des gardiens se fait au sein d'académies internationales, tel que le groupe scolaire Werenia en Autriche dirigé par Aezoth Yartelnac.

## La classification des dangers

- Le **blanc** est utilisé pour tout ce qui n’est pas des menaces à proprement parler mais doit être réglé, tel que des créatures inoffensives mais surnaturelle qui se retrouve dans un espace non-magique. Les trois quarts des missions blanches sont réquisitionnées par l’Académie de Werenia pour ses élèves gardien, le reste étant confié à des équipes toutes récemment diplômées.

- Le **vert** est utilisé pour les menaces mineures, qui comportent soit de légers risques de causer des blessures à des êtres vivants, soit provoquant des dégâts matériels ou du grabuges. Les missions de « convocation » de praticiens de la magie utilisant leur pouvoir à tors et à travers sans grand danger, ainsi que les petites créatures qui pourraient blesser quelques gens. Ces missions sont toujours faisables théoriquement par des équipes de novices. Ce sont les missions les plus courantes.

- Le code de couleur **jaune** est réservée aux menaces qui pourrait à coup sûr causer des dégâts matériels importants ou causer des blessures graves à des individus, mais sans véritable danger de morts. Il est utilisé pour une bonne partie des créatures magiques qui se retrouvent dans des espaces non-magiques, et pour une partie des criminels/délinquants les moins dangereux. Il est généralement confié à des équipes accomplies, mais considéré comme n’étant pas capable d’affronter les menaces mortelles. Avec les missions vertes, ce sont les missions les plus courantes.

- Le code de couleur **orange** est utilisé pour les menaces qui pourraient conduire à la mort d’un ou plusieurs individus, où provoquer des dommages économiques et matériels considérables. Il est utilisé pour les *mages obscurs* (utilisation illégale violente de magie) ou tout ce qui est criminels étant considéré comme capable de tuer - même s'ils ne l'ont pas encore fait. Il est généralement accomplies par des équipes confirmées, qu'on considère capable de survivre à des missions avec dangers de mort.

- Le code couleur **rouge** est donné aux menaces qui pourraient ou peuvent causer de nombreux morts (tels que des phénomènes magiques très dangereux, des créatures magiques puissantes qui pourraient s’énerver…) ou à des criminels endurcis dangereux (tels que des trafiquants d’armes magiques, ou des tueurs à gage). On demande rarement à une équipe seule de résoudre une menace rouge, ou alors on le demande à une équipe considérée comme de très haut niveau.

- Le code couleur **écarlate** est donné aux menaces qui peuvent à coup sûr provoquer la mort de centaines, voir de milliers de personnes, tel que des groupes terroristes, un dragon enragé qui s’approche d’une ville, ou certaines créatures purement démoniaques titanesques. C'est un code couleur extrèmement rares, et ont y envoie généralement un ensemble de trois à quatre équipe de très haut niveaux.

- Le code couleur **noir** est réservée aux menaces qui demandent une armée entière de gardiens de haut niveaux pour être vaincue, qui pourraient déclencher une guerre entre les deux "mondes" – ou dont le niveau de menace peut toucher un pays entier. C’est le code le plus haut jamais utilisé dans la hiérarchie des codes de couleurs, et aucune créature seule dans l'époque moderne été capable de déclencher une alerte de ce niveau, même si théoriquement les dieux en seraient capable. À titre rétroactif, toutes les attaques de dieux envers des humains ont été classifié en couleur noire.

- Deux autres codes théoriques existent encore, les codes « **apocalypse** ». Le code « apocalypse » est utilisé dans le cas de menace pouvant causer la destruction totale d’un continent, et le code « apocalypse X » est utilisé pour les menaces de destructions planétaire, ou de l’humanité. Ces deux codes demandent que tous les gardiens participent à leur niveau à l’effort de guerre, dans la zone concernée, ce qui veut dire toute la planète pour le code « apocalypse X ». Aucun de ces deux codes n’a jamais utilisé, et ne sont que théorique, tout comme les menaces qui pourraient les provoquer.

## Les Ombres

*Les Ombres* sont un mouvement d'espionnage international. Plus qu'un groupe organisé, ils sont une idéologie et une mouvance. Chaque pays à son organisation nationale faisant partie du mouvement des ombres, et ils n'ont pas de direction international.

Les ombres utilisent la magie pour assurer leur discrétion, et ont comme vision de collecter le plus d'information possible, mais de ne jamais les révéler sauf en cas de besoin.
