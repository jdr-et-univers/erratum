---
layout: layouts/base.njk
title: Les moines
tags: Guildes
models:
  - Guildes
  - Guildes métaphysiques
parent: Les guildes
---

# Les moines

Les moines de la force sont une ancienne guilde qui consiste à améliorer sa propre force physique en injectant de la magie dans ses propres poings.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Sac à PV | +1 PV pour chaque stack de PV obtenu |

## Effet des moines

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Poings enchantés | Peut, à la place d'une compétence d'art martiaux, peut apprendre un effet d'arme qui pourront être appliqué sur ses points durant un combat, pour 5 éclat. Non-cumulable. <br />+2 aux poings |