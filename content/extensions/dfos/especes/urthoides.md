---
layout: layouts/base.njk
title: Les Urthoïdes
tags: Espèce DfOS
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les Urthoïdes

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Insectes | Nombreux | Ovipare | Aucun | 45 ans |

Une espèce venant d'Alpha Centauri Ab, plus précisément d'Anthropax.

Les Urthoïdes sont une espèces insectoïdes vivant sur des planètes avec de fort taux en arsenic. L’atmosphère terrestre leur est fatale sans des appareils spécifique. Doté de carapace solide et de pince coupante, ce sont de formidable combattant. Leur technologie est principalement fondée sur les biotechnologies. Population très nombreuse à la reproduction rapide, leur patrimoine génétique est modifié suivant leur rôle dans la société.

Cette espèce est une espèce connue pour avoir été particulièrement expansionniste, mais étant peu dans les 4 systèmes principaux (Sol, Alpha Centauri, Tau Ceti et Epsilon Eridani), à part Alpha Centauri, s'étant surtout étendu sur les "systèmes sauvages".

Les Urthoïdes ont une individualité, mais pas véritablement de notion de "soi" comme étant opposé au groupe. Cela dit, leur individualité s'exprime dans l'une des plus grandes importances de leur civilisation : le jeu. Le jeu est chez eu la forme ultime de l'art, celle qui a partir de règle permet d'inventer des expériences. Chez les Urthoïdes, trouver des règles de jeu amusante est encore plus haut que les arts graphiques ou auditifs. L'humour est aussi très important, mais l'humour urthoïdes est souvent difficile à comprendre chez les autres espèces. Ceux-ci en sont parfaitement conscient... et du coup le font encore plus.

L'amusement et l'humour sont donc deux des vertues les plus importantes chez les urthoïdes.


| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 14 | 8 | 8 | 4 | Pince (3) | HAB, CON, DIS (55) | CHA, VOL (45) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Le pouvoir du jeu | VOL/2 pour résister à un défi ou un jeu |
| Exosquelette | Blessure font x2 dégats, mais armure +2 |
| Jet de venin (2 tours de cooldown) | Attaque à distance spécial à 4 dégats qui à l'élément poison |