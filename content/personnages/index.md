---
layout: layouts/home.njk
eleventyNavigation:
  key: Personnages
  order: 1
---

# Personnages

Les personnages sont le coeur d'un JDR. Cette catégorie décrit le processus de création d'un personnage, ainsi que quelques éléments qui vont influencé comment il va fonctionner (ses stats, etc).

## Pages dans cette catégories
