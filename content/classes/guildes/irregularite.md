---
layout: layouts/base.njk
title: Les irrégularités
tags: 
  - Guildes métaphysiques
  - L'anomie
models:
  - Guildes
  - Guildes métaphysiques
  - L'anomie
parent: Les guildes
---

# Les irrégularités

La *guilde des irrégularités* est un groupe de personne atteinte de l'anomie qui ont décide de l'utiliser comme source de pouvoir. S'ils ont acquis une meilleurs résistence à l'anomie, ils vivent cependant sur un temps empreinté, puisque chaque utilisation d'anomie leur fait monter leur taux d'anomie. C'est également ce dont ils ont besoin afin de gagner en puissance.

## Critères d'entrées

- Avoir de l'anomie
- Demande de passer une nuit pour rencontrer le "recruteur"

## Pouvoirs de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Ami de l'anomie | Augmente de +5 le niveau d'anomie maximum avant de devenir une abbération. Chaque niveau obtenu augmentera cette limite aussi de +5. Cependant, au dessus de 100 d'anomie : -1 armure magique tout les 5 points, -1% VOL et SAG tout les 1 points  |

## Liste des compétences

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Frappe anomique (VOL/CHA) | Une attaque frappant physique et mental avec un effet d'anomie. +1 Anomie |
| 2 | Malédiction anomique (VOL/CHA) | Fait faire un jet d'anomie. +1 Anomie |
| 3 | Armure d'anomie (trait) | Toute l'anomie provoqué par des jets d'anomie est baissée de 1.
| 4 | Terre anomique | Produit un terrain d'anomie. +1D4 Anomie |
| 5 | Bouclier d'anomie | Peut créer une défense anti-anomie sur un personnage, qui sera immunisé de l'anomie jusqu'à la fin de la partie. +1D6 Anomie |
| 6 | Forme abbération | Voir section |

## Pouvoir ultime

*Le pouvoir ultime peut être pris quel que soit le niveau, à partir du moment ou le personnage à > 100 anomie*

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
|   | Pacte avec le dieu ancien | Se retrouver lié au dieu de l'anomie. C'est sans doute une mauvaise idée. Voit ses PE divisé par deux définitivement. Entends des murmures venant d'un autre monde qui l'influencent. Est immunisé à la transformation complète en abbération et échouer un jet de survie ne fait "que" diviser par 2 ses PV. |

## Transformation en abbération

A partir du niveau 6, il est possible de temporairement se transformer en abbération. C'est très dangereux, mais très redoutable. Le personnage à le droit au pouvoirs suivants. L'effet est proche d'une prise de contrôle du corps par un dieu.

- Stats "divine", à le droit à `+1D4+2 × 10` à toutes ses statistiques (et peut dépasser les limites de stats)
- Toutes ses armures x2
- Prend une forme horrible faisant 1D4 dégats mentaux à ses alliés par tour
- Eclats infini
- Toutes les magies font un effet d'anomie
- Le personnage se prend 1D6+2 anomie par tour