---
layout: layouts/base.njk
title: Ondines
tags: Espèces additionnelles
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Ondines

Les ondines sont des humains s'étant rapproché du poisson par convergence évolutive - sans doute amplifiée magiquement par l'Océan lui-même. Ce peuple des mers, suiveuses de l'Océan, se divisent grandement entre les ondines nomades et sédentaires. Chez les ondines, on genre les groupes au féminin au pluriel si y'a une femme ou plus.

Les ondines sédentaires vivent dans de grandes cités sous-marines, et s'occupent peu des affaires de la surfaces, tandis que les ondines nomades forment des tribus arpentant les océans.

Elles sont amphibie, capable de résister sous l'eau et étonnamment immunisée à la pression de l'eau. Cependant, comme les loup-garou, elles ont une espérence de vie plus faible que les êtres humains, se rapprochant des 65 ans.

Longtemps considérée comme étant à la limite entre "l'humanité" (au sens large) et l'animalité, elle n'ont vraiment des liens diplomatiques avec les états humains que depuis la révélation du secret magique.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 16 | 16 | 10 | 4 | Griffes (2) |REL, CON (60) | HAB, CHA, PER (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Hydrophile | Respire sous l’eau et +15 en HAB, PER et FOR dans l’eau + N’est pas gênée par la brume. | 
| Désechemment | jette 1d4 dégat sous zénith ou sous fort soleil. |
| Vis sous l’eau | Dans la brume, jette un d4 pour regagner des PV par tour. D6 sous l’eau. |
| Conducteur | Se prend 2x dégats par la foudre. |