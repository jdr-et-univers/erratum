---
layout: layouts/base.njk
title: Alpha Centauri
tags: Autre systèmes
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Alpha Centauri

Alpha Centauri est un système triple d'étoile situé à environs 4 années lumières de la Terre. C'est l'une des systèmes stellaire du groupe local. C'est un système aux populations relativement nouvelles, les Centaurien et les Urthoïdes étant vu comme des "peuples jeunes".

Les civilisations centauriennes ont développé très tôt le voyage interplanétaire, en grande partie de par la manière dont fonctionne leur système planétaire.

## Les étoiles

Le système est composé de trois étoiles : Alpha Centauri A (aussi nommé Rigil Kentaurus) et Alpha Centauri B (aussi nommé Toliman), qui sont les deux étoiles les plus brillantes.

Alpha Centauri C, aussi nommé Proxima est la plus petite étoile du groupe, une naine rouge avec plusieurs planètes autour.

## Les planètes

En tout 6 planètes se trouvent autour des différentes étoiles d'Alpha Centauri. Cette proximité est ce qui a permis aux Centaurien de rapidement développer le voyage interstellaire, ayant des objectifs plus viables de voyage.

### Alpha Centauri Ab

( Note : dans notre monde, l'existence de cette planète n'est pas confirmé )

Une planète gazeuse cité à 1.1 unité astronomique (soit 1.1× la distance terre-soleil). Cette planète possède la particularité d'avoir de nombreuses lunes vivables, ayant des climats assez différents, mais souvent peu différencié à l'intérieur même d'une lune.

Les Centauriens y ont construit des villes depuis environs 200 ans.

Un de ces satellites est nommé Anthropomax, et est l'origine du peuple des Urthoïdes, qui de par des liens avec les Centaurien, découvrira les technologies spatiales et commenceront à former leurs propres voyages.

### Alpha Centauri Bb

( Note : dans notre monde, l'existence de cette planète est sans doute infirmée )

Cette planète est une planète chtionniene présentant tout le temps la même face au soleil. Elle a une orbite de 3 jours terrestre, et une température de surface atteignant les 1000 °C, provoquant l'existence de magma même en surface.

Cette planète n'est pas peuplée.

### Alpha Centauri Bc

( Note : dans notre monde, l'existence de cette planète n'est pas confirmé )

Alpha Centauri Bc est une planète rocheuse, désertique et sans athmosphère, situé très proche de son étoile. Cette planète est cependant extrêmement riche en minéraux, et contient plusieurs sondes qui servent à explorer son contenu.

La planète est peuplé de nombreux exodomes reproduisant le climat de Centauri Prime.

### Proxima Centauri d

Proxima Centauri d est la moins massive et la plus proche de son étoile des planètes d'Alpha Centauri.

### Centauri One (Proxima Centauri b)

Centauri One est la planète d'origine des Centaurien. C'est une planète multi-culturelle (et multi-ethnique depuis l'arrivée de nombreux Urthoïdes). 

La planète a une orbite de 11 jours, et un puissant champ magnétique la protégeant des éruptions solaires. Les Centaurien et les Urthoïdes vivant sur la planète ont achevé il y a 30 ans la construction d'un grand bouclier renforçant le champ magnétique, pour prévenir le risque d'une expulsion de l'athmosphère dans 80 ans. C'est aussi la planète d'origine du *Spectre*.

La planète est plus froide et plus froide que la terre, avec une température moyenne de 10 °C.

### Proxima Centauri c

( Note : dans notre monde, l'existence de cette planète n'est pas confirmé )

Cette planète est une super-terre orbitant autour de Proxima Centauri. La planète n'est pas habitable, avec une température moyenne de -234°C.

La planète possède peu d'activité, et n'a jamais eut de base à cause de sa forte pesanteur. Elle possède 3 lunes, doit une entièrement méchanisée par les Centarien nommée Mekanicus, qui sert de base externe au système stellaire.