---
layout: layouts/home.njk
---

# Fiches téléchargeable

Voici une petite liste de fiches téléchargeables pour pouvoir gérer vos personnages plus facilement. Elles sont disponible au format odt et pdf (potentiellement plus tard des fichiers tableurs ayant des formules utiles pour pouvoir gérer tout seul une partie de vos trucs)

## Format .ODT

- [Fiche de Personnage](/fiches/fiche-personnage.odt)
- [Fiche de Créature](/fiches/fiche-creature.odt)
- [Fiche de PNJ](/fiches/fiche-pnj.odt)
- [Fiche de Suiveur](/fiches/fiche-suiveur.odt)
- [Fiche de Véhicule](/fiches/fiche-vehicule.odt)

## Format .PDF

- [Fiche de Personnage](/fiches/fiche-personnage.pdf)
- [Fiche de Créature](/fiches/fiche-creature.pdf)
- [Fiche de PNJ](/fiches/fiche-pnj.pdf)
- [Fiche de Suiveur](/fiches/fiche-suiveur.pdf)
- [Fiche de Véhicule](/fiches/fiche-vehicule.pdf)
