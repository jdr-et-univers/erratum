---
layout: layouts/home.njk
eleventyNavigation:
  key: Lore et univers
  order: 6
---

# Lore d'Erratum

Erratum est un univers de JDR, mais il vise à avoir un lore qui se veut riche et intéressant à découvrir. Le but de ces pages sera de vous présenter ce monde, même si une partie des informations de lore pourront se trouver dans différentes autres pages. N'hésitez pas à explorer tout ce rulebook pour découvrir plus sur l'univers et ses secrets.

## Pages dans cette catégorie

- [Les lieux d'Erratum](monde/) - Présente les différents lieux et endroit à visiter dans ce monde
- [Les organisations et factions](organisations/) - Présente les différents groupes qui s'y trouve