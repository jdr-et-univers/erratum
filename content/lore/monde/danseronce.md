---
layout: layouts/base.njk
tags: Établissements scolaires
title: DanseRonce
models:
  - Lieux magiques
  - Établissements scolaires
parent: Lieux importants
---

# Le pensionnat de DanseRonce

Le pansionnat de DanseRonce est un petit pensionnat pour enfants « étranges » se trouvant dans la petite ville de Ergué Plouernec en Bretagne profonde. Un petit pensionnat né d'un ex-couvent, qui n'aurait rien eu de particulier si ce n'était par pur hasard un rôle important lors de l'ancienne *guerre des vertues*. À partir de cela, le pensionnat est devenu un lieu de plus en plus chaotique, provoquant moult déboire à la petite ville qui l'abrite, ainsi qu'à son grand rival, le lycée privé George Pompidoux.

Ce pensionnat est aussi connu pour la quantité de louveteau-garou abandonné s'y trouvant, victime de la destruction progressive des clan de loup-garou sous la pression de la société.