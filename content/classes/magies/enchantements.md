---
layout: layouts/base.njk
title: Les enchantements
tags: Magies Rares
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# Enchantement

Les enchantements sont une magie plutôt rare, consistant à appliquer des effets magiques sur des objets différents, où a modifié les propriétés de la matière.

Cette magie se fonde sur les circuits de magie existant partout, formé par la circulation naturelle de l'éclat dans tout ce qui existe.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Détection d’enchantement (PER)  |  Peut détecter et reconnaître un enchantement.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Enchantement élémentaire Niv1 (3 Eclat)  |  Peut ajouter les effets élementaire Eau, Feu, Terre et Air à une arme.  |
| 1 |  Pacte d’annulation  |  Peut annuler ou remplacer tout enchantement qu’il a fait, sans aucun jet. A également la capacité d’annuler tout enchantement d’un allier qui lui donne explicitement le droit de le faire.  |
| 2 |  Rupture de charme (HAB)  |  Peut supprimer un enchantement déjà reconnu. Malus de difficulté suivant le niveau.  |
| 2 |  Sabotage (PER+HAB)  |  Peut faire un jet d’enchantement pour tenter de saboter un appareil entièrement technologique avec la magie.  |
| 2 |  Piège élémentaire  |  Peut prendre un tour pour enchanter une partie du sol pour créer un piège et le poser à un endroit. Si un ennemi arrive à proximité, il se prend 1D6 dégats + effet élémentaire.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Amélioration d’arme (HAB, 3 Eclats)  |  Augmente le dé utilisé par une arme pendant 3 tours (non-cumulable)  |
| 3 |  Renforcement (HAB, 3 Eclats)  |  Rajoute à une armure pendant 3 tours la moitié de ses resistances.  |
| 3 |  Enchantement élémentaire Niv2  (3 Eclat)  |  Peut ajouter les effets élementaire Foudre, Metal, Vegetal et Glace à une arme.  |
| 4 |  Boucli-arme (HAB, 2 Eclat, dure trois tours)  |  Permet de rendre une arme utilisable comme bouclier.  |
| 4 |  Cadenas magique (INT+HAB)  |  Peut tenter de bloquer une porte pour empêcher qu’elle soit ouverte.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Déverrouillage magique (PER+HAB)  |  Peut ouvrir toutes les portes et les coffres non-magiquement scellées.  |
| 5 |  Auto-enchantement (3 Eclat)  |  Permet de faire un enchantement élémentaire non pas sur une arme mais sur son propre corps. Effet à voir avec le MJ.  |
| 5 |  Non-conservation de la matière  |  Peut augmenter ou diminuer de moitié le poids d’un objet dans ses modifications  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Ré-assemblage  |  Peut modifier les caractéristiques moléculaires d’un objet inanimé (soumis à validation MJ)  |
| 6 |  Enchantement élémentaire Niv3  (5 Eclat)  |  Peut ajouter des enchantements de Temps, d’Espace, d’Ombre et de Lumière à un objet.  |