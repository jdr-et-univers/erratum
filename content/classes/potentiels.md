---
layout: layouts/base.njk
eleventyNavigation:
  key: Les potentiels
  parent: Classes et experiences
  order: 7
---

# Les potentiels

Le potentiel est la manière dont les traits de personnalités de personnages peuvent devoir source d'un pouvoir qui leur permet de dépasser leur limites. Il se débloque de manière RP, à partir d'interactions avec certains personnages capables de le faire. Un potentiel est une transformation de trait de personnalité, et le joueur doit choisir quel trait de personnalité devient potentiel.

Il existe deux niveaux de potentiels : le **potentiel** et le **potentiel divin**.

Le potentiel permet de dépasser la limite de statistiques de 80 aux statistiques, ainsi que d'obtenir des pouvoirs supplémentaires lié aux traits de personnalités. Le potentiel divin demande des contreparties.

## Obtenir le potentiel

Les potentiels s'obtiennent de manière purement RP, via des interactions avec des personnages particuliers.

- **Potentiel** : Peut s'obtenir de la part de chef angéliques, démons supérieurs, dieux, 
- **Potentiel divin** : Peut s'obtenir de l'un des quatres dieux anciens où d'entités puissantes présente dans le noumène ou dans certains autres lieux métaphysiques. Un dieu du néant peut aussi, mais rajoutera ses propres contreparties.

## Pontentiel simple

Le potentiel simple touche un premier trait de personnalité et lorsqu'une vous avez une personnalité transformée en potentiel, vous obtenez les aventages suivants :

* Les compétence améliorée voient leur limite de stat maximum passer à 120

* Le personnage débloque un pouvoir de potentiel unique.

## Pontentiel divin

Le potentiel divin est considéré comme le moyen de pouvoir affronter les dieux, pour un prix souvent fort. Le potentiel divin touche un deuxième trait de personnalité et lorsqu'une vous avez une personnalité transformée en potentiel, vous obtenez les aventages suivants :

* Les compétence améliorée voient leur limite de stat maximum passer à 120, de 150 si elle avait déjà été améliorée dans la compétence.

* Le personnage débloque le pouvoir de potentiel du trait de personnalité touché.

* Permet de passer le filtre de destinée des dieux et à moitié l'armure divine.

* Le personnage obtient le pouvoir Évhèmère : iel peut sacrifier un *point de karma* pour temporairement passer sa limite dure d'une de ses stat à `65535`, ou baisser celle d'un dieu à `255` pendant un tour (se base sur les stats améliorées par le potentiel).

> **Note:** Il est possible lors de l'accès au potentiel divin de reprendre d'utiliser comme second potentiel celui déjà utilisé en tant que potentiel. Cela possède des aventages au niveau des stats, mais des inconvéniant en terme de pouvoir, comme vous pourrez le voir ci-dessous.

### Contrepartie

Le pouvoir du potentiel divin à un coût : permettant de dépasser les limites du monde, et de dépasser les destinées causées par les dieux. Le personnage doit donc accepter une des trois contreparties suivantes, fonctionnant de manière proche d'une malédiction antique, puisqu'elles vont tordre la réalité autour du personnage.

- Le **destin de l'oublié** consiste à accepter de perdre l'aventage et la gloire que donne le potentiel divin. Le personnage sera régressé à un niveau entre 5 et 10, ne pourra garder que deux classes, et sa contribution sera globalement oubliée par le "grand monde".
- Le **destin du héros** fait que le personnage sera condamné à être poursuivi par la gloire et la grandeur. Iel vivra une vie de danger permanent, pour iel et ses proches. Ses ennemis reviendront parfois, et iels disparaitront souvent au combat.
- Le **destin du banni** permet d'avoir le beurre et l'argent du beurre, mais à un prix. Iel pourra garder pouvoir, et sera plus ou moins preservé des dangers de la gloire… mais devra partir vivre loin du monde des mortels/humains.

Ces contreparties commencent à partir du moment ou *la quète du personnage est finie*.