---
layout: layouts/base.njk
title: Les boucliers
tags: Les équipements
models:
  - Les équipements
parent: Les équipements
---

# Les boucliers

Les boucliers permettent de diminuer les dégats si un jet d'encaissement est réussi, en ajoutant leur puissance de protection à l'armure physique ou magique du personnage.

| Nom | Type | Mains | Effet | Force | Prix |
|:------:|:------:|:------:|:------:|:------:|:------:|
| Bouclier simple | Bouclier | 1 | | 1 | 100 |
| Bouclier renforcé | Bouclier | 1 | | 2 | 200 |
| Bouclier lourd | Bouclier | 1 | Besoin de >2 niveau de réussite pour encaisser/esquiver | 4 | 400 |
| Pavois | Bouclier | 2 | Besoin de >3 niveau pour encaisser/esquiver | 6 | 550 |

<!-- tabs:start -->

## Effets

Les armes peuvent avoir un effet supplémentaire.

| Nom| Effet| Surcout |
|:------:|:------:|:------:|
| Améliorée | x1.5 protection | +50% |
| Supérieur | x2 protection | +100% |
| Ultime | x3 protection | +200% |
| Léthal | Double les chances de critique | +50% |
| Vorpale | Les critique font x3 au lieu de x2 | +100% |
| Consolidé | Effet du brise-armure divisé par 2 | +25% |
| Miroitante | Renvoie la moitié des dégâts spéciaux infligés | +50% |
| de verre | Double la protection, mais ne peut être utilisé que deux fois | +10% |
| À accroche | À besoin d'une main de moins | +10% |
| Élémentaire* | Produit l'effet de l'élément lié | +25% |
| des Âmes | -2 armure mentales, cependant +20% de chance de réussir les esquive mentales | +25% |
| Funeste | Provoque un jet de panique si réussite critique | +100% |
| De sang | -25% PV ; 1D6, si 1, l'ennemi fait un jet d'affliction physique | +100% |
| Des Héros | +50% protection sur les créatures démoniaques | +100% |
| Des Ombres | Fait 3 dégâts bruts à l'attaquant (même si a échoué) | +100% |
| Energétique | +50% protection sur l'armure spéciale | +100% |
| Nanotechnologie | Tout effet de brise armure s'estomp à la fin de la partie | +100% |
| Ferrofluide | Sur un jet d'HAB/2, peut prendre les effets d'un autre type d'arme. | +200% |

\*À noter que le brise-armure de la terre devient du *brise-arme*