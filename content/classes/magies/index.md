---
layout: layouts/base.njk
eleventyNavigation:
  key: Les magies
  parent: Classes et experiences
  order: 3
title: Les magies
models:
  - Magies Communes
  - Magies Rares
---

# Les magies

En plus de la magie naturelle des signes, les êtres vivants peuvent apprendre un certains nombres de pratiques magiques. Ces pratiques sont généralements divisées en deux grandes familles.

## Magies communes

Les magies communes sont le nom donnée aux magies n'ayant pas besoin d'un apport magique intérieure de la part de l'utilisateur. Ces magies sont particulièrement pratique si vous voulez faire un personnage qui n'est pas magicien à la base, puisqu'elles utilisent soit des forces extérieures, soit simplement la force et les compétences du personnage.

- Les [arts martiaux](artmartiaux/) sont l'art qui président l'utilisation de son corps au plus haut point, pour effectuer des attaques optimales.
- Les [invocations](invocations/) sont l'art d'empruter la magie naturelles d'être spirituels ou divins, généralement des démons.
- L'[herboristerie](herboristerie/) est l'art d'utiliser les forces magiques présentent dans les plantes.
- La [magie runique](runisme/) est l'art d'utiliser les écritures magiques telles que les runes.

## Magies rares

Les magies rares sont des magies tirant leur pouvoir de la force magique interne à l'utilisateur. Elles nécessites une certaine force magique pour être effectuées et ne sont pas accessible à tout⋅e⋅s.

- La [magie élémentaire](elementaire/) est la maitrise des éléments, et la capacité à les utiliser pour faire des sorts.
- La [magie créatrice](creatrice/) est l'art de créer de la matière, des constructions d'une matière nommée le sidérale.
- Les [enchantements](enchantements/) sont l'art d'ajouter des effets, des sorts incrustés dans des objets.
- La [magie spirituelle](spirituelle/) est l'art de communiquer avec les esprits et de les appeller à soi.
- La [magie naturelle](naturelle/) est l'art d'utiliser les forces magiques déjà présentent dans la nature.

## Les guildes

Les guildes sont des groupes magiques visant à perfectionner certaines magies. Ils ont généralement des pratiques favorites, et offrent des manières différentes, des approches alternatives à leur utilisation. Les guildes perfectionnent généralement deux ou trois magies, mais des utilisateurs d'autres magies peuvent en faire partie.

- Les [druides](../guildes/druides/) sont une guilde tirant leur pouvoir de la nature et des forces anciennes.
- Les [mages occultes](../guildes/occultes/) sont une guilde tirant le pouvoir des forces sombres et obscures
- Les [prêtres](../guildes/pretres/) sont une guilde tirant leur pouvoirs de leur lien avec les anges et la lumière.
- Les [sorcières](../guildes/sorcieres/) sont une "guilde" complètement chaotique faisant ce qu'elles veulent. Et tant pis pour les gens mécontant.
- Les [moines](../guildes/moines/) sont une guilde renforçant leur propre poings pour frapper.

### Guildes métaphysiques

Les guildes métaphysiques sont des guildes visant à plonger dans les secrets les mieux garder du monde, malgré les dangers que cela peut causer.

- Les [alchimistes](../guildes/alchimie/) cherchent à comprendre la réalité et à la manier au niveau le plus haut.
- Les [irrégularités](../guildes/irregularites/) sont un groupe de personne atteinte de l'anomie qui ont décide de l'utiliser comme source de pouvoir.
- Les [dévots](../guildes/devots/) sont les suiveurs des dieux, acquérant des pouvoirs de la part de leurs dieux protecteurs.
