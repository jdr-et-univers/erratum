---
layout: layouts/base.njk
title: Les mages occultes
tags: Guildes
models:
  - Guildes
  - Guildes métaphysiques
parent: Les guildes
---

# Les mages occultes

Les mages occultes, aussi appellé *mages noirs*, *occultistes* ou *mages sombres* sont des mages utilisants les magies puissantes. Cette guilde peut apprécier deux types de magies : les éléments et la magie shamaniques. Ces magies sont transformées par les forces occultes utilisées par les mages occultes pour devenir quelques chose de nouveaux et… pas mal plus dangereux.

Cette magie à une très mauvaise réputations de par sa dangerosité, et de par les actions néfastes qui ont été fait avec.

Ils apprécient les pouvoirs des éléments ou du shamanisme.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Fléau de la lumière | Lui ou les êtres sous son commandement font ×1.5 dégâts, et ont +5 de réussite critique face à toutes les créatures de lumière, d’énergie et d’ordre. |

## Nécromancie

La nécromancies est une transformation par la magie occulte de la magie *spirituelle*, qui en fait quelques choses.

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Nécromancie | Lorsqu'il invoque un mort sur le terrain, il peut l'invoquer sous forme de zombie ou de squelette. Il pourra attaquer, et aura des statistiques déterminées à ce moment. |

## Éléments occultes

Les éléments occultes sont une transformations de la magie élémentaire en rajoutant à la place des éléments de base un élément métaphysique.

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Éléments occultes | Peut à la place de prendre un élément classique prendre un élément occulte. L'élément occulte pourra être utilisé avec toutes les capacités élémentaires. |