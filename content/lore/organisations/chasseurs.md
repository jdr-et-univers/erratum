---
layout: layouts/base.njk
tags: Orgnaisations criminelles
title: Les chasseurs
models:
  - Organismes officiels
  - Sociétés secrètes
  - Orgnaisations criminelles
parent: Les organisations
---

## Les chasseurs

*Les chasseurs* sont un groupe criminelle présent partout à travers le monde. Il s'agit d'un groupe initialement composé de non-mage choqué de la révélation magique, ayant décidé de s'organiser en groupe armée pour se défendre contre « les créatures monstrueuses » magiques. Ce groupe se voit comme les chasseurs de dragons de jadis, et sont persuadés que les créatures magiques risque de finir par faire la guerre aux humains et les exterminer. Ils sont d'autant plus actifs contre les créatures considérées comme « presque humains » tels que les Lycanthropes et les Hybrides.

Ils fonctionnent de manière clandestine, et sont officiellement totalement illégaux et combattus. Leurs noms de codes sont généralement basé sur des grands chasseurs/héros/chevaliers... Aux USA, les noms de grands "héros" de l'ouest sauvage sont particulièrement appréciés.

Ils sont parfois employés comme mercenaires par des puissants pour des fins privées. Ils font également du trafique d'arme et de créatures magiques.