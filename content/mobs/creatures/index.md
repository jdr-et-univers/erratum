---
layout: layouts/base.njk
eleventyNavigation:
  key: Les créatures
  parent: Créatures et PNJs
  order: 0
title: Les créatures
models:
  - Informations créatures
  - Les créatures
---

# Les créatures

Dans tout JDR, les mobs sont une partie intégrante de ce qu'on peut rencontrer. Erratum utilise un système d'espèce pour construire ses créatures. Il est à noter que les créatures ne sont pas forcément des êtres sans intelligences, cela peut aussi concerner des créatures capables de raisons. Il s'agit ici de tous ce que vous rencontrerez qui ne seront pas des PNJs récurrants.

## Types de créatures

> **Attention:** Toutes les créatures reçoivent x2 (x4 si faiblesse à l'éclat) par les attaques d'anomie, sauf les êtres anomique qui reçoivent x1, et les DernierSouffle qui reçoivent x0.5.

<div id="dataTable">


| Créatures | Type | Spécificités | Faiblesses | Résistances | Immunité |
|:---------:|:----:|:------------:|:----------:|:-----------:|:--------:|
| Humanoïdes | Biologique | Peut manier des armes |  |  | |
| Bêtes | Biologique | Des griffes et des crocs | Metal, Chaos, Glace | Air | |
| Reptiles | Biologique | | Glace, Ombre | Feu, Lumière | |
| Aquatique | Biologique | Peuvent respirer sous l'eau | Foudre, Végétal | Eau, Feu | |
| Végétaux | Biologique |  | Feu, Metal, Poison | Eau, Végétal | |
| Insectes | Biologique | | Terre, Feu, Glace | Ordre | |
| Aerien | Biologique | | Air, Glace | Terre, Espace | |
| Machines | Non-biologique |  Ne sont pas soumis aux besoins vitaux normaux | Eau, Foudre, Chaos | Metal, Végétal, Énergie | Poison |
| Minéraloïdes | Non-biologique | Armure naturelle élevée | Eau, Temps | Foudre, Feu, Air | Poison |
| Créature de glace | Non-biologique | | Feu, Lumière, Énergie | Glace | |
| Volutes | Non-biologique | Créatures gazeuses | Végétal, Air, Foudre | Terre, Metal, Temps | Poison |
| Gelatineux | Non-biologique | Créatures liquides et semi-liquide | Metal, Vegetal, Son | Force, Poison, Eau | |
| Feufolets | Non-biologique | Créatures de feu | Eau, Terre | Glace | Feu |
| Démons | Magique | Liés au chaos | Ordre, Lumière | Chaos, Ombre | |
| Mort-vivant | Magique | Des corps maintenu par la magie | Lumière | Ombre | |
| Parasite | Magique | Peuvent parasiter d'autres créatures | Feu, Ordre | Végétal | Chaos |
| Calorique | Magique | Créatures d'énergie pure | | | Poison |
| Psychiques | Magique | Créatures avec une forte puissance mentales | Ombre, Force, Son | Esprit | |
| Âmes | Magique | Des êtres d'esprit, sans corps | Tout ce qui est métaphysique, Eclat | Son, Esprit | Tout ce qui est physique, Force, Poison |
| Titans | Paradoxal | Peuvent être lié élémentairement | *Divers* + Éclat | *Divers* | *Divers* |
| Effacés | Paradoxal | Liés au néant | Énergie, Éclat | Chaos, Ombre | Néant |
| Célèste | Paradoxal | Lié à l'ordre | Ombre, Chaos | Lumière, Énergie, Ordre | |
| Irrégularité | Anomique | Des êtres corrompus par l'anomie | *Divers* + Anomie | *Divers* + Eclat, Paradoxe | *Divers* |
| Aberration  | Anomique | Des êtres transformé par l'anomie | | Eclat, Paradoxe | Anomie |

</div>

## Créatures géantes

> **Attention:** Les informations données ici sont des informations générale, donnée à titre indicatif. Pour la création de boss géant, le joueur peut bien évidemment adapter cela.

Les créatures géantes sont des créatures très grandes. Mais genre très très grandes. Cela entraîne quelques particularités dans leur manière d'être gérée. En effet, les créatures géantes sont plus durables et plus puissante, mais ne peuvent plus esquiver les entités non-géantes (sauf pouvoir spécifique). Cela entraîne les règles et particularités suivantes:

- Une créature géante doit avoir 3 stack d'une affliction infligée pour qu'elle s'active
- Une créature géant ne peut pas esquiver les attaques de personnage ou véhicule non-géant
- Ses PV seront égal à environ 5 fois ceux équivalent d'une créature de taille "normale" (par exemple, un rat géant aura 100 PV au lieu de 20)
- Son armure et ses statistiques physiques sont généralement le double d'une créature normale.

Les joueurs peuvent bénéficier de l'aide de créature/forme géante, cependant pour des raisons d'équilibrage, une créature géante ne peut rester que trois tour en combat normal, si elle est invoquée face à des ennemis normaux.

## Troupes et meutes

Les troupes/meutes sont des groupes de créature ou PNJ fonctionnant comme un seul. Il s'agit d'un moyen de faire affronter des grands nombre d'ennemis plus facilement. Les troupes sont plus durable au début, mais s’affaiblissent au fur et à mesure qu'elles subissent des dégâts. Elles sont constitué d'un certain nombre d'ennemis de même type, ce qui augmentent les PV et leur nombre d'action de la manière suivante:

- Leur nombre de PV initial est égal au nombre de créature × le nombre de PV de base. Par exemple, pour un pack de six rat de 20PV aura 120PV en tout.
- De même, le nombre de créature restantes est lié au nombre de PV. Par exemple, pour le pack de six rat, si vous lui faite 43 dégats, deux rats seront vaincus.
- Lorsqu'elles font une action, chaque créature supplémentaire la fait également (genre si elles frappent, elles font 1 coup par créature), avec le même jet.
- L'adversaire les encaisse toute avec un seul jet (l'armure et l'encaissement s'appliquant sur chaque attaque séparément).
- L'adversaire doit esquiver chaque attaque séparément (avec un malus de 5% à chaque nouvelle attaque). Toute réussite critique esquive deux attaques.