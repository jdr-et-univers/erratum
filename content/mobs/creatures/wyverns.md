---
layout: layouts/base.njk
title: Les wyverns
tags: Les créatures
models:
  - Les créatures
parent: Les créatures
---

# Les Wyverns

Les Wyverns sont une espèce proche des dragons, mais bien plus petite. Ils vivent principalement dans les grandes plaines, et n'ont pas la vitesse ou la longévité de leurs illustres cousins. Leur longévité se rapproche plus des 80~100 ans. Les Wyverns produisent naturellement un champ d'énergie autour d'elle, protégeant aussi bien elles et leur cavalier des conditions météorologiques. Elles peuvent également emmagasiner de l'énergie solaire et la recracher sous forme d'énergie pure. De base, les wyvernes ne parlent pas, et son plus proche des animaux… Cependant, avec le temps, une wyvern peut apprendre la parole.

Elles peuvent être domestiqué par un personnage, et pourront devenir lié télépatiquement. Elles ont comme chaque PJ deux trait de personnalité, et des stats et pouvoirs de base. Elle gagnent aussi des niveaux lors des parties ou elles sont présentes. La wyvern est unique en ce qu'elles peuvent aussi apprendre des *compétences de classes de leur propriétaire*.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 18 | 10 | 8 | 6 | Griffe (2) | FOR, HAB, REL (60) | CON, SAG, DIS (40) | Le reste (50) |

## Pouvoirs de base

| Trait | Effet |
|:-----:|:------|
| Alignement élémentaire | Peuvent posséder un élément, qui leur activera des pouvoirs. Se prennent x2 dégat par l'élément opposé. |
| Peau écailleuse | Ont une armure de base à 2 en spécial et physique |

## Pouvoirs de level-up

### Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 | Souffle élémentaire (FOR, 1 éclat)  | Peut faire une attaque élémentaire frapant sur l'armure magique  |
| 1 | Liens affectif | Peut retrouver la position de son maitre naturellement (sauf protection magique) |
| 2 | Long courrier | Ne se prennent aucun malus de fatigue sur les longs voyages |
| 2 | Monture | Sur un jet de HAB, permet au maitre d'arriver à temps dans un combat. |
| 2 | Renforcement élémentaire  | Le souffle élémentaire fait +2 dégats |

### Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 | Copie de posture  |  Peuvent passer automatiquement dans la posture de leur propriétaire |
| 3 | Télépathie |  Forme un lien télépathique avec son maitre. |
| 3 | Vol (2 éclat) | S'envolent le premier tour, attaque le second. Hein quoi ça vous rappelle quelque chose ? Si le propriétaire est sur la wyvern, pourra participer à l'attaque |
| 4 | Monture qui pète la classe | À l'arrivée en combat via *monture*, provoque une attaque d'opportunité mentale pour la wyvern et son maitre |
| 4 | Ambidexgriffe  | Attaque une deuxième fois avec FOR/2 quand utilise ses griffes |

### Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 | Je ne suis plus timide | Peut parler et faire comprendre ses idées sur un jet de REL. En cas de stress, le jet est divisé par deux |
| 5 | Double typage | Peut obtenir un deuxième type. À noter que le second type ne compense pas la faiblesse élémentaire. |
| 5 | Renforcement élémentaire | Le souffle élémentaire fait +2 dégats |

### Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 | Protection élémentaire | La wyvern (et son maitre si sur elle) est immunisé aux terrains élémentaire de ses éléments |
| 6 | Immunité élémentaire | Se prend x0 dégats à son élément principal |