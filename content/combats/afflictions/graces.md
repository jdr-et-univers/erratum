---
layout: layouts/base.njk
tags: Afflictions communes
models:
  - Afflictions communes
  - Malédictions
title: Les grâces
parent: Les afflictions
---

# Les grâces

Les grâces sont des effets positifs durant *une partie* et qui peuvent aider potentiellement votre personnage durant la partie. Certaines se consomment, d'autre sont passives. Voici les différentes graces qui existent, aux nombres de douzes. Les grâces peuvent aussi obtenues principalement par le jet de bénédiction, détermination ou par des évenements durant une partie.

<div class="table-auto">

| id | Nom de la grace | Pouvoir de la grace |
|:--:|:---------------:|:--------------------|
|    | Grace de la royauté | Tout les échecs critiques en charisme deviennent des réussites critiques. |
|    | Grace de la mort | Ne mourra pas durant la partie. |
|  1 | Grace de la protection | Le personnage peut annuler jusqu'à trois échecs critiques d'autres joueurs. |
|  2 | Grace de la guerre | Les dés de vigueur, jet et mental montent d'un cran. |
|  3 | Grace de la plaisance | Le personnage peut réussir automatiquement un jet de social. |
|  4 | Grace de la fortune | Le personnage aura le droit à un bonus de 20% de l'argent qu'il aura gagné durant la partie. |
|  5 | Grace de la victoire | Le personnage pourra choisir un ennemi (hors boss) qui abandonnera immédiatement un combat, avec toutes les récompenses de s'ils avaient gagnés. |
|  6 | Grace du soleil | Le personnage gagne +10 % à toute ses statistiques pour la partie. |
|  7 | Grace de la forge divine | Le personnage gagne un jet de loot bonus. |
|  8 | Grace de la pureté | Peut renvoyer une affliction morale reçue. |
|  9 | Grace de la catastrophe | Le personnage peut provoquer un échec critique. |
| 10 | Grace de l'abondance | Le personnage possède moitié plus de PV durant la partie |

</div>

Lors d'un **jet de bénédiction**, le personnage jette un *dé 12* et prend dans le tableau le résultat du dés. S'il fait 11, il regagne tout ses PV et PM, s'il fait 12 il pourra à un moment de la partie restaurer entièrement *toute sa capacité à faire des actions* (éclat, cooldown, actions limité par partie, etc).
