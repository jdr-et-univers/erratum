---
layout: layouts/base.njk
eleventyNavigation:
  key: Émotions
  parent: Créatures et PNJs
  order: 3
---

# Émotions et attitude

> **Attention:** Ce système d'émotion ne remplace pas le RP, et est ouvert aux interprétations. Si ces jets peuvent donner des "grandes lignes", l'interprétation fine doit être faites par les rôlistes. De plus, si votre personnage est affecté par une émotion en RP, vous n'avez pas besoin de la mapper avec ce système sauf si jamais un autre personnage utilisant ce système agit.

Les PNJ et les créatures peuvent avoir des comportements différent suivant les situations, et peuvent être régis par diverses émotions. Ces émotions peuvent influencer comment les mobs agissent en combat, et peuvent être utilisée pour influencer le RP des joueurs dans certaines circonstances (genre via des compétences spéciale).

**Le RP à l'ascendant sur ce système** et peut le régir. C'est à dire que si les joueurs réussissent une action qui peut faire changer l'émotion d'un être (attaquer quelqu'un de joyeux, réussir à consoler quelqu'un de triste), l'émotion peut changer (parfois avec besoin d'un jet, avec peut-être difficulté adéquate). Ce système sert surtout à régler les créatures et les pouvoirs de certains signes (capricorne, etc).

## Effets des émotions

Les émotions peuvent avoir un certains nombres d'effets sur le PNJ, voir le joueur si jamais il se retrouve "pris" dans ce système. Evidemment, cela influence en premier lieu le RP et le comportement, mais cela peut avoir un certains nombre d'effet "gameplay". Les effets gameplay peuvent être mitigé par les joueurs si c'est sur leur perso soit via des raisons RP (tout simplement), soit par des jets de SAG ou VOL.

- Fait privilégier certaines actions en combat. À noter qu'il s'agit ici d'une *préférence*, qui sera plus ou moins forte suivant les circonstances.

- Fait privilégier certaines afflictions morales (2 à 5 chances sur 8 suivant la force de l'émotion) plutôt que le jet normale. A noter qu'au niveau **violente**, il est possible d'ajouter des risques de jet d'affliction morale aléatoire.

- Peut être plus simple ou difficile à influencer, voir à domestiquer. Cette simplicité/difficulté à influencer peut affecter l'aggro, etc.

## Tableau des émotions

> **Note:** La domestication/le recrutement indiquée ici est si vous êtes le sujet de l'émotion. Sinon, elle dépendra de comment vous réussissez à vous liez avec. Cependant, plus l'émotion est forte, plus il vous sera difficile de domestiquer/recruter.

Ce tableau des émotions offre. Il est à noter que ces émotions ont une granularité assez "grossière" dans le sens ou on peut ajouter des indications (pourquoi la créature et/ou le PNJ est joyeux/triste/en colère) qui influencera. Si un animal ou une personne n'est pas en colère contre vous, on peut imaginer que sa colère sera moins un soucis, voir même peut-être une aide pour vous.

<div class="table-auto">

| Emotion  | Dé | Effets | Influence | Domestication / Recrutement | Préfère en combat | Afflictions morales |
|:--------:|:--:|:-------|:---------:|:-------------:|:-----------------:|:-------------------:|
| Joie/Confiance | 1 | L'être sera plus à l'écoute.<br />Peut être moins perceptif de certaines choses. | Simple | Facile | N/A | Amour, Inoffensif |
| Indifférence | 2 | Ne s'intéresse pas à la situation.<br /> Peut être concentré sur un aspect particulier de celle-ci. | Normale à Difficile | Complexe | Se concentre sur autre chose si possible | Égoïsme, Méfiance |
| Colère/Agressivité | 3 | N'est pas content, plus de chance d'attaquer.<br />Sera moins sage et à l'écoute. | Simple à Très Difficile | Difficile | Attaque aggressive, aggro facilement, peu de stratégie | Confusion, Bersek |
| Tristesse | 4 | Va être plus porté sur l'objet de sa tristesse.<br />Peut évoluer en une autre émotion facilement | Complexe à Très Difficile | Difficile | Très variable | Méfiance, Boarf contagieux, Inoffensif |
| Peur | 5 | Va tenter de fuir/d'éviter le sujet de la peur.<br />Peut être influençable par qq1 dont iel a pas peur. | Simple à Très Difficile | Difficile | Fuite, Défense, Attaque pouvant le protéger.<br />Peut se retrouver stunt par la peur. | Toutes, mais forte chance de fuir |
| Surprise | 6 | Évolue très vite en autre chose, agis peu quand ça arrive. | Simple, mais aléatoire | Difficile | Toute, risque fort de ne pas agir. | Confusion, Bersek, changement d'émotion soudain |
| Observation | 7 | Est plus attentif, remarque plus ce qui se passe autour.<br />Peut être méfiant. | Difficile | Difficile | Observe, se protège beaucoup | Méfiance, Égoïsme |
| quoi. | 8 | Tout est possible.<br />Bonne chance. | Simple, mais aléatoire | ??? | Actions aléatoires | Toutes |
| Neutre | 9+ | Pas d'effet particulier. | Normale | Normale | N/A | Toutes |

</div>

### Niveau d'émotion

A l'émotion peut se rajouter un niveau d'émotion. Ce niveau d'émotion aura pour effet de rendre plus complexe les jets opposé à l'émotion (genre si un membre à une émotion de colère) et les jets pour tenter de lui faire changer d'émotion. En RP, plus le niveau est fort, plus on peut imaginer l'individu comme affecté par les effets de l’émotion.

Il existe quatre niveau d'émotions (**Normale**, **Forte**, **Très forte**, **Violente**) qui corresponde à une augmentation de la difficulté des actions contraires à l'émotion (+0 à +3), ou à son changement. Au contraire, ce niveau peut aussi augmenter la facilité d'influence allant dans le sens de l'émotion. Si le modificateur dépasse hardcore/simplissime, il peut atteindre les niveau inratable ou impossible (ou leur quasi).

Le RP peut également influencer les modificateurs.

### Sans Esprit

Un "sans-esprit" est le nom donné à une créature sans personnalité (ombre, robot automatique, créature métaphysique, énergie…) dont l'attitude sera réagit par un programme ou quelque chose de "magique". Les monstres fonctionnent comme les créatures d'un point de vue "fiche", mais n'ont pas de jet de personnalité.

Les sans-esprit ne peuvent pas être affecté par les PM, les émotions ou le social. Certains auront même la tendance à n'agir que de manière aléatoires. Ils ne peuvent pas être domestiqué ni recruté.

### Jet de rencontre

Lors d'une rencontre complètement aléatoire (ou dans certaines circonstances), les ennemis peuvent faire un jet de rencontre, ce qui peut affecter leur émotion au début du combat. Le MJ décide un dé (D10 à D20), et prend les résultats avec le tableau des émotions (Les émotions à 9 ou + seront "neutres").

Dans certains cas, le MJ peut décider d'une "émotion par défaut" qui remplacera "neutre" dans les émotions > 9.

## Influence

Il est possible pour des personnages d'avoir une influence sur les mobs autour, pour changer leur attitude ou profiter de leur émotions. Pour cela, le joueur peut tenter un *jet d'influence*, avec du CHA, DIS ou du REL suivant l'attitude que le joueur veut faire attendre au PNJ/à la créature qu'iel vise (qui résistera sur sa VOL ou REL inversé suivant la situation).

Changer d'émotion est toujours plus dur qu'influencer pour, et si l'émotion s'oppose à la précédente, cela passe directement au niveau le plus difficile d'influence possible.
