---
layout: layouts/base.njk
eleventyNavigation:
  key: Dragons from Outer Space
  parent: Extensions et AU
  order: 1
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
title: Dragons from Outer Space
---

# Dragons from Outer Space

> *Il y a longtemps, la grande calamité s'est abattue sur nous. On raconte qu'avant elle, nos ancêtres voyageaient librement entre les planètes, sans craindre les porteurs de désastres. Aujourd'hui, seuls nous, les équipages des Arches peuvent encore voyager. Nous sommes les liens du système de Dagda*

*Dragons from Outer Space* est une extension d'Erratum amenant un aspect space-fantasy, mélangeant des éléments de high/heroic fantasy dans un environnement de space-opera. L'histoire dépasse la Terre (qui est globalement celle d'Erratum) et ajoute du voyage interplanétaire pour aller dans le système solaire et les systèmes alentours.

Les quatres anciennes civilisations ont exploré il y a plus de 10000 ans le système solaire et ont construite diverses colonies et ruines anciennes. 

Un élément importants de cette extension sont les *dragons stellaires* des créatures spatiales de plusieurs kilomètres se nourrissant de l'énergie des étoiles et les *wyverns*, *kobolds* et *dragons* y ont naturellement le pouvoir de voler dans l'espace et de se protéger du vide, protégeant aussi celleux qui monteraient dessus. 

Le but de cet univers est d'avoir une orientation très "pulp", offrant la possibilité de mélanger des tropes de divers univers, avec des inspirations à la fois de science-fiction à la Pern, de space-opera et de High Fantasy plus classique.

## Le système solaire

Il y a eut quelques étapes de début de colonisation du système solaire à l'époque des quatres civilisations anciennes, il y a environ 15 000 à 20 000 ans. Cependant, ces peuples n'ont jamais réussis ou pu faire de terraformation ou d'adaptation des planètes.

Il existe cependant encore quelques colonies et base qui ont réussi à subsister jusqu'à aujourd'hui sur quelques planètes ou satellites, et d'autres civilisations aliens se trouvent dans les étoiles autour du système solaire.

L'histoire de cette extension se passe quand *Frontières* à commencé à lancer l'exploration du système solaire et commence à construire certaines bases.

## Autres systèmes

D'autres espèces aliens vivent dans d'autres systèmes stellaire autour du notre.

-> Centaurien très "monde sci-fi", super avancée avec des petit gris
-> Planète typée barbare façon musclor avec des humains et danaïte.
-> Planète Urthoïde, trouver un nom bien cliché façon monde insectoïde et tout
-> Homeworld Eirons monde aquatique très "précieux/noble/gros bourge là"
-> Monde à la Dune