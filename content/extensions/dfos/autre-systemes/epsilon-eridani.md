---
layout: layouts/base.njk
title: Epsilon Eridani
tags: Autre systèmes
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Epsilon Eridani

Epsilon Eridani est un petit système stellaire situé à 10,5 années-lumière du notre, orbitant autour d'une étoile naine orange. L'autre nom de son étoile est Ran. C'est le système d'origine des Eirons et des Xipéhuz.

## AEgir

AEgir (aussi connu sous le nom de Epsilon Eridani b) est une des deux planètes du système Epsilon Eridani. Il s'agit d'une exo-planète jupiterienne, faisant 1,5 fois la masse de notre géante gazeuse. Elle n'est pas habitée, mais possède de nombreux satellites qui le sont. Ils ont en grande partie une chimie basée sur le méthane, et sont habité par les Eirons.

Peu de chose est connu sur cette planète et ses satellites, les Eirons étant très secrets. Quelques bases y ont été créé pour accueillir les Centauriens et les espèces solaires. Ces civilisations sont complètement aliens pour les notres, formé sur des types de vie que nous n'avons pas dans notre système stellaire.

De plus, ces espèces sont très anciennes, puisqu'elles forment une civilisation de type galactique depuis au moins 8000 ans. Cependant, elles ne se sont jamais vraiment intéressé à l'expensionnismes, si ce n'est avoir envoyé quelques Xipéhuz pour attaquer des humains au début de leur ère interstellaire, craignant cette espèce dont ils avaient vu l'expensionnisme 2000 ans avant, avant l'effondrement des quatres grandes civilisations. Cette civilisation est nommé la "Fédération d'AEgir" faute de mieux.

Les membres de la fédération d'AEgir ont une grande méfiance des autres espèces, mais surtout de celles humaines et des phosphorien. Certaines rumeurs racontent qu'ils auraient accéléré le développement des technologies Centauriennes par peur de voir l'humanité devenir une menace. S'il est vrai qu'il y a eut des liens entre les deux civilisations il y a 200 ans, cette théorie est réfutée par les Centauriens et la fédération d'AEgir.