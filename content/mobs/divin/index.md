---
layout: layouts/base.njk
eleventyNavigation:
  key: Les divinités
  parent: Créatures et PNJs
  order: 3
title: Les divinités
models:
  - Les divinités
---

# Les divinités

Les divinités sont des êtres puissants aux pouvoirs exceptionnels, avec une forte présence d'éclat dans le corps. Ces êtres ont souvent des longévités plus grandes et des capacités qui leurs sont propre. Ils tirent leur éclat du paradoxe directement.

Les dieux ont une limite "dure" de statistique à 65535.

## Attribut de divinité

Chaque divinité possède un attribut, sur lequel elle a un pouvoir et un contrôle au dela de ce qu'un mortel peut avoir, via une manipulation (souvent inconsciente) de l'éclat et de la magie même. Cet attribu peut être une compétence, un élément, une statistique, etc… La divinité aura alors le pouvoir de modifier sa propre fiche et un contrôle absolu sur ce domaine. Cet attribut est réservé aux titans. Les démons et les demi-dieux doivent avoir accès à de l'ambroisie pour pouvoir l'utiliser.

Les divinités ont également tous un trait de personnalité de prédiléction, ainsi qu'un élément de prédiléction (même quand ils n'en sont pas maitre).

### Le filtre de destinée

Le dieu à un filtre qui fait qu'il ne peut pas être dépassé dans son domaine par un⋅e mortel (les demi-dieux peuvent tenter avec de très fort malus). Le dieu aura toujours l'ascendant si une confrontation est faite dans son domaine, et ne pourra pas être dépassé dans le domaine.

C'est en grande partie ce qui a fourni aux dieux leur puissance.

## L'armure divine

Les divinités sont protégé des dégats s'ils ne sont pas fait par des armes à éclat, un élément auquel ils sont faible et/ou sous l'effet de système anti-dieux.

Cette armure est réservé aux titans. Les démons et les demi-dieux doivent avoir accès à de l'ambroisie pour pouvoir l'utiliser.

## Les types de divinités

- Les [titans](dieux/) sont les divinités les plus puissantes, souvent appellé des "dieux".
- Les [demi-dieux](demidieux/) sont des croisement entre les divinités.
- Les [démons](demons/) sont des entités du chaos, résident grandement en enfer.