# La Civilisation de Graal

La Civilisation de Graal est l'ancienne civilisation de Type I (se rapprochant du type II) qui a été présente sur tout le système de Dagda, avant la *Grande Catastrophe de Graal*, et l'apparition de la Calamité dans tout le système solaire.

Cette civilisation regroupaient les nombreuses espèces qui vivaient dans le système et étaient bien plus proche des eirons que les civilisations actuelles. Sa culture est peu connue, si ce n'est d'ancienne légende.

Son centre civilisationnel était la planète Dumannios, et elle était contrôlée par les titans qui y vivaient.

## Technologies

Il s'agissait d'une civilisation multiplanétaire avancée, ayant construit les arches et de nombreux vaisseaux et technologie. De fait, une grande partie des technologies actuelles sont obtenues en étudiant les restes de la civilisation.

C'est eux qui ont créé les arches, qui sont les seules de leurs structures spatiales encore véritablement utilisables aujourd'hui. Cependant, plusieurs de leurs ruines existent encore…

## Effondrement

L'effondrement de la civilisation a été soudain et brutal, lorsque la Calamité s'est abattue sur Dumannios, il y a de cela plusieurs milliers d'années.

On ne sait pas exactement comment cela est arrivé, mais l'immense catastrophe a provoqué la mort de la majorité des habitants de la planète et de ses satellites, la destruction de toute leur flotte de vaisseau stationnée autour de la planète-capitale, l'anéantissement de tout leurs systèmes de communications.

L'onde de choc a eu cependant des effets dans tout le système de Dagda, puisque l'adamant est resté corrompu avant de retrouver ses propriétés pendant plus d'un siècle, provoquant de nombreuses pertes à cause des pertes technologiques, et celle de nombreux savoirs par la destructions des méthodes de stockages. Le reste des pertes a été faites par le temps, et par la grande guerre qui s'en est suivi pour prendre le contrôle des restes de la puissance, guerre qui a provoqué notamment la destruction totale d'un satellite habité.

Aujourd'hui, seule les arches et quelques débuts de vaisseaux technologiques permettent de voyager à travers l'espace, et le système continue de se remettre de la catastrophe.

## La survivance d'Ys

Une dernière survivance de la civilisation a exister sur la citadelle militaire de Ys, située en orbite autour de Dagda, entre Graal et Partholon. C'est ici que c'était repliée les dernières forces de la civilisation de Graal, dont le roi de l'époque Ys Gradlon et son grand-druide Gwenole. Ils tentèrent de reprendre petit à petit contrôle du système de Dagda grace à leurs armées.

Cette survivance a été de courte durée, puisque la propre fille du roi Ys Gradlon ouvrit une porte vers le champ orogonal dans la station, provoquant en grande partie sa destruction, et la mort du roi. On raconte qu'elle se battit fortement contre son druide, mais personne ne sait exactement ce qui se passa ce jour là.

On raconte que Dahut, devenue une Morganne, hante la station depuis.
