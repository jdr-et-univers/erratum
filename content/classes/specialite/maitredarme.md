---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Le maitre d'arme
parent: Les spécialités
---

# Le maitre d'arme

Le maitre d'arme est un combattant émérite qui sait bien utiliser ses différentes armes pour mener ses missions à bien.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Armurerie ambulante | Ne prend pas de tour pour changer d'arme |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Maître des mains |  |  | Le personnage à l'équivalent de trois mains. |
| 1 | Inversion |  |  | Peut inverser sa stat utilisée pour les armes à distances ou au CaC |
| 2 | Passion |  |  | Permet de définir une cause, et si la cause arrive en jeu, +2 pt de pression tous les 5 % en commençant à 70 % |
| 2 | Lame acérée (trait) |  |  | 1/2 chances d'infliger saignement si critique avec n'importe quelle arme au corps à corps. Doit avoir fait des dégats. |
| 2 | Tir blessant |  |  | Peut sacrifier l'augmentation de puissance de son critique avec une arme à feu pour infliger une blessure (deux si l'arme est vorpale). Doit avoir fait des dégats. |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture offensive |  |  | Peut passer en posture offensive |
| 3 | Deux mains sont mieux qu'une (trait) |  |  | Si n'utilise qu'une arme à une main dans ses deux mains, gagne moitié plus de niveaux de réussite à son attaque. |
| 3 | Attaque circulaire | FOR |  | Attaque deux ennemis avec moitié de niveaux de réussite. |
| 4 | Habile de ses mains (trait) |  |  | Au CaC, +1 dégats par 10 HAB au dessus de 100 |
| 4 | Recharge rapide (trait) |  |  | Recharge toute ses armes à feu en même temps |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Tir en cloche | PER |  | A distance, fait une attaque qui touchera au tour suivant mais touchera forcément un point faible si elle réussie |
| 5 | Charge destructrice | FOR/PER | 1/combat<br />3 tours cooldown | Avec une certaine distance, fonce dans des ennemis et les touches tout les ennemis avec +5 dégats. |
| 5 | J'AI EU LE KILL |  |  | S'il tue un boss, il gagne ses PV en argent, ainsi que 1D20+10 respect et confiance |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Puissance cachée |  | 1/partie | Peut découvrir une technique secrete avec une arme particulière, qui lui rajoutera un effet supplémentaire (tiré aléatoirement). Seul lui pourra l'utiliser. |
| 6 | Rendez-vous en enfer (trait) |  |  | Si tombe KO, peut effectuer deux actions gratuite avec 2x pression positive et moitié plus réussites de dégats |