---
layout: layouts/base.njk
tags: Signes de Force
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Sagittaire
parent: Les signes
---

# Sagittaire (le mouvement)

Les sagittaire tirent leur pouvoir du mouvement et de la vitesse. Capable d'effectuer des actions particulièrement rapide, ils sont capable de manipuler la vélocité même comme une arme.

Rapide, précis, cette maitrise les rendent généralement cependant plutôt fragile et ils privilégie un grand nombre d'action de faible puissance comparé à une seule action extrèmement forte.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :sagittarius: | ??? (asie), ??? (égypte) | Force et Malice (DPS Trickster) | Ordre | Commun (12.0%) | HAB et PER | Turquoise |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Toujours premier  |  Joue en premier, sauf échec critique. (trait)  |
| Double |  Vitesse extreme  |  En cas de réussite critique à l’initiative, à le droit à deux tours bonus au lieu d’un.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Arc de lumière  |  Peut générer des « flèches » en mouvement pur et les utiliser comme attaque (1, PER). L’arc de lumière n’est pas enchantable ou empoisonnable.  |
| 1 |  Ralenti (2 Eclat)  |  Peut faire jouer en dernier un ennemi (II : deux ennemis) pour trois tours.  |
| 2 |  Trop lent !  |  Permet de contre-attaquer avant un coup qui sera donné (si encaissement).  |
| 2 |  Tir préventif (2 Éclats)  |  Peut faire une attaque à l’arc de lumière lorsqu’un combat est sur le point de se déclencher.  |
| 2 |  Parkour  |  Pour 2 Eclats, peut ignorer les difficultés d’un terrain.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture offensive  |  Peut passer en posture offensive (II : posture offensive +)  |
| 3 |  Coup chirurgical  |  Perds un niveau de malus pour toucher une cible (II : deux)  |
| 3 |  Flèche de mouvement  |  L’arc de mouvement obtient + 1 flèche  |
| 4 |  Tir enchainés  |  Chaque tir au but sur une même cible avec l’arc de lumière apporte un bonus de +1, jusqu’à un maximum de +8 (II : +12). Le bonus disparaît si l’ennemi meurt, si le coup manque où en cas de changement de cible (même avec Trop Lent !).  |
| 4 |  Plus rapide que la lumière (HAB)  |  Peut faire passer un jet de DIS physique sur un jet d’HAB.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Spécialisation  |  Peut utiliser l’arc de lumière au CaC. Il fonctionne alors comme une attaque de CaC.  |
| 5 |  Zig-Zag (3 Eclat)  |  Peut ignorer tous ce qui pourrait gêner une attaque (hors taunt ou garde du corps) (II: Peut passer un garde du corps sur PER/2)  |
| 5 |  Flèche de mouvement  |  L’arc de mouvement obtient + 1 flèche  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Multi-coup (3 Eclat)  |  Au sacrifice de son armure, peut attaquer deux (II: trois fois) fois pendant un tour.  |
| 6 |  Embuscade (PER) (6 Eclats)  |  Peut passer en mode embuscade pendant trois tours : Le personnage ne peut plus se déplacer mais tous les ennemis se déplaçant à proximité se prendront une attaque d’OP.  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Fleche unique (6 Eclat)  |  Peut combiner toutes les attaques qu’il aurait faites en un tour en une seule. Tous les jets d’armes s’additionnent en un seul, mais n’a que les niveaux de réussite d’une attaque.  |
| Herault |  Barrage handicapant (4 Eclat)  |  Peut faire une série de tir de flèche de lumière qui touche jusqu’à 5 ennemis, et inflige des dégats de type VIOLENT sur un membre au choix du tireur.  |