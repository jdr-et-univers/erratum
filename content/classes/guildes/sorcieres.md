---
layout: layouts/base.njk
title: Les sorcières
tags: Guildes
models:
  - Guildes
  - Guildes métaphysiques
parent: Les guildes
---

# Les sorcières

Les sorcières sont considérée comme une "guilde", mais toutes les autres guildes n'aiment pas DU TOUT considérer les sorcières comme une guilde. Situé à de nombreux endroits dans le monde, dont notamment la cité belge de Walpurgis, cette guilde est connue pour ne pas suivre les règles que pose l'OIM, et de vivre selon leur propres règlementation. Elles forme des sociétés dirigée par des matriarche, mais n'importe quelle sorcière peut former un couvant de sorcière.

Les sorcières protègent fortement les leurs, notamment les enfants qui sont dans les cités de sorcières.

## Pouvoirs de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Balai volant | Peut utiliser n'importe quel outil pour se déplacer |

## Les maléfices de sorcières

Les sorcières remplacent généralement leur potions par des *maléfices*, des potions qui n'ont pas besoin d'être bues et dont les effets sont tordues et transformés, à distance.

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Maléfices | Transforme les potions en "maléfice", qui permet de placer un effet négatifs sur les ennemis pendant une durée longue. |

## Les forces anciennes

La magie naturelle telle que pratiqué par la sorcière permet d'accéder à des membres du petit peuple et de les domestiquer, souvent sous des formes animales.

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Connaissance du petit peuple | À la place des animaux, peut domestiquer des êtres du petit peuple, souvent sous forme zoomorphique (chat qui parle, etc). |