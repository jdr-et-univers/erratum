---
layout: layouts/base.njk
title: La magie élémentaire
tags: Magies Rares
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# Magie élémentaire

La magie élémentaire est une magie se basant sur les pouvoirs des grands élémentaires (aussi nommée incarnations élémentaires), qui fourni aux gens la puissance de leurs éléments.

Les utilisateurs font un pacte avec certains nombres d'éléments qui pourront ensuite être utilisé.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Pacte élémentaire  |  Obtient un élément principal (eau, feu, terre, air) qui devient l’élément principal de l’élémentariste, qui lui confère naturellement une résistance élémentaire (l’élément fait -50 % de dégats).  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  (Petits) Tirs élémentaires (HAB/INT, 2 Eclat)  |  1D4×2 – Effet élémentaire  |
| 1 |  Soin élémentaire (HAB/INT, 2 Eclat)  |  Jet de soin, 1D10+2. Donne pendant trois tours une immunité aux effets secondaires de l’élément en question.  |
| 2 |  Déplacement élémentaire (INT/HAB)  |  Peut faire tomber un adversaire (quoi doit faire un jet d’hab pour résister). Peut avoir un effet élémentaire si pertinent  |
| 2 |  Deuxième élément  |  Peut utiliser un deuxième élément (élément secondaire : glace, foudre, metal, vegetal)  |
| 2 |  Couloir élémentaire (HAB/INT, 2 Eclats)  |  Peut sur une confrontation d’HAB ou INT dévier une attaque physique vers un autre personnage  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Armure élémentaire (HAB/INT, 2 Eclats, 3 tours)  |  Lorsque le personnage encaisse une attaque, peut faire 1D4 dégats + effet élémentaire si l’attaquant est au CaC  |
| 3 |  Tirs élémentaires (HAB/INT, 3 Eclat)  |  1D4×3 – Effet élémentaire  |
| 3 |  Oh, un trou (HAB/INT, 3 eclat)  |  Fais tomber quelqu’un dans un trou. Il ne peut plus esquiver les attaques, ni se déplacer tant qu’il n’en est pas sorti  |
| 4 |  STAB  |  Les jet de dés de techniques de l’élément principal passe aux dés supérieurs.  |
| 4 |  Terrain élémentaire (HAB/INT)  |  Peut créer un effet élémentaire sur tout le terrain (voir terrains élémentaires)  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Voile élémentaire (trait)  |  L’élémentariste n’est pas touché par les effets élémentaires (terrain comme brut) de son élément principal  |
| 5 |  Puissants tirs élémentaires (HAB/INT, 4 Eclat)  |  1D6×3 – Effet élémentaire  |
| 5 |  Troisième élément  |  Peut utiliser un troisième élément (principal ou secondaire)  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Combo élémentaire (HAB/INT, 6 Eclats)  |  L’utilisateur peut faire trois Tirs Élémentaires à la suite, un pour chaque élément.  |
| 6 |  Canon élémentaire (HAB/INT, 6 Eclat)  |  1D12+4 – Effet élémentaire.  |