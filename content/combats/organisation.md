---
layout: layouts/base.njk
eleventyNavigation:
  key: Organisation
  parent: Système de combat
  order: 0
models:
  - Combats spéciaux
title: Organisation
---

# Organisation d'un combat

## Début du combat

Lorsque le combat commence, tout les personnages présent durant le combat font un *jet d'initiative* pour connaître leur place dans le combat, en utilisant leur statistique d'HAB. L'organisation des tours des personnages est fait avec une [comparaison](bases/D100.md) des résultats. *Aucune compétence n'agit sur le jet d'initiative*, mais certains traits peuvent le faire, ainsi que des effets tels que toujours premiers.

Les personnages ayant eu une réussite critique ont le droit à une action supplémentaire dans un tour 0 avant le premier tour. Les personnages ayant eu un échec critique ne peuvent jouer qu'à partir du deuxième tour.

## Attaques surprises

En cas d'attaque surprise, le groupe ayant mener l'attaque surprise aura tout ses membres (sauf ceux ayant fait un échec critique) dans un tour supplémentaire avant le combat (le tour 0 mentionné plus haut). Les membres du groupe adversaire ayant effectué une réussite critique participeront à ce tour d'attaque surprise, ainsi que les personnages ayant une capacité contre les attaques surprises.

Pour effectuer une attaque surprise, il faut que le groupe adverse ne soit pas au courant de la présence du groupe qui attaque, et que les deux groupe fasse une confrontation de DIS vs PER.

## Tours et actions

Le tour commence au jeu du premier joueur du tour et se termine à la fin de celui du dernier joueur. Le décompte des tours est effectué par le MJ, et peut affecté les différents effets pouvant se produire en combat.

Pour les effets durant plusieurs tours, certains sont compté au début du tour, d'autre à la fin, d'autres à celui des personnages, suivant les catégories suivantes :

- Tout les décomptent globaux, liés à tout les personnages et le combat dans son entiereté (countdown avant qu'un personnage arrive, décompte du nombre de tour, malédiction placée par un élément hors du combat) se décomptent.

- Tout les effets lié à un seul personnage où à un sort se décomptent au début du tour du personnage (poison, cooldown d'une capacité). Il y a une seule exception à cela : les déplacement, comme vous le verrez plus bas.

- Les "effets d'environnement" (tirs perdu d'une armée autour, pluie aléatoire de boule de feu), s'effectuent à la fin du tour.

### Actions d'un personnage

Lors d'un tour, chaque personnage peut faire 1 ou plusieurs des trois actions suivantes :

- Un *déplacement* (pour changer de position après s'être mis à découvert, attaquer un ennemi qui n'est pas immédiatement devant soi, etc) avant ou après son action. Certaines capacités consommes un déplacement en plus de l'action (qu'elle soit avant ou après le tour). Le déplacement ne se regénèrent pas au début du tour du personnage, mais de celui global ! Cela veut dire que si vous désirez réserver votre déplacement pour une action qui se passerait hors tour, jouer en dernier peut devenir aventageux.

- Une *action*. En règle générale : tout sort, capacité non-passif ou récupération/changement d'équipement présent dans le sac (recharger son arme, changer d'épée) consomme une action, qui représente ce que le personnage peut faire à son tour. Toute action simple et courte avec un objet déjà en main (boire une potion, retirer un gants, mettre ses lunettes de soleil en tournant le dos à une explosion) pourra être fait en plus de l'action.

- Un *changement de posture* suivant celles qui lui sont accessibles.

## Fin du combat

Le combat se termine quand les ennemis n'ont plus de raison de se battre, ou n'en sont plus capable pour une raison ou une autre. C'est à ce moment là que se produit la regénération de l'éclat (cela signifie que l'éclat utilisé avant un combat n'est *PAS* regénéré avant le combat ).