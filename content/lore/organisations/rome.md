---
layout: layouts/base.njk
tags: Orgnaisations criminelles
title: Nouvelle armée impériale
models:
  - Organismes officiels
  - Sociétés secrètes
  - Orgnaisations criminelles
parent: Les organisations
---

## Le Nouvel Empire Romain.

Le Nouvel Empire Romain est l'une des principales organisations criminelles qui sévit en Europe. Ce groupe fascisant pan-européen a pour objectif de prendre le contrôle de toute l'Europe, pour recréer un Empire Romain tel qu'ils le fantasme, controlant toute autour de la méditérannée. Pour cela, elle combine l'utilisation de moyens de peur et de pression politique. Cette organisation à comme caractéristique qu'elle contient parmis ses membres à la fois des mortels et des titans, et son territoire se compose plus d'une quinzaine de cité célèste répartie sur l'Italie.

Sa capitale est située dans la *Cité Célèste des Empereurs*, situé aux abords de Rome, dans un univers-bulle. Son chef officiel est le dieu Jupiter, après une petite période ou Appolo Phoebus avait été le dirigeant de la cité célèste, secondé par un *Sénat Latin* composé de familles magiques puissantes et influantes, refusant le pouvoir de l'OIM en Europe. Jupiter estime avoir "repris le titre d'Empereur" après la fin du dernier empereur du Saint Empire Romain Germanique, règle ayant été ratifié par le Sénat Latin (qu'il avait lui-même fondé). Cette organisation mélange des aspects autocratique (l'empereur ayant une très grande puissance), et des aspects oligarchique plus traditionnel (de nombreux aspect de la vie dans l'Empire Romain sont controlée par le Sénat plutôt que par Jupiter). 

Chose unique, les dieu Romain ont offciellement avoir "renoncé au titre de dieu" et "embrassé la religion chrétienne". Ils ne se définissent plus comme des "dieux" (laissant se titre au monothéisme chrétien) mais prenne celui de "titan". Ce geste politique leur a permis de gagner en réputation et en popularité, et d'endomager la réputation d'autres groupes de dieux, les faisant passer pour des "orgueilleux se proclamant plus qu'ils ne le sont". Les dieux romain ont cette particularité d'être les *uniques dieux signés*, mais peu de choses sont connus de leurs origines. Ils servent de généraux à l'armée romaine.

Ils apprécient tout particulièrement les membres forts et résistant. Ce groupe est principalement en guerre face à d'autres cité célèste, et déclare "ne pas avoir d'intention" envers les pays existant. Malgré le decrêt de l'OIM instaurant leur illégalité, de nombreux dirigeant et puissant d'Europe se réfère à eux.