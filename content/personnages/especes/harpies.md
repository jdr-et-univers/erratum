---
layout: layouts/base.njk
title: Harpies
tags: Espèces additionnelles
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Harpies

Les harpies sont une espèce des montagnes, proche des humains avec des caractères proches des oiseaux. L'une des rares espèces volantes qui existent sur la planète.

Cette espèce a évoluée de la branche des homininé comme les humains, les loup-garou, les ondines et les hybrides. Ces espèces ont souvent été considérée de manière très similaires à celles des ondines, souvent comme étant "moins humaines" que les humains, décrites souvent comme des monstres dans de nombreuses légendes.

Elles vivent souvent originellement en petit groupe, dans les zones rocheuses et montagneuses, ou le fait de voler leur offrait un aventage direct.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 10 | 12 | 10 | 7 | Griffe (2) | HAB, FOR (50) | Aucune | CON, DIS, REL (40) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Des ailes | Permet d'avoir la compétence "voler" | 
| Cri fulgurant | Ses attaques mentale font deux dégats perce-armure mentale |