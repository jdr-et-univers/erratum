---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Les gardes
parent: Les spécialités
---

# Les gardes

Vous les connaissez. D'habitude, c'est ces gens que vous voyez venir 15 à la douzaine dans une partie, et que vous zigouillez à tour de bras. Le guets, la garde, ils sont souvent considéré comme pas très malin, ces zigotos en armure, mais peut-être font-ils bien leur taff ?

En tout cas, ici, vous pouvez en jouez. Êtes vous un vrai garde, ou juste quelqu'un qui fait un peu le guet quand vous piquez les cartes à collectionner dans le bureau de votre prof autoritaire ? Welp, ici c'est un garde dans les deux cas.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | À moi la garde ! | Un allié peut lui appeller à l'aide. Alors le garde pourra faire un jet d'HAB pour aller se prendre l'attaque |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Par ici ! | CHA |  | Peut attirer l'aggro de jusqu'à 3 ennemis au hasard sur lui (jet fait séparément pour chaque). |
| 1 | Imperturbable |  |  | Ne peut être déconcentré ou arrêté quand fait une action. |
| 2 | Collègues |  |  | En situation standard, +20 aux stats sociales face à un autre garde. |
| 2 | Coup de bouclier |  |  | Peut frapper avec son bouclier pour la moitié des dégats |
| 2 | Pacification (trait) |  |  | 1/8 chance de stun sur ses attaques physiques, mais ne peut plus rendre KO |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture défensive |  |  | Permet de passer en posture défensive |
| 3 | Chevalier servant |  |  | Protège une personne ou un lieu. Peut effectuer des encaissement ou esquive d'opportunité (avec en plus jet de FOR), mais ne peut plus bouger du lieu ni aider quelqu'un d'autre |
| 3 | Balèze |  |  | Divise par deux les malus sur les armures lourdes |
| 4 | Forteresse de fer |  |  | Si n'attaque pas durant le tour +3 armure brute jusqu'à ce qu'il attaque |
| 4 | Charger dans le tas | FOR+HAB | 3 tours de cooldown | Permet de faire une charge au bouclier sur un ennemi pour le faire tomber. Il fera des dégats de chute, et mettra un tour pour se relever. |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Patience et longueur de temps |  |  | A l'effet "Toujours Derniers", mais ne peut pas être stun. |
| 5 | Tu ne passera pas ! | CHA | 3 tours de cooldown | Peut empêcher un mouvement ennemi |
| 5 | Le mur |  | 4 tours de cooldown | Est immobilisé, mais encaisse automatiquement et à 1/8 chance de faire perdre 10% aux stats physiques quand il attaque. Dure trois tour |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | La garde meurt mais ne se rend pas |  | 1/partie | Peut ignorer un KO, mais se prend l'effet saignement grave et perd la possibilité de faire un jet de survie jusqu'à ce qu'il soit soigné. |
| 6 | Jusqu'au bout |  | 1/partie | Pendant 3 tour, ne se prenda que 1 dégats (après calcul armure) après chaque attaque |