---
layout: layouts/base.njk
title: Les Sélénites
tags: Espèce DfOS
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les Sélénites

Des sortes de petits lapin aliens. Ils ont des antennes et vivent sur la lune. Les légendes racontent que ce peuple proviennent d'un rêve d'un dieu, qui les a fait alors naitres. Ils sont relativement serviables et pacifiques. On raconte que les attaquer porte malheurs, mais que les aider porte bonheur.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:----------:|:-------:|
| 10 | 10 | 10 | 8 | Poing (0) | REL, DIS et PER (65) | FOR, VOL et CHA (35) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Sens aiguisés | peut sur un jet de PER éviter tout malus de vision. | 
| Les actes horribles sont punis | Si un sélénite est tué, met +2 de pression négative définitif à celui qui l'a tué |
| Bonté | Font regagné 1PV/tour à tout ceux ne leur ayant pas fait de mal |