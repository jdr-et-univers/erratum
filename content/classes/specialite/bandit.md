---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Les bandits
parent: Les spécialités
---

# Les bandits

Les bandits sont des voleurs, petits voyous ou toute autre personne qui aime bien faire des choses en dehors des cadres légaux - les choses en questions étant généralement vue comme répréhensible. Cela ne veut cependant pas dire que ce sont des personnes "mauvaises". Ce sont cependant souvent des petits troublions !

Ces petits DPS peuvent faire des actions qui peuvent nuire aux ennemis de différentes façons… mais ils sont souvent surtout les spécialistes de la discretions et des petites attaques en screds.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Pickpocket | Sur un jet de DIS+HAB, peut voler un objet présent sur un PNJ |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Disparition | DIS | 5 tours de cooldown | Peut faire un jet de DIS pour disparaitre de la vision des ennemis |
| 1 | Vol | DIS+HAB | 2 tours de cooldown | Tente de voler un objet commun à un ennemi. S'il ne peut pas en avoir, échoue. |
| 2 | Pillage (trait) |  |  | Tire 1D8. Si fait 1 ou 2, gagne 1D4 pièce d'or en frappant l'ennemi |
| 2 | Frappe sournoise | DIS |  | Si le bandit attaque un ennemi ne savait pas qu'il était là, augmente de moitié ses niveaux de réussite |
| 2 | Fuite (trait) |  |  | Divise par 2 les malus à la fuite d'un combat |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture offensive |  |  | Peut passer en posture offensive |
| 3 | Frappe lâche | (jet d'arme) | 3 tours de cooldown | Une attaque qui fait 1.5x dégats sur les ennemis ayant au moins une affliction |
| 3 | L'emplacement du portefeuille |  |  | Gagne 1D12+4 Po si réussi un vol |
| 4 | Doigt de fée |  |  | N'a pas de malus pour crocheter des serrures |
| 4 | Connaisseurs des pièges | HAB |  | Si le bandit trouve un piège, peut tenter de le passer sans le désactiver. |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Distraction | DIS | 4 tours de cooldown | Pour 100 Po, peut rendre stun un ennemi |
| 5 | Ambidextre |  |  | N'a pas de malus d'utilisation de deux dagues ou petites armes |
| 5 | Déshabillage | 3 tours de cooldown |  | Peut retirer l'effet d'un accessoire/vêtement jusqu'à ce que l'ennemi prenne un tour pour le remettre |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Le prince des voleurs |  |  | Si réussi un vol, jete 1D4, si fait 1, l'objet sera un objet rare |
| 6 | Truand (trait) |  |  | Fait +2 dégats par vols réussis avec succès. |
