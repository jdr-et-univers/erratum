---
layout: layouts/base.njk
title: Les armes
tags: Les équipements
models:
  - Les équipements
parent: Les équipements
---

# Les armes

Lors d'un combat, vous pouvez utiliser toutes sortes d'armes ou d'objets pour agir. Voici un petit panel de ce qui est possible d'avoir de base.

## Liste des armes

<div id="dataTable">

| Nom | Type | Mains | Effet | Force | Prix |
|:------:|:------:|:------:|:------:|:------:|:------:|
| Mains | Arme au CaC | 2 | Comptent comme main nue. Non-Lethal | 0 | 0 |
| Projectile | Arme distance | 1 | Lance un projectile (pierre, etc) | 0 | 0 |
| Poings | Arme au CaC | 2 | Comptent comme main nue. Non-Lethal | 2 | 80 |
| Griffe | Arme au CaC | 2 | Comptent comme main nue. Saignement si critique | 3 | 150 |
| Batons | Arme au CaC | 2 | Non-Lethal | 2 | 80 |
| Fouet | Arme au CaC | 1 | Peut toucher à distance | 3 | 150 |
| Couteau | Arme au CaC | 1 | Peuvent être lancée. Saignement si critique | 3 | 150 |
| Épée | Arme au CaC | 1 | | 5 | 400 |
| Épée à deux mains | Arme au CaC | 2 | | 8 | 550 |
| Hachette | Arme au CaC | 2 | Nécessite >2 niveau de réussite; Peut être lancé; Saignement si critique | 5 | 550 |
| Haches | Arme au CaC | 2 | Nécessite >3 niveau de réussite; Saignement si critique | 9 | 600 |
| Lance | Arme au CaC | 2 | Peut être lancé ; Touche des ennemis plus éloigné ; +50% dégat si sur/contre monture | 5 | 550 |
| Hallebarde | Arme au CaC | 2 | Nécessite >2 niveau de réussite; Peut être lancé ; Touche des ennemis plus éloigné ; +50% dégat si sur/contre monture | 12 | 600 |
| Marteau | Arme au CaC | 1 | Effet de brise-armure | 4 | 400 |
| Masses | Arme au CaC | 2 | Nécessite >2 niveau de réussite; Effet de brise-armure | 6 | 600 |
| Lance-Pierre | Arme distance | 1 | Peuvent être utilisée pour jeter des projectiles spécifiques | 1 | 150 |
| Fronde | Arme distance | 1 | Peuvent être utilisée pour jeter des projectiles spécifiques | 2 | 200 |
| Boumerang | Arme distance | 1 | Nécessite deux jet d'HAB réussi pour être esquivé | 3 | 200 |
| Pelle | Outil | 2 | Peut être utilisé pour creuser | 1 | 100 |
| Fourchette | Outil | 2 | Manger rapporte +1 PM | 1 | 30 |
| Arc court | Arme distance | 2 | Perce-armure sur les hélicoptères | 4 | 350 |
| Arc long | Arme distance | 2 | Perce-armure sur les hélicoptères | 6 | 400 |
| Arbalète à une main | Arme distance | 1 | Semi perce-armure ; Peut s'enrayer | 6 | 500 |
| Arbalète | Arme distance | 2 | Semi perce-armure ; Peut s'enrayer | 8 | 600 |
| Escopette/Arquebuse | Arme à feu | 1 | Doit être rechargé tout les tir ; Réussite de DEF/2 | 5 | 400 |
| Revolver | Arme à feu | 1 | Doit être rechargé tout les trois tir ; Réussite de DEF/2 | 5 | 550 |
| Fusil | Arme à feu | 2 | Nécessite >2 niveau de réussite; Doit être rechargé tout les deux tir ; Réussite de DEF/2 | 9 | 600 |
| Tromblon | Arme à feu | 2 | Nécessite >2 niveau de réussite; Doit être rechargé tout les deux tir ; Touche une zone; Réussite de DEF/2 | 5 | 600 |
| Pistolet automatique | Arme à feu | 1 | Nécessite >3 niveau de réussite; Réussite de DEF/2 | 5 | 900 |
| Fusil automatique | Arme à feu | 2 | Nécessite >5 niveau de réussite; Réussite de DEF/2 | 9 | 1200 |
| Fusil à double canon | Arme à feu | 2 | Nécessite >5 niveau de réussite; Doit être rechargé tout les tirs ; Réussite de DEF/2 | 12 | 1200 |
| Baguette de soin | Baguette | 1 | Utilise VOL ou INT; Les dégats sont fait en tant que soin | 4 | 350 |
| Baguette de guérison | Baguette | 1 | Utilise VOL ou INT; Les dégats sont fait en tant que soin; Soigne + 50 % de chance de soigner une altération | 2 | 550 |
| Baguette de combat | Baguette | 1 | Utilise VOL ou INT; Fait des dégâts à la cible; | 3 | 350 |
| Baguette maléfique | Baguette | 1 | Utilise VOL ou INT; Fait des dégâts sur les PM (dé de mental) | 3 | 550 |
| Sceptre de soin | Baguette | 2 | Utilise VOL ou INT; Les dégats sont fait en tant que soin | 8 | 500 |
| Sceptre de combat | Baguette | 2 | Utilise VOL ou INT; Fait des dégâts à la cible | 5 | 500 |
| Sceptre maléfique | Baguette | 2 | Utilise VOL ou INT; Fait des dégâts sur les PM (dé de mental) | 5 | 800 |

</div>

## Types d'armes

- Les **armes au Corps-à-Corps** sont des armes qui utilisent un jet de FOR, et attaquent sur la défense normale. Les dégats effectué sont égaux aux *niveaux de réussite* plus la force de l'arme.
- Les **armes à distances** sont des armes qui utilisent un jet de PER, et attaquent sur la défense normale. Peuvent toucher à distance (logique). Les dégats effectué sont égaux aux *niveaux de réussite* plus la force de l'arme.
- Les **armes à feu** sont des armes qui utilisent un jet de PER, et attaquent sur la défense normale. Peuvent toucher à distance (logique). Les dégats effectué sont égaux uniquement à la force de l'arme, mais les *chance de réussite critiques* augmentent avec chaque % de compétence au dessus de 100.
- Les **outils** utilisent un jet de FOR, et attaquent sur la défense normale. Les dégats effectué sont égaux aux *niveaux de réussite* plus la force de l'arme. Ils ont une utilité première autre qu'attaquer.
- Les **baguette** utilisent un jet de VOL ou INT, et attaquent sur la défense magique. Les dégats effectué sont égaux aux *niveaux de réussite* plus la force de l'arme. Certainent servent à soigner.

## Effets

Les armes peuvent avoir un effet supplémentaire.

| Nom| Effet| Surcout |
|:------:|:------:|:------:|
| Améliorée | x1.5 force | +50% |
| Supérieur | x2 force | +100% |
| Ultime | x3 force | +200% |
| Pouic | x0 force | +0% |
| Non-léthal | Tout coup avec ne fera pas tomber en dessous de 0 PV | +10% |
| Léthal | Double les chances de critique | +50% |
| Vorpale | Les critique font x3 au lieu de x2 | +100% |
| de verre | Double la force, mais ne peut être utilisé que deux fois | +10% |
| À accroche | À besoin d'une main de moins | +10% |
| Élémentaire | Produit l'effet de l'élément lié | +25% |
| des Âmes | Chaque mort fait avec lui confère un dégât d'1PM qui sera aussi appliqué au héros | +25% |
| Funeste | Provoque un jet de panique si réussite critique | +100% |
| De sang | Provoque un jet d'affliction physique si réussite critique | +100% |
| Des Héros | +50% force sur les créatures démoniaques | +100% |
| Des Ombres | Fait 3 dégâts bruts (même si a échoué) | +100% |
| Laser | Attaque sur l'armure spéciale | +100% |
| Nanotechnologie | Tout effet de brise armure ou brise-arme s'estomp à la fin de la partie | +100% |
| Ferrofluide | Sur un jet d'HAB/2, peut prendre les effets d'un autre type d'arme. | +200% |
| Anomique | Provoque 1D10 anomie quand touche la personne visée | Invendable |