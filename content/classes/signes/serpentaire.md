---
layout: layouts/base.njk
tags: Autour des signes
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Serpentaire
parent: Les signes
---

# Le serpentaire

Le serpentaire est le 13 signes, le signe inconnu, celui qui ne devrait pas exister. Il est le pouvoir exclusif de la vertue du serpentaire. On raconte que certains tenteraient de le synthétiser.

Le serpentaire n'est en fait pas un signe. Il est un méta-signe. Il permet de contrôler les flux même de la magie.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :ophiuchus: | ??? (asie), ??? (égypte) | Hors du système | Éclat | Exceptionnel | SAG et VOL | Toutes/blanc. |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Annulation magique  |  Tant que le serpentaire est en contact direct (hors outil protégeant de cela) avec un personnage, celui-ci ne peut plus faire de magie.  |
| Double |  Blocage magique (1/partie)  |  Peut bloquer l’utilisation de magie d’un personnage pour toute la partie.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Copie de capacité (VOL)  |  Peut utiliser une capacité d'un autre signe présent, si la personne l'accepte. Sinon, confrontation VOL vs VOL. À -25 % (I:-10%) pour faire l’action.  |
| 1 |  Vol d’éclat (SAG)  |  Peut voler 1D4 (II: 1D6) éclat à un autre personnage. Confrontation SAG vs SAG.  |
| 2 |  Offre d’éclat  |  Peut transférer des éclats à un autre personnage au choix.  |
| 2 |  Tir d’éclat  |  1D4×2 (II: 1D6x3 dégats), effet d’éclat. Peut faire d’autres compétences élémentaire avec éclat si connaît magie élémentaire.  |
| 2 |  Aura anti-éclat (4 Eclats)  |  Augmente de 1 la consommation d’éclat de chaque capacité, pendant 3 tours (II: cinq tours).  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Copie de posture  |  Peut copier la posture d’un autre personnage présent sur le terrain.  |
| 3 |  Aura pro-éclat (4 Eclats)  |  Baisse de 1 la consommation d’éclat de chaque capacité (II: cinq tours).  |
| 3 |  Emprunt (VOL)  |  Peut temporairement prendre le signe d’un autre personnages, si la personne l'accepte. Sinon, confrontation VOL vs VOL.   |
| 4 |  Enchantement d’éclat  |  Ajoute un effet d’éclat à une arme. Peut faire d’autres effets d’enchantement avec de l'éclat si connaît les enchantements.  |
| 4 |  Antidote magique (VOL ou SAG) (1 karma)  |  Soigne une (II: deux) affliction magique au choix.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Maîtrise de l’éclat  |  Toutes les compétences d’éclat passent aux dés supérieurs.  |
| 5 |  Détection métaphysique  |  Peut sur un jet de PER + INT analyser un phénomène métaphysique pour en obtenir une meilleure compréhension. Attention aux effets secondaires.  |
| 5 |  Anti-critique (5 Eclat, 1/partie)  |  Bloque tous les critiques sur le terrain, quels qu’ils soient.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  ChangeSigne (6 Eclat, SAG)  |  Peut modifier le signe d’un autre personnage, (ou lui conférer un signe s’iel n’en avait pas) temporairement durant le combat. Confrontation de SAG.  |
| 6 |  Partage de détermination (1 karma)  |  Peut partager les effets d’un jet de détermination positif à un allié après avoir jeté le dé.  |

## Pouvoirs du Hérault

Ce signe n'a pas de pouvoir de héraults.