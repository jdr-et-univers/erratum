---
layout: layouts/base.njk
title: Lycanthropes
tags: Espèces de base
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Lycanthropes

Les lycanthropes (ou loup-garou) sont une espèce dérivée de l'espèce humaine. D'après les légendes, ils seraient les descendant des suiveurs du *Dieu-Loup*, maudits à être mi-humains mi-loup, il y a de cela plus de 10 000 ans. Ils sont grand, fort, et généralement poilu.

Espèce sociale vivant en meute, deux types de loups garous spéciaux existent : les régaux (qui ont un pouvoir de contrôle sur les autres), et les libres (qui sont des loups qui ont un pouvoir de neutralité, d'être des arbitres). Un dernier type de loup-garou, les sans-meutes, est très rare, et correspond à des loup-garou qui sont entièrement hors du système même de meutes.

Les loup-garou ne se transforment pas à la pleine lune, mais les meutes ont souvent eut l'habitude de mener de grande chasse et de grands combats vers cette période de l'année. Une grande partie des légendes autour du loup. Dans les mythes loup-garou, le pouvoir du Dieu-Loup serait le plus présent à cette période de l'année.

Malgré leur force et leur compétence physique, suite à la disparition d'une partie de leurs territoires naturels, les loup-garou vivent une crise, leur rendant de plus en plus difficile d'élever leurs enfants. L'espérence de vie est aussi plus faible chez les loup-garou que chez les humains, se rapprochant plus des 65 ans. Cependant, cela est affecté par un plus grande nombreux de loup garou mourant jeunes.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:----------:|:-------:|
| 18 | 10 | 10 | 4 | Griffes (2) | FOR, CON et CHA (60) | SAG, VOL et PER (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Haine de l’argent | Subit un effet de poison (cumulable avec un poison normal) par l'argent |
| Apparence brute | +2 arme mentale | 
| Rage lupine | Peut passer dans l’état rage |

