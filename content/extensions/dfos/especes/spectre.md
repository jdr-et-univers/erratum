---
layout: layouts/base.njk
title: Le Spectre
tags: Espèce DfOS
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Le Spectre

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Spectre + autre espèce | Rares | Dépend de l'autre espèce | Aucun | Dépend de l'autre espèce |

Le Spectre, "Parasite Ultime" est le nom donné à une créature extraterrestre composé de bactérie intelligentes lié entre elles. Cette créature parasitique, ayant le pouvoir de prendre contrôle de nombreuses espèces en les infectant, peut prendre également l'apparence d'une forme de brume fantômatique.

Si "le spectre" est souvent utilisé au singulier puisqu'il existe un esprit de ruche entre les différentes bactéries le composant, en vérité cet esprit ne fonctionne que sur l'échelle d'une planète, ce qui fait que chaque planète atteinte par le spectre possède un spectre différent. Ils peuvent cependant communiquer entre eux grace à diverses technologie, ce qui fait que le Spectre fait partie des civilisations à être une nation galactique malgré son fonctionnement radicalement différent.

Le spectre est capable de s'adapter à plusieurs chimies, plusieurs type de température. Il est vu comme le "parasite ultime". D'après les légendes, ce serait à l'origine une arme.

Le Spectre à une réputation très mauvaise, celle d'être sournois, incapable de toute invention et de voler la technologie à ses "hôtes". S'il est vrai que cette créature fonctionne par "parasitage" d'une certaine manière, d'une autre elle fonctionne par symbiose, puisque l'esprit de la personne parasitée est le plus souvent copié dans la ruche, ce qui fait qu'il en fait partie, d'une certaine manière. Cela fait de chaque spectre une créature singulière, de part les esprits et la culture des peuples contaminé. Cependant, le plus souvent, ce sont des espèces "primitives" qui sont capturé, le spectre pouvant apporter l'intelligence de lui-même. De nombreuses planètes sont remplies de créatures "sauvages" parasitée par le spectre, formant le plus gros de leur empire.

D'un point de vue galactique, ce peuple est très peu guerrier, et pratique plutôt l'espionnage. Les espions spectre sont des êtres parasité par le spectre, avec une technologie limitant en partie l'esprit de ruche, ne permettant de transmettre que quelques informations entre les différents individus parasité sur la planète.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| -2 | +2 | +0 | -1 | N/A | DIS, INT (+10) | FOR, CON (-10) | |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Membre du spectre | Peut reconnaitre et communiquer avec tout membre du spectre présent dans la partie. |
| Moteur de recherche | Peut consulter l'esprit collectif à tout moment pour poser une question, comme une recherche internet |
| Cartographie | Peut trianguler sa position à tout endroit |
| Force collective | +50 en VOL pour résister à une attaque psychique |