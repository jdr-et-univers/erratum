---
layout: layouts/base.njk
title: Les créatures anomiques
tags: Les créatures
models:
  - Les créatures
parent: Les créatures
---

# Les créatures anomiques

Il existe deux types de créatures lié à l'anomie, les **abberations** et les **irrégularité**. La première correspond à une créature entièrement corrompue par l'anomie, la seconde comme une créature comme n'étant qu'en partie corrompue. L'irrégularité à comme particularité d'être un autre type de créature en même temps, et d'avoir ses faiblesses et résistances.

Les deux correspondent à des stades supérieurs à une simple malédiction anomique.

## Irrégularités

Une irrégularité est un être qui a été corrompu par l'anomie. Il en existe principaux deux types : les conscient (qui gardent leur raison), et les inconscient (qui sont devenu des sansesprit). Les irrégularités ont les caractéristiques suivantes:

- Leurs attaques magiques non-élementaire reçoivent l'élément *anomie*
- Ils sont insensible aux effets de l'anomie.

Ils reçoivent les faiblesses et résistances des irrégularités.

## Aberrations

Une aberration est un être né de l'anomie, ou entièrement transformé par celle-ci. Les aberrations apparaissent naturellement dans les mondes entièrement corrompus par l'anomie, étant souvent considéré comme des reste des. Les aberrations peuvent être géante, mais peuvent également être des créatures de tailles régulières. Les aberrations sont classifiées par des niveaux, correspondant au niveau de corruption pour les faire apparaître et à la puissance de l'être corrompu. Elles deviennent de plus en plus puissantes avec le temps.

Les aberrations ont les caractéristiques suivantes, en plus de celles des irrégularités:
- Les aberrations sont nécessairement des sansesprit.
- Elles peuvent instaurer un terrain élémentaire anomique
- Chaque personnage qui la frappe en contact physique ou la touche à distance avec un sort subit un contrecoup anomique faisant 1 dégats.
- En leur présence, les êtres particulièrement magique subissent un malus de 25% à toutes les stats mentales et sociales.

Les aberrations possèdent aussi chacune des pouvoirs et des gimmicks unique, souvent des déformations du "concept" de l'être qu'elles ont corrompu, comme une forme cauchemardesque et corrompue de la créature originelle.
