---
layout: layouts/base.njk
tags: Signes de Guérison
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Cancer
parent: Les signes
---

# Signe du Cancer

Le signe cancer, signe du phénotype, est un signe rare offrant le pouvoir de contrôler les phénotypes et génotypes, à échelle plus ou moins grande. Ils modifient directement les structures biologiques à leur avantage, pouvant en faire une arme comme une manière d'aider.

Cela dit, cela rend aussi leur soins dangereux pour ceux qui le subissent. Ils peuvent créer des soins particulièrement performant au prix d'effet secondaire redoutable. Il s'agit d'un signe rare et puissant.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :cancer: | Rat (asie) | Guérison et Force (Soigneur DPS) | Chaos | Rare (01.2%) | INT et HAB | Gris |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Vampirisme (FOR, 1 Eclat)  |  Une attaque au corps à corps qui soigne à l'attaquant 50% des dégats infligés à l'ennemi. L'attaque utilise des batons de soin comme arme et compte comme attaque physique  |
| Double |  Vampirisme continue (FOR, 4 Eclat)  |  Une attaque au corps à corps qui n'infligera que les dégats du baton de soin, mais qui se répéteront pendant cinq tour. Permet de récupérer 50% des dégats infligés, chaque tour  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Soin physique (INT)  |  Peut soigner l'équivalent des niveaux de réussite du jet (+1 si pas de baton de soin)  |
| 1 |  Aura de pestilence  |  Créer une aura qui baisse de 15% la résistance aux afflictions physiques chez les ennemis pendant trois tours (II: 5 tours) (ne peut pas être placé s'il y a déjà une aura)  |
| 2 |  Aura de santé  |  Créé une aura qui augmente de 15 % la résistance aux afflictions physiques chez les alliés pendant 3 (II: 5) tours. (ne peut pas être placé s'il y a déjà une aura)   |
| 2 |  Réjuvénation (2 Eclats)  |  Peut se retirer un débuff à une stat (II : peut le faire à un autre personnage)  |
| 2 |  Tonification (INT, 2 Eclats)  |  Ajoute +3 (II : +5) d’armure physique à un personnage pendant trois tours si réussite du jet.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture d’aide  |  Permet de passer en posture d'aide (II: posture d'aide+)  |
| 3 |  Vaccin (INT, 1 Eclat)  |  Peut apporter une résistance de 20 % à une affliction à un personnage. (Si échec critique, lui inflige l’affliction)  |
| 3 |  Frappe pestilentielle (trait)  |  Pour chaque attaque, si celle-ci est critique, l’ennemi fait un jet d’affliction physique.  |
| 4 |  Frappe négative (trait)  |  Une attaque qui inflige en termes de dégats le nombre de point de dégats reçu (II: l'attaque devient à 50% perce armure)  |
| 4 |  Réécriture du phénotype (6 Eclats)  |  Peut modifier son corps sur un jet d’INT, dans les limites possibles par la physique.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Soin Négatif (INT)  |  Fait un premier soin en tant qu’attaque sur un ennemi. Profite de tous les bonus et malus passif de soin. L'attaque se produit sur l'armure magique (II: perce-armure total)  |
| 5 |  Soin pestilentiel (INT)  |  Un soin de base, mais dont le résultat est multiplié par 1D4. Cependant, le personnage fait le résultat du D4-1 jet d’affliction physique.  |
| 5 |  Purification extrème (INT, 5 Eclats)  |  Retire toutes les afflictions physiques et débuf aux stats physique à un personnage, mais fait un 1D6 dégat sur les PE.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Annihilation (1 karma)  |  Peut vaincre une fois par partie un ennemi non-boss et mortel  |
| 6 |  Résurrection (1 karma)  |  Ramène un personnage à la vie, à 0 PV (le personnage reste inconscient)  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Sur-soignement (trait)  |  Les soins du cancer peuvent dépasser le maximum de PV si le personnage visé est avant le soin en dessous de ce maximum  |
| Herault |  Choc opératoire (INT)  |  Si un personnage est atteint d'une ou plus altération physique, peut faire 1D12 dégat par altération, mais les soigne toutes.  |

