---
layout: layouts/base.njk
tags: Signes de Guérison
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Balance
parent: Les signes
---

# Balance (justice et equilibre)

Les balances tirent leur pouvoir de l'équilibre : que celui-ci soit entre les situations, les valeurs numériques ou les énergies qui traversent le corps. Ce pouvoir en font donc de parfais représentants des arcanes de la guérison et de la malice. Elles peuvent manipuler les énergies vitales, les redistribuer, mais également manipuler certaines quantités autour d'elles, notamment la chance.

On rapproche pour ce fait de l'arcane de la justice, avec l'ide de manipuler les gains et les blessures.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :libra: | Chien (asie), Maât (égypte) | Guérison et malice (Healer Trickster) | Ordre | Peu commun (05.5%) | SAG et INT | Vert |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Synchro (trait)  |  Lorsque la balance se prend une altération physique, jette un dé 6, si le résultat est au-dessus ou égal à 5, l'ennemi ayant attaqué se prend l'altration (sauf immunité)  |
| Double |  Synchro totale (trait)  |    |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Aide de PV (1 Éclat)  |  Choisi un personnage allié. À la prochaine attaque que subira ce personnage, les dégats (après armures) seront transférés à un autre PJ au choix du perso Balance.  |
| 1 |  Soin physique (INT)  |  Peut soigner l’équivalent des niveaux de réussite du jet (+1 si pas de baton de soin)  |
| 2 |  Transfert de PV  |  Peut sacrifier ses PV pour augmenter la puissance d’un soin de base.  |
| 2 |  Transfert de dégats  |  Transfert des dégats d’un ennemi à un autre (après armure).  |
| 2 |  Gains communs (trait)  |  Augmente de 20 % les gains d’argent ou les jet de loot de toute l’équipe (II : toute le reste de l’équipe gagne 10 % du jet original)  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture d’aide  |  Permet de passer en posture d'aide (II: posture d'aide+)  |
| 3 |  Répartition égalitaire des dégats (SPI, 6 Éclats)  |  Réparti toutes les blessures des ennemis et alliés de manière équitable.  |
| 3 |  Situation de crise  |  Double toutes les pressions, allié comme ennemi.  |
| 4 |  Inversion (3 Eclat)  |  Inverse-les dégats physiques et d’esprit reçu par la cible.  |
| 4 |  Partage de souffrance (INT, 2 Eclats)  |  Peut répartir les dégâts d’une attaque subit par un ennemi sur le groupe ennemi.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Justice des critiques (3 Eclat, 1/partie)  |  Si le personnage fait un échec critique, peut forcer un échec critique sur un autre dé.  |
| 5 |  Repos du juste  |  Peut faire un soin (quel qu’il soit) après la réussite critique d’un autre joueur.  |
| 5 |  Le bras armé de la justice (1/tour)  |  Si un ennemi attaque un allié (ou l’inverse, au choix du joueur) en position de faiblesse ET qui ne pourrait pas se défendre, ou presque pas, ou en posture d’aide, le personnage obtient une attaque d’OP. (II : peut le faire 2 fois par tour).  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Échange de peines (1 karma)  |  Peut déplacer un KO ou une mort vers un autre PJ ou PNJ (ennemi ou allié) au choix  |
| 6 |  Lame de la… vengeance ? (trait)  |  Gagne +2 dégat pour chaque personnage qui a été mis K.O. dans l’équipe durant la partie. (II : +3)  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Jugement de Salomon (INT ou SAG, 4 Eclat)  |  Fait les jets pour un soin de base, et une attaque « normale ». Cumule les deux, et en fait un soin pour un personnage allié, et une attaque sur un ennemi.  |
| Herault |  Colère de la Balance (4 Eclats 1/combat)  |  Lance une attaque sur un ennemi à 1D8 pour chaque allié qui s’est pris une attaque de la part de cet ennemi dans le combat. Effet d’Ordre.  |