---
layout: layouts/base.njk
eleventyNavigation:
  key: Actions de combat
  parent: Système de combat
  order: 2
---

# Déroulement d'une attaque

Dans une confrontation, des personnages tapent sur d'autres, que ce soit en terme physique, moral ou social. Le but de cette page est de présenter un peu le fonctionnement d'un combat.

## Ordre des actions

L'attaque utilise deux jets pour être effectuée. Vous devez d'abord effectuer un jet du dé dont vous avez besoin, et compter le *nombre de niveaux de réussite*. À cela vous rajouterez le *jet de votre arme* (si une armure correspond au type d'attaque), ce qui vous donnera la puissance de l'attaque en question. Le personnage visé peut ensuite se défendre, soit en encaissant, soit en tentant une esquive.

> **Attention:** Les échecs critiques passent par dessus les réussite et les réussites critiques.

## Attaquer

Pour attaquer votre ennemi, vous pouvez avoir deux grandes manières de faire : les attaques physiques et les attaques mentales. Les attaques physiques visent les PV, et sont résisté sur l'armure physique. Les attaques mentales visent les PE et sont résisté sur l'armure mentale. 

Si un personnage utilise deux armes à une main, il peut alors effectuer deux attaques, mais verra ses niveaux de réussites divisé par deux.

| Type d'attaque | Stat utilisée | Dégats sur | Résiste sur | Esquive sur | Armure utilisées | Armes possibles  |
|:--------------:|:-------------:|:----------:|:-----------:|:-----------:|:----------------:|:----------------:|
| Corps-à-corps  | FOR | PV | CON | HAB | Physique | Épées, bâtons, etc |
| Distance | PER | PV | CON | HAB | Physique | Arme à feu, arc, etc |
| Intimidation  | CHA | PE | VOL | DIS | Physique | Quelques armes uniques |
| Manipulation | DIS | PE | VOL | DIS | Physique | Quelques armes uniques |

Des magies peuvent exister également pour toucher l'armure spéciale. Si un personnage peut faire plusieurs attaques en un coups (par exemple avec l'ambidextrie), le personnage fait un jet par attaque.

## Se défendre

Pour se défendre, un personnage à le choix entre l'encaissement et l'esquive. De base, si un personnage ne fait rien ou échoue sa protection, il ne résistera que de son armure.

- L'encaissement utilise un jet de CON (VOL pour les attaque mental, REL pour celles sociales) pour réussir, et permet de réduire les dégats des niveaux de réussites, auquel peut s'ajouter les boucliers.

- L'esquive fait une confrontation d'HAB avec l'attaque ennemie pour éviter entièrement l'attaque.

Si un personnage utilise deux bouclier, il peut cumuler l'effet des deux boucliers.

|  | Réussite critique | Réussite | Échec | Échec (critique) |
|:-|:--------------------|:---------|:------|:-----------------|
| **Encaissement** | Ajoute bouclier + niveaux de réussites à l'armure (doublé) | Ajoute bouclier + niveaux de réussites à l'armure | Se prend l’attaque. | Se prend l’attaque (doublée) |
| **Esquive** | Évite totalement l’attaque + a le droit à une attaque d’OP | Évite totalement l’attaque. | Se prend l’attaque. | Se prend l’attaque (doublée) |

Les personnages ont trois types d'armure : l'armure physique (qui permet d'encaisser les attaques physiques), l'armure magique (qui permet d'encaisser les attaques magiques) et l'armure mentale (qui permet d'encaisser les attaques mentales).

## Attaques spéciales

Différents effets peuvent s'appliquer aux attaques et affecter les personnages différemment :

- L'effets **perce-armure** vont voir une partie de leurs dégats ignorer l'armure, et ne pouvoir être défendu que par le jet de défense.

- Les dégâts **perce-défense** font que les dégâts vont traverser la défense (et l'esquive) et ne pouvoir être protéger que par l'armure.

- Les dégâts **brise-armure** font perdre un points d'armure tout les six dégâts (en priorité sur le bouclier s'il est utilisé).

- Les dégats de **chute** sont à 1D12, lancé le nombre de fois correspondant au nombre d'étage de la chute. Ils sont brises-armures.

- Les dégats d'**explosions** font des dégats fixes suivant la liste des dés (4, 8, 10, 12, 20) et sont perce armure *ET* à moitié perce-défense.

### Viser un endroit

Viser un membre précisément permet d’affecter le membre et de faire des dégâts dessus. Une attaque sur un membre met des points d’incapacitation.

| Membre | Malus de précision | Effet d’impact | Incapacitation | Incapacité par |
|:----|:----------:|:------------|:-------------|:------:|
|Tête|-40%|+5 dégat si touché.|N/A|N/A|
|Œil|-60%|+5 dégat si touché.|Malus de 50 % pour voir.|Perçant|
|Bras|-20%|N/A|Malus de 50 % en FOR.|Tranchant|
|Main|-40%|Jet d’HAB ou CON pour garder son arme.|Ne peut plus utiliser cette main.|Tranchant|
|Jambes|-20%|Le personnage fait un jet d’HAB pour ne pas tomber|Malus de 50 % en HAB pour se déplacer|Tranchant|
|Genoux|-40%|Dis qu’il n’a plus de genoux.|Tout les jets de volonté sont des jets affaibli (/2)|Perçant|

## Soins

Lorsqu’un allié s’est prit des dégâts, il est possible de vouloir le soigner. Lorsqu’on tente de soigner quelqu’un, un jet de soin doit alors être fait. Pour pouvoir soigner, il faut avoir une capacité qui offre la possibilité de soigner.

Le soin peut se faire généralement sur l’INT ou l’HAB (voir SAG dans certaine condition). Des objets peuvent améliorer le soin, mais l’utilisateur à un niveau de soin général de 1.

| Réussite critique | Réussite | Échec | Échec critique |
|:-----------------:|:--------:|:-----:|:--------------:|
| Soigne le niveau de réussite ×2 + potentiel équipement de soin. | Soigne le niveau de réussite + potentiel équipement de soin. | Bah t’as pas soigné. C’est ballot. | Retire le niveau d’échec à la personne visée. |