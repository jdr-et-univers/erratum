---
layout: layouts/home.njk
eleventyNavigation:
  key: Les espèces
  parent: Personnages
  order: 2
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
title: Les espèces
---

# Les espèces

Erratum propose de nombreuses espèces jouables, divisé en trois "pack" d'espèces. Certaines espèces peuvent avoir des espèces dérivées, ayant de potentielles différences de stats et de pouvoirs.

## Éspèces jouables

Ces espèces sont les espèces que vous pouvez jouez avec vos personnages.

### Espèces de base

Ce sont les espèces les plus courantes, couvrant une majorité des espèces intelligentes terrestres. Dans cette catégorie, les homininés sont majoritaires (avec les humains, hybrides et lycanthrope), et une espèce féérique est présente (les danaïtes).

- Les [humains](humains/) sont l'espèce la plus commune sur Terre. Membre du groupe des Homo Sapiens, ils sont connus pour leur caractère industrieux.
- Les [hybrides](hybrides/) sont une espèce ingéniéré par les humains vers le 15~16e siècle, par les alchimiste européens. Créé en masse, ils sont depuis une espèce très présente sur terre.
- Les [lycanthropes](lycanthropes/) sont une espèce d'humains qui ont été affecté par l'influence du dieu-loup. Ils ont une forme plus ou moins lupine, et des liens psychiques forts entre eux.
- Les [danaïtes](danaites/) sont une espèce végétale aux pouvoirs magique forts, de la branche des fées.

### Espèces additionnelles

Ce sont des espèces moins communes dans les territoires habituels, parce qu'ils vivent en marge de la société. Cependant, vous pouvez très bien les jouer dans une campagne normale.

- Les [ondines](ondines/) sont une espèce humanoïde vivant dans les océans. Leur humanité a été longtemps déniée.
- Les [vampires](vampires/) sont une espèce très rare, vivant en petites société fermées, de morts vivants.
- Les [trolls](trolls/) sont une espèce minérale, fonctionnant radicallement différemments des êtres biologiques. Ils sont présents surtout dans les montagnes.
- Les [kobolds](kobold/) sont les serviteur des dragons, très proche d'eux biologiquement.
- Les [pumas](pumas/) sont des pumas qui parlent. Yep. Une longue histoire.

### Espèces légendaires

Ces espèces sont extrèmement rares, voir impossible à jouer. Ils ont souvent des importances scénaristiques, étant lié soit aux dieux, soit aux aspects "mystiques" de l'univers.

- Les [anostiens](anostiens/) sont une espèce disparue aujourd'hui, le quatrième peuple antique.
- Les [anges](anges/) sont une espèce créé par les dieux pour les servire.
- Les [diablons](diablons/) sont une espèce créé par les dieux pour combattre leurs guerres.

## Classification biologique

- Les **hominines** sont les descendants des homo sapiens, s'étant adaptés à différents milieux et conditions, ou ayant été affecté par diverses magies.
- Les **fées** sont des êtres végétaux, fortmeent influencés par l'état naturelle et la force magique environnante.
- Les **morts-vivants** sont des corps ramenés à la vie et animés par une magie ancienne.
- Les **êtres de magies pures** comme les anostiens sont des êtres composé à 100% d'éclat, qui créé un corps physique dans lequel ils peuvent se mouvoir.
- Les **dragonoïdes** sont des reptiles anciens et intelligent, souvent dotés d'ailes.
- Les **minéraloïdes** sont des êtres minéraux, composé de roche se mouvant grâce à la magie, tel que les troll.
