---
layout: layouts/base.njk
tags: Autour des signes
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Heraults
parent: Les signes
---

# Herault

Les *hérault* des signes sont des êtres ayant poussé leur signe extrèmement loin. On raconte qu'ils possèdent tous quelque part dans le monde un temple qui permettrait de débloquer les secrets cachés de son signe.

Le serpentaire n'a pas de hérault.

## Liste des héraults

Les 12 **héraults de signes** permettent d'obtenir les pouvoirs de herault de chaque signe.

<div class="table-responsive">

| Signe | Hérault | Description | Période | Localisation |
|:-----:|:-------:|:-----------:|:-------:|:------------:|
| Cancer | **Ibn Sina** (Avicenne) | Fondateur de la médecine moderne | 10~11e siècle | Hamadan |
| Vierge | **Bakhna Rakhna** | Première reine des Yumboes, grande ruche danaïte d'Afrique de l'Ouest | 7e siècle | Afrique de l'Ouest |
| Balance | **Salomon** | Antique roi d'Israel, réputé pour sa sagesse | -950 | Jérusalem |
| Scorpion | **Baba Yaga** | Fondatrice des sorcières de Russie | -1200 | Caucase |
| Lion | **Bodicée** | Reine des Icenie, héréoïne ayant combattu l'Empire Romain | 1er siècle | Norfolk |
| Saggitaire | **Jinmu** | Fondateur légendaire du japon, célèbre pour son arc divin | -600 | Japon |
| Bélier | **Tasi Hangbé** | Fondatrice du corps des « Amazones » dans le royaume de Dahomey | 17e siècle | République du Bénin |
| Taureau | **Pemulwuy** | Grand résistant aborigène | 17e siècle | Australie |
| Verseau | **Lao Tseu** | Fondateur du taoisme | -600 | Chine |
| Gémeaux | **Xolotl** | Frère jumeaux du dieu Quetzacoatl | 10e siècle | Mexique |
| Capricorne | **Médée** | Héroine et sorcière grecque, ayant aidé et été trahie par les Argonaute | 17e siècle avant JC | Méditérannée |
| Poisson | **?????** | Hérault inconnu⋅e. On raconte que son temple est entièrement une illusion. | ??? | Partout |

</div>

## Hérault d'Arcane

Les **hérault d'Arcane** ou **Grand-Hérault** sont quatre être ayant maitrisé tout les aspect d'une arcane, et pouvant donc offrir des nouveaux pouvoir à un⋅e manipulateur⋅ice de ladite arcane. Ces quatre êtres sont à la limite entre divinité et humanité. Ils sont aux nombres de 4.

<div class="table-responsive">

| Arcane | Hérault | Description | Période | Localisation |
|:-----:|:-------:|:-----------:|:-------:|:------------:|
| Guérison | **Nokomis** (Grand-Mère Feuillage) | Danaïte mythique devenu une avec la nature | Ancestrale | Amérique du Nord |
| Force | **Gilgamesh** | Le roi-héros, aux exploits légendaires | -3000 | Babylone |
| Protection | **Hervor** | Grande héroïne Skjaldmö ayant pu jusqu'à rivaliser avec les valkyrie | 4e siècle | Scandinavie |
| Malice | **Maui** | Grand trickster et protecteur des hommes en polynésies et dans tout le pacifique | -3000 | Polynésie |

</div>

### Pouvoirs des Héraults

| Arcane | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Guérison | Restoration (HAB/2, 1/partie) | Peut soigner une malédiction (hors malédiction antique) |
| Force | Éradication (1/partie, 1 karma) | Peut éradiquer toute un groupe d'ennemis faible. Le récompenses sont obtenues normalement |
| Protection | Sanctuaire (1/partie) | Créer une zone qui offre +25% armure, une regen de 5PV&PE/tours à tout les alliés, et +20% aux jets de survies |
| Malice | Situation "critique" (1/partie, 1 karma) | Toutes les actions seront des réussites critiques durant 1 tours. |

## Anciens hérault (≤ v2.4)

> **Note:** Cette liste de herault représente les heraults avant la version 2.4.1

Les 12 heraults permettent d'obtenir les pouvoirs de herault de chaque signe.

<div class="table-responsive">

| Signe | Hérault | Description | Période | Localisation |
|:-----:|:-------:|:-----------:|:-------:|:------------:|
| Cancer | **Ibn Sina** (Avicenne) | Fondateur de la médecine moderne | 10~11e siècle | Hamadan |
| Vierge | **Bakhna Rakhna** | Première reine des Yumboes, grande ruche danaïte d'Afrique de l'Ouest | 7e siècle | Afrique de l'Ouest |
| Balance | **Salomon** | Antique roi d'Israel, réputé pour sa sagesse | -950 | Jérusalem |
| Scorpion | **Baba Yaga** | Fondatrice des sorcières de Russie | -1200 | Caucase |
| Lion | **Gilgamesh** | Le roi-héros, aux exploits légendaires | -3000 | Babylone |
| Saggitaire | **Jinmu** | Fondateur légendaire du japon, célèbre pour son arc divin | -600 | Japon |
| Bélier | **Tasi Hangbé** | Fondatrice du corps des « Amazones » dans le royaume de Dahomey | 17e siècle | République du Bénin |
| Taureau | **Shaka** | Fondateur du royaume Zoulou | 17e siècle | Afrique du Sud |
| Verseau | **Einstein** | Découvreur de nombreux théories scientifiques | 20e siècle | USA |
| Gémeaux | **Xolotl** | Frère jumeaux du dieu Quetzacoatl | 10e siècle | Mexique |
| Capricorne | **Socrate** | Philosophe, et un peu un troll aussi | 5e siècle avant JC | Grèce |
| Poisson | **?????** | Hérault inconnu⋅e. On raconte que son temple est entièrement une illusion. | ??? | Partout |

</div>