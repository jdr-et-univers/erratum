---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: L'artiste
parent: Les spécialités
---

# Les artistes

Les artistes sont une classe de supports qui peuvent fournir des bonus ou des soins à tout les alliés.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Pouvoirs de l'amour | Les effets positifs posé par les alliés durent un tour de plus |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:------|
| 1 | Solo d'Enfer | REL/CHA | 2 tours de cooldown | Inflige 2 dégat brut si réussi (pas de niveaux de réussites) | 
| 1 | Menuet du Calme | REL | 2 tours de cooldown | Soigne 2 points de moral à toute l'équipe (pas de niveau de réussite) |
| 2 | Chant de Renforcement |  |  | Ajoute +2 d'armure mentale durant 4 tours | 
| 2 | Balade d'encouragement | REL |  | Augmente de 10% les chances des alliés de toucher les ennemis pendant 4 tours | 
| 2 | Saga épique | REL |  | Les personnages alliés font +2 dégats pendant 3 tours | 

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:------|
| 3 | Posture d'aide |  |  | Peut passer en posture d'aide | 
| 3 | Pamphlet incendiaire | REL |  | Baisse de 10% les chances des ennemis de toucher les alliés pendant 4 tours | 
| 3 | Mon héros! | REL |  | Tout allié protégeant l'artiste à +3 armure et +15% de CON si l'artiste réussi sont jet de REL |
| 4 | Clasheur (posture) |  |  | En combat social, fait 1.5 dégat mais se prend 1.5 dégâts sociaux | 
| 4 | Chant de mouscle |  |  | Ajoute +2 d'armure physique à toute l'équipe durant 4 tours | 

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:------|
| 5 | Canon (trait) |  |  | Peut cumuler deux fois un même chant en même temps | 
| 5 | Choeur |  | 3/partie | Si d'autres artistes sont présent, peut effectuer une actions de coeur pour amplifier les effets d'un chant. | 
| 5 | Hymne des ombres | REL/CHA |  | Une attaque mentale touchant tout les ennemis | 

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:------|
| 6 | Encore | REL | 1/partie | Peut offrir une utilisation supplémentaire gratuite à une action limité d'un⋅e allié⋅e | 
| 6 | Le final | REL | 1/partie | Une attaque qui fait 5 dégats bruts, +5 par chants effectués durant le combat | 

