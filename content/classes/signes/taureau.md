---
layout: layouts/base.njk
tags: Signes de Protection
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Taureau
parent: Les signes
---

# Taureau (le corps)

Les taureaux tirent leur pouvoir de leur propre corps. Ils sont capable de l'utiliser pour se défendre où pour combattre, tirant partie de tout ce qui le compose. Ils ont le pouvoir de tirer le maximum de ce que leur corps permet.

Combattants émérites, ils sont souvent aussi bien capable de protéger que d'attaquer. Signe le plus commun, mais en aucun cas le plus faible.

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :taurus: | Boeuf (asie), ??? (égypte) | Protection et Force (Tank DPS) | Ordre | Commun (22.0%) | FOR et CON | Marron |

</diV

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Armure naturelle  |  L’utilisateur à naturellement +3 en armure physique  |
| Double |  Double épaisseur d’armure naturelle  |  Rajoute +2 à l’armure naturelle physique.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  A deux mains  |  Gagne un bonus de +10 % en Force (II : 25 %) et 3 dégats supplémentaire si le personnage utilise une arme à deux mains.  |
| 1 |  Force brute  |  Rajoute +2 (II : +4) aux attaques en corps à corps (sans armes).  |
| 2 |  Boost du taureau (6 Eclat)  |  Augmenter de 30 % ses stats physiques pour les deux tours (II : 3 tours)  |
| 2 |  Cri du taureau (CHA)  |  Cri de zone qui étourdira les ennemis (effet hypoactivité) proches  |
| 2 |  Anticorps  |  Résistance de 15 % pour recevoir une affliction physique hors injection, ou potion (ou effet principal d’une attaque).  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture défensive  |  Peut passer en posture défensive (II : posture défensive +)  |
| 3 |  Taunt (CHA)  |  Peut attirer l’aggro d’un ennemi pour 3 tours (II : 5 tours)  |
| 3 |  Blocage (FOR)  |  Après un jet d’encaissement, peut bloquer l’ennemi.L’ennemi doit faire une confrontation de FOR pour se libérer.  |
| 4 |  Prêt à riposter (3 Eclats)  |  Peut pendant trois tours riposter à chaque attaque qui lui sera donnée.  |
| 4 |  Brutalité  |  Peut provoquer un saignement à main nue, sur 1D4 (II : le fait aux résultats 1 et 2)  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Homerun  |  Peut éjecter un personnage d’un combat, ennemi ou allié, une fois par parti. Attention, l’ennemi revient affronter dans un autre combat s’il est éjecté.  |
| 5 |  Chateaudepierre  |  Si n’attaque pas, encaisse la moitié [3/4] des dégâts d’une attaque physique.  |
| 5 |  Frappe verticale (3 Eclats)  |  Une attaque qui peut projeter un ennemi en plus des dégâts. À la chute, l’ennemi fait 1D4, si 4, il ne pourra pas attaquer au tour suivant. Dégâts de chutes.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Armure parfaite  |  Ignore tous les perce-armure (II : Ignore 25 % des perce-défense)  |
| 6 |  Coagulation (trait)  |  Les saignements soignent au bout de 3 tours.  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Tenacité (trait, 1/partie)  |  Le joueur survit à un coup qui devait le faire tomber dans le coma avec 1PV.  |
| Herault |  Second souffle (SAG, 1/partie)  |  Si le personnage est en dessous de 25 % de ses PV, regère tout ses PV.  |