---
layout: layouts/base.njk
title: Diablons
tags: Espèces légendaires
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Diablons

Les diablons sont le pendant guerrier des anges. Ces êtres rouges assexués, dôtés de cornes et d'une queue très similaires à celles des anostiens, sont également des êtres créé par les dieux pour les servir, mais cette fois dans le domaine guerrier.

Les diablons forment le gros de l'armée des dieux, et leur obéïssent et son prêt à combattre jusqu'à la mort pour leur dieu. Ils sont puissant et dangereux, et sont souvent construit à partir de fantômes ou spectres.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| +2 | -2 | +2 | -1 | Tir de diablon (4 perce-armure) | VOL, FOR (+10) | SAG, REL (-10) | |

## Traits et compétence

| Trait | Effet |
|:-----:|:------|
| Nature diabolique | +3 dégat sur toutes les attaques magiques. | 
| Serviteurs | -20 % en volonté face aux dieux. |
| Elements faibles | Lumière, Ordre, Éclat |
| Éléménets forts | Ombre, Chaos |