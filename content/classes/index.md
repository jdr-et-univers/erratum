---
layout: layouts/home.njk
eleventyNavigation:
  key: Classes et experiences
  order: 2
---

# Classes et experiences

Les personnages peuvent évoluer dans Erratum suivant un concept de classes se voulant offrir des possibilités riches et diversifiés. En effet, les classes y existent en plusieurs types, et offrent des possibilités différentes suivant si votre personnage à dans son lore des pouvoirs magiques ou non.

Les classes utilisent [l'expérience](experience/) pour évoluer, offrant des capacités de plus en plus puissantes. De plus, l'évolution et l'accès aux différents pouvoirs peut se produire de manière RP de plusieurs manières.

## Différents types de classes

- [Les signes](signes/) sont des pouvoirs magiques naturels qui s'acquierts ou apparaissent dans une petite partie de la population.
- [Les magies](magies/) sont des disciplines magiques plus ou moins accessibles aux personnages, offrant chacune une mécanique qui lui est propre.
- [Les talents](talents/) sont des pouvoirs communs, accessibles à tous suivant leurs personnalités.