---
layout: layouts/home.njk
eleventyNavigation:
  key: Inventaire
  order: 4
---

# Inventaire

De nombreux objets pourront aider vos personnages durant votre aventure. Ces objets peuvent être soit des consommables, soit des équipements, soit d'autres types d'entités pouvant avoir un effet plus ou moins particulier sur votre personnage, son entourage, les ennemis, etc.

## Pages dans cette catégories

- [Objets](objets/) - Présente les objets consommables
- [Équipements](equipements/) - Présente les différents équipements (armes, boucliers, armures, etc) que vous pourrez porter
- [Véhicules](vehicules/) - Présente les différents véhicules que vous pourrez conduire ou utiliser durant votre aventure