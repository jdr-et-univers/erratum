---
layout: layouts/base.njk
eleventyNavigation:
  key: L'univers magique
  parent: Lore et univers
  order: 1
models:
  - La société magique
title: L'univers magique
---

# L'univers magique

À côté de la société scientifique exite en parallèle toute une société lié à la magique. Cette société s'est développé et à ses propres aspects, et est connue un peu de loin par les personnes. Les magiciens ont leurs propres organisations, leurs propres groupes de pouvoirs et leurs propres façons de fonctionner. Cependant, ils sont aussi généralement des membres du monde "normal", et sont lié par leurs loi gouvernementales.

## L’éclat

L'éclat est le nom donnée à la magie, ou plus précisément à la forme pure de la magie tel qu'elle existe naturellement. Se comportement comme un gaz supercritique, ou la différence entre liquide et gazeux est difficilement compréhensible.

Cette particule est particulière, parce qu'elle n'entre pas dans le cadre du modèles standard des particules, ne correspondant à aucune particule connue. Cependant, on pense que le "champ" auquel elle est lié est le champ entropique, ce qui en ferait un boson au même titre que le photon ou le gluon.

### Éclat naturel

L'éclat est extrêmement difficile à trouver à l'état naturel (qui sont créateur d'information). Il est en grande partie produit par les êtres vivants, plantes, êtres. L'éclat est toujours légèrement concentré dans l'air : plus il y a d'être vivant dans la zone, plus il y a d'éclat.

Il circule également sous forme d'immense courant de magie dans la terre, courants nommées les *courants telluriques*. Quelques grandes sites magiques sont situés dans des *nœuds telluriques*.

Les créatures magiques ont tendance à produire plus d'éclats que les autres.

## L'entropie

L'entropie (ou entropie magique) est un phénomène théorisée par les alchimiste. Sa définition est quelque peu différente de l'entropie telle qu'exprimé en physique.

En effet, l'entropie désigne ici l'entropie de l'énergie magique environnante, c'est à dire son degré d'imprédictibilité et la quantité d'information. Cependant, la quantité d'information affectant la magie est tout simplement immense : chaque donnée physique, métaphysique ou ne serait-ce que pouvant être posée en hypothèse fait partie de l'information magique. Dis d'une autre manière, l'entropie magique représente les différences configurations possible de l'univers.

Naturellement, l'entropie magique à tendance à s'uniformiser. Cependant, c'est là où peut arriver une *modification de l'entropie*, se rapprochant de la notion de hasard tel que présente dans l'informatique. En effet, la modification de l'entropie magique est l'ajout d'un élément extérieur dans le système magique, c'est à dire la création d'une *conséquence sans cause*. Par effet boule de neige, chaque information pouvant être lié aux autres dans un système magique, sans forcément de liens "physique" mais parfois juste conceptuel, une modification de l'entropie peut avoir des effets impressionnant.

Les légendes racontent que le peuple des anostiens avaient une certaine maitrise de l'entropie.

## Sortilèges pré-conçus

Les sortilège préconçus sont des sortilèges qui ont été conçu et adapté précisément à partir d'une écriture magique - voir plusieurs, même si le mixage d'écriture magique peut donner parfois des résultats inattendu pour qui ne connais pas, à cause de "soucis de traduction", de concept n'existant pas forcément dans la conception de l'autre langue.

Si tout le monde ayant une certaine maitrise de son éclat peut activer un sortilège, pré-conçus, les construire demande une maîtrise plus grande cette fois de l'écriture magique en question.

Depuis le début de l'ère contemporaine, le conglomérat Frontière travaille sur des sortilège préconçus informatisé accroché à une batterie à éclat.