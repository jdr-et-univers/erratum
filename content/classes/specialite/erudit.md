---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Les érudits
parent: Les spécialités
---

# Les érudits

L'érudit est la classe des personnes curieux et voulant obtenir de la connaissance. Cette classe peut utiliser l'érudition pour activer ses compétences.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Les savoirs de l'érudit | Peut prendre un tour d'action d'exploration pour obtenir des infos sur le lieux ou ils sont |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | L'armure du savoirs |  |  | Peut dépenser 5 points d'érudition pour gagner 1 points d'armure mentale durant la partie |
| 1 | Analyse basique | PER |  | Permet d'avoir les informations basiques d'un ennemi en combat (pv/pe, statistiques, armes/armures visibles). |
| 2 | Perce-mensonge (trait) |  |  | À naturellement un bonus de 30 % pour détecter les bobards et les mensonges. |
| 2 | Cache ses muscles | DIS |  | Si réussi à faire une attaque physique. |
| 2 | Laisser trainer l'oreille | PER/INT/REL |  | Dans un lieu de sociabilisation, obtient jusqu'à trois rumeurs en faisant des actions social |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture focus |  |  | Peut passer en posture focus |
| 3 | Libraire | PER |  | +20% de trouver des livres lié aux connaissance du lieu lors des fouilles |
| 3 | Loredump | INT |  | Inflige sommeil et 1D10 dégat mentaux à un ennemi, qui peut résister sur un jet de VOL |
| 4 | L'amour de la sagesse |  |  | Gagne +5 armure mentale quand du savoir est en jeu |
| 4 | La quête du savoir (trait) | | | Peut gagner 1D4+2 érudition par discussion importante du groupe avec un PNJ ou énigme réussie |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Découverte des secrets |  | 1/partie | Peut poser une question au MJ pour 10 points d'érudition pour obtenir une nouvelle information non-révélée pour le moment. S’il refuse, les points n’est pas perdu. |
| 5 | Self-reflection | INT/2 | 3 tours de cooldown | Peut se soigner d'une affliction mentale |
| 5 | Léonard de Vinci | INT+HAB |  | Peut construire avec des matériaux présent des armes simples ou des pièges |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Maître des contes et légendes | SAG/DIS | 1/partie | Fait un jet de DIS ou SAG et ajoute les niveaux de réussite à tout ses stats |
| 6 | Lire le rulebook |  | 1/partie | Peut apprendre de manière canon une information présente sur une classe, une créature, ou un PNJ écrite dans le rulebook pour 20 points d'éruditions |