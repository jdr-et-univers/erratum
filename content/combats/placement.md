---
layout: layouts/base.njk
eleventyNavigation:
  key: Placements
  parent: Système de combat
  order: 3
---

# Placements et déplacement

Les personnages peuvent se déplacer et se placer en combat, ce qui affecte comment le combat se déroule.

## Déplacement

À chaque tour, les personnages peuvent avoir *un point de déplacement*, utilisable avant OU après leur actions. Ces points de déplacement peuvent être utilisé pour se déplacer sur le combat, notamment *changer de ligne* de combat, ou chercher à se cacher. Le point de déplacement est aussi utilisé pour *changer de posture*.

Certaines compétences peuvent utiliser le point de déplacement afin d'utiliser un pouvoir particulier dans ou hors de leur tour, ce qui rajoute un aspect stratégique à l'utilisation du déplacement.

## Placement libre

Lors de combat plus "libre", ou les ennemis peuvnet bien se disperser, le placement des personnages est basé sur leur distances relatives les uns aux autres. Pour les actions, ils faut considérer qui est plus ou moins rapide.

## Placement en rang

Un combat ou chaque groupe est bien "rangé" peut se diviser principalement en deux groupe, chacun composé de deux ligne :
- La ligne frontale est celle le plus proche de l'autre groupe
- L'arrière ligne est plus éloignée.

Ces lignes sont également présentent dans d'autre cas, si les ennemis entoures les héros, ou l'inverse, par exemple (avec des spécificités).

Dans ce genre de disposition, une attaque au corps à corps ne pourra que toucher la front-ligne, mais l'arrière-ligne ne pourra pas faire d'attaque au corps à corps sur les ennemis. Changer de ligne *coute un déplacement*.

### Attaque et défense d'opportunité

Ce système permet certaines *actions d'opportunité* :
- Si un ennemi tente de traverser la ligne pour aller attaquer quelqu'un de l'arrière ligne au cac, des membres de la ligne frontale pourront avoir une *attaque d'opportunité* ou une *défense d'opportunité* pour protéger leurs collègues.
- De même, certains tirs (non en cloche) pourront provoquer des défense d'opportunité de la ligne frontale.

## Les postures

En combat où en action, les personnages peuvent adopter une des quatre postures (correspondant à quatres visions possible d’un personnage : DPS, Tank, Healer et Trickster). Chaque posture possède également une version améliorée. C’est au MJ et au créateur du jeu de décider quand les joueurs obtiennent les postures. En utilisant un système de compétence par arbre, le niveau 2 semble un bon moment pour l’acquisition des postures. Changer de posture désactive l’ancienne posture et reste active jusqu’à être désactivée et ne peut être changé qu’une fois par tour, avant action.

Lors du changement de posture, on peut également choisir la posture neutre, qui n’a aucun effet positif ou négatif. Lorsqu'un personnage est KO, toute posture est annulée. 

Changer de posture coute un déplacement

| Nom posture | Effet positif | Effet négatif | Version + |
|:-----------:|:-------------:|:-------------:|:---------:|
| Posture d'aide | Soin x2 ; +30% pour aider | Plus d'action offensive direct | Soin x3 ; +50% pour aider |
| Posture offensive | +10% réussite critique ; +1 dégats par tranche de 5 dégats d'armes | Ne peut plus se défendre ; -10% pour action non aggressive | +1 dégats bruts par tranche de 5 dégats d'armes ; -20% action non-aggressive |
| Posture défensive | +15% esquive et encaissement ; +4 armure naturelle | -20% pour attaquer | Ne peut plus attaquer ; +8 armure naturelle |
| Posture focus | +5% à tout les jets ; conso éclat -1 | Ne peut plus esquiver | +10% tout les jets ; -2 éclat |