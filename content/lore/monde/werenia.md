---
layout: layouts/base.njk
tags: Établissements scolaires
title: Werenia
models:
  - Lieux magiques
  - Établissements scolaires
parent: Lieux importants
---

# La ville universitaire de Werenia

La ville universitaire de Werenia est une ville de taille moyenne d'Autriche entièrement dédiée à l'apprentissage de la magie. C'est très simple : 100% de l'activité de la ville est directement ou indirectement dédiée à l'apprentissage de la magie.

La ville contient 61 écoles, dans 16 langues différentes, allant de l'école maternelle aux universités. Elle peut accueillir un nombre d'élève considérable, et presque toute la classe magique moyenne et élevée à fait ses études dedans.

C'est dans cette ville que se trouve également toutes les grandes écoles qui permettent d'accéder à des postes magiques importants.

