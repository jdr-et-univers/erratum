---
layout: layouts/base.njk
title: Les prêtres
tags: Guildes
models:
  - Guildes
  - Guildes métaphysiques
parent: Les guildes
---

# Les prêtres

Les prêtres sont des suiveurs de la république des anges, qui utilisent les pouvoirs de la lumière afin de renforcer des objets. Ils sont fort contre les créatures "sombres". Ils sont souvent aux antipodes des mages occultes. Ils forment des ordres rigides qui vivent en monastère, communiquant régulièrement avec des anges.

Ils ont généralement peu de pouvoirs propre, et leur pouvoir vient de la lumière.

## Pouvoirs de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 | Fléau des ombres (trait) | Lui ou les êtres sous son commandement font ×1.5 dégâts, et ont +5 de réussite critique face à toutes les créatures d’ombres, de néant et de chaos. |

## Angélisme

L'angélisme est une modification de l'invocation qui consiste en invoquer des anges plutôt que des démons.

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Angélisme | Remplace les démons par des anges.<br />Ils auront des pouvoirs positifs et de soins généralement. |

## Bénédiction

Les bénédictions sont une modifications des enchantements.

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Bénédictions | À la place des enchantement, apporte des pouvoirs positifs aux armes/équipements.<br />Peut créer des zones bénies plutôt que des pièges qui vont infleuncer positivement tout les personnages. |