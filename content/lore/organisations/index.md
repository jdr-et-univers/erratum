---
layout: layouts/base.njk
eleventyNavigation:
  key: Les organisations
  parent: Lore et univers
  order: 0
models:
  - Organismes officiels
  - Sociétés secrètes
  - Orgnaisations criminelles
title: Les organisations
---

# Les organisations

Plusieurs grandes organismes et groupes internationaux existent dans le monde.

> **Note:** Tout ces groupes n'ont pas à apparaitre dans toutes les campagnes possibles.

## Organismes officiels

- L'[Organisme International des Mages](oim/) est l'organisme mondial visant à régenter la magie et son fonctionnement.

## Organisation criminelle

- Les [chasseurs](chasseurs/) est un groupe s'attaquant aux créatures magique, tentant de conserver une suprémacie humaine.
- Le [schisme](schisme/) est un groupe dérivé des cultistes.

## Sociétés secrètes

- Les [cultistes](cultistes/) sont les protecteurs des anciennes reliques magique, ayant pour but d'éviter qu'elles tombent dans toutes les mains.
- [Frontières](frontieres/) est une société visant à dépasser