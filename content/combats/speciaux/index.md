---
layout: layouts/base.njk
models:
  - Combats spéciaux
parent: Organisation
---

# Combats spéciaux

Le jeu peut contenir un certain nombre de combats spéciaux.

## Pages présentes

- Les [combats sociaux](sociaux/) sont des confrontations basé sur le social, ou le but est de s'attaquer à la réputation d'un groupe plutôt que ses PV ou PE.