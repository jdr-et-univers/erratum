---
layout: layouts/base.njk
title: Les Phosophoriens
tags: Espèce DfOS
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les Phosophoriens

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Humains ? | Quelques centaines d'individus à travers le cosmos | Sexuée | Aucun | 120 ans |

Des humains qui ont été plusieurs siècles en contact avec le voyage interdimensionnel. Tous les humains ne sont pas Phosphorien : les phosophoriens sont des humains "évolués" ayant existé avant les débuts de l'univers dans un monde alternatifs. Certains sont arrivés dans notre univers, et ils sont aujourd'hui presque tous éteins, à quelques exceptions. Ils sont l'espèce de Lux et de Pulsar, ainsi que des trois des quatres grands dieux.

Certains ont pu survivre hors de la planète terre aux évenements conduisants à la fin des anciennes civilisations, mais ils ont perdu leurs savoirs anciens et secrets.

Niveau apparence, ils sont un peu plus elfiques. Avec des couleurs plus « froides », allant du noir au blanc mais en passant par des teintes d’un gris « froid ».

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 14 | 14 | 12 | 5 | Poings (0) | SAG, INT et VOL (60) | Aucune | Le reste (45) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| La technique | +20 % de capacités à utiliser les machines. |
| Ascension | Peuvent passer naturellement d’une forme physique à une forme psychique. |

## Versatiles

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 15 | 15 | 8 | 4 | Poings (0) | Aucune | Aucune | Tout (50) |

Des robots capables de changer d’apparence à volonté, étant composé de nanorobot. Ils sont des phosphoriens modifiés.

### Stats et compétences

| Trait | Effet |
|:-----:|:------|
| La technique | +20 % de capacités à utiliser les machines. | 
| Réparation | Peuvent remanier leur stat à volonté en début de partie (avec un maximum de +30 par stat). |
| Esprit artificiels | Insensible à la télépathie (pas de lien mental possible avec eux). |