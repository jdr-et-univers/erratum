<!-- docs/_sidebar.md -->

* [Dés et actions](bases/D100.md "Dés et actions")

* [Organisation d'une campagne](bases/campagnes.md "Organisation d'une campagne")

* [Exploration et déplacement](bases/exploration.md "Exploration et déplacement")

* Univers

  * [Le monde](lore/monde.md "Le monde d'Erratum")

  * [Les organisations](lore/organisations.md "Les organisations")

* Personnages

  * [Generalités et stats](personnages/generalites.md "Generalités et stats")

  * [Création](personnages/creation.md "Création")

  * [Les espèces](lore/especes.md "Les espèces")

  * [Personnalités et potentiels](personnages/personnalites.md "Personnalités et potentiels")

  * [Dégats et mort](personnages/vitalite.md "Dégats et mort")

* Amélioration des personnages

  * [L'expérience](personnages/niveaux.md "L'expérience")

  * [Les signes](lore/signes.md "Les signes")

  * [Les magies](lore/magies.md "Les magies")

  * [Le prestige](personnages/prestige.md "Le prestige")

* Combat

  * [Organisation d'un combat](combats/presentation.md "Organisation d'un combat")

  * [Postures de combat](combats/postures.md "Postures de combat")

  * [Déroulement d'une attaque](combats/attacc.md "Déroulement d'une attaque")

* Inventaire

  * [Equipements](combats/equipements.md "Equipements")

  * [Objets](add-ons/objets.md "Objets")

  * [Vehicules](add-ons/vehicules.md "Vehicules")

* [Créatures](add-ons/creatures.md "Créatures")

* [Les vertus](lore/vertues.md "Les vertus")

* [Afflictions et jets](add-ons/afflictions.md "Afflictions et jets")

* [Éléments](add-ons/elements.md "Effets élémentaires")
