---
layout: layouts/base.njk
title: Démons
tags: Les divinités
models:
  - Les divinités
parent: Les divinités
---

# Démons

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  Démons + autre espèce | Rares | Dépend de l'autre espèce | Le chef des démons | 500 ans |

Les démons sont une espèce de créature étrange, proche des demi-dieux, mais sans l'aspect "accouplement entre humain et dieu". On peut les considérer comme une forme de "sous-titans" (des sortes de titans incomplet naturels, non hybridés), dont les pouvoirs seraient les égaux plus de ceux des demi-dieux.

Les démons peuvent provenir de *titans déchus*, dont les pouvoirs ont été diminués à partir d'un pouvoir. De nombreux titans du moyen orient ont subit ce sort lorsqu'est apparu la République des Anges, des titans vaincus par les anges ayant été déchus au rang de titans. Les démons ont un roi, le dieu de troisième génération *Satan*, et vivent généralement dans un univers bulle nommé les Enfers.

| PV | PE | Eclat | Pression | Armes |
|:--:|:--:|:-----:|:--------:|:-----:|
| +5 | +5 | +5 | +2 |  |

## Stats et compétences

|   | Forces | Faiblesses | Neutres |
|:--|:------:|:------:|:------:|
| **Stats** | Deux stats (+15) | Deux stats (-15) | |
| **Élements** | Deux éléments au choix du joueur | Deux éléments au choix du joueur + Éclat (double) |  |
| **Traits** | Pouvoir démoniaque : obtiennent un pouvoir unique propre, avec un effet positif et négatif. | |  |

## Les démons goétiens

Les démonts goétiens sont des démons ayant fait un pacte avec l'espèce humaine, leur accordant leur pouvoir en échange de confiance ou de crainte. Grace à ce pacte, ces démons peuvent, comme les titans de troisième génération, monter en puissance plus de gens dépendent d'eux.

Cela à souvent été vu comme une manière de *donner son âme* aux démons, du à certaines personnes ayant même promis que leur spectre rejoindrait l'armée du démon pour la *Dernière Bataille*. Mais la plupars du temps, cette idée de "vendre son âme" doit être prise au sens figurée, puisque faire ça augmente la puissance des démons pour cette "dernière bataille".

Ces démons utilisent une forme de magie démoniaque pour pouvoir faire apparaitre des instances d'eux sur Terre, capable d'aider les gens appellant leur aide… Cependant, ils restent des démons… ce n'est pas forcément facile de les faire ce qu'on veut.

Il existent de nombreux démons aidant la Goétie.
