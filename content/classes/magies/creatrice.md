---
layout: layouts/base.njk
title: La magie créatrice
tags: Magies Rares
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# La magie créatrice

La magie créatrice consiste à maîtriser une matière nommée le sidérale, composée d'une forme d'éclat solidifié perdant une partie de ses propriétés, qui peut être transformée pour créer l'apparence parfaite de n'importe quelle matière.

C'est une des magies les plus rares et difficile à maitriser.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Manipulation du sidéral (INT)  |  Peut générer une matière nommée le sidéral, qui a la particularité d’être d’abord une sorte de liquide épais, qui se fige vite pour être solide.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Pacte du créateur  |  Peut annuler ou remplacer toute création qu’il a fait, sans aucun jet. A également la capacité d’annuler toute création d’un allier qui lui donne explicitement le droit de le faire  |
| 1 |  La boule magique (INT+HAB)  |  Peut créer des petites sphères en sidéral, et les lancer comme des attaques à distances (1D6)  |
| 2 |  Don de création (SAG+HAB, n eclat)  |  Peut tenter de recréer un objet inanimé déjà vu. La difficulté et la consommation d’éclat seront déterminés avec le MJ. L’objet à une existence limitée.  |
| 2 |  Duplication (4 Eclat)  |  Peut dupliquer une arme déjà présente dans le combat le temps du combat.  |
| 2 |  Conception de piège  |  Peut passer un tour pour créer un piège et le poser à un endroit. Si un ennemi arrive à proximité, il se prend 1D4 dégats bruts (non encaissable ni esquivable) + un effet de pestilence.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Imagination (n eclat)  |  Peut tenter de créer un objet inanimé. La difficulté et la consommation d’éclat seront déterminés avec le MJ. L’objet à une existence limitée.  |
| 3 |  Création et destruction (INT)  |  Peut tenter de dématérialiser un objet en sidéral créé par un ennemi.  |
| 3 |  Bunker magique (INT)  |  Peut créer un mur à côté un personnage qui peut se cacher dedans. Le bunker ne peut ni esquiver, ni encaisser, mais à 5 d’armure et 30 PV.  |
| 4 |  Construction simple (n eclat)  |  Peut tenter de créer un objet animé simple. Le niveau d’autonomie de l’objet serait faible (genre brosse à dent éléctrique). L’objet à une existence limitée.  |
| 4 |  Champs de Force (INT, 2 Eclat)  |  Peut empêcher les ennemis de fuir le combat.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Ingénierie magique (n eclat)  |  Peut tenter de créer un objet animé complexe. Le niveau d’autonomie de l’objet sera de l’ordre d’un appareil électronique et pourra répondre à des ordres basiques. L’objet à une existence limitée.  |
| 5 |  Explosifs (3 Eclat)  |  Peut rendre explosif un objet en sidéral. Si l’objet explose, il fera 1D8 bruts à tous les personnages autour.  |
| 5 |  Armure de combat  |  Peut se créer une armure de combat en sidéral. L’armure de combat a les effets suivants :<br/>– +5 armures physique/magique<br />– 80 points de statistiques à re-répartir.<br /> <br />Cependant, l’armure consomme un éclat par tour (voir 2 ?)  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Créations autonomes (n eclat)  |  Peut tenter de créer un objet autonome type IA complexe, avec fonctionnement automatique et capable plus ou moins d’improviser. L’objet à une existence limitée. Quand il est recrée, il garde mémoire des événements passé s’il a une mémoire.  |
| 6 |  Pluie de météorite de sidéral (5 Eclat)  |  Une attaque non-élémentaire magique, à 1D8 sur tout les ennemis. Possibilité d’esquiver ou encaisser.  |