---
layout: layouts/base.njk
title: Pumas
tags: Espèces additionnelles
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Les pumas

Oui, vous avez bien lu. Non, ce ne sont pas des anthro. Cette espèce a été découvert par un équipage pirate malchanceux qui se retrouvait constamment à affronter des pumas. Après de nombreuses aventure, ils sont tombé sur ces pumas (et un raconte que le capitaine est devenu lié à l'un de ces pumas jusqu'à la fin de ses jours).

Bref, ce sont des pumas. Qui parlent. Intelligent. 

Enfin, presque.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 14 | 10 | 10 | 4 | Griffe (2) | FOR, HAB, DIS (60) | INT, SAG (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Presque des lions | Peuvent faire des sauts de 28m. Font un jet d’HAB-30. Si touche, font un dégat de chute physique, sinon, mental (ça fait peur). | 
| Presque des panthères | +25 en HAB pour rattraper quelqu’un qui fuit. | 
| Presque des chats | Se prennent en plus 50 % des dégâts d’attaque d’eau sur le mental. |
