---
layout: layouts/base.njk
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
eleventyNavigation:
  key: Les signes
  parent: Classes et experiences
  order: 2
title: Les signes
---

# Les signes

Les « signes » sont le nom donné à une magie naturelle qui apparaît chez environs 0.7 % de la population mondiale. Cette magie est rare, et les signés ont toujours eu la possibilité d’avoir une influence non-nulle sur les événements de par cette magie

Il est toujours inconnu de si le signe est inné ou acquis, mais il se déclenche de manière incontrôlable à un moment de la vie. Il y a souvent un lien entre le signe et la personnalité de quelqu'un, mais des théories différente existent pour savoir lequel influence lequel (beaucoup d'alchimiste répondent : "c'est compliqué, sans doute un peu des deux").

Les signes sont divisés en quatre arcanes, deux alignements.

## Liste des signes

Les arcarnes sont une division des signes suivant quatre grand principe : la force, la guérison, la protection et la malice, qui représente les types de pouvoirs suivant :

### Arcane de la Guérison

*La guérison* représente le pouvoir de guérir, améliorer la situation des gens autour de soi. Elle peut aussi représenter le partage, le don et l'aide apporté à ceux qui nous entoure.

- [Le Cancer](cancer/) représente le *génotype* et le *phénotype*. Il a le pouvoir d'influencer la biologie et le corps.
- [La Vierge](vierge/) représente la *confiance*. Elle a le pouvoir de renforcer l'esprit et de le soigner de manière puissante.
- [La Balance](balance/) représente l'*équilibre*. Elle a le pouvoir de déplacer et rééquilibrer les forces vitales.

### Arcane de la Force

*La force* représente la puissance, l'attaque, la capacité brute d'action. Cette arcane est souvent dangereuse, mais n'est pas forcément qu'une arme : un couteau peut servir à attaquer, mais aussi d'outil.

- [Le Scorpion](scorpion/) représente le *poison*. Il a le pouvoir d'injecter diverse toxine et produit faisant des effets passifs négatifs ou positifs.
- [Le Lion](lion/) représente l'*héroïsme*. Il a le pouvoir de faire des duels et d'être le centre d'attention. De faire des actions insensées, aussi.
- [Le Sagittaire](sagittaire/) représente le *mouvement*. Il a le pouvoir de se déplacer très vite et de frapper à distance.

### Arcane de la Protection

*La protection* représente le pouvoir de défendre son entourage des menace, de garantir la sécurité et le bien-être de tous. Cela peut être une protection physique, mentale, ou métaphorique (tel qu'une protection contre les maux).

- [Le Bélier](belier/) représente le *groupe*. Il a le pouvoir de se jeter pour défendre les autres, ou de renforcer le groupe.
- [Le Taureau](taureau/) représente la *force*. Il est strong. Très strong. Il peut taper fort et se défendre fort.
- [Le Verseau](verseau/) représente les *flux*. Il a le pouvoir d'influencer le temps qui passe, d'annuler des actions, etc.

### Arcane de la Malice

*La malice* représente le pouvoir de manipuler, de changer, de transformer. Elle est souvent l'arcane apportant le plus de surprise, bonne comme mauvaise, de par son pouvoir de changement.

- [Les Gémeaux](gemeaux/) représente l'*esprit*. Son esprit est tellement puissant qu'il créer deux corps connectés, pouvant avoir des perssonalités similaires ou non.
- [Le Capricorne](capricorne/) représente les *émotions*. Il a le pouvoir d'affecter voir influencer les émotions.
- [Le Poisson](poisson/) représente les *illusions*. Il a le pouvoir de créer des illusions, des apparences trompeuse et OUPS il était pas là.

### Le treizième signe

Une "treizième signe" existe, le signe du Serpentaire, qui n'existe pas à l'état naturel. Le [Serpentaire](serpentaire/) à le pouvoir d'affecter les flux magiques et de les transformer.

## Alignement

Chaque signe possède un alignement, qui influencera une affinité à un élément métaphysique :

- Le *chaos* représentent les signes dont les pouvoirs possède la plus grande part de probabilité, d'effets secondaires. Elles sont proche des forces du chaos, maniée généralement par les démons, les diablons et la chance.
- L'*ordre* représente les signes dont les effets sont les plus stables. Elles sont proches des puissancces de l'ordre, maniée par les anges, certains dieux et le cosmos.

Il est possible pour les multi-signés de possédé des signes de plusieurs alignement.

## Apothéose

L'apothéose est le phénomène mystérieux pouvant faire apparaitre un signe chez une personne. Si le premier signe apparait souvent sans apothéose, naturellement, le second voir troisième signe ne peut apparaitre qu'après celle-ci.

Ce phénomène se produit dans des circonstantes très particulières, qui peuvent être les suivantes :
- Quand une personne surpasse tout ce qu'elle est, atteint son summum, dans les moments ou "on est le plus nous-même".
- Aux portes de la mort, lorsque quelqu'un est conduit à ce moment fatidique par des actions le représentant.
- Très rarement, des situations extrèmes peuvent aussi le provoquer.

La personne en apothéose se réveille alors dans un lieu entièrement sombre, faisant partie du *noumène* et se retrouve en discussion avec Lux, l'Arbitre. Lux cependant ne choisira pas le signe.

Les règles de gains de signe sont les suivantes :
- Le premier signe est légèrement déterminé par la personnalité de l'individu
- Le second signe est limité par uniquement les signes ayant une arcane en commun avec celui déjà présent
- Le troisième signe doit avoir l'arcane commune des deux premier signes.

Il est possible de reprendre une fois un signe déjà acquis, permettant de le *doubler*.

## Signe anomique

Un signe anomique est un signe qui a été atteint par l'anomie, une variante corruptrice et dangereuse de l'éclat. Un signe anomique ne fonctionnera pas bien, et provoquera des effets secondaire à chaque utilisation. Il peut aussi répendre l'anomie chez d'autre personne.