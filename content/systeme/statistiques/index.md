---
layout: layouts/base.njk
eleventyNavigation:
  key: Statistiques
  parent: Système de jeu
  order: 1
title: Statistiques
models:
  - Compétences spéciales
---

# Généralités et stats

Un personnage dans Erratum possède un certain nombre de caractéristiques qui lui sont propre. Dans cette page, on va voir ce que représente chaque caractéristique présente dans Erratum, à l'exception de celles liés au levelling qui ont leur section à part. Ici, nous allons donc voir tout les types de stats et leurs propriétés.

## Statistiques d'action

Les dix statistiques d'actions sont divisés en statistiques physiques, mentales et de caractères, ainsi que la perception. Une personne à en moyenne « 50 » à une stat. Les statistiques sont les suivantes :

| Stat | Signification |
|:---:|:---------|
| **FORCE** | Force brute physique *(frapper, soulever, faire un bras de fer…)* |
| **CONSTITUTION** | Résistance physique *(encaisser, résister aux maladies…)* |
| **HABILITÉ** | Agilité et dextérité (minutie, esquive, course à pied…) |
| **INTELLECT** | Analyse et réflexion (érudition, calculer, réparer une machine) |
| **SAGESSE** |  Faire les bons choix *(voir les manipulations, les pièges)* |
| **VOLONTÉ** | Force mentale *(rester sur sa voie, résister aux pressions)* |
| **CHARISME** | Aura extérieure *(confiance en soi, prestance, impressionner)*  |
| **DISSIMULATION** |  Capacité à masquer la vérité *(mentir, se cacher, bluffer)* |
| **RELATIONNEL** | Sociabilité et empathie *(comprendre les autres, se faire apprécier)* |
| **PERCEPTION** | Sens et observation *(regarder au loin, écouter, chercher des indices)* |

Lors d'une action, les statistiques peuvent être utilisées telles quelle, ou divisées par deux avec l'aide d'une compétence.

Chaque statistique à les limites suivantes :

|        | Normale | Potentiel | Potentiel ×2 |
|:-------|:-------:|:---------:|:------------:|
| Limite | 80      | 120       | 150          |

La limite maximum d’une statistique pour quelqu’un de normal est de 80, tandis qu’on peut imaginer une limite maximum pour quelqu’un qui serait à même d’exprimer tout son potentiel d’une manière très forte à 120. C’est ensuite aux MJ d’implémenter ces deux limites de manière intéressante.

**NOTE :** *En plus de la limite soft de statistique à 80/120/150, un personnage ne peut pas dépasser 255 à une statistique après l'addition de la compétence, et tout les bonus ajoutés au personnage*

## Pression

La pression est la statistique de base indiquant les échecs et réussites critique. Chaque personnage a un niveau de pression différent (la base est 5) qui sert de base du calcul du niveau de critique (des compétences peuvent se rajouter après).

## PV et PE

La vitalité du personnage se représente de deux manières principale : les PV et les PE, dont les caractéristiques sont les suivantes :

| PV<br />(point de vie) | PE<br />(point d'esprit) |
|:----------------------:|:------------------------:|
|Représente la vitalité du personnage. Le personnage tombe inconscient à 0 PV, et meurt à -max PV. Limité à max PV|Représente la force d'esprit du personnage. Les ennemis peu courageux fuient le combat à 0 PE. Les ennemis plus courageux pourront le fuir à -max PE, voir ne pas fuir.|
| 14 (base) | 14 (base) |
| Soin physique | Soin mental |

## L'éclat et le karma

Afin d'utiliser la magie, deux types de points peuvent être utilisé par les personnages : **l'éclat** et le **karma**.

- **L'éclat** représente la force magique générale du personnage. Il s'agit d'une source abondante et souvent utilisable. L'éclat à un maximum de 10 et se regénèrent automatiquement à la fin de chaque combat (même si un MJ peut parfaitement le bloquer par moment afin de rendre une situation plus tendue).

- **Le karma** lui est une source plus rare, représentant à quel point le personnage à le droit de tordre le destin - et il n'est généralement pas intégré directement à la diégèse du récit. Le karma est offert au joueur dans des conditions très précises, et n'a aucune régenération autre que ces moyens normalement rares.

## Les armures

Sur les fiches de personnages, vous pouvez remarquer trois types d'armures. Chacune permet de protéger dans les conditions suivantes :

- **L'armure physique** protège des attaques faites sur les PV, par les attaques physiques.

- **L'armure magique** protège à la fois les PV et les PE, mais des attaques produites par la magie.

- **L'armure mentale** protège des attaques faites sur les PE, par les attaques mentales.

## Pouvoirs

Les personnages peuvent avoir plusieurs types de pouvoirs, déterminant les choses qu'ils peuvent faire :

- **Les compétences** sont des bonus pour effectuer certains actions.

- **Les techniques** sont les capacités que peuvent utiliser les personnages d'eux-mêmes.

- **Les traits** sont les pouvoirs passifs actifs en permanence du personnages.

## Compétences

> **Attention:**
> Les compétences doivent avoir un aspect RP, par exemple en s'étant essayé à cela durant les parties, ou liées à la backstory.

Les compétences sont le coeur de Pélican, servant à effectuer les actions, avec l'aide des statistiques. Une compétence est une représentation générique d'une action ou d'un domaine d'activité.

Les actions n'ayant pas besoin de compétences utilisent uniquement leur statistique. Une action commence à +0 de bonus, et augmente de 10 en 10 jusqu'à la limite possible par le rang de l'utilisateur.

**NOTE :** *En plus de la limite soft de statistique à 80/120/150, un personnage ne peut pas dépasser 255 à une statistique après l'addition de la compétence, et tout les bonus ajoutés au personnage*

### Exemples de compétences


| Compétence |   |
|:-----------|:--|
| **Diversion (HAB/INT)** | Permet de détourner l'attention | 
| **Pickpocket(HAB+DIS)** | Permet de voler quelque chose discrêtement | 
| **Perce-mensonge** | Permet de savoir quand quelqu'un ment | 
| **Pistage (PER)** | Permet de suivre une liste | 
| **Avenant (REL)** | Permet d'être apprécié naturellement | 
| **Juge de caractère(SAG/REL)** | Permet de savoir quand se méfier de quelqu'un | 
| **Connaissance du monde** | Permet d'avoir des connaissances même de lieux non visités. | 
| **Connaissances occultes** | Permet d'avoir des connaissances des choses occultes et surnaturels | 
| **Apnée** | Permet de retenir plus longtemps sa respiration. | 
| **Natation** | Permet de nager. | 
| **Spiritualité** | Représente sa connection avec les choses spirituelles. | 
| **Déguisement** | Permet de se déguiser et dissimuler son apparence. | 
| **Escalade** | Permet de gravir des obstacles. | 
| **Fouille** | Permet d'obtenir plus facilement des gains lors de fouilles | 
| **Détection des pièges** | Permet de trouver des pièges. | 
| **Langue**<br />*(+nom de la langue)* | Indique la maitrise d'une langue | 
| **Épée** | Permet de manier des épées | 
| **Lance** | Permet de manier des lances | 
| **Hache** |  Permet de manier des haches | 
| **Arc** | Permet de manier des arcs | 
| **Fouet** | Permet de manier des fouets. | 
| **Pistolet** | Permet de manier des pistolets | 
| **Fusil** | Permet de manier des fusils | 
| **Bouclier** | Permet de manier des boucliers |


