---
layout: layouts/base.njk
title: Les druides
tags: Guildes
models:
  - Guildes
  - Guildes métaphysiques
parent: Les guildes
---

# Druidisme

Le druidisme est un art ancien proche de la nature. Les druides sont traditionnellement divisé en 3 groupes : les druides, les vates et les bardes. Les druides apprécient beaucoup Merlin, et sont traditionnellement prochent des dieux celtiques.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
|  0  | Maître des contes anciens | Si le joueur invente une légende ou un conte ancien en rapport avec le sujet, le groupe peut gagner un bonus aux stats sociales lors d'un débat, d'une tentative de convaincre ou d'un discours (marche aussi en confrontation sociale), hors potentiels effets RPs. | 

## Magie de la forêt

La magie de la forêt est la magie qui permet de faire pousser des arbres et de leur donner la vie.

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Biologie créatrice | Les créations magiques sont biologique (faites de bois et de plante) à la place d'être composé de sidéral. Elles ont toutes les particularités de végétaux. |

## Magie Oghamique

| Pouvoirs | Effet |
|:--------:|:-----------------------------------------|
| Magie oghamique | Remplace les runes par des oghams. Les Ogham, au lieu d'avoir des pouvoirs actifs, se trace sous forme de mots et ont un effet passif sur toute une partie. |

## Liste des oghams

(TODO)