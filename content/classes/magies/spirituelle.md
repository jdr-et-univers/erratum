---
layout: layouts/base.njk
title: La magie spirituelle
tags: Magies Rares
models:
  - Magies Communes
  - Magies Rares
parent: Les magies
---

# La magie spirituelle

La magie spirituelle, ou shamanisme, est la magie consistant à utiliser les pouvoirs des spectres, des êtres entièrement mentaux. Magie proche de la magie élémentaire, mais portant sur la magie métaphysique, et de la magie naturelle, on peut imaginer qu'elle s'est formé par un mélange des deux.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 0 |  Recherche d’esprits (PER)  |  Peut détecter les esprits et les fantômes.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Appel d’âme (INT, REL)  |  Peut invoquer des esprits présents à proximité, sur un jet d’INT pour le faire venir, et REL (avec potentiel malus ou bonus) pour voir s’il est sympathique. Les esprit fonctionnent comme des suiveurs.  |
| 1 |  Fleche d’Ame (DIS)  |  1D4×2 (2 Éclats) dégat mental + effet élémentaire métaphysique si un est connu. S'encaisse sur l'armure magique.  |
| 2 |  Calmeur d’âme (INT+20)  |  Peut faire un soin mental, exclusivement sur les spectres. Si a déjà un premier soin (mental), +10 % sur les spectres.  |
| 2 |  Soin élémentaire mental (INT+20)  |  Jet de soin, 1D10+2 PE. Donne pendant trois tours une immunité aux effets secondaires de l’élément en question.  |
| 2 |  Esprit ancien  |  Peut se lier à un esprit ancien, et dans ce cas-là, peut faire naturellement un effet d’Ombre, Lumière, Énergie ou Néant sur ses attaques de shamanisme.  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Comeback (1 Eclats)  |  Peut envoyer une attaque mentale après s’être pris une attaque mentale   |
| 3 |  Armada de Fleche d’Ame (DIS)  |  1D4×3 (2 Éclats) + effet élémentaire. S'encaisse sur l'armure magique.  |
| 3 |  Psychologie (4 Eclats)  |  Soigne 1 status mental et y ajoute une immunité 5 tours  |
| 4 |  Ban (3 eclat, 1/combat)  |  Peut interdire une capacité d’être utilisée durant tout le combat.  |
| 4 |  Deuxième esprit ancien.  |  Peut choisir un deuxième élément métaphysique à utiliser dans la liste.  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Pluie de flèches d’âme (3 Eclats)  |  1D6×3 (2 Éclats) + effet élémentaire. S'encaisse sur l'armure magique.  |
| 5 |  Mes amis, de l’au-delà  |  Peut appeler à tout moment un spectre devenu amis avec.  |
| 5 |  En fait c’est sa mère (INT, 3 Eclat)  |  Peut rediriger une attaque mentale.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Possession d’objet (4 Eclats)  |  Peut permettre à un de ses fantômes de posséder un objet, n’importe lequel.  |
| 6 |  Terres des Esprits  |  Enchante le terrain avec un effet élémentaire spirituel.  |