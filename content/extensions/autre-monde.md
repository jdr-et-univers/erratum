---
layout: layouts/base.njk
eleventyNavigation:
  key: La seconde guerre divine
  parent: Extensions et AU
  order: 0
---

# La seconde guerre divine

Depuis des temps immémoriaux, deux « mondes » existent, que tout oppose, mais qui doivent partager la même planète. Le monde « réel », de la rationalité, de la science. Le monde de l'imprévu, du mystérieux et de la magie. En effet, environs 3% de la population mondiale est capable de pratiquer à l'aide de force occultes la « magie », et 0.7% est même capable d'en faire d'eux même, à l'aide d'un mystérieux pouvoir nommé les « douzes signes ». Des créatures magiques vivent cachés dans le monde, éloignées de tous.

Ce monde est resté caché pendant des siècles, alors que la plus grande partie de la population cessait petit-à-petit d'y croire. Cependant, en Mai 1943, le célèbre mage Merlin à décidé de révéler le monde et les créatures magiques face aux horreurs de la seconde guerre mondiale. Il estimait que face à de tels massacres, les mages n'avaient pas le droit de cacher leur pouvoir. Cependant, si les horeurs de la seconde guerre mondiale, une telle découverte déséquilibra entièrement le monde. Pays magiques et non-magiques se disputèrent territoires et se firent une longue guerre de 9 ans, jusqu'à ce qu'en 1951 c'est une toute nouvelle face qu'avait la terre à la fin de ces batailles.

Depuis, le monde semble s'être stabilisé. Practiciens de la magie et non-practiciens vivent chacun de leur côté, se connaissant mais minimisant les véritables interactions. L'OIM et les Gardiens s'occupent de s'assurer que la magie n'est pas utilisée à tors, et à combattre les "forces du mal". Cependant, bien des conflits larvés existent et pourraient recommencer… Et quels secrets cachent encore ce monde, hors de la vue même des magiciens ? »

Et face aux déséquilibre, les dieux s'intéressent forcément à la chose. Dans un monde ayant été changé par des conflits récents, les espoirs d'un ordre qui durerait pendant des millénaires donne beaucoup de gens prêt à donner leur pouvoirs aux pires despotes.

## Concept

Cette extension se passe durant la "seconde guerre des dieux", ou les divinité tentent de profiter des changements eut en Europe pour tenter de créer de nouveaux empire avec eux à leur tête. Cette extension fournis trois grands groupes d'ennemis qui vont tenter d'envahir l'Europe. Ils profitent fortement de leur grande popularité et de la connaissance que le grand public à d'eux.

- Les **Dieux Latins** sont alliés avec l'Empire Romain, et font partie intégrante de l'armée romaine.
- Les **Dieux Nordiques** forment une armée indépendante, dirigé par Odin
- Les **Dieux Grecs** depuis l'olympe cherche à regagner leur gloire jadis, formant plus des stratégies et menant une guerre sans pitié face aux dieux latins.

Cependant, à côté de cela, un autre groupe tente en secret de regagner en puissance. Le *Groupe Odala*, groupe néo-nazi, visant à fonder une "Europe pure et divine" en offrant à ceux qu'ils estiment les plus "propre" un breuvage basé sur l'Ambroisie pour réussir à en faire des demi-dieux.

## L'Europe d'Erratum

L'Europe a été la région du monde la plus touchée par la révélation magique. Depuis la révélation, les Nations Occultes remplaçant petit à petit les états tel qu'on les connaît, contrairement à certaines autres parties du monde où c'est l'inverse ou aux Amériques ou les deux continuent à vivre plus ou moins superposés.

Dans Erratum, l'Europe est donc extrèmement différente, influencé aujourd'hui par deux grande puissance : L'Empire Romain, La République Populaire de Prusse.

Ces deux pays sont nés des pays magiques de l'époque, qui ont pris le pouvoir dans les deux belligerant de la Seconde Guerre Mondiale, l'Italie Fasciste et l'Allemagne Nazi. Vainquant les dictatures, ils en ont profité pour se servir un peu également niveau territoire.

Cela affecta d'autres pays, puisque les troubles européen facilitèrent la décolonisation.

### L'Empire Romain

L'Empire Romain actuel du monde d'Erratum est un état puissant controlant une grande partie du Sud-Ouest de l'Europe (Catalogne, Sud de la France, Italie). Le pays est l'une des plus grande puissance de la Nouvelle Europe, avec cependant comme principale faiblesse un manque d'adaptation aux nouvelles technologies. Il s'agit officiellement d'une Monarchie Constitutionnelle, mais est gangréné par des luttes de pouvoirs et par une élite corrompue. L'actuel Empereur est Claudius II, 143e empereur de l'Empire Romain, ayant succédé en 1987 à l'empereur Constantin III.

Il s'agit de la continuation du Saint Empire Romain Germanique - qui n'a paradoxalement plus aucune terre en Germanie. Fondée par une petite caste de noble de l'ex Saint Empire ayant enlevé Joseph d'Autriche afin de perpétuer l'Empire, il a comme capitale Rémora, la ville cachée jumelle de Rome, qui a subsité comme cité état jusqu'à la révélation, où les armées Rémorienne ont participé à l'arrestation et l'execution de Mussolini. Prenant le pouvoir sur l'italie, ils ont également envahi toute la "France Libre", profitant des déchirement de pouvoir entre France magique et non-magique.

Ils ont également fait la guerre avec l'Espagne Franquiste, acquierant la Catalogne.

### La Fédération de Neustrie

La Neustrie est un état fédéral du nord-ouest de l'Europe, comprenant les anciens territoires de la France Occupée. La Neustrie est décomposé 8 état. Trois pays indépendant sont lié à la Neustrie par des pactes particulier, les "pays associés" : La Belgique, le Luxembourg et le récemment indépendant Duchée de Neustrie.

La Neustrie est en grande partie fondée sur trois centre de pouvoir : Les trois consuls et leur conseils des ministres représente le pouvoir executif. Le Parlement représente le pouvoir legislatif. Le Directoire représente un pouvoir mixte, mélangeant un pouvoir executif et legislatif, mais sur toutes les affaires magiques ou culturelles (tandis que le consulat conserve les pouvoirs régaliens).

Très inspiré du consulat Napoleonniens en plus du directoire, le pays est très affecté par l'importance d'une noblesse magique venant de ses différents étant membres dont l'influence se voit dans différents pans de la sociétés. Le pays n'est pas vraiment démocratique, mais plus une forme d'oligarchie.

La Neustrie est officiellement en froid diplomatique avec l'Empire Romain, véritable guerre froide.

#### Le duchée de Bretagne

Le duche de Bretagne est un pays officiellement indépendant, mais - contrairement aux autres pays liés à la Fédération de Neustrie - dans les faits sous le contrôle économique et en grande partie politique de la Neustrie.

Fondée lors de la libération par Etienne de Louarn au milieu des guerres entre les différentes forces voulant prendre le contrôle de l'ex-territoire Français, le pays à rapidement eu besoin d'alliance avec son puissant voisin pour pouvoir tenir - en en grande partie pour éviter l'invasion par ledit voisin.

### La république populaire de Prusse.

Composée d'une grande partie du territoire allemand - à l'exception de la Bavière, la République Populaire de Prusse est un pays industriellement puissant, qui dans ce monde à réussi à couper les ponts avec l'URSS (comme la Yougoslavie, en gros).

Les noms Prusse comme Allemagne sont tout autant utilisé, le nom Prusse étant à la base le nom de la Nation Magique, en pleine lutte contre le régime Nazi.

### L'URSS / La République Soviétique de Russie

En URSS, ce fut l'état normal qui pris le contrôle de l'état magique. Cela lui entraina une grosse montée en puissance (qui inquieta les Etats Unis, faisant naitre le projet *Witch Eyes*)

La République Soviétique de Russie est composé du territoire actuel de la russie. Dans ce monde, l'URSS s'est contenté "d'offrir gracieusement l'indépendance" aux autres territoires lorsque les troubles arrivèrent, profitant des rentrées d'argent de son états magique très productif pour réussir à tenir. Cependant, cette indépendance est grandement de façade.

L'URSS continue plus ou moins d'exister, étant officiellement composée uniquement de la RSR, mais pouvant intervenir dans les autres ex-nations de l'URSS. Ils interviennent notamment en Estonie, où se trouve une ancienne ville divine nommée *Les Terres de l'Arbitrage*.

## L'Amérique

Les nations magiques d'Amériques sont très différentes des nations que l'on connait, parce qu'elles ont été affectée différemment par les changements de frontières de ce territoire.

Cependant, contrairement en Europe, les deux types de nations continuent de subsister de manière séparé, malgré des jeux d'influences complexes. Cette politique est nommée la "politique des deux mondes", contrairement à la politique du monde unique d'Europe.

De cela naquis de nombreux types de phénomènes possibles :

- Le Canada, les États Unis et le Mexique regroupent plusieurs états magiques.

- Des états d'Amériques Centrale et du Sud virent le phénomènes inverse : les Nations Occultes étaient généralement plus étandues, faisant des unions faisant souvent référence aux anciens territoires pré-coloniaux.

- De nombreux peuples premiers des Amériques formèrent des Nations Occultes plus enclavé, parfois sur le même territoire qu'une autre Nation Occultes (un phénomène qui existait beaucoup pré-révélation).

Ce fait fit même que contrairement en Europe, de nombreux lieux continuent à avoir plusieurs "pays superposés".

### Les Etats-Unis

Les Etats-Unis comportent (en plus des Nations Occultes Premières) quatre grands états magiques sur leur territoires.

- Les États de la Cote Est (nommé officiellement États Unis Occultes d'Amérique), composé d'une grande partie de l'amérique pré-achat de la Louisianne.

- La nouvelle Louisianne, correspondant à l'ancien territoire Louisiannais, avant l'achat de Napoléons. En effet, l'Empereur ne pu vendre que la Louisianne non-magique, puisque la Louisianne Magique était la propriété du Directoire de l'Époque, que Napoléon, connaissant très mal le monde magique, n'avait pas réussi à contrôler aussi. La Louisianne pris cependant son indépendance peu après, devenant une république occulte.

- La république du Texas, dont la variante magique n'a jamais fusionné avec le reste des US magiques, suite à un différent sur la manière de gérer les signes "rares". Cette controverse bloqua toute union possible - ce qui n'était pas facilité par le gros bloc Louisiannais entre les deux - et au fil du temps l'idée fut oubliée.

- La république de Grande-Californie, composé des anciens territoires Mexicains annexés par les US. En effet, la guerre ne fut mener que par les US non-magiques, ceux magique ayant à l'époque déjà bien commencé à divergé du aux séparations précédantes. Cependant, les US mirent une clause forçant l'indépendance de la Calfifornie magique, afin d'éviter d'avoir une partie de territoire commun avec le Mexique Magique.

Par dessus cela, de nombreuses nations premières des US prirent leur indépendance en tant que Nation Occultes, dans des territoires-bulles ou autres choses du genre.

## Asie et Océanie

L'Asie et l'Océanie n'ont que peu changé, si ce n'est dans les ex états coloniaux qui ont rapidement pris leur indépendance après l'effondrement de l'Europe.

En effet, la magie ayant été moins persécuté, la notion de "Nation Occultes" y est présente de manière différente. De ce fait, lors de la grande révélation, les discussions sur l'union furent plus pacifique, ne provoquant ni de guerre comme en Europe, ni de "politique des deux mondes" totale comme aux Amériques.

Cependant, suivants les pays, il peut encore exister des Nations Occultes, ayant plus ou moins une manière de fonctionner différente de l'État. Mais souvent, ces états sont plus liés à l'état existant (comme au Japon, par exemple).

L'Australie est un contre exemple, fonctionnant totalement comme les Amériques.

## Afrique et Moyen Orient

L'Afrique et le Moyen Orient pourraient sembler avoir peu changé par rapport à notre monde, si ce n'est que la décolonisation a été grandement aidée par les nations magiques. En effet, les occidentaux avaient formé très peu de Nations Occultes en Afrique, qui se firent rapidement donc débouter par celles qui existaient déjà avant.

L'Afrique fonctionne plus ou moins avec du "deux monde", mais avec une très forte collaboration entre les nations "civiles" et les nations occultes, qui aident grandement l'Afrique à se renforcer économiquement.

### Egypte

L'Egypte Occulte est un pays existant par dessus l'Egypte actuelle et débordant un peu sur ses voisins du sud. De nouveau dirigé par un Pharaon, l'Egypte magique parle grandement le copte, devenu dans notre monde ecriture liturgie des coptes.

Contrairement à de nombreuses croyances, l'Egypte ne fait plus véritablement de pyramide, même s'il est arrivé dans les temps récent que justement des Pharaon en fasse de manière à "rappeller" l'époque ancienne.

### Babylone

La Cité-État de Babylone existe encore et est grandement puissance dans le Moyen Orien magique. Dirigé désormais par le Sultan de Babylone, gardien des Archives Akashiques s'y trouvant, la cité état s'est longtemps caché en ayant créé un monde-bulle faisant croire que la ville n'était plus que des Ruines. Babylone possède de nombreux territoires magiques d'Iran.
