---
layout: layouts/base.njk
tags: Orgnaisations criminelles
title: Le schisme
models:
  - Organismes officiels
  - Sociétés secrètes
  - Orgnaisations criminelles
parent: Les organisations
---

## Le schisme

Le Schisme est un groupe qui s'est séparé des cultistes et de frontière suite à la guerre des vertues, parce qu'ils estimaient qu'il faudrait être plus radical dans l'utilisation de la magie et avaient trop peur d'utiliser les forces les plus dangereuses (mais énergétique) tel que le Néant et l'Anomie. La plupars des grands incidents de magie obscurs du début du XXIe siècle sont du à eux, mais il y a des débats pour savoir s'ils sont véritables plus actifs que les Chasseurs ou le Nouvel Empire Romain, ou si c'est leurs méthodes qui sont plus dans la norme de ce qu'on appelle de la magie obscure.

Peu est connu cependant du schisme : il s'agit d'un groupe extrèmement secret, et dont bien des membres ont été corrompus par les énergies qu'ils utilisent. Les quelques éléments les plus connus de leurs actions sont :

- Ils sont dirigé par un mystérieux homme portant toujours un masque de plâtre, ancien signe de l'armée du dieu *Akash*, principal dirigeant de l'armée unie des dieux lors de la *Guerre des Vertus*

- Le fait qu'ils aient une forte propention à s'attaquer aux dieux et autre créatures extrèmements puissantes.

- Des rumeurs disent que leur objectif serait de permettre à toute l'humanité de s'élevé aux rangs de dieux, mais d'autres diraient qu'ils essaient d'eveiller un mal ancien.

Ils sont ennemis à la fois des cultistes, de frontière… et même des autres organisations criminelles.