---
layout: layouts/base.njk
tags: Combats spéciaux
models:
  - Combats spéciaux
title: Combat de bataille
parent: Organisation
---

# Combat de bataille

Une bataille est un combat où l'équipe fait partie d'un combat bien plus grand. Il s'agit souvent d'un combat d'endurence, où le MJ décide de comment se produire le combat global tout autour. Ici, ces règles permettent de décider un peu comment la bataille se produit du côté des personnages.

Le principe de ce combat est de gérer le nombre d'ennemis qui apparait et arrive dans la zone de combat concernant les personnages. Ce qui affecte cela est à quel point les personnages sont **dans** la bataille, notamment à quel point il s'y font remarquer. Cela est géré par un *score de visibilité*.

Chaque tour, s'il y a de la place pour l'arrivée d'ennemi (par défaut, on peut caper à nombre de joueur + 50%, à adapter suivant la force des ennemis), un jet sera fait sur le score de visibilité pour déterminer leur arrivée. Pour chaque ennemis en moins que els héros, ce score est augmenté de +10. Ce score augmente également à chaque fois que les personnage font un exploit visible, une attaque puissante, etc.

Pour baisser ce score, il y a plusieurs moyens :

- Utiliser des compétences spécifiques ou de baisse d'aggro
- Faire des jets de discretions pour être discrets (pour détourner vers les autres combattants alliés)
- S'éloigner du coeur des combats