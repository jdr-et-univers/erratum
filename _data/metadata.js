module.exports = {
	title: "Erratum",
	url: "https://erratum.kazhnuz.space/",
	language: "fr",
	description: "Un petit JDR de fantasy urbaine",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
