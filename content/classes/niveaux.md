---
layout: layouts/base.njk
---

# Les niveaux

> Note: Cette page représente le nouveau système de gestion de l'expérience d'Erratum, pour l'ancien système, voyez la page [**Expérience**](/classes/experience)

Pour symboliser l'évolution d'un personnage, Erratum utilise un système de niveau. Contrairement à l'argent et aux points de prestige, les niveaux ne sont pas un consommable, mais une forme de *paliers* représentant l'évolution du personnage. Erratum utilise 20 niveaux en tout.

Chaque augmentation de niveau donne :
- Des augmentations ou modifications de pouvoirs
- Des augmentations de compétences
- Des augmentations de statistiques

De plus, certains groupes de niveaux représentent des *rang* qui affecte quel genre de compétences sont disponibles, quels limites de stats sont possibles, etc.

## Level up

Un personnage peut gagner de niveau quand ces explois sont au delà de ce qu'on peut attendre de son rang. Il est possible de gagner jusqu'à 2 niveau d'un coup (mais ce sera dans des cas vraiment d'exception. Les niveaux peuvent augmenter pour les conditions suivantes (entre autre). Cependant, le choix final est à la discretion du MJ.

- Affrontement d'un ennemi ayant un rang supérieur au siens (voir rangs)
- Réussite d'un combat de boss, ou d'une partie importante scénaristiquement, ou d'un autre combat.
- Réussite d'un accomplissement qui dépasse le niveau qu'on pourrait lui donner.

Dans les cas trouble, il est possible de lancer un D100, et augmenter de niveau suivant un score défini par le MJ. L'augmentation de niveau est à voir par l'idée que le personnage à "atteint un nouveau palier", plus que "il a gagné +1 expérience" : il faut voir ça comme "après ce que vous avez fait, ouais c'est logique que vous soyez niveau 6".

## Amélioration de compétence et statistiques

Chaque personnage obtient à chaque niveau *5 points d'entrainement*, qui peuvent être utiliser pour acquérir et augmenter des compétences et/ou des statistiques de la manière suivante :

- Permet d'ajouter +10% à une compétence
- Permet d'ajouter +5% à une statistiques
  - (Coute 2 point d'exp en supérieur à 100)
- Permet d'ajouter +3 PE/PV à un personnage
- Permet de diminuer ou augmenter d'un la pression
- Permet de gagner un pouvoir d'une classe
- Permet d'améliorer ou de personnaliser un pouvoir d'une classe (1/compétence)

En tout, un personnage utilisera 100 points d'expérience au court de son aventure.

## Les rangs

Les rangs sont des groupes de niveaux débloquant des compétences et des pouvoirs. Il y a en tout six rangs.

<div class="table-responsive">

| Niveaux | Rang | Niveau classe | Débloque | Max bonus PV/PE | Max compétence |
|:-------:|:---:|:-------------:|:--------:|:---------------:|:--------------:|
| 1 à 2  | D | Novice (1 et 2) |  | +100% | 30% |
| 3 à 5 | C | Novice (1 et 2) |  | +200% | 30% |
| 6 à 10 | B | Adepte (3 et 4) | Potentiel | +300% | 50% |
| 11 à 15 | A | Expert (5) | Herault | +400% | 50% |
| 16 à 18 | S | Ultime (6) | Potentiel+ | +500% | 50% |
| 19, 20 | X | Ultime (6) |  | +600% | 70% |

</div>

Les deux premiers rang, *D* et *C* représentent la plupart des "civils", ou personnes n'ayant pas obtenu des compétences incroyables dans leur domaine, ni vécus d'aventures.

## Classes de départ

Le personnages peut commencer avec *deux classes* de la manière suivante :

- 1 signes + 1 magie ou spécialité
- 2 magies et/ou spécialités

### Gagner de nouvelles classes

Les classes peuvent se gagner de la manière suivante :

- Les signes s'obtiennent par *apothéose* (nécessite que le personnage a du potentiel magique)
- Les spécialité doivent s'apprendre de manière RP
- Les magies "communes" doivent s'apprendre de manière RP
- Les magies "rares" doivent s'apprendre de manière RP (nécessite que le personnage a du potentiel magique) 

Un personnage peut atteindre jusqu'à *quatre classes* et une guilde.
