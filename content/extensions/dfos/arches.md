---
layout: layouts/base.njk
title: Les arches
tags: Dragons from Outer Space
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Arches et équipages

Les équipages sont une des notions les plus importante de l'univers de *Dragons From Outer Space* : Ces groupes multiculturels sont ceux qui vivent dans les arches, et sont parmis les seuls à pouvoir faire des voyages interplanétaires.

Les équipages ayant un liens télépathique avec leur arche généralement dans leur jeunesse, il est souvent difficile pour un membre d'une arche de la quitter. C'est souvent pour cela que - pour éviter la consanguinité, les enfants partent vivre dans une différente arche durant leur enfance, devenant des *novices* dans la nouvelle arche.

## Rôle et importance

Les équipages ayant des arches, ils sont les seules à pouvoir traverser l'espace, et donc les seules à pouvoir transporter des êtres et marchandises à travers le système solaire, et également à pouvoir affronter les menaces spatiales, telles que les créatures née de la Corruption.

Ils ont de ce fait une influence certaines, étant souvent les bienvenus à des cours, des lieux importants, etc.

## Les Arches

Les Arches sont le seul moyen de déplacement interplanétaire du système de Roma. Ces vaisseaux vivant sont en fait des créatures de l'espèce des dragons, qui ont été via bioingéniérié modifié pour devenir des dragons vivants. Ils ont une longévité comparable à celle des dragons, et forme des liens émotionnels avec leur équipages, dont ils veulent la protection (que ce soit les êtres "civilisé" ou les wyverns). Les Arches ressemblent à des dragons-tortues, avec de grands pics sur la carapaces, des petites ailes, pattes et une petite tête. Au dessus de la tête, sur la carapace, se trouvent généralement la cabine du commandant ou il peut voir l'espace devant lui, et diriger l'Arche.

Plus petits que les dragons, ils ont souvent de quoi habiter un équipage de quelques centaines de personnes, qui forment de véritables petites communautés. L'intérieur de la carapaces contient des couloirs et des pièces, là ou vivent l'équipage. Ces pièces naissent naturellement lors de la croissance de l'Arche, avec les instruments nécessaires. L'intérieur d'une arche ressemble à de long couloir d'écailles ocre, avec les technologies intérieures ajoutées. L'ajout de ces technologie ne font pas mal à l'arche.

### Capacités

Les arches ont toutes les capacités nécessaire pour être de bons vaisseaux, et notamment les seules possibles. Leur carapaces sont d'une solidité extrême, et peuvent protéger des chocs, du vide spatiale mais également des radiations. Cependant, elles ne sont pas indestructible et peuvent être abimée, voir détruite, ce qui sera généralement fatal pour l'équipage.

Les arches peuvent cracher de l'énergie comme une wyvern ou un dragons, mais cela peut la fatiguer.

Leur liens psychique puissant permet aux membres d'un équipage sur une planète de communiquer avec l'Arche (et donc les membres présents dessus) si celle-ci est en orbite autour (cela peut être plus difficile si elle est de l'autre côté de l'astre), et les arches peuvent communiquer entre elle à des plus grande distances.
