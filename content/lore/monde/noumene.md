---
layout: layouts/base.njk
tags: Lieux magiques
title: Le noumène
models:
  - Lieux magiques
  - Établissements scolaires
parent: Lieux importants
---

# Le noumène

Le phénomène et le noumène sont parmi les plus grandes théories de l'alchimie, et l'un des sujets les plus controversé dans le monde d'Erratum. En effet, le monde est divisé en deux parties, la partie visible et la partie invisible. Le phénomène est notre monde, mais au delà se trouve un autre monde, le "noumène".

Le noumène est le monde derrière le monde, connu surtout de celles et ceux qui font *l'ascension*. Ces personnes ont accès à un espace connu par les alchimiste tels que *l'orée du noumène*, une sorte de frontière entre le noumène et le monde réel, ou le voile qui sépare les deux. Dans ce lieu mystérieux, ils peuvent rencontre Lux, un être mystérieux aussi appellé *l'arbitre*.

Cependant, les alchimiste théorise qu'il existerait plus derrière ce voile. Qu'il existerait un lieu entrée, qui serait les coulisses de notre réalité, et qui contiendrait les réponses aux quelques que l'on se pose sur notre monde.

L'un des buts des alchimistes serait de trouver un chemin vers le *vrai noumène*.
