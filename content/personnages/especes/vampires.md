---
layout: layouts/base.njk
title: Vampires
tags: Espèces additionnelles
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Vampires

Les vampires sont une espèce particulièrement rare, nés de la civilisation antique de *l'Hyperborée*. On raconte que *Mabon*, leur dieu, est celui qui leur aurait offert l'immortalité, faisant d'eux des morts-vivants. En effet, les vampires sont des corps morts, maintenu par une magie extrèmement puissante. Le sang ne coule plus dans leur corps, ils sont incapable de se reproduire et ont besoin de sang pour maintenir le sortilège.

Pour se reproduire, ils sont obligé de "vampiriser quelqu'un", le transformer en vampire par une morsure, transformant n'importe quelle autre espèce en vampire, lui faisant perdre ses traits naturels. Ils ont des pouvoirs de contrôles puissant, pouvant transformer des êtres en ghoule. Ils ne sont cependant pas totalement invulnérable. Le soleil les affaiblis, ils sont allergique à l'ail, et peuvent parfaitement être tué (et pas juste besoin d'un pieu dans le coeur).

Les vampires vivent entre eux, généralement en société savantes et culturelles. Cette façon de vivre est en grande partie liée à l'Esthète.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 10 | 14 | 10 | 5 | Croc (2) | FOR, DIS, SAG (60) | VOL, CON (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Haine de l'or| Subit un effet de poison (cumulable avec un poison normal) par l'or solaire |
| Ghoule | Peut « zombifier » un personnage sur une confrontation de CHA, pendant 3 tours, une fois par combat. | 
| Photophobe | -15 % en PRE et en CON à la lumière. | |

## Réputation

Si les vampires ont longtemps eu la réputation d'être des chasseurs de proies invétérés, en grande partie en Europe lié aux comtes Vlad Dracula et Friedrich Orlock (dit « Nosferatu »). Le premier est un individu controversé, à la fois fondateur de l'une des plus grandes société de vampire, et despote sévère qui n'hésitait pas à tuer ses ennemis. Il a été tué par l'également controversé Professeur Abraham Van Helsing, professeur d'université devenu médecin puis chasseur de vampire. Le comte Nosferatu est lui une figure négative à la fois pour les non-vampire et vampire. Cet ancien élève de Dracula, vampirisé par lui, a vécu jusqu'à sa mort en 1927. Il est tristement connu pour avoir fondé une brigade fasciste vampirique en 1920, commençant des vampirisations de masse avec ses suiveurs. En 1922, une coalition se fait de vampire et d'humain contre le fascisme, et méneront une guerre de 5 ans contre lui qui finira par sa mort.

Cependant, la majorité des vampires ont été vampirisé sur leur demande, par un vampire mystérieux uniquement connu sous le nom de l’Esthète, s'employant à "rendre immortel ceux qui permettent le monde d'être plus beau". De plus, aujourd'hui, le marché regorge de sang animaux, même déshydraté. Les vampires restent cependant la cible numéro 1 des chasseurs.
