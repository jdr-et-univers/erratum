---
layout: layouts/home.njk
eleventyNavigation:
  key: Système de jeu
  order: 0
---

# Système de jeu

Erratum utilise un système de jeu D100 homebrew, proche de mon système pélican (avec quelques différences). C'est un système centré sur le fait d'être simple d'utilisation et de privilégier le RP et les actions d'éclat à une connaissance accrue et précise des règles. (Il a notamment pour but d'être plus accessible aux débutants qu'un système à la D&D ou Pathfinder)

Un autre de ses buts est d'utiliser uniquement les dés les plus communs d'un rôliste, facile à trouver ou acheter en ligne. Ce site contient également des recommendations en terme de bienveillance envers les joueurs.

## Pages dans cette catégories

- [Dés et actions](D100/) - Les dés et les actions de bases du système
- [Statistiques](statistiques/) - Les statistiques présentent sur la page du MJ (hors spéciales telles que la métaphysique ou l'anomie)
- [Campagnes](campagnes/) - Organisation d'une campagne de base
- [Exploration](exploration/) - Les règles liées à l'explorations et la découverte de lieux
