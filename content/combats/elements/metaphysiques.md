---
layout: layouts/base.njk
tags: Les éléments
models:
  - Les éléments
title: Éléments métaphysique
parent: Les éléments
---

# Éléments métaphysique

Les éléments métaphysiques représentent plus des structures de la réalité, des éléments fondamentaux qui la composent. Ils sont en cela souvent plus rare et plus puissants.

|             |           |            |
|:-----------:|:---------:|:----------:|
| **Lumière** | **Ordre** | **Espace** |
| **Énergie** | *(Éclat)* | **Néant**  |
| **Temps**   | **Chaos** | **Ombre**  |

Ils peuvent avoir les effets suivants en combat :

## Effets

| Élément | Effet |
|:-------:|:------|
| **Énergie** | Le personnage à 15 % de chance de se régénérer d’un éclat en faisant l’attaque. |
| **Lumière** | 30 % de provoquer cécité à l’ennemi. |
| **Ordre** | Si attaque sur l’armure la plus forte de l’ennemi, attaquera sur la deuxième plus forte. |
| **Espace** | 30 % de déplacer à une autre position sur le terrain l’ennemi touché, au choix du joueur. |
| **Néant** | Retire 1 PV/PE max à l'ennemi touché pour chaque tranche de 6 dégats potentiels (ils reviendront une fois en lieu sur) |
| **Ombre** | Ajoute un effet de vampirisme mental (50 % des dégâts effectués régénéré en tant que PE) à l’attaque |
| **Chaos** | L'attaque fait moitié dégats sur le corps et l'esprit. |
| **Temps** | Si l’attaque échoue : jette 1D4. Si 1, peut relancer l’attaque. |

## Terrains élémentaires

| Élément | Terrain | Effet |
|:-------:|:-------:|:------|
| **Énergie** | *Apogée cosmique* | À chaque jet de panique, le personnage jette 1D4, si 1 ou 3 : le jet de panique ne se produit pas. |
| **Lumière** | *Zénith divin* | Les attaques de lumière, plante et feu ont une amélioration au dé supérieur. Les attaques d’ombre, glace et eau passent aux dés inférieurs. Les êtres d’ombres ont un désavantage. |
| **Ordre** | *Égalisation ultime* | Les critiques, réussite comme échec, ne peuvent pas se produire. |
| **Espace** | *Distorsion spatiale* | Les êtres « proches » sont traités comme loin, les êtres « loins » sont traités comme proche. |
| **Néant** | *Vide cosmique* | Tous les tours, 1D4, si 2 ou 4 : un personnage au hasard fait un jet de panique. |
| **Ombre** | *Éclipse divine* | Les attaques d’ombre, glace et eau ont une amélioration au dé supérieur. Les attaques de lumière, plante et feu passent aux dés inférieurs. Les êtres de lumière ont un désavantage. |
| **Chaos** | *Désordre ultime* | Les critiques, réussite comme échec, sont doublé. |
| **Temps** | *Distorsion temporelle* | Inverse l’ordre de passage de tout les personnages. |
