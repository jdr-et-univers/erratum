---
layout: layouts/base.njk
tags: Afflictions communes
models:
  - Afflictions communes
  - Malédictions
title: Effets basiques
parent: Les afflictions
---

# Effets basiques

Ces effets disparaissent généralement rapidement, au maximum à la fin d'un combat. Ils peuvent être positif ou négatif.

## Liste des effets

|   |    |
|:-:|:-:|
| **Superactivité** | Agis deux fois par tour | 
| **Hypoactivité** | Jette 1D4, si fait 4, ne peut pas agir. |
| **Toujours premier** | Agis en premier | 
| **Toujours dernier** | Agis en dernier |
| **Dépassement** | Agit avant le joueur juste avant | 
| **Dépassé** | Agit après le joueur juste après |
| **Bis Repetita** | Le perso doit répéter la même action que le tour précédant | 
| **Non Repetita** | Ne peut pas faire 2x la même action. |
| **Hyperchargement** | Augmente de moitié les dégâts (physique/magique/mental) reçus comme donné. | 
| **Chateaudepierre** | Affaibli de moitié les dégâts (physique/magique/mental) reçus comme donné |
| **Faiblesse élémentaire** | Les attaques de cet élément feront un bonus de 50 % de dégâts. | 
| **Résistance élémentaire** | Les attaques de cet élément ne font que moitié dégâts. |
