---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Les majordomes
parent: Les spécialités
---

# Les majordomes

Les majordomes sont une classe de soutiens ayant des pouvoirs diversifié. Les majordomes sont au service des autres personnages, pouvant les aider avec leurs capacités, que ce soit en terme de stats, de soin, ou production de nourriture. 

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | A votre service | Le majordome choisi un PJ ou un PNJ ou il est à son service. Peut utiliser ses points de *confiance* pour aider ou influencer le PJ/PNJ. |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Premier soin | HAB/INT |  | Soigne le niveau de réussite auquel on rajoute 1 pv ou les effets d’un éventuel baton de soin |
| 1 | Tout le monde aime le majordome |  |  | Peut dépenser 3 point de confiance pour soigner un PE à toute l'équipe. |
| 2 | Esprit sain |  |  | Bonus de 10 % pour résister aux afflictions mentales. |
| 2 | Avenant |  |  | Les gens apprécient naturellement la personne (rajoute un bonus choisi par le MJ) |
| 2 | Eat the weed | INT+HAB |  | Permet de préparer des aliments avec des ingrédients de potions. 5 ingrédient / plat. |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture d'aide |  |  | Permet de passer en posture d'aide |
| 3 | Le majordome est toujours prêt |  |  | Soutiens une personne ou un lieu. Peut effectuer une aide ou soin d'opportunité par tour, mais ne peut plus bouger du lieu ni agir sur les ennemis |
| 3 | Relations (trait) | | | Peut gagner 1D4+2 confiance par discussion importante du groupe avec un PNJ ou énigme réussie |
| 4 | Moitié-moitié |  | 3 tours de cooldown | Permet d'utiliser un objet sur toute l'équipe à 50% de son effet, en en consommant qu'un |
| 2 | Maître de la cuisine |  | 1 | Peut partager une portion de vivre sur toute l'équipe |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Le marché des majordomes |  |  | Peut acheter des objets communs avec sa stat de confiance |
| 5 | Même les animaux l'aiment |  |  | Ne se prendra aucune aggro des animaux s'il ne les attaque pas. |
| 5 | Aide au mensonge |  |  | Peut utiliser 10 point de confiance pour faire oublier un mensonge précédant d'un allier s’il contredirait un nouveau. |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Pas mon maître |  | 3/partie | Peut effectuer une action d'OP si quelqu'un attaque son maître pour 20 confiance |
| 6 | Clean en toute circonstance |  | 1/partie | Peut annuler un échec critique et en faire qu'un échec pour 30 confiance |