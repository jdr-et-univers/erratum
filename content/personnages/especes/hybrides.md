---
layout: layouts/base.njk
title: Hybrides
tags: Espèces de base
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Hybrides

Une espèce créé artificiellement par les alchimistes lors des 15e et 16e siècles. Ces créatures sont quasiment à 100% humaines, mais avec cependant des "gènes magiques" (noms modernes, avant ils étaient appelé "essences diluées d'espèce") venant d'animaux.

Ils ont été créé afin d'éviter la maltraitance d'autre humains, mais c'était sans compter l'incroyable capacité qui peut exister à maltraiter son prochain : ils ont juste eux plus d'être à maltraiter.

Les hybrides ont depuis gagnés la liberté, notamment grace à des révoltes tels que la *commune de Lutecia* en France, mais sont souvent mal vu dans les populations.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 10 | 14 | 10 | 7 | Poing (0) | HAB, REL et DIS (60) | CON et CHA (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Sens aiguisés | peut sur un jet de PER éviter tout malus de vision. | 
| Semi-animal | À la création, après le choix du caractère, peut déplacer jusqu’à 20 % entre ses stats (par tranche de 5%) mais ça doit fait sens avec son espèce. |