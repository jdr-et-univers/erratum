---
layout: layouts/base.njk
eleventyNavigation:
  key: Les éléments
  parent: Système de combat
  order: 5
title: Les éléments
models:
  - Les éléments
---

# Les éléments

Il peut être également intéressant d’ajouter des effets élémentaires aux différentes capacités. Erratum propose un système de huit éléments « physiques » qui plus qu’un système de défense/résistance offrent un système « d’effet spéciaux ». Ces effets peuvent également se lier aux effets de terrains pour créer des faiblesses et résistances.

En règle général, les éléments n’aiment pas les effets de terrains de l’élément opposé.

## Groupes d'éléments

- Les [éléments physiques](physiques/) sont les éléments les plus communs, agissant sur la réalité physiques.
- Les [éléments métaphysiques](metaphysiques/) sont des éléments plus rares, touchant sur la réalité même du monde, derrière ses apparences.
- Les [éléments paradoxaux](paradoxaux/) sont des éléments exceptionels, lié à la nature même de la magie.
- Les [pseudo-éléments](pseudo/) sont des "faux" éléments, n'ayant pas de nature "magique", mais ayant des effets quand même.

## Faiblesses et résistances

Certains personnages ou créatures peuvent avoir des **faiblesses** ou des **résistances** élémentaires.

Pour chaque résistance, un personnage ne se prendra que x0.5 dégats par une attaque de l'élément en question, tandis que pour chaque faiblesse, le personnage se prendra x2 dégats.