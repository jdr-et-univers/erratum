---
layout: layouts/base.njk
title: Les tenues
tags: Les équipements
models:
  - Les équipements
parent: Les équipements
---

# Les tenues

Les tenues sont un équipement offrant protection et effets divers à votre personnage. Les personnages ne peuvent porter qu'une tenues à la fois, il peut être donc intéressant d'adapter sa tenue aux circonstances. Les tenues se divisent principalement en deux types : Les vêtements (qui apportent peu de protection, mais des effets utiles) ou les armures (qui ont moins d'effets, mais plus de protection).

<div id="dataTable">

| Nom | Effet | Physique | Spécial | Prix |
|:------:|:------:|:------:|:------:|:------:|
| Vêtements communs | | 1 | 1 | 200 |
| Vêtements confortables | +1 armure mentale* | 1 | 1 | 300 |
| Tenue terrifiante | L'ennemi à +2% en échec critique | 1 | 1 | 300 |
| Tenue polaire | +20% pour résister au froid ; -20% pour résister au chaud | 1 | 1 | 300 |
| Tenue légère | +20% pour résister au chaud ; -20% pour résister au froid | 1 | 1 | 300 |
| Séduisante | +20% pour charmer | 1 | 1 | 300 |
| Maillot de Bain | +20% pour equiver* | 0 | 0 | 300 |
| Tenue discrète | +20% en DIS dans un lieu sombre | 1 | 1 | 400 |
| Uniforme de faction | Permet de donner l'impression d'être un membre de cette faction si l'on est pas trop connu | 1 | 1 | 400 |
| Costard | +20% sur les reventes ; +20% risque de se faire attaquer dans les rencontre aléatoire | 1 | 1 | 800 |
| Tenue majestueuse | +20% dans les statistiques sociales en milieu riche | 1 | 1 | 600 |
| Tenue du Héros | +20% CHA | 2 | 1 | 600 |
| Tenue de voyage | +20% pour les exploration de grand espace | 2 | 1 | 600 |
| Tenue renforcée | | 2 | 2 | 400 |
| Côte de maille | -10 % HAB | 4 | 2 | 750 |
| Armure d'apparat | -25 % DIS et HAB ; +20% dans les stats sociales en milieu riche ; +3 armure mentale* | 4 | 1 | 1050 |
| Armure | -25 % DIS et HAB | 5 | 2 | 950 |
| Armure Lourde | -40 % DIS et HAB | 7 | 3 | 1200 |

</div>

\* Les bonus de boost d'armure physique renforce cet élément

## Les effets

| Nom| Effet| Surcout |
|:------:|:------:|:------:|
| Améliorée | Fait 50% de défense physique en plus | +50% |
| Supérieur | Fait 100% de défense physique en plus | +100% |
| Ultime | Fait 200% de défense physique en plus | +200% |
| Presque surnaturelle | Fait 50% de défense magique en plus | +50% |
| Magique | Fait 100% de défense magique en plus | +100% |
| Occulte | Fait 200% de défense magique en plus | +200% |
| Intégrale | Masque les points faibles | +25% |
| Consolidée | Effet du brise-armure divisé par 2 | +25% |
| Miroitante | Renvoie la moitié des dégâts spéciaux infligés | +50% |
| Élémentaire | Evite l'effet secondaire de l'élément lié | +25% |
| Scellée | Ne peut être retirée que par le porteur | +10% |
| D'Âme | -2 armure morale, cependant +20% de chance de réussir les esquive mentales | +50% |
| De sang | -25% PV ; 1D6, si 1, l'ennemi fait un jet d'affliction physique | +100% |
| Radiante | +2 éclat ; Rajoute faiblesse à tout les éléments métaphysique | +100% |
| Funeste | -25% PM ; Double les dégats mentaux ; 1D6, si 1, l'ennemi fait un jet d'affliction morale | +100% |
| Célèste | -2 éclat ; Protège de l'éclat et si devrait devenir anomique 1D4 chance d'en être protégé | +100% |
| Des Ombres | -25% PM et PV ; 1D6, si 1, un encaissement réussi devient une esquive | +100% |
| Des Héros | -n×10 DIS ; +20% dans toutes les stats face aux êtres démoniaques. | +100% |
| Biotechnologie | Ramène 1 PV/tour. | +50% |
| Nanotechnologie | S'auto-répare de 1/partie | + 50% |
| Crystalline | Brise-Arme: 1/4 de faire perdre 1 points de dégat à une arme l'attaquant sur le physique | + 25% |
| Champs d'énergie | Les attaques perce-armure font moitié dégats | +100% |
| Pare-balle | Anti-perce défense face aux armes à feux | +25% |
| Anti-explosion | Encaisse les explosions | +25% |