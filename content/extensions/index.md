---
layout: layouts/home.njk
eleventyNavigation:
  key: Extensions et AU
  order: 7
---

# Extensions

Le système d'Erratum peut comporter plusieurs AU/Extensions qui permettent de faire des JDRs dans des mondes légèrement différents.

## Liste des extensions

- [La Seconde Guerre Divine](autre-monde/) est un AU se déroulant dans une terre alternative ou Merlin à révélé le monde après la seconde guerre mondiale, et ou les dieux veulent regagner leur pouvoir.