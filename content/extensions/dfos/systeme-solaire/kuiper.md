---
layout: layouts/base.njk
tags: Système Solaire
title: La ceinture de Kuiper
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# La ceinture de Kuiper

La ceinture de kuiper est la partie complètement inexplorée du système solaire… si ce n'est par les pirates de l'espace qui y ont formé leurs base principale.

## Pluton

Pluton est la plus grande des planètes naines du système solaire, et c'est aussi une planète habitée par de grande forces occultes. Par un grand hasard, son nom la décrit bien, puisqu'elle contient un *monde des morts* spécifiques aux espèces spatiales, et le plus vieux monde des morts du système solaire, datant de l'époque des *Crétaciens*.

Il contient notamment les esprits de quelques phosophoriens (mais pas d'Anostiens anciens, puisque les Anostiens ne peuvent pas laisser de fantômes par définition, étant *déjà* des formes d'esprits). On peut y découvrir de nombreux secrets. On y trouve aussi de nombreux vampires anciens qui ont décide d'y vivre. Entouré de tellement d'énergie ectoplasmatique, ils sont d'une puissance considérable.

Sortir de la planète est extrèmement difficile.

## Sedna

Sedna est la planète autour de laquelle se positionne souvent la Cour des Pirate, perché sur un ancien dragon stellaire qui gravite autour de la planète. 

La planète se trouve la population de pirates qui ne sont pas en activité, ou qui viennent prendre des vacances, qui vivent dans d'anciennes bases qu'ils ont retapé. La vie y est très rustique et peu confortable, mais ils arrivent à se débrouiller, ayant quelques bases pour faire pousser des vivres. Sur le dragon se trouve les dirigeant, qui sont éloigné de la planète (et peuvent fuir facilement). Leur dragon, dont le noms est connu d'eux-seul, est extrèmement désobéïssant aux autres dragons stellaires.

Cependant, les dirigeant ne dirigent pas vraiment la planète naine, dont les habitants en faisant plus qu'à leur tête.

## Eris

Eris est la planète naine ou se trouve A'tolk, le dragon stellaire anomique, qui vole autour de la planète naine. On y trouve de nombreux dragons (pas des stellaires, heureusement y'en a qu'un) et des kobolds anomique. Peu de chose sont connus sur l'astre, qui est considéré comme une menace majeure par les spécialistes de frontières.

Petit à petit, de nombreux anciens du schisme s'y sont rendu, survivant dans la planète grâce à l'anomie (ouais ils ont pas de trucs leur permettant de vivre ils bouffent leur anomie pour ça). Elle est recouverte d'une brume anomique pouvant transformer rapidement un être en irrégularité.

## Le nuage d'Oort

Y'a pleins de comètes, et on y trouve de nombreux dragons stellaires, qui y vivent tranquillement. Peut de chose y sont connue, c'est la limite du système solaire.