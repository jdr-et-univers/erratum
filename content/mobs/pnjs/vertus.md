---
layout: layouts/base.njk
title: Les vertus
tags: PNJs spéciaux
models:
  - PNJs spéciaux
parent: PNJs
---

# Les vertus

Les vertus sont sept êtres existant depuis des temps immémoriaux. Le rôle de vertue se retransmet de personne à personne à la mort d'une vertue, une nouvelle personne naissant et devenant vertue. Chacune - sauf la vertue de la tempérence, ont deux signes. Les vertues peuvent également le retransmettre volontairement à une personne en sentant leur fin approcher.

Chaque vertu possède une arme unique, leur permettant des exploits incroyables. Elles se repèrent par les lignes de lumières traversant tout leur corps, chacune d'une couleur différente. Elles peuvent briller fort lorsque la vertu utilise ses pouvoirs où dans des émotions trop intense. Une vertu ayant ses pouvoirs actif est dite *exhaltée*

Elles avait disparue à la fin de la *grande guerre des vertues*, mais ont été recrée par les cultiste au début du XXIe siècle, leur pouvoir ayant été mise dans sept orphelins afin de les érigées en protecteur et protectrices du monde.

## Les sept vertus

Les septs vertus ont été nommée par la culture occidentale suivant l'une des septs vertus de l'église catholique. Chacune à une couleur qui lui est reservée, qui est la lumière que font les lignes sur son corps quand elle passe sous sa forme *exaltée*.

| Nom vertu | Signes | Couleurs | Source du pouvoir |
|:----------:|:------:|:------------:|:------------:|
| Bravoure   | Taureau / Lion | Rouge | L'héroïsme protecteur |
| Confiance  | Vierge / Cancer | Rose | Le don de soi pour soigner |
| Espérance  | Poisson / Scorpion | Violet sombre | L'espoir, autant un soin qu'un poison |
| Justice    | Sagittaire / Balance | Vert | Le mouvement vers la justice |
| Charité    | Bélier / Gémeaux | Or | Le groupe et le liens avec les autres |
| Prudence   | Verseau / Capricorne | Bleu | Les probabilité et le contrôle |
| Tempérance | Serpentaire | Blanc | La mesure |

## Les égides

Les égides sont les artefacts des vertus. Elles ont chacune un pouvoir unique qui les rend spéciale par rapport à une arme normale. Plus que des armes, ces armes sont l'expression du pouvoir de la vertu. Les égides "armes" ont toute la particularité de faire double dégats sur les aberrations.

| Vertu      | Égide | Pouvoir de l'égide |
|:----------:|:-----:|:------------------:|
| Bravoure   | Les poings du courage | Peut encaisser sur un coup de poing, perce-armure |
| Confiance  | Le Grand Caducée | Peut soigner les afflictions métaphysiques, 1/partie |
| Espérence  | Les sabres de la dualité | Peut frapper sur le physique et le mental ; restaure 1 PE à toute l'équipe par coup qui inflige des dégâts |
| Justice    | L'arc de la justice | A une puissance brute équivalente à celle de l'arme tenue par l'ennemi. Si face à une créature puissante sans arme, elle est un arc de niveau 3 |
| Prudence   | Le pendule de la prudence | Peut stocker 5 critique arrangeant les joueurs, et les redistribuer quand il veut. |
| Charité    | Les instruments de la charité | Peut effectuer des chants pour renforcer les alliés ou les protéger. |
| Tempérence | ?????? | ?????? |

## Forme exaltée

La forme exaltée de la vertu se produit quand les lignes sur son corps s'illumine, et elle gagne 10% à tout ses stats. La vertu confère alors des pouvoirs à tout les gens autours. Une seule vertu peut avoir son pouvoir exalté actif, mais chaque autre vertue exalté lui rajoute +5% à toute ses statistiques.

L'exaltation n'a pas d'effet secondaire HRP, mais en RP, les vertus sont après une exaltation fatiguée et pourront moins utiliser leurs pouvoirs de vertues, **d'autant plus** si elle dure longtemps.

| Vertu      | Pouvoir exalté |
|:----------:|:--------------:|
| Bravoure   | Les réussite comme échec critiques sont doublés. |
| Confiance  | Annule le premier jet de survie du combat. |
| Espérance  | Chaque jet de panique à 50% de chance de ne pas se produire. |
| Justice    | Si un ennemi inflige des dégâts, il se prend une flèche faisant 1 dégats brut. |
| Charité    | Le bonus de vertu exaltée est partagée |
| Prudence   | Offre deux jets à relancer, même critiques. |
| Tempérance | Annulation de l'aléatoire métaphysique : empêche l'entropie, mais aussi les jets d'anomie, etc |
