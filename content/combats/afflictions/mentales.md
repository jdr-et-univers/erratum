---
layout: layouts/base.njk
tags: Afflictions communes
models:
  - Afflictions communes
  - Malédictions
title: Les afflictions mentales
parent: Les afflictions
---

## Afflictions mentales

| id| Affliction | Effet |
|:-:|:----------:|:------|
|   | **Terreur** | Perd 10% PM/tour (si grave, tout les deux tours la terreur fait 10% de PV/tour en plus) |
|   | **Amour** |  Ne peut obéïr qu'à la personne dont le perso est amoureux, avec +50% si allié, -20% si ennemi avec. |
| 2 | **Bersek** | -50 % pour esquiver ou encaisser les attaques physiques comme morales, où pour décider de se mettre à l’abris. (ne le fait plus si grave) |
| 3 | **Cassandre** | -1D4 PM à toutes les personnes autour par tour. (2D4 si grave) |
| 4 | **Egoïsme** | Jette 1D4, si 2 ou 4, ne peut pas aider ses alliés. (ne le fait plus si grave) |
| 5 | **Confusion** | Jette 1D4. Si 2 ou 4, attaque un de ses alliés au hasard. |
| 6 | **Méfiance** |  Le personnage ne se laisse plus soigner ou aider. |
| 7 | **Inoffensif** |  Ne peut plus attaquer les adversaires. |
| 8 | **Seum** | Le personnage boude dans un coin, et ne voudra pas agir durant le prochain tour. |

Pour être resistée lorsqu'elle est infligée, une affliction physique demande un jet de CON/2.

## Jet de panique

L’état de panique est l’état qui se produit lorsqu’un personnage passe en état panique (-100 % des PE max). Lors d'un **jet de panique**, le personnage jette un *dé 8* et prend dans le tableau le résultat du dés. S'il fait 1, rien ne se passe. Si le jet de panique est produit lorsque le personnage a atteint -100 % des PM max, au lieu de tomber stun, il tombe dans les pommes.
