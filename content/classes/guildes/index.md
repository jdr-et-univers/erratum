---
layout: layouts/base.njk
eleventyNavigation:
  key: Les guildes
  parent: Classes et experiences
  order: 10
title: Les guildes
models:
  - Guildes
  - Guildes métaphysiques
---

# Les guildes

Les guildes sont des groupes magiques visant à perfectionner certaines magies.

Ils ont généralement des pratiques favorites, et offrent des manières différentes, des approches alternatives à leur utilisation. Les guildes perfectionnent généralement une ou deux magies, mais des utilisateurs d'autres magies peuvent en faire partie.

## Entrer dans une guilde

Une guilde s'obtient lorsque le personnage remplis les demandes de la guilde pour entrer dedans, et rencontre de manière RP la guilde. 

Entrer dans la guilde est à la fois un évenement RP (même si le rapport réel in-story avec la guilde peut être totalement différents), et apporte au personnage des compétences (ou variation de compétences) et des effets potentiellements passifs.

## Guildes ordinaires

Ces guildes apportent deux éléments : un pouvoir unique passif, et la variation d'une magie. Cette variation permet de d'utiliser d'une manière différente le pouvoir d'une classe, ou de remplacer les pouvoirs qui sont utilisés. Pour rentrer dans l'une de ces guildes, le personnage doit avoir une affinité RP avec la guilde et avoir l'une des classes de la guilde (ou avoir comme plan d'entrer dans la guilde).

Le changement se fait à la prise d'un pouvoir. Læ personnage peut alors prendre la variation du pouvoir à la place du pouvoir concerné. À l'entrée dans la guilde, il est possible de remplacer les pouvoirs qui devraient être changé par ceux affecté.

- Les [druides](druides/) sont une guilde tirant leur pouvoir de la nature et des forces anciennes.
- Les [mages occultes](occultes/) sont une guilde tirant le pouvoir des forces sombres et obscures
- Les [prêtres](pretres/) sont une guilde tirant leur pouvoirs de leur lien avec les anges et la lumière.
- Les [sorcières](sorcieres/) sont une "guilde" complètement chaotique faisant ce qu'elles veulent. Et tant pis pour les gens mécontant.
- Les [moines](moines/) sont une guilde renforçant leur propre poings pour frapper.

## Guildes métaphysiques

Les guildes métaphysiques sont des guildes visant à plonger dans les secrets les mieux garder du monde, malgré les dangers que cela peut causer. Ces guides n'apportent pas de variations de classes, mais apportent un set de six pouvoirs.

Chaque pouvoir à son propre set de règle pour pouvoir rentrer dans la guilde.

- Les [alchimistes](alchimie/) cherchent à comprendre la réalité et à la manier au niveau le plus haut. Elle possède deux variations, l'alchimie hermétique, et l'alchimie créatrice.
- Les [irrégularités](irregularite/) sont un groupe de personne atteinte de l'anomie qui ont décide de l'utiliser comme source de pouvoir.
- Les [dévots](devots/) sont les suiveurs des dieux, acquérant des pouvoirs de la part de leurs dieux protecteurs. La dévotion possède trois variations : l'illuminé, le fanatique et le corrompus.