---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Les capitaines
parent: Les spécialités
---

# Les capitaines

Les capitaines sont des chefs charismatique qui peuvnet aussi bien utiliser leur charisme comme une arme que comme un moyen de protéger eux et ceux qu'ils défendent.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Tous en formation ! | Retire 2 points d'échec critique à l'initiative aux alliés grace à ses conseils avisés |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Diversion | CHA |  | Peut faire passer temporairement l’aggro d’un ennemi lui ou un lieu (se résiste sur SAG/VOL). |
| 1 | La force de la légende | CHA |  | Peut faire une attaque mentale à 1 dégat pour 2 point de respect dépensé, + niveaux de réussite |
| 2 | Stoïcisme |  |  | +1 armure psy<br />– tt les 10 % au-dessus de 70% de VOL<br />– tt les 5 % au-dessus de 100% de VOL |
| 2 | Contre-attaque | CHA | 3 tours de cooldown | Peut permettre à un allier de faire une contre-attaque après qu'un tank ait bloqué une attaque/se la soit pris du à une redirection d'aggro |
| 2 | Repli stratégique | CHA |  | Peut tenter de permettre à toute l'équipe de fuir un combat |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture défensive |  |  | Peut passer en posture défensive |
| 3 | Charisme du capitaine |  |  | +1 arme psy<br />– tt les 10 % au-dessus de 70% CHA<br />– tt les 5 % au-dessus de 100% CHA |
| 3 | Don't mess with the Capitaine (trait) | | |  Peut gagner 1D4+4 respect par ennemie tombé, ou énigme réussie |
| 4 | Double attaque | CHA | 3 tours de cooldown | Peut faire en sorte de que l'attaque de deux alliés se passe en même temps : la première attirera l'attention des ennemis |
| 4 | Permutation | CHA |  | Peut permuter le tour de deux alliés |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Pluie de flêche | CHA | 3 tours de cooldown | Peut faire agir tout les tireurs à distance dans le tour du premier, divisant par deux les chances d'esquives |
| 5 | Double-posture | PER | 3 tours de cooldown | Peut partager les pouvoirs de sa posture avec un allié pour un tour |
| 5 | J'avais vu venir le combat |  | 3/partie | Offre une réussite critique obligatoire à un allié au jet d'initiative pour 20 respect |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Stratégie de dernière minute |  | 1/partie | Peut annuler une attaque ou un malus touchant un allié pour 50 points de respect |
| 6 | Fortifications | CHA | 5 tours de cooldown | Peut diriger toute les prochaines attaques de tout les ennemis sur les tanks pendant 3 tours |
