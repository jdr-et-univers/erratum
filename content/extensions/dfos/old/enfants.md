# Le Enfants de Dagda

Les Enfants de Dagda sont des descendants extrèmement puissants de l'ancienne civilisation de Graal, qui sont souvent décrété comme étant né de l'étoile elle-même. Ils ont le droit à des cultes à travers tout le système stellaire, dont ils forme les dieux principaux. En plus d'être des titans, iels font partie d'une espèce à part, nommé les Eirons.

Les Eirons peuvent avoir des dévots, auquels ils confèrent un pouvoir spécial.

## Les Eirons

Les eirons sont une espèce mystérieuse, fortement magique et presque mystique vivant dans l'espace intersidérale. Iels ressemblent à des sortes "d'oiseau-poissons", constitué d'un long corps armé de 6 "bras-ailes-nageoires".

Les eirons ont une espérence de vie infinie, sauf accident.

Cette espèce peut flotter dans les airs ou dans les mers, cette espèce à un mode de déplacement grandement différent des autres espèces, et sont même capable d'effectuer des déplacements parfaits, fluide et d'ignorer les obstacles. Iels sont capable également d'effectuer des cris stridents. Mais leur plus grand pouvoir est leur connection à l'orgone, les rendants lié à tout ce qui est magique, mais *extrèmement* sensible à l'adamant sombre.

## Les Enfants de Dagda

Les Enfants de Dagda sont les "dieux" protecteurs du système de Dagda, et des êtres puissants, ayant une maitrise totale des énergies magiques. Iels n'ont cependant aucun pouvoir de commandement sur les sociétés Eirons, les protégeants juste de loin. On raconte que les 8 enfants sont des êtres anciennement important de la civilisation de Graal.

Les huit Enfants de Dagda sont les suivants:

- **Tadig Kozh** est l'être de la lumière. Guérisseur et doux, iel est avant tout un⋅e soigneur⋅euse et un guérisseur⋅euse. Mais on raconte que sa lumière peut également repousser les forces néfastes.

- **Ésus** est l'être de l'énergie. Épris⋅e de marchandage, toute sa vie est fondé sur le principe d'échange, de transfert.

- **Ankou** est l'être du néant. Selon la légende, iel aurait plongé dans le néant entre les mondes et vu les horreurs qui s'y trouve. Aujourd'hui, iel s'occupe d'amener les morts vers le système solaire de Sidh, ou ils peuvent vivre une nouvelle vie.

- **Ir** est l'être des ombres. Voleur⋅euse et tricheur⋅euse, iel est cependant le protecteur de tout⋅e⋅s celleux qui sont en marge de la société. On peut læ trouver à la tête de la guilde des rodeurs.

- **Nuada** est l'être de l'ordre. Iel est droit⋅e et vertueux⋅se, et n'accepte aucun écart moral.

- **Brigit** est l'être du temps. Mystérieux⋅se et peu bavard⋅e. Iel est cellui qui cache les secrets mais diffuse l'histoire. C'est læ protecteur⋅ice des druides, vates et bardes.

- **Llyr** est l'être de l'espace. Explorateur⋅ice, voyageur⋅se et scientifique, iel a pour but de comprendre le plus possible l'univers et ses secrets.

- **Moi** est l'être du chaos. Espiègle et joueur⋅se, iel joue des tours à celleux qui tentent de la défier, et qu'iel l'inventeur⋅ice d'une grande partie des jeux anciens. Son nom est une de ses nombreuses blague, répondant toujours "Je suis moi" quand on lui demande qui il est. Il est un grand amis des Re Vihan.

## Les morgannes

Les morgannes sont des types de eirons née de la transformation d'être "normaux" en eirons. Capable de passer à volonter et changer de forme, les morgannes sont souvent méfiée et appréciée à la fois. L'une des plus célèbre est Dahut, dernière princesse d'Ys, mais aussi Arès, vivant dans les limes.

La majorités des Morganes vivent autour de la naine brune Sidh. Les Morgannes peuvent avoir des dévots, mais que deux~trois maximums, contrairement aux Enfants de Dagda qui peuvent en avoir un nombre illimité.