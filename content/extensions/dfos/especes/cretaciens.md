---
layout: layouts/base.njk
title: Les Crétaciens
tags: Espèce DfOS
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les Crétaciens

Les créataciens, ou dinosauriens, sont une espèce de dinosaure ayant atteint l'intelligence. Ils ont formé une civilisation spatiale il y a de cela 60 millions d'année, n'ayant subsisté cependant que quelques millénaires avant de se faire exterminer en grande partie par l'astéroïdes ayant percuté la Terre. Ils ont été la première espèce intelligente sur la planète, à l'exception des trolls qui sont encore plus anciens.

Vivant maintenant majoritairement sur Mars (avec quelques espèces présente sur Korchis), coincé dans des bases après de nombreuses catastrophe, ils sont une espèce agile et bourrine.

Oui, ils ont des plumes.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:----------:|:-------:|
| 18 | 10 | 10 | 6 | Griffes (2) | FOR, HAB et CHA (60) | CON, SAG et PER (40) | Le reste (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| Oh non y'en a plusieurs | Gagne +1 pression par crétacien présent dans le combat |
| Attention Deficit Hyperactivity Dinosaure | Gagne une attaque d'op à -20% et ou les échecs critique sont doublé quand un ennemi tente de s'enfuir |
| A sang froid | Faiblesse contre la glace ; -20% HAB et CON en milieu froid |