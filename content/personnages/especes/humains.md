---
layout: layouts/base.njk
title: Humains
tags: Espèces de base
models:
  - Espèces de base
  - Espèces additionnelles
  - Espèces légendaires
parent: Les espèces
---

# Humains

Les humains sont l'espèce la plus commune sur terre, de loin, au point où l'être humain est considéré comme l'espèce dominante, et la population la plus puissante politiquement.

Cet animal ayant évolué de la branche des homininé est à l'origine un chasseur cueilleur dont l'inventivité et la capacité d'adaptation à aidé à évoluer. Il a aussi la particularité d'avoir peu de faiblesses magiques, et d'être très polyvalent.

Considéré comme l'une des *quatre grandes espèces*, descendant de l'Atlantide, les humains sont aussi appellé le *peuple de l'automne* et *la grande espèce guerrière* (titre partagé avec les dragons). Depuis la révolution industrielles, ils sont considéré aussi comme les peuples de la *technique*, utilisant des technologies à base d'assemblement et de construction.

Leur protecteur est Lith, dieu de l'automne.

| PV | PE | Eclat | Pression | Armes | Forces | Faiblesses | Neutres |
|:--:|:--:|:-----:|:--------:|:-----:|:------:|:------:|:------:|
| 12 | 12 | 10 | 5 | Poing (0) | Aucune | Aucune | Toutes (50) |

## Traits et compétences

| Trait | Effet |
|:-----:|:------|
| La technique | +20 % de capacités à utiliser les machines. | 
| Animal rationnel | Possède une armure mentale de 2 par défaut. | 