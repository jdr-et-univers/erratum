---
layout: layouts/base.njk
eleventyNavigation:
  key: Campagnes
  parent: Système de jeu
  order: 3
---

# Organisation d'une campagne

Une campagne est l'ensemble d'un scénario de JDR. Dans Erratum, l'accent a été mis sur des campagnes moyennes ou longues, avec des joueurs.

Sur cette page se trouve quelques notions liées à ce système de campagne.

## Multi-personnages

Chaque joueur dans Erratum peut avoir plusieurs personnages. S'il n'est pas conseillé d'aller au delà de 4 par joueur, il n'y a pas vraiment de limite en terme de nombre de personnages.

Le MJ peut rajouter des limitations cependant en plus du nombre maximale de personnages par joueur :

- Le nombre de personnage par combat
- Le nombre de fouille possible par joueur
- Le nombre de personnage par partie (voir section suivante)

## Épisodes et parties.

> **Note:** Il est aussi possible d'utiliser ce système avec plusieurs joueurs, en gérant quand les joueureuses ne sont pas présent⋅e⋅s pour jouer à votre JDR, et du coup n'avoir que quelques membres présents par partie.

Erratum distingue la notion d'épisode et de partie. L'épisode consiste à une unité de scénario, contenant plusieurs parties. L'expérience et le prestige sont gagné à la fin de l'épisode et non de la partie.

Les parties peuvent avoir plusieurs utilités :

- Tout simplement diviser l'épisode en 2 si le temps de jeu est limité.

- Limiter le nombre de personnage par partie (voir section multi-personnage), tout en faisant que tous aient joué durant l'épisode complet. Il est en effet déconseillé de dépasser les deux personnages par joueur chaque partie.

Au début de chaque épisode, les joueurs indiquent quels personnages seront attribués à chaque partie de l'épisode (chose que le MJ peut faire changer pour des raisons intra ou extra-diégétique). Si un joueur à moins de personnages que les autres, et que cela fait sens d'un point de vue RP, un même personnage peut participer à deux parties, mais gagnera autant d'XP et prestige que les autres.

À la fin de l'épisode, le MJ indiquera les gains à la fin de l'épisode. C'est à ce moment là, ou avant la première partie de l'épisode suivant, que les joueurs pourront améliorer et customiser leur personnage.

## JDR one-shot

Erratum étant fondée sur l'évolution et la customisation de personnage, il n'est pas optimisé à priori pour les JDR one-shot. Cependant, voici quelques éléments qui peuvent aider à l'élaboration de partie one-shot :

- Au lieu de faire commencer vos personnages sans expérience, donnez-en une certaine quantité pour que les joueureuses puissent le customiser et avoir des personnages plus avancés que ceux "de départ".

- Vous pouvez également créer vous-même un personnage de toute-pièce à l'aide de compétences et d'éléments qui vous chantent, afin de permettre à chaque personnage d'être adapté pour ce que vous voulez dans ce one-shot.

- Donner un moment en cours de partie pour amplifier les personnages, afin de donner un sentiment de progression dans la partie.

Pour les parties one-shot, il peut être intéressant de reprendre ce système, mais en limitant les possibilités d'expérience et en prenant à un rang plus élevés les personnages.
