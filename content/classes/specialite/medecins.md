---
layout: layouts/base.njk
tags: Les spécialités
models:
  - Les spécialités
title: Le médecin
parent: Les spécialités
---

# Le médecin

Médic ! Le médecin est une classe basée essentiellement sur le soin, mais elle peut faire aussi de nombreux boost et aide utilitaire.

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:------|
| 0 | Corps sain | Bonus de 10 % pour résister aux afflictions physiques et maladies |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 1 | Premiers soins | HAB/INT |  | Soigne le niveau de réussite / 2 et retire un saignement. |
| 1 | Stimulant | HAB | | Ajoute +10% chance de toucher les ennemis et fait faire +3 dégats pendant 3 tours. |
| 2 | Soin psychologique | REL/INT |  | Soigne le niveau de réussite / 2 et retire terreur |
| 2 | Consultation | INT/HAB |  | Peut soigner une altération d'état au choix |
| 2 | Surdosage | INT+HAB |  | Peut infliger poison (résistence sur CON/2) |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 3 | Posture d'aide |  |  | Peut passer en posture d'aide |
| 3 | Boost osseux | HAB | | Ajoute +3 armure à un allié pendant 3 tours. |
| 3 | Soin d'altération | HAB/INT |  | Soigne une altération physique |
| 4 | Boost immunitaire | HAB | | Ajoute +50 résistence aux afflictions pendant 3 tours. |
| 4 | Multi-patient |  | 4 tour de cooldown | Peut soigner jusqu'à deux personnages supplémentaire avec son jet de soin |

## Pouvoirs experts (5)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 5 | Stabilisation d'urgence | HAB | 1/partie | Peut ranimer hors-tour un perso KO |
| 5 | Connaissance du corps | HAB |  | Peut frapper au CaC une partie et lui faire un effet d'incapacitation pendant 3 tours |
| 5 | Serment d'hypocrate |  |  | Gagne 1D6 confiance à chaque soin réussi tant qu'il n'a causé aucun dégat et n'a pas échoué de soin. |

## Pouvoirs ultimes (6)

| Niv | Pouvoir | Stats | Cout | Effet |
|:---:|:-------:|:-----:|:----:|:-----:|
| 6 | Séance longue | INT+HAB |  | Lors d'un déplacement, peut prendre du temps pour offrir +10% max PV/PE à tout ses alliés |
| 6 | Soin parfait (trait) |  |  | Les premiers soin et soin psychologique font maintenant tout les niveaux de réussites en soin |