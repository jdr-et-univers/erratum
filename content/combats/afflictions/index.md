---
layout: layouts/base.njk
eleventyNavigation:
  key: Les afflictions
  parent: Système de combat
  order: 4
title: Les afflictions
models:
  - Afflictions communes
  - Malédictions
---

# Les afflictions et effets

En combat ou non, différentes afflictions peuvent également affecter votre personnages, appliquant des effets sur lui jusqu'à ce qu'elles disparaissent. Elles peuvent toucher sa fois sa manière de fonctionner, les actions qui lui seront possibles… et être plus ou moins grave suivant les situations.

Les afflictions sont les effets négatifs, tandis que peuvent exister des effets plus positifs/neutres. Les afflictions se soignent avec des soins spéciaux, qui doivent être indiqué comme (phys), (ment) ou (mag) pour pouvoir soigner le bon type d’affliction.

Certains de ces types d'effets peuvent s'obtenir par des jets aléatoires.

## Effets

Les effets ne sont pas forcément positif et négatif, et ont leur durée à une durée limitée (un combat, une partie).

- Les [effets de combat](effets/) sont des effets qui durent tout le long d'un combat ou d'une séquence d'action.
- Les [grâces](graces/) sont des effets magiques positifs durant toute une partie.

## Afflictions communes

Les afflictions sont des statuts de personnages qui ont la particularité de continuer même après le combat. Ces statut ont également besoin d'être soigné avec des soins particulier, où en profitant des soins d'une auberge. Elles sont divisées en deux types : physiques et mentales. Les afflictions physiques sont des maux du corps, tandis que les afflictions mentales sont des maux de l'esprit.

- Les [afflictions physiques](physiques/) sont des maux du corps qui peuvent affecter votre personnage.
- Les [afflictions mentales](mentales/) sont des maux de l'esprit qui peuvent affecter votre personnage.

## Malédictions

Les malédictions sont des phénomènes plus graves, pouvant affecter votre personnage sur de nombreuses parties. Elles ont très peu de moyen d'être soignées

- Les [malédictions](maledictions/) sont des effets permanent plus ou moins grave.
- Les [malédictions antiques](antiques/) sont sept malédictions puissantes pouvant affecter l'esprit et la réalité d'un personnage.
- La [malédiction anomiques](anomie/) est un maux évolutif affectant la magie et l'essance même d'un personnages.

## Détermination

La détermination est l'état d'esprit qui permet à un personnage, à travers l'éclat, à dépasser certaines règles naturelles du monde. Cependant, la détermination n'est pas forcément positive : parfois elle peut provoquer des effets secondaires... imprévus.

Le jet de détermination peut être fait par le joueur à tout moment, une fois par partie. Il se fait sur un D20.

| Résultat | Effet |
|:--------:|:------|
| 01 | Le personnage jette 1D100. Si « réussite critique » (pression/2 si déjà fait une apothéose), il fait une apothéose, sinon il gagne un point de karma. |
| 02 | Le personnage gagne un point de karma |
| 03 | Le personnage fait un jet de bénédiction. |
| 04 | Le personnage pourra choisir un ennemi (hors boss) qui abandonnera immédiatement un combat, avec toutes les récompenses de s'ils avaient gagnés. |
| 05 | Le personnage gagne un jet de loot bonus durant la partie |
| 06 | Le personnage aura le droit à un bonus de 20% de l'argent qu'il aura gagné durant la partie |
| 07 | Le personnage pourra relancer un jet gratuitement |
| 08 | Le personnage regagne 2D10 PV et 2D10 PE |
| 09 | Le personnage gagne +10 % à toute ses stats pour la partie. |
| 10 | Le personnage pourra échangé entre un jet de pestilence/panique/malédiction |
| 11 | Tu es heureux : le personnage regagne 1PE. |
| 12 | Le personnage lance une pièce : si pile, il sera immunisé pendant la partie aux altération mentale, si face à celles physique, mais les autres feront des jets doubles, |
| 13 | Ouch : le personnage perd 1PV |
| 14 | Le personnage perd 10% à ses stats durant toute la partie |
| 15 | Mauvaise santé : Le personnage ne pourra pas passé au dessus des trois quart de ses PVs pour la partie |
| 16 | Fragilité émotionnelle : Les attaques de mental feront 1.5× plus d'effet |
| 17 | Le personnage fait un jet de panique immédiatement, dont l'effet durera jusqu'à la fin de la partie. |
| 18 | Le personnage à un malus de deux éclat pour la partie.
| 19 | Le personnage fait un jet de malédiction, dont l'effet durera jusqu'à la fin de la partie. |
| 20 | Le personnage reçoit l'affliction anomique, et doit faire un jet. Si échec critique, l'affliction sera permanente. |

### Apothéose

L'apothéose est le moment où un personnage dépasse tout ce qu'il est, atteint le paroxisme de lui-même. Elle permet d'obtenir un signe supplémentaire pour un signé, et permet d'obtenir d'autres types de pouvoirs pour un non signé (voir obtenir un signe suivant les circonstances).

L'apothéose peut s'obtenir par un jet de détermination, où est être demandé sous forme d'un jet d'apothéose (D100) par le MJ, qui déterminer à partir du résultat si l'apothéose se produit.

