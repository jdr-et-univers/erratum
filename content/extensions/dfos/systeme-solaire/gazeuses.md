---
layout: layouts/base.njk
tags: Système Solaire
title: Les géantes gazeuses
models:
  - Dragons from Outer Space
  - Espèce DfOS
  - Système Solaire
  - Autre systèmes
parent: Dragons from Outer Space
---

# Les géantes gazeuses

Les géantes gazeuses sont bien moins exploré que celles telluriques. Cependant, elles possèdes de nombreuses bases orbitales, anciennements construites par les quatres anciennes civilisations, et quelques civilisations existent sur les satellites.

## Jupiter

Jupiter est la plus grande planète du système solaire, qui possède de nombreuses anciennes bases en gravitation autour de la planète. La plupars sont plutôt bien conservées et ont commencé à être restaurée par les organismes terrestres. Trois des quatres civilisations (hors anostiens) ont construit des bases autour de la planète.

Jupiter Prime est la plus grande de cette base, basée sur une ancienne base Danaïte. La base ressemble à un astéroïdes de plusieurs kilomètre autour de la planète. Cependant, son intérieur est végétalisée et à une gravité artificielle.

### Satellite troyens

Les satellites troyens sont les quatres plus grand satellites de Jupiter, et sont ceux qui ont été le plus exploité et explorés.
- *Io* est un satellite hautement volcanique, habité par quelques troll spéciaux, les *trolls de magma*.
- *Europe* est la plus explorée des satellite du système solaire externe, en particulier pour son océan interne. Quelques bases sous-marines très profondes et très anciennes s'y trouvent, et contiendraient des secrets. Cependant, y accéder est difficile, à cause des *monstres sous-marins qui s'y trouvent*. Quelques bases s'y trouve en surface pour l'étudier.
- *Ganymède* est la plus grande lune du système solaire, plus grand même que Mercure ! Mais en vrai elle est assez peu peuplée et étudiée comparé à d'autres comme Io et Europe. Son océan contient cependant une immense base de danaïte marine, vivant dans un palais de corail. 
- *Callisto* est la lune la moins peuplée de Jupiter, ayant qu'une seule base.

## Saturne

Saturne est la géante gazeuse célèbre pour ces anneaux, et c'est la source de presque toute l'activité autour de la planète : les stations tourisstiques. Les gens peuvent s'y enfermer pendant des jours comme un hotel lorsqu'il pleut, le tout pour bien plus cher, mais au moins avec une très belle vue sur les anneaux de jupiter.

Etrangement, cela a suffit pour faire la fortune de nombreuses entreprises, qui rivalisent pour vendre leur place dans des cages dorées avec une jolie vue. Une partie de leur population est composée de milliardaire s'ennuyant et cherchant un moyen de donner l'impression que leur vie est incroyable.

Son satellite peuplé est Titan, qui est un autre des satellites possédant un océan intérieur. Une étrange onde radio basse fréquence s'y trouve, et l'origine est peu connue. On soupçonne la présence d'un dieu ancien Atlante qui y vivrait toujours.

## Uranus

Uranuse est une planète assez "banale" du système solaire externe. Elle possède quelques bases militaires, et quelques constructions autour. Quelques entreprise veut exploiter les hydrocarbures de son athmosphère.

### Satellite uraniens

Les satellites d'Uranus, notamment Titania et Oberon, sont tous peuplées par des trolls et autres peuples assimilées. Ces trolls notamment y sont différent, y étant des trolls de glace.

## Neptune

Neptune est la plus éloignée des planètes du système solaire.

Neptune est surtout connue parce qu'autour tourne le *Portail à Trou de Ver* (nommé le "propulseur") du système solaire, permettant d'accéder aux planètes. Il a été réactivé dans les temps après les débuts de l'exploration du système solaire, et à permis de relancer des communications avec les autres systèmes solaires.

### Triton

Triton est un des satellites important, ayant plusieurs immenses bases militaires. C'est là ou se trouvent notamment la surveillance des évenements pouvant se trouver au niveau du portail, et au niveau de la ceinture de kuiper.