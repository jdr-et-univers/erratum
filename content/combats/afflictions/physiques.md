---
layout: layouts/base.njk
tags: Afflictions communes
models:
  - Afflictions communes
  - Malédictions
title: Les afflictions physiques
parent: Les afflictions
---

## Afflictions physiques

| id| Affliction | Effet |
|:-:|:----------:|:------|
|   | **Saignement/Brulure** | Perd -1PV/tour. Peut se stack. Ne peut pas être grave, ×2 blessure si critique |
| 2 | **Poison** | Perd 2 PV/tour (si grave, tout les tours le poison fait 1 PV/tour en plus) |
| 3 | **Aveuglé** | Tout jet de PER voit sa statistique divisé par 2 (x0 si grave/Cécité) |
| 4 | **Sommeil** | Ne peut pas agir. Peut se réveiller si attaqué ou grand bruit, ou avec un jet de VOL/2 chaque tour. |
| 5 | **Épuisement** | Chaque tour, -5% HAB et CON | 1 au dessus de 100, capé à 6 baisses) |
| 6 | **Paralysie** | Le personnage à 1 chance sur 4 de ne pas pouvoir agir chaque tour. |
| 7 | **Silence** | Ne peut plus faire d'action demandant la parole (magie, discussion orale, etc). |
| 8 | **Stun** | Le personnage ne peut pas jouer |

Pour être resistée lorsqu'elle est infligée, une affliction morale demande un jet de SAG/2. Si jet de panique à cause de manque de PM, pas de possibilité de resister.

## Jet de pestilence

Lors d'un **jet de pestilence**, le personnage jette un *dé 8* et prend dans le tableau le résultat du dés. S'il fait 1, rien ne se passe, s'il fait 8, il reçoit l'effet stun. Il peut se produire suite à une capacité ou si votre personnage

À ces afflictions on peut ajouter techniquement les états de morts et de KO.
