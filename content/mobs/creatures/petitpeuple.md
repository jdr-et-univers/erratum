# Le petit peuple

Le petit peuple est un ensemble de petits êtres magiques souvent lié à des lieux ou des espaces, mais qui peuvent faire partie de plusieurs des grands groupes d'espèces cités ci-dessus. Parmis les membres du petit peuple, on peut trouver :

- Les *peuples des forêts* (et des plaines) de la famille des fées et danaïtes
- Les *esprits domestiques*, de la famille des êtres de magies pures et anostiens.
- Les *peuples des montagnes*, de la famille des minéraloïdes et trolls.

## Peuple des forêts

Le peuple des forêts est le nom donnée à toutes les fées de petites tailles. Ces êtres sont fortement lié aux danaïtes ou "grande fée", puisque le peuple des forêts est généralement 10 à 15 fois plus nombreux autour des ruches danaïtes, et ils sont affecté par les danaïtes, en particulier les reines. Si ce liens à longtemps été comparé à un lien de suzeraineté par les humains, dans les faits il est plus complexe : le peuple des forêts ne doit aucune obéissance aux danaïtes, mais les deux s'entraident, ayant besoin l'un de l'autre.

Le peuple des forêts peut avoir des apparences et des particularités diverses, suivants les lieux.

Les plus connus sont les scintillante (aussi appelée "petite fée") ou les elfes. Souvent des petites tailles, excellent pour se cacher et être discrets, ces petites êtres sont des experts de la survie en forêt, parfois de ce fait un peu cruels. D'autres vivent plus proche des plaines (tels qu'en terre celtique les korrigans et les farfadets) et sont plus farceurs, vive en petite colonies et sont connus pour piéger les humains pour leur amusement.

Le peuple des forêts fait partie du *petit peuple*.

## Les esprits domestiques

Les esprits domestiques (aussi appellé "peuple des foyers") sont des êtres de magie pure, formée par la magie environnante présents dans certaines maisons. Ils sont les protecteurs de la maison, et les bienfaiteurs de la famille qui y vit. Cependant, il est important de savoir que la plupars du temps, les esprits domestiques n'ont pas de "loyauté" magique à la famille, mais uniquement *à la maison*. Cela ne veut pas dire qu'ils ne peuvent pas avoir de l'amitié (ou de l'inimitié) envers la famille qui y vit.

Parmis les esprits domestiques on peut trouver les lutins, les lares, les nisses, les brownies est bien d'autres créatures dont les pouvoirs et les capacités changent suivant les yeux. Il est à noter que parfois, les esprits domestiques se forment à faire des volontés de défunts qui ont vécu dans la maison (dans ce genre de cas, ils auront souvent un attachement plus grand à leur famille qu'à leur maison)

Les esprits domestiques sont souvent prêt à aider pour certaines tâches, mais doivent être traités avec le plus grand respect. En effet, ils sont parfois farceurs, et n'hésiteront pas à se venger si jamais on les trompe ou abuse de leur hospitalité… Par contre, leur faire des farces est souvent acceptable… si on est prêt à en avoir en retour !

Les esprits domestiques font partie du petit peuple.

## Peuple des montagnes

Le peuple des montagnes est le noms donnés à de petits êtres minéraux vivant souvent en montagne, et né des collisions entre géants. Trop petits pour devenirs des gobelins, ils sont extrèmements magiques et ont souvent une façon de vivre plus souterraines. Les plus célèbres sont les gnomes, des petits êtres souterrains se nourrissant de silicates, et les nains, connus pour leurs grandes barbes de fibre de silicium.

Ces êtres vivents dans des grande galeries et exploitent des filons de minerais, dont certains qui ne leur sont visible que d'eux.

Le peuple des montagnes fait partie du *petit peuple*.