---
layout: layouts/base.njk
title: Accessoires
tags: Les équipements
models:
  - Les équipements
parent: Les équipements
---

## Accessoires

> **Note:** Pour des raisons de gameplay, les personnages sont limité à deux accessoires. Si vous vous demandez pourquoi le personnage ne peut pas avoir des lunettes, des chaussures et un slip à la fois, dites vous que dans les faits, oui il a un superbe slip magnifique et vos baskets qui vous empêcheront d'entrer en boite de nuit.

<div id="dataTable">

| Accessoire | Effet | Prix |
|:----------:|:------|:----:|
| Faux mouscle | +15 en FOR | 650 |
| Bracelet de santé | +15 en CON | 650 |
| Chaussures de courses | +15 en HAB | 650 |
| Blouse | +15 en INT | 650 |
| Medaillon de philosophe | +15 en SAG | 650 |
| Porte-clef en peluche anti-stress | +15 en VOL | 650 |
| Cape de héros | +15 en CHA | 650 |
| Masque de voleur | +15 en DIS | 650 |
| Collier mignon | +15 en REL | 650 |
| Monocle ultime | +15 en PER | 650 |
| Anneau de contrôle satanique | +15% CHA/VOL contre démons | 750 |
| Débardeur d'acier | +3 armure physique | 750 |
| Joli clochette | +3 armure magique | 750 |
| Lunettes de soleil swag | +3 armure mental | 750 |
| Lunettes Optic Grosbill | Protège de tout malus en perception | 750 |
| Masque de terreur | En cas de jet de panique, 1D4. Si 4, un allié fait aussi un jet de panique. Sinon, un ennemi le fait | 1500 |
| Slip divin | +20 aux jets de survies | 2550 |
| Toge Ancienne de Cultiste | Boost de SAG + 10% de résistance mentale à la métaphysique | 1000 |
| Toge Ancienne de Frontière | Boost de INT + 10% de métaphysique | 1000 |
| Ceinture Octarine | La capacité avec le moins d'éclat d'un combo d'art martial devient gratuite si plus d'une est présente | 2000 |
| Pendentif Biélémentaire | Peut passer en STAB l’un de ses autres éléments | 2000 |
| Collier de fleur | Ajoute +10 % aux stats de tout les animaux, créature de type bestiales ou membres du petit peuples (hors danaïtes) | 2000 |
| Bague sombre | Les techniques d’âme de l’élément principal (le premier choisi) voient leur dé passer au niveau supérieur | 2000 | 

</div>