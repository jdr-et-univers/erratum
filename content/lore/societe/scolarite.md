---
layout: layouts/base.njk
tags: La société magique
title: L'éducation magique
models:
  - La société magique
parent: Lieux importants
---

# L'éducation magique

Comment apprends-on la magie ? La question de l'apprentissage de la magie à une grande importance depuis la découverte du monde magique. Les enseignements sont cependant largement différents suivants les pays et les continents. Il ne serait pas vraiment possibles de lister les écoles, mais il peut être possible de faire une petite présentation de quelques manières d'apprendre la magie dans le monde d'Erratum.

Cette enseignement possède deux rôles : permettre aux élèves dôté de capacités magique de les exploiter ou intéressé par les magies communes, mais également d'apprendre aux élèves ayant des pouvoirs magique à les controler et à éviter de blesser leurs contemporains ou eux-même.

Trois grandes manières existent afin de créer des permettre l'enseignement de la magie :

- Les *programmes magiques*, consistent à ajouter des cours adaptés dans un programme commun, faisant cotoyer enfant magiques et non-magique.
- Les *écoles spécialisées de magi*e, très présente dans le monde anglo-saxon, consiste à la fondation d'école spécialisée dans la magie.
- L'apprentissage de la magie via un *maître de magie*.

## Les programmes magiques

Dans de nombreux pays, l'apprentissage de la magie s'est en grande partie institutionnalisé et existe comme annexe des programmes classiques. Il peut se faire via des filières spécifiques et/ou des cours supplémentaires ou des options. Cette façon de faire est très présent dans les pays utilisant du code civil (europe continentale notamment), ou de l'ex-bloc communiste. Le principe est de mixer au possible les populations magiques et non-magique pour éviter au possible que les deux sociétés soient trop séparées.

Cette façon de faire se fait surtout également plus lors du primaire et du secondaire, moins d'université public ayant des grands parcours de magie. Ces programmes sont également surtout suivi par les magiciens de classes populaires et moyenne, qui suivent leur apprentissage magique dans le système scolaire ordinaire, et très fortement déprécié par les grandes familles magiques.

### En France

Il est possible dans les établissement scondaire en france de suivre le *Programme Commun Français Magique*, programme géré par le ministère de l'éducation nationale. Environ 30% des établissement scolaire sont capable de gérer correctement la présence d'un élève magique, et de lui donner des enseignements adaptés. Cette idée d'un programme de magie commun vient en grande partie de la révolution Française, qui a voulu unifier les territoires magiques et non-magique de France.

## Enseignements spécialisés

Il est également possible de suivre des enseignements de magie dans des enseignements spécialisée dans des établissements réservés à l'apprentissage de la magie, parfois réservé à ceux ayant naturellement des pouvoirs magiques. Ces écoles sont notamment très présente dans les pays anglo-saxon, notamment la plus connue d'elle, *l'Ancestrale Ecole de Camelot*, école de magie privée ayant connu quelques critiques de parents d'élève pour sa vision traditionnaliste de la magie, allant jusqu'à mettre en hypercompétition ou en danger les élèves.

Ces écoles ont aussi des arguments pour leur existence : estimant que cela évite les discriminations entre élève magique et non-magique, les disputes, et que c'est moins dangereux de la présence uniquement d'être comprenant la magie.

Ces écoles deviennent bien plus courante partout dans le monde à partir de l'université.

### La ville universitaire de Werenia

La ville universitaire de Werenia est une ville de taille moyenne d'Autriche entièrement dédiée à l'apprentissage de la magie. C'est très simple : 100% de l'activité de la ville est directement ou indirectement dédiée à l'apprentissage de la magie.

La ville contient 61 écoles, dans 16 langues différentes, allant de l'école maternelle aux universités. Elle peut accueillir un nombre d'élève considérable, et presque toute la classe magique moyenne et élevée à fait ses études dedans.

C'est dans cette ville que se trouve également toutes les grandes écoles qui permettent d'accéder à des postes magiques importants. Une grande partie des enseignement.

### Le pensionnat de DanseRonce

Le pansionnat de DanseRonce est un petit pensionnat pour enfant « étranges » se trouvant dans la petite ville de Ergué Plouernec en Bretagne profonde. Un petit pensionnat né d'un ex-couvent, qui n'aurait rien eu de particulier si ce n'était par pur hasard un rôle important lors de l'ancienne *guerre des vertues*.

À partir de cela, le pensionnat est devenu un lieu de plus en plus chaotique, provoquant moult déboire à la petite ville qui l'abrite, ainsi qu'à son grand rival, le lycée privé George Pompidoux.

Techniquement parlant, ce pensionnat suit le Programme Commun, mais possède la particularité d'être *avant tout* une école magique, chose rare en France.

### Autres écoles

- L'école sombre de **Sæmundr fróði**, une école islandaise de magie, apprennant à maitriser et à se prémunir de nombreux arts obcurs, et garde notamment de nombreuses éruditions.
- L'école de **Domdaniel** est une école situé au fond des océans, la grande Université des Ondines, ou notamment se trouvent de nombreuses créatures magiques.
- L'université de **Salamanque** possède l'un des plus grandes université magique d'Europe ayant un cursus non-magique.
- **Scholomance** est une école de démonologie, enseignée par des démons eux-même. L'école ne permet que 10 élève par an, et dans chaque cycle, un élève doit rester dans l'école pour la tenir.

## Les maîtres de magie

L'une des méthode traditionnelle d'apprentissage de la magie, et celle encore pratiquée dans les grandes familles traditionnelles magique. L'apprentissage via un maitre de la magie est un des grandes organes traditionnels du monde magique, et avoir un maître prestigieux est souvent un signe de prestige pour soi-même.

En règle générale, quelques règles sont présente dans ce genre d'apprentissage :

- Les maîtres doivent posséder le signe de leur élève.

- Les élève apprendront la même pratique magique que leur maître.

Cet enseignement permet généralement d'apprendre de nombreux secrets passé de maître en élève. Cette vision est contestée, étant vu comme une certaine forme d'entre-soi des grandes familles magiques. Elle est très utilisée (et très encadrée) dans les pays utilisant un Programme commun de magie.
