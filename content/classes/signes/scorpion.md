---
layout: layouts/base.njk
tags: Signes de Force
models:
  - Signes de Guérison
  - Signes de Force
  - Signes de Protection
  - Signes de Malice
  - Autour des signes
title: Scorpion
parent: Les signes
---

# Scorpion (le poison)

Les scorpions tirent leurs pouvoirs des poisons. Ils deviennent capables d'en sécreter naturellement et de les injecter aux autres personnes, pour pouvoir obtenir différents effets.

Ils sont aussi bien médecins comme attaquants ; ne dit-on pas que les médecines ne sont que des poisons bien utilisés ?

<div class="table-responsive">

| Symb | Autre nom | Arcanes | Alignement | Rareté | Stats privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :scorpius: | Serpent (asie), ??? (égypte) | Force et Guérison (DPS Soigneur) | Chaos | Commun (15.0%) | HAB | Violet sombre |

</div>

## Pouvoir de base

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Simple |  Injection (capacité)  |  Peut injecter cinq tours un poison qu’il a sécrété à un ennemi, sur un jet d’HAB. L’ennemi peut esquiver. Peut de base injecter l’effet « poison ». |
| Double |  Potion = poison (trait)  |  Peut pour chaque nouveau niveau, peut apprendre une potion ou un effet utilisable comme poison à la place.  |

## Pouvoirs novices (1 & 2)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 1 |  Faiblesse (poison)  |  Produit un effet de faiblesse, à -15% sur les stats physiques.  |
| 1 |  Élixir ("poison")  |  Injecte une version "positive" de l'effet de poison.  |
| 2 |  Cécité (poison)  |  Inject l’effet « Aveuglé »  |
| 2 |  Sommeil (poison)  |  Inflige l’effet « Sommeil ».  |
| 2 |  Lame empoisonnée (jet simple)  |  Met un poison sur sa lame, +25 % de chance pour l’injecter en cas d’attaque qui réussi à infliger des dégats. Se cumule aux autres « lame emploisonnée ».  |

## Pouvoirs adepte (3 & 4)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 3 |  Posture offensive  |  Peut passer en posture offensive (II : posture offensive +)  |
| 3 |  Antidote (INT)  |  Peut soigner une (II : 2 affliction) affliction physique sur un personnage au choix.   |
| 3 |  Poison+  |  Le poison et l'elixir deviennent progressifs  |
| 4 |  Multi-inoculation (1 Eclat par personnage supplémentaire)  |  Peut injecter 5 tours un même poison, jusqu’à 5 (II : 7) personnages.  |
| 4 |  Choc toxique (3 Eclat, 3 tour de cooldown)  |  Le poison s’annule, mais fait l’effet du nombre total de tour de poison d’un coup (+2 tour)  |

## Pouvoirs experts (5)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 5 |  Adrénaline (4 Eclats)  |  Peut sortir de l’inconscience un personnage.  |
| 5 |  Drogue de fatigue (poison)  |  Injecte l’effet « épuisement ».  |
| 5 |  Poison++  |  Le poison et l'elixir voient leurs dégats/soins doublés.  |

## Pouvoirs ultimes (6)

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| 6 |  Essence de mort (1 karma)  |  Active un poison sur tous les personnages en combat (II : le poison ne dure que trois tours), qui dès qu’il sera fini, met à -5 PV, s’annule après KO du personnage.  |
| 6 |  Renforcement des poisons  |  Peut pour 4 éclats supplémentaire faire durer un poison 8 tours.  |

## Pouvoirs du Hérault

| Niv | Pouvoirs | Effet |
|:---:|:--------:|:-----------------------------------------|
| Herault |  Cocktail détonnant (1/combat)  |  Cumule jusqu’à 5 poisons d’un coup sur un seul et même personnage. Tous resteront actif pendant les 5 tours, combinés.  |
| Herault |  Le poison coule dans mes veines (trait)  |  Immunité totale aux poisons. Si le personnage est atteint du status *saignement*/*brulure*, les ennemis qui l’attaquent au CaC se prennent le dernier poison utilisé par le scorpion.  |

## Scorpion²

Le scorpion au carré peut apprendre des potions, ou bien directements des effets ou afflictions. S'il décide d'apprendre des effets, il peut apprendre :

- Niveau 1 à 4 : Des effets, afflictions (physiques ou mentales)
- Niveau 5 & 6 : Il peut même ajouter des malédictions, voir des grâces (qui ne dureront que le temps du poison.)

Il est aussi possible de voir avec le MJ des nouveaux types d'effets qui serait une "invention" du scorpion.