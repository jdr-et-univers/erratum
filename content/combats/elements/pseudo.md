---
layout: layouts/base.njk
tags: Les éléments
models:
  - Les éléments
title: Pseudo-éléments
parent: Les éléments
---

# Pseudo-éléments

Les pseudo-éléments sont un ensemble de quatre entités qui ont des propriétés proches d'un élément, sans en être véritablement. Contrairement aux vrai éléments, les pseudo-éléments n'ont normalement pas de vrai réalité "magique", ou en tout cas une bien moins importante.

Cependant, elles ont toutes les autres propriétés.

| Toxine | Son | Force | Esprit |
|:-------|:----|:------|:-------|
| Tous les poisons et entités qui affaiblissent le corps et l'esprit. | Des ondes qui peuvent être détectée et utilisée comme information par la plupart des créatures. | L'énergie née de la combattivité et de la force physique du corps... LE MOUSCLE | L'énergie formée par l'activité cérébrale, lorsque celle-ci réussi à s'échapper de son enveloppe charnelle |

## Effets

Ils peuvent avoir les effets suivants

| P-Élément | Effet |
|:---------:|:------|
| Toxine    | Provoque un poison progressif (augmentant de 1 dégat par tour à partir de 1) qui ne s'arrêtera qu'après le combat ou si soin d'alteration physique. |
| Son       | 30% de chance de rendre confus l'ennemi |
| Force     | Provoque un dégât supplémentaire par tranche de 5. |
| Esprit    | 30% de chance d'endormir l'ennemi |

## Terrains élémentaires

| P-Élément | Terrain | Effet |
|:---------:|:-------:|:------|
| Toxine    | *Brume miasmatique* | Tout les personnages voient leurs stats physique baissée de 30% |
| Son       | *Déconcentration absolue* | Empêche l'utilisation de magie sur le terrain |
| Force     | *Zone de guerre* | Provoque rage sur tout les personnages présents en combat. |
| Esprit    | *Terres astrales* | Inversions attaques/soins physiques et mentales. |
